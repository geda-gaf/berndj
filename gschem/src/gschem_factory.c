/* gEDA - GPL Electronic Design Automation
 * gschem_factory.c - concrete factory for creating gschem objects
 * Copyright (C) 2007, 2008 Bernd Jendrissek
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glib.h>
#include <string.h>

#include "gschem.h"

struct st_gschem_factory {
  struct st_factory base;
  GSCHEM_TOPLEVEL *w_current;
};

OBJECT *gschem_new_object(struct st_factory *factory, char kind, char const *name);

struct st_factory gschem_factory_prototype = {
.new_object = &gschem_new_object,
};

OBJECT *gschem_new_object(struct st_factory *factory, char kind, char const *name)
{
  struct st_gschem_factory *gf = (struct st_gschem_factory *) factory;
  OBJECT *o;

  /* For now we just instantiate the base class... */
  o = libgeda_factory.new_object(factory, kind, name);

  /* ...and then apply some type-specific initializations. */
  switch (kind) {
    case OBJ_TEXT:
      o_text_notice_create(gf->w_current, o);
      break;
  }

  return o;
}

struct st_factory *gschem_factory(GSCHEM_TOPLEVEL *w_current)
{
  struct st_gschem_factory *factory;
  static int initialized = 0;

  if (!initialized) {
    s_factory_derive(&gschem_factory_prototype, &libgeda_factory);
    initialized = 1;
  }

  factory = g_new(struct st_gschem_factory, 1);
  memcpy(&factory->base, &gschem_factory_prototype,
	 sizeof (gschem_factory_prototype));
  factory->w_current = w_current;

  return &factory->base;
}
/* vim: set sw=2: */
