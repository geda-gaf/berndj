/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>
#include <stdio.h>

#include "gschem.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

void o_buffer_kill_uuid(GList *buffer_objects)
{
  GList *iter;

  printf("Killing UUIDs\n");
  for (iter = buffer_objects; iter; iter = g_list_next(iter)) {
    OBJECT *o_current = iter->data;

    o_attrib_decay_uuid(o_current);
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_buffer_copy(GSCHEM_TOPLEVEL *w_current, int buf_num)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  GList *s_current = NULL;

  if (buf_num < 0 || buf_num > MAX_BUFFERS) {
    fprintf(stderr, _("Got an invalid buffer_number [o_buffer_copy]\n"));
    return;
  }

  s_current = geda_list_get_glist( toplevel->page_current->selection_list );

  if (object_buffer[buf_num] != NULL) {
    s_delete_object_glist(toplevel, object_buffer[buf_num]);
    object_buffer[buf_num] = NULL;
  }

  toplevel->ADDING_SEL = 1;
  object_buffer[buf_num] =
    o_glist_copy_all_to_glist(toplevel, s_current,
                              object_buffer[buf_num], SELECTION_FLAG);
  toplevel->ADDING_SEL = 0;

  o_buffer_kill_uuid(object_buffer[buf_num]);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_buffer_cut(GSCHEM_TOPLEVEL *w_current, int buf_num)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  GList *s_current = NULL;

  if (buf_num < 0 || buf_num > MAX_BUFFERS) {
    fprintf(stderr, _("Got an invalid buffer_number [o_buffer_cut]\n"));
    return;
  }

  s_current = geda_list_get_glist( toplevel->page_current->selection_list );

  if (object_buffer[buf_num] != NULL) {
    s_delete_object_glist(toplevel, object_buffer[buf_num]);
    object_buffer[buf_num] = NULL;
  }

  toplevel->ADDING_SEL = 1;
  object_buffer[buf_num] =
    o_glist_copy_all_to_glist(toplevel, s_current,
                              object_buffer[buf_num], SELECTION_FLAG);
  toplevel->ADDING_SEL = 0;
  o_delete_selected(w_current);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_buffer_paste_start(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y,
                          int buf_num)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  GList *new_place_list;
  int rleft, rtop, rbottom, rright;
  int x, y;

  if (buf_num < 0 || buf_num > MAX_BUFFERS) {
    fprintf(stderr, _("Got an invalid buffer_number [o_buffer_paste_start]\n"));
    return;
  }

  w_current->last_drawb_mode = LAST_DRAWB_MODE_NONE;

  toplevel->ADDING_SEL = 1;
  new_place_list = o_glist_copy_all_to_glist(toplevel, object_buffer[buf_num],
					     NULL, SELECTION_FLAG);

  s_page_replace_place_list(toplevel, toplevel->page_current,
			    g_list_copy(new_place_list));

  if (!world_get_object_glist_bounds(new_place_list,
                                     &rleft, &rtop,
                                     &rright, &rbottom)) {
    /* If the place buffer doesn't have any objects
     * to define its any bounds, we drop out here */
    g_list_free(new_place_list);
    return;
  }

  /* Place the objects into the buffer at the mouse origin, (w_x, w_y). */

  w_current->first_wx = w_x;
  w_current->first_wy = w_y;

  /* snap x and y to the grid, pointed out by Martin Benes */
  x = snap_grid(toplevel, rleft);
  y = snap_grid(toplevel, rtop);

  o_glist_translate_world(w_x - x, w_y - y, new_place_list);
  toplevel->ADDING_SEL = 0;

  w_current->inside_action = 1;
  i_set_state(w_current, ENDPASTE);
  o_place_start (w_current, w_x, w_y);
  g_list_free(new_place_list);
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_buffer_init(void)
{
  int i;

  for (i = 0 ; i < MAX_BUFFERS; i++) {
    object_buffer[i] = NULL;
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_buffer_free(GSCHEM_TOPLEVEL *w_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  int i;

  for (i = 0 ; i < MAX_BUFFERS; i++) {
    if (object_buffer[i]) {
      s_delete_object_glist(toplevel, object_buffer[i]);
      object_buffer[i] = NULL;
    }
  }
}
