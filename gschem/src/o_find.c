/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <math.h>
#include <stdio.h>

#include "gschem.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

static gboolean o_find_test(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current,
			    int w_x, int w_y, int w_slack)
{
  if (inside_region(o_current->w_left - w_slack, o_current->w_top - w_slack,
		    o_current->w_right + w_slack, o_current->w_bottom + w_slack,
		    w_x, w_y) &&
      o_shortest_distance(o_current, w_x, w_y) < w_slack) {
    if (o_current->selectable &&
	o_current->type != OBJ_HEAD &&
	(o_current->type != OBJ_TEXT || o_text_display_p(o_current))) {
      /* FIXME: should this switch be moved to o_select_object()? (Werner) */
      /* FIXME: Move side effects out of this function. */
      if (o_current->type == OBJ_NET && w_current->net_selection_mode) {
	o_select_connected_nets(w_current, o_current);
      } else {
	o_select_object(w_current, o_current, SINGLE, 0); /* 0 is count */
      }
      i_update_menus(w_current);
      return TRUE;
    }
  }

  return FALSE;
}

struct find_object_context {
  GSCHEM_TOPLEVEL *w_current;
  int w_x, w_y;
};

static enum visit_result find_object_one(OBJECT *o_current, void *userdata)
{
  struct find_object_context *ctx = userdata;
  GSCHEM_TOPLEVEL *w_current = ctx->w_current;
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  int w_x = ctx->w_x, w_y = ctx->w_y;
  int w_slack = WORLDabs(page, w_current->select_slack_pixels);

  if (o_current == page->object_lastfound) {
    return VISIT_RES_ABORT;
  }

  if (o_find_test(w_current, o_current, w_x, w_y, w_slack)) {
    page->object_lastfound = o_current;
    return VISIT_RES_EARLY;
  }

  return VISIT_RES_OK;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
gboolean o_find_object(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y)
{
  struct find_object_context ctx = {
    .w_current = w_current,
    .w_x = w_x,
    .w_y = w_y,
  };
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  enum visit_result found;

  /* Decide whether to iterate over all object or start at the last
     found object. If there is more than one object below the
     (w_x/w_y) position, this will select the next object below the
     position point. You can change the selected object by clicking
     at the same place multiple times. */
  if (page->object_lastfound == NULL) {
    /* Start at the beginning of the page's object list. */
    found = s_visit_list(page->object_head, LIST_KIND_HEAD,
			 &find_object_one, &ctx, VISIT_LINEAR, 1);
  } else {
    /* Start at the next object after the last found one. */
    found = s_visit_list(page->object_lastfound, LIST_KIND_CDR,
			 &find_object_one, &ctx, VISIT_LINEAR, 1);
  }

  if (found == VISIT_RES_EARLY) {
    return TRUE;
  }

  if (GEDA_DEBUG) {
    printf("SEARCHING AGAIN\n");
  }

  if (page->object_lastfound != NULL) {
    /* Cycle back to the head of the object list, up to object_lastfound. */
    found = s_visit_list(page->object_head, LIST_KIND_HEAD,
			 &find_object_one, &ctx, VISIT_LINEAR, 1);
    if (found == VISIT_RES_EARLY) {
      return TRUE;
    }
  }

  /* Didn't find anything; reset state. */
  page->object_lastfound = NULL;

  /* deselect everything only if shift key isn't pressed and 
     the caller allows it */	
  if (!w_current->SHIFTKEY) {
    o_select_unselect_all (w_current);
  }

  i_update_menus(w_current);

  return FALSE;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
gboolean o_find_selected_object(GSCHEM_TOPLEVEL *w_current,
				int w_x, int w_y)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  OBJECT *o_current=NULL;
  GList *s_current;
  int w_slack;

  w_slack = WORLDabs(page, w_current->select_slack_pixels);

  s_current = geda_list_get_glist(page->selection_list);
  /* do first search */
  while (s_current != NULL) {
    o_current = s_current->data;
    if (inside_region(o_current->w_left - w_slack, o_current->w_top - w_slack,
                      o_current->w_right + w_slack, o_current->w_bottom + w_slack,
                      w_x, w_y)) {

      if (GEDA_DEBUG) {
	printf("o_find_selected_object:\n");
	printf("Object bounds:\n\tL: %i\tR: %i\n\tT: %i\tB: %i.\n",
	       o_current->w_left, o_current->w_right, o_current->w_top, o_current->w_bottom);
	printf("Screen pointer at: (%i,%i)\n", w_x, w_y);
      }
      if (o_current->selectable &&
	  o_current->type != OBJ_HEAD &&
	  (o_current->type != OBJ_TEXT || o_text_display_p(o_current))) {
	return TRUE;
      }
    }
    
    s_current = g_list_next(s_current);
  } 

  return (FALSE);
}
