/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif
#include <math.h>

#include "gschem.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/* No special type for attributes */
/* You can only edit text attributes */

/* be sure in o_copy o_move o_delete you maintain the attributes */
/* delete is a bare, because you will have to unattach the other end */
/* and in o_save o_read as well */
/* and in o_select when selecting objects, select the attributes */

/* there needs to be a modifier (in struct.h, such as a flag) which
 * signifies that this is an attribute */

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *  Copy all attributes select to the selection list.
 *
 *  \todo get a better name
 */
void o_attrib_add_selected(GSCHEM_TOPLEVEL *w_current, SELECTION *selection,
                           OBJECT *selected)
{
  OBJECT *a_current;
  GList *a_iter;

  g_assert( selection != NULL );

  /* deal with attributes here? */
  if (selected->attribs != NULL) {
    /* first node is head */
    a_iter = selected->attribs;

    while (a_iter != NULL) {
      a_current = a_iter->data;

      /* make sure object isn't selected already */
      if (a_current->saved_color == -1) {
        o_selection_add(selection, a_current);
        o_redraw_single(w_current, a_current);
      }

      a_iter = g_list_next (a_iter);
    }
  }

  return;
}

/*! \brief Change visibility status of attribute object.
 *  \par Function Description
 *  This function toggles the visibility status of the attribute \a
 *  object and updates it. The object is erased or redrawn if
 *  necessary.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] object     The attribute object.
 */
void o_attrib_toggle_visibility(GSCHEM_TOPLEVEL *w_current, OBJECT *object)
{
  TOPLEVEL *toplevel = w_current->toplevel;

  g_return_if_fail (object != NULL && object->type == OBJ_TEXT);

  if (o_text_printing_p(object)) {
    o_erase_single(w_current, object);

    o_text_change(object, NULL, INVISIBLE, LEAVE_NAME_VALUE_ALONE);

    if (o_text_display_p(object)) {
      /* draw text so that little I is drawn */
      o_text_draw(w_current, object);
    }
  } else {
    /* if we are in the special show hidden mode, then erase text first */
    /* to get rid of the little I */
    if (o_text_display_p(object)) {
      o_erase_single(w_current, object);
    }

    o_text_change(object, NULL, VISIBLE, LEAVE_NAME_VALUE_ALONE);
    o_text_draw(w_current, object);
  }

  toplevel->page_current->CHANGED = 1;
}

/*! \brief Set what part of an attribute is shown.
 *  \par Function Description
 *  This function changes what part (name, value or both) of an
 *  attribute is shown by its attribute object. The attribute object
 *  is erased, updated and finally redrawn.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] object     The attribute object.
 *  \param [in] show_name_value  The new display flag for attribute.
 */
void o_attrib_toggle_show_name_value(GSCHEM_TOPLEVEL *w_current,
                                     OBJECT *object, int show_name_value)
{
  TOPLEVEL *toplevel = w_current->toplevel;

  g_return_if_fail (object != NULL && object->type == OBJ_TEXT);

  o_erase_single(w_current, object);
  o_text_change(object, NULL, LEAVE_VISIBILITY_ALONE, show_name_value);
  o_text_draw(w_current, object);

  toplevel->page_current->CHANGED = 1;
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
/* This function no longer returns NULL, but will always return the new */
/* text item */
OBJECT *o_attrib_add_attrib(GSCHEM_TOPLEVEL *w_current,
			    const char *text_string, int visibility, 
			    int show_name_value, OBJECT *object)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  OBJECT *new_obj;
  int world_x = - 1, world_y = -1;
  int color; 
  int left, right, top, bottom;
  OBJECT *o_current;

  color = toplevel->detachedattr_color;

  o_current = object;

  /* creating a toplevel or unattached attribute */
  if (o_current) {
    /* get coordinates of where to place the text object */
    switch(o_current->type) {
      int w_x1, w_y1, w_x2, w_y2;

      case(OBJ_COMPLEX):
      case(OBJ_PLACEHOLDER):
        world_x = o_current->complex->x;
        world_y = o_current->complex->y;
        color = toplevel->attribute_color;
        break;

      case(OBJ_ARC):
	s_basic_get_grip(o_current, GRIP_ARC_CENTER, &world_x, &world_y);
        color = toplevel->attribute_color;
        break;

      case(OBJ_CIRCLE):
	s_basic_get_grip(o_current, GRIP_CIRCLE_CENTER, &world_x, &world_y);
        color = toplevel->attribute_color;
        break;

      case(OBJ_BOX):
	s_basic_get_grip(o_current, GRIP_UPPER_LEFT, &w_x1, &w_y1);
	s_basic_get_grip(o_current, GRIP_LOWER_RIGHT, &w_x2, &w_y2);
        world_x = min(w_x1, w_x2);
        world_y = max(w_y1, w_y2);
        color = toplevel->attribute_color;
        break;

      case(OBJ_LINE):
      case(OBJ_NET):
      case(OBJ_PIN):
      case(OBJ_BUS):
	s_basic_get_grip(o_current, GRIP_1, &world_x, &world_y);
        color = toplevel->attribute_color;
        break;

      case(OBJ_TEXT):
	o_text_get_xy(o_current, &world_x, &world_y);
			
        color = toplevel->detachedattr_color;

	o_current = NULL;
        break;
    }
  } else {
    world_get_object_list_bounds(toplevel->page_current->object_head, LIST_KIND_HEAD,
                                 &left, &top, &right, &bottom);
	
    /* this really is the lower left hand corner */	
    world_x = left; 
    world_y = top;  

    /* printf("%d %d\n", world_x, world_y); */
    color = toplevel->detachedattr_color;
  }

  /* first create text item */
  new_obj = o_text_new(toplevel, OBJ_TEXT, color, world_x, world_y,
             LOWER_LEFT, 0, /* zero is angle */
             text_string,  w_current->text_size,  /* current text size */
             visibility, show_name_value);
  s_page_append(toplevel->page_current, new_obj);

  /* now toplevel->page_current->object_tail contains new text item */

  /* now attach the attribute to the object (if o_current is not NULL) */
  /* remember that o_current contains the object to get the attribute */
  if (o_current) {
    o_attrib_attach (toplevel, toplevel->page_current->object_tail, o_current);
  }

  o_selection_add( toplevel->page_current->selection_list,
                   toplevel->page_current->object_tail );

  o_erase_single(w_current, toplevel->page_current->object_tail);
  o_text_draw(w_current, toplevel->page_current->object_tail);

  /* handle slot= attribute, it's a special case */
  if (g_ascii_strncasecmp (text_string, "slot=", 5) == 0) {
    o_slot_end (w_current, text_string, strlen (text_string));
  }

  /* Run the add attribute hook */
  if (scm_hook_empty_p(add_attribute_hook) == SCM_BOOL_F &&
      o_current != NULL) {
	scm_run_hook(add_attribute_hook,
		     scm_cons(g_make_object_smob(toplevel,
						 o_current),
			      SCM_EOL));
      }
  
  toplevel->page_current->CHANGED = 1;

  return(toplevel->page_current->object_tail);
}
