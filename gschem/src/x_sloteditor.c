/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * x_sloteditor.c - A dialog to edit slots in a schematic.
 * Copyright (C) 2008 Bernd Jendrissek <bernd.jendrissek@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdarg.h>
#include <gtk/gtk.h>

#include <libgeda/libgeda.h>

#include "glade_compat.h"
#include "gschem_struct.h"
#include "globals.h"
#include "prototype.h"
#include "x_sloteditor.h"

enum slot_editor_columns {
  /* Visible columns: */
  SLOT_EDITOR_FRIENDLY_NAME,
  /* Invisible columns: */
  SLOT_EDITOR_OBJECT,
  SLOT_EDITOR_SIGNAL_HANDLER,
  /* Total number of columns: */
  SLOT_EDITOR_N_COLUMNS
};

enum slot_editor_properties {
  PROP_ZERO,
  PROP_W_CURRENT,
};

/* Class and instance methods. */
static void slot_editor_class_init(gpointer g_class, gpointer class_data);
static void slot_editor_init(GTypeInstance *instance, gpointer g_class);
static void slot_editor_dispose(GObject *object);
static void slot_editor_finalize(GObject *object);
static void slot_editor_set_property(GObject *object,
				     guint property_id,
				     GValue const *value,
				     GParamSpec *pspec);
static void slot_editor_get_property(GObject *object,
				     guint property_id,
				     GValue *value,
				     GParamSpec *pspec);

/* Signal handlers. */
void slot_editor_row_activated(GtkTreeView *treeview,
			       GtkTreePath *path,
			       GtkTreeViewColumn *column,
			       gpointer user_data);

static GObjectClass *slot_editor_parent_class = NULL;

GType slot_editor_get_type(void)
{
  static GType slot_editor_type = 0;

  if (!slot_editor_type) {
    static const GTypeInfo slot_editor_info = {
      sizeof(SlotEditorClass),
      NULL, /* base_init */
      NULL, /* base_finalize */
      (GClassInitFunc) slot_editor_class_init,
      NULL, /* class_finalize */
      NULL, /* class_data */
      sizeof (SlotEditor),
      0,    /* n_preallocs */
      (GInstanceInitFunc) slot_editor_init,
    };

    slot_editor_type = g_type_register_static(G_TYPE_OBJECT,
					      "Sloteditor",
					      &slot_editor_info, 0);
  }

  return slot_editor_type;
}

static void slot_editor_class_init(gpointer g_class, gpointer class_data)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(g_class);

  slot_editor_parent_class = g_type_class_peek_parent(gobject_class);

  gobject_class->set_property = &slot_editor_set_property;
  gobject_class->get_property = &slot_editor_get_property;
  gobject_class->dispose = &slot_editor_dispose;
  gobject_class->finalize = &slot_editor_finalize;

  g_object_class_install_property(gobject_class, PROP_W_CURRENT,
				  g_param_spec_pointer("w_current", "", "",
						       G_PARAM_WRITABLE));
}

gboolean slot_editor_delete(GtkWidget *widget, GdkEvent *event,
			    gpointer user_data)
{
  SlotEditor *se;

  g_object_get(widget, "user-data", &se, NULL);

  /* The dialog window "owns" the SlotEditor: drop the reference. */
  g_object_unref(se);

  /* Let the default handler destroy the window (?). */
  return FALSE;
}

static gboolean slot_editor_dispose_row(GtkTreeModel *model,
					GtkTreePath *path,
					GtkTreeIter *iter,
					gpointer data)
{
  OBJECT *o;
  gulong handler_id;

  gtk_tree_model_get(model, iter,
		     SLOT_EDITOR_OBJECT, &o,
		     SLOT_EDITOR_SIGNAL_HANDLER, &handler_id,
		     -1);

  g_signal_handler_disconnect(o, handler_id);

  return FALSE;
}

void slot_editor_delete_row(GtkButton *button, gpointer user_data)
{
  GtkTreeSelection *sel;
  GtkTreeModel *model;
  GtkTreeIter row;
  SlotEditor *se;

  g_object_get(button, "user-data", &se, NULL);

  sel = gtk_tree_view_get_selection(se->treeview);
  if (gtk_tree_selection_get_selected(sel, &model, &row)) {
    OBJECT *o, **attribs;
    int i;

    gtk_tree_model_get(model, &row,
		       SLOT_EDITOR_OBJECT, &o,
		       -1);

    gtk_tree_store_remove(se->treestore, &row);

    /* First, free all the attributes attached to the slot. */
    attribs = o_attrib_return_attribs(o);
    for (i = 0; attribs && attribs[i]; i++) {
      s_delete(se->w_current->toplevel, attribs[i]);
    }
    o_attrib_free_returned(attribs);

    /* s_delete() will implicitly destroy the OBJECT_EVENT_CHANGED observer. */
    s_delete(se->w_current->toplevel, o);
  }
}

void slot_editor_edit(GtkButton *button, gpointer user_data)
{
  SlotEditor *se;

  g_object_get(button, "user-data", &se, NULL);

  slot_editor_row_activated(se->treeview, NULL, NULL, NULL);
}

void slot_editor_finish(GtkButton *button, gpointer user_data)
{
  SlotEditor *se;

  g_object_get(button, "user-data", &se, NULL);

  gtk_widget_destroy(GTK_WIDGET(se->sewindow));

  /* The dialog window "owns" the SlotEditor: drop the reference. */
  g_object_unref(se);
}

static gboolean slot_editor_update_name(GtkTreeModel *model,
					GtkTreePath *path,
					GtkTreeIter *iter,
					gpointer data)
{
  OBJECT *subject = data;
  OBJECT *o;

  gtk_tree_model_get(model, iter,
		     SLOT_EDITOR_OBJECT, &o,
		     -1);
  if (o == subject) {
    gtk_tree_store_set(GTK_TREE_STORE(model), iter,
		       SLOT_EDITOR_FRIENDLY_NAME, g_strdup(subject->slot->name),
		       -1);
  }

  return FALSE;
}

static void slot_editor_notice_changed(OBJECT *subject, SlotEditor *se)
{
  g_message("slot_editor_notice_changed: %p has changed to %s\n",
	    subject, subject->slot->name);
  gtk_tree_model_foreach(GTK_TREE_MODEL(se->treestore),
			 &slot_editor_update_name, subject);
}

void slot_editor_new_row(GtkButton *button, gpointer user_data)
{
  GtkTreeIter newrow;
  GtkTreeViewColumn *column;
  GtkTreePath *path;
  SlotEditor *se;
  TOPLEVEL *toplevel;
  OBJECT *slot;
  gulong handler_id;

  g_object_get(button, "user-data", &se, NULL);
  toplevel = se->w_current->toplevel;

  /* Add a new, empty slot to the current page. */
  slot = s_slot_read(toplevel, "S");
  s_page_append(toplevel->page_current, slot);

  /* Keep an eye on this new slot. */
  handler_id = g_signal_connect(G_OBJECT(slot), "changed",
				G_CALLBACK(slot_editor_notice_changed), se);
  g_assert(handler_id);

  gtk_tree_store_append(se->treestore, &newrow, NULL);
  gtk_tree_store_set(se->treestore, &newrow,
		     SLOT_EDITOR_FRIENDLY_NAME, g_strdup(""),
		     SLOT_EDITOR_OBJECT, slot,
		     SLOT_EDITOR_SIGNAL_HANDLER, handler_id,
		     -1);

  /* Focus the name of the new slot. */
  column = gtk_tree_view_get_column(se->treeview, SLOT_EDITOR_FRIENDLY_NAME);
  path = gtk_tree_model_get_path(GTK_TREE_MODEL(se->treestore), &newrow);
  gtk_tree_view_set_cursor(se->treeview, path, column, TRUE);
  gtk_widget_grab_focus(GTK_WIDGET(se->treeview));
}

void slot_editor_row_activated(GtkTreeView *treeview,
			       GtkTreePath *path,
			       GtkTreeViewColumn *column,
			       gpointer user_data)
{
  GSCHEM_TOPLEVEL *w_current;
  SlotEditor *se;
  GtkTreeSelection *sel;
  GtkTreeModel *model;
  GtkTreeIter row;
  SELECTION *selection;

  sel = gtk_tree_view_get_selection(treeview);
  if (gtk_tree_selection_get_selected(sel, &model, &row)) {
    OBJECT *o;

    gtk_tree_model_get(model, &row,
		       SLOT_EDITOR_OBJECT, &o,
		       -1);

    g_object_get(treeview, "user-data", &se, NULL);
    w_current = se->w_current;

    selection = o_selection_new();
    o_selection_add(selection, o);
    o_attrib_add_selected(w_current, selection, o);
    fprintf(stderr, "selected object %s type = %c\n", o->name, o->type);
    x_multiattrib_open(w_current, selection);
    x_multiattrib_update(w_current, selection);
  }
}

static void slot_editor_name_edited(GtkCellRendererText *renderer,
				    gchar *path_string,
				    gchar *new_text,
				    gpointer user_data)
{
  SlotEditor *se = SLOT_EDITOR(user_data);
  GtkTreeIter row;
  GtkTreePath *path;
  OBJECT *o, *slotname_object;
  TOPLEVEL *toplevel;

  /* Update the tree model with the new slot name. */
  path = gtk_tree_path_new_from_string(path_string);
  gtk_tree_model_get_iter(GTK_TREE_MODEL(se->treestore), &row, path);

  gtk_tree_store_set(se->treestore, &row,
		     SLOT_EDITOR_FRIENDLY_NAME, new_text,
		     -1);

  /* Force the slotname= attribute with the new value. */
  gtk_tree_model_get(GTK_TREE_MODEL(se->treestore), &row,
		     SLOT_EDITOR_OBJECT, &o,
		     -1);
  toplevel = se->w_current->toplevel;

  /* XXX - should factorize this into s_slot_rename(). */
  g_free(o_attrib_search_name_single_exact(o, "slotname", &slotname_object));
  if (slotname_object) {
    o_text_take_string(slotname_object, g_strconcat("slotname=", new_text, NULL));
  } else {
    g_free(o_attrib_search_name_single_force(toplevel, o, "slotname",
					     &g_strdup, new_text,
					     &slotname_object));
  }

  se->page->CHANGED = 1;
}

static enum visit_result
slot_editor_visitor(OBJECT *o, void *context)
{
  SlotEditor *se = SLOT_EDITOR(context);
  GtkTreeIter newrow;
  gulong handler_id;

  if (o->type != OBJ_SLOT) {
    return VISIT_RES_OK;
  }

  handler_id = g_signal_connect(G_OBJECT(o), "changed",
				G_CALLBACK(slot_editor_notice_changed), se);
  g_assert(handler_id);

  gtk_tree_store_append(se->treestore, &newrow, NULL);
  gtk_tree_store_set(se->treestore, &newrow,
		     SLOT_EDITOR_FRIENDLY_NAME, g_strdup(o->slot->name),
		     SLOT_EDITOR_OBJECT, o,
		     SLOT_EDITOR_SIGNAL_HANDLER, handler_id,
		     -1);
  fprintf(stderr, "Found slot %s, handler = %lu\n", o->slot->name, handler_id);

  return VISIT_RES_OK;
}

static void slot_editor_init(GTypeInstance *instance, gpointer g_class)
{
  GladeXML *xml;
  SlotEditor *se = SLOT_EDITOR(instance);
  GtkWidget *widget;
  GtkWindow *sewindow;
  GtkTreeView *treeview;
  GtkTreeStore *treestore;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;

  widget = x_glade_get_widget(&xml, "slot_editor",
			      "slot_editor_delete",
			      G_CALLBACK(&slot_editor_delete),
			      "slot_editor_delete_row",
			      G_CALLBACK(&slot_editor_delete_row),
			      "slot_editor_edit",
			      G_CALLBACK(&slot_editor_edit),
			      "slot_editor_finish",
			      G_CALLBACK(&slot_editor_finish),
			      "slot_editor_new_row",
			      G_CALLBACK(&slot_editor_new_row),
			      "slot_editor_row_activated",
			      G_CALLBACK(&slot_editor_row_activated),
			      NULL);
  sewindow = GTK_WINDOW(widget);

  treeview = GTK_TREE_VIEW(x_lookup_widget_set(xml, "slot_treeview",
					       "user-data", se, NULL));
  x_lookup_widget_set(xml, "delete_button", "user-data", se, NULL);
  x_lookup_widget_set(xml, "edit_button", "user-data", se, NULL);
  x_lookup_widget_set(xml, "new_button", "user-data", se, NULL);
  x_lookup_widget_set(xml, "ok_button", "user-data", se, NULL);

  g_object_unref(xml);

  /* Let the dialog window "own" the SlotEditor. */
  g_object_set(sewindow, "user-data", se, NULL);
  g_object_ref(se);

  treestore = gtk_tree_store_new(SLOT_EDITOR_N_COLUMNS,
				 G_TYPE_STRING,
				 G_TYPE_POINTER,
				 G_TYPE_ULONG);
  gtk_tree_view_set_model(treeview, GTK_TREE_MODEL(treestore));
  gtk_tree_view_set_search_column(treeview, SLOT_EDITOR_FRIENDLY_NAME);

  renderer = gtk_cell_renderer_text_new();
  g_object_set(renderer, "editable", TRUE, NULL);
  g_signal_connect(renderer, "edited",
		   G_CALLBACK(&slot_editor_name_edited), se);

  column = gtk_tree_view_column_new_with_attributes("name", renderer,
						    "text",
						    SLOT_EDITOR_FRIENDLY_NAME,
						    NULL);

  gtk_tree_view_append_column(treeview, column);

  se->sewindow = sewindow;
  se->treeview = treeview;
  se->treestore = treestore;
  se->dispose_has_run = FALSE;
}

static void slot_editor_set_property(GObject *object,
				     guint property_id,
				     GValue const *value,
				     GParamSpec *pspec)
{
  SlotEditor *se = SLOT_EDITOR(object);

  switch (property_id) {
  case PROP_W_CURRENT:
    se->w_current = g_value_get_pointer(value);
    /* Also remember which page was the current one. */
    se->page = se->w_current->toplevel->page_current;

    gtk_tree_store_clear(se->treestore);
    s_visit_page(se->w_current->toplevel->page_current,
		 &slot_editor_visitor, se, VISIT_ANY, 1);
    break;
  }
}

static void slot_editor_get_property(GObject *object,
				     guint property_id,
				     GValue *value,
				     GParamSpec *pspec)
{
}

static void slot_editor_dispose(GObject *object)
{
  SlotEditor *se = SLOT_EDITOR(object);

  if (se->dispose_has_run) {
    return;
  }
  se->dispose_has_run = TRUE;

  gtk_tree_model_foreach(GTK_TREE_MODEL(se->treestore),
			 &slot_editor_dispose_row, se);
  g_object_unref(se->treestore);

  slot_editor_parent_class->dispose(object);
}

static void slot_editor_finalize(GObject *object)
{
  /* The window is already dead if our refcount has dropped to zero. */

  slot_editor_parent_class->finalize(object);
}

static void slot_editor_present(SlotEditor *se)
{
  gtk_window_present(se->sewindow);
}

/*! \brief Create the slot chooser dialog
 *  \par Function Description
 *  This function creates the slot chooser dialog.
 */
void x_sloteditor_open(GSCHEM_TOPLEVEL *w_current)
{
  SlotEditor *se;

  se = SLOT_EDITOR(g_object_new(TYPE_SLOT_EDITOR,
				"w_current", w_current,
				NULL));

  slot_editor_present(se);

  g_object_unref(se);
}
