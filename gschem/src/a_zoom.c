/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#include <math.h>

#include "gschem.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/* Kazu - discuss with Ales
 * 1) rint
 * 2) SWAP & SORT
 */

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 * 
 */
/* dir is either ZOOM_IN, ZOOM_OUT or ZOOM_FULL which are defined in globals.h */
void a_zoom(GSCHEM_TOPLEVEL *w_current, PAGE *page, enum zoom_dir dir, enum zoom_from selected_from, enum pan_flags pan)
{
  double world_pan_center_x,world_pan_center_y,relativ_zoom_factor = - 1;
  int start_x, start_y;

  /*calc center: either "mouse_to_world" or center=center */
  if (w_current->zoom_with_pan == TRUE && selected_from == HOTKEY) {
    if (!x_event_get_pointer_position(w_current, FALSE, 
				      &start_x, &start_y))
      return;
    world_pan_center_x = start_x;
    world_pan_center_y = start_y;
  }
  else {
    world_pan_center_x = (double) (page->left + page->right ) / 2;
    world_pan_center_y = (double) (page->top + page->bottom ) / 2;
  }

  /* NB: w_current->zoom_gain is a percentage increase */
  switch(dir) {
    case(ZOOM_IN):
    relativ_zoom_factor = (100.0 + w_current->zoom_gain) / 100.0;
    break;	
	
    case(ZOOM_OUT):
    relativ_zoom_factor = 100.0 / (100.0 + w_current->zoom_gain);
    break;

    case(ZOOM_FULL):
    /*hope someone have a better idea (hw)*/
    relativ_zoom_factor = -1;
    break;
  }

  if (GEDA_DEBUG) {
    printf("relative zoomfactor: %E\n", relativ_zoom_factor);
    printf("new center: x: %E, y: %E \n",
	   world_pan_center_x, world_pan_center_y);
  }

  /* calculate new window and draw it */
  a_pan_general(w_current, page, world_pan_center_x, world_pan_center_y,
                relativ_zoom_factor * page->to_screen_y_constant, pan);

  /* Before warping the cursor, filter out any consecutive scroll events 
   * from the event queue.  If the program receives more than one scroll 
   * event before it can process the first one, then the globals mouse_x 
   * and mouse_y won't contain the proper mouse position,
   * because the handler for the mouse moved event needs to 
   * run first to set these values.
   */
  GdkEvent *topEvent = gdk_event_get();
  while( topEvent != NULL ) {
    if( topEvent->type != GDK_SCROLL ) {
      gdk_event_put( topEvent );
      gdk_event_free( topEvent );
      break;
    }
    gdk_event_free( topEvent );
    topEvent = gdk_event_get();
  }
	
  /* warp the cursor to the right position */ 
  if (w_current->warp_cursor) {
     WORLDtoSCREEN(page, world_pan_center_x, world_pan_center_y,
		   &start_x, &start_y);
     x_basic_warp_cursor (w_current->drawing_area, start_x, start_y);
  }
  else {
    /*! \bug FIXME? trigger a x_event_motion() call without moving the cursor 
     *  this will redraw rubberband lines 
     *  Find a way to trigger the x_event_motion() without moving
     *  the mouse cursor (werner) 
     */
    /* x_basic_warp_cursor(w_current->drawing_area, mouse_x, mouse_y); */
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 * 
 */
void a_zoom_extents(GSCHEM_TOPLEVEL *w_current, PAGE *page, enum pan_flags pan)
{
  int lleft, lright, ltop, lbottom;
  double zx, zy, zoom_new;
  double world_pan_center_x,world_pan_center_y;

  if (!world_get_object_list_bounds(page->object_head, LIST_KIND_HEAD,
				    &lleft, &ltop, &lright, &lbottom)) {
    return;
  }

  if (GEDA_DEBUG) {
    printf("in a_zoom_extents:  left: %d, right: %d, top: %d, bottom: %d\n",
	   lleft, lright, ltop, lbottom);
  }

  /* Calc the necessary zoomfactor to show everything
   * Start with the windows width and height, then scale back to world
   * coordinates with the to_screen_y_constant as the initial page data
   * may not have the correct aspect ratio. */
  zx = (double) page->width / (lright-lleft);
  zy = (double) page->height / (lbottom-ltop);
  /* choose the smaller one, 0.9 for paddings on all side*/
  zoom_new = (zx < zy ? zx : zy) * 0.9;
	
  /*get the center of the objects*/
  world_pan_center_x = (double) (lright + lleft) /2.0;
  world_pan_center_y = (double) (lbottom + ltop) /2.0;
	
  /* and create the new window*/
  a_pan_general(w_current, page, world_pan_center_x, world_pan_center_y,
                zoom_new, pan);

  /*! \bug FIXME? trigger a x_event_motion() call without moving the cursor 
   *  this will redraw rubberband lines after zooming
   *  removed!, it has side effects in the preview of the part dialog 
   *  need to find another way to trigger x_event_motion() (Werner)
   */
  /* x_basic_warp_cursor(w_current->drawing_area, mouse_x, mouse_y); */
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 * 
 */
void a_zoom_box(GSCHEM_TOPLEVEL *w_current, PAGE *page, enum pan_flags pan)
{
  double zx, zy, relativ_zoom_factor;
  double world_pan_center_x, world_pan_center_y;

  /*test if there is really a box*/
  if (w_current->first_wx == w_current->second_wx ||
      w_current->first_wy == w_current->second_wy) {
    s_log_message(_("Zoom too small!  Cannot zoom further.\n"));
    return;
  }
	
  /*calc new zoomfactors and choose the smaller one*/
  zx = (double) abs(page->left - page->right) /
    abs(w_current->first_wx - w_current->second_wx);
  zy = (double) abs(page->top - page->bottom) /
    abs(w_current->first_wy - w_current->second_wy);

  relativ_zoom_factor = (zx < zy ? zx : zy);
	
  /* calculate the center of the zoom box */
  world_pan_center_x = (w_current->first_wx + w_current->second_wx) / 2.0;
  world_pan_center_y = (w_current->first_wy + w_current->second_wy) / 2.0;

  /* and create the new window*/
  a_pan_general(w_current, page, world_pan_center_x, world_pan_center_y,
                relativ_zoom_factor * page->to_screen_y_constant, pan);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 * 
 */
void a_zoom_box_start(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y)
{
  w_current->first_wx = w_current->second_wx = w_x;
  w_current->first_wy = w_current->second_wy = w_y;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 * 
 */
void a_zoom_box_end(GSCHEM_TOPLEVEL *w_current, int x, int y)
{
  TOPLEVEL *toplevel = safe_toplevel(w_current);
  PAGE *page = safe_page_current(toplevel);

  g_assert( w_current->inside_action != 0 );

  a_zoom_box_rubberband_xor(w_current, page);
  w_current->rubber_visible = 0;

  a_zoom_box(w_current, page, A_PAN_FULL);

  if (w_current->undo_panzoom) {
    o_undo_savestate(w_current, UNDO_VIEWPORT_ONLY); 
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 * 
 */
void a_zoom_box_rubberband(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y)
{
  TOPLEVEL *toplevel = safe_toplevel(w_current);
  PAGE *page = safe_page_current(toplevel);

  g_assert( w_current->inside_action != 0 );

  if (w_current->rubber_visible)
    a_zoom_box_rubberband_xor(w_current, page);

  w_current->second_wx = w_x;
  w_current->second_wy = w_y;

  a_zoom_box_rubberband_xor(w_current, page);
  w_current->rubber_visible = 1;
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 * 
 */
void a_zoom_box_rubberband_xor(GSCHEM_TOPLEVEL *w_current, PAGE *page)
{
  int x1, y1, x2, y2;
  int box_width, box_height;
  int box_left, box_top;

  WORLDtoSCREEN(page, w_current->first_wx, w_current->first_wy, &x1, &y1);
  WORLDtoSCREEN(page, w_current->second_wx, w_current->second_wy, &x2, &y2);

  box_width  = abs(x1 - x2);
  box_height = abs(y1 - y2);
  box_left   = min(x1, x2);
  box_top    = min(y1, y2);

  gdk_gc_set_foreground(w_current->xor_gc,				
			x_get_darkcolor(w_current->zoom_box_color));
  gdk_draw_rectangle(w_current->backingstore, w_current->xor_gc, FALSE,
		     box_left, box_top, box_width, box_height);
  o_invalidate_rect(w_current, box_left,
                               box_top,
                               box_left + box_width,
		               box_top + box_height);
}

/*! \brief Set the contraints for the current page.
 *  \par Function Description
 *  This function will set the current page constraints.
 *
 *  \param [in,out] page       The PAGE object to set constraints on.
 *  \param [in]     xmin       The minimum x coordinate for the page.
 *  \param [in]     xmax       The maximum x coordinate for the page.
 *  \param [in]     ymin       The minimum y coordinate for the page.
 *  \param [in]     ymax       The maximum y coordinate for the page.
 */
void set_window(PAGE *page, int xmin, int xmax, int ymin, int ymax)
{
  double fs,f0,f1;
  double fw0,fw1,fw;

  page->left   = xmin;
  page->right  = xmax;
  page->top    = ymin; 
  page->bottom = ymax;

  /* now do the constant setups */

  /* pix_x */
  f0 = page->left;
  f1 = page->right;
  fs = page->width;
  page->to_screen_x_constant = fs / (f1 - f0);

  /* pix_y */
  f0 = page->top;
  f1 = page->bottom;
  fs = page->height;
  page->to_screen_y_constant = fs / (f1 - f0); 

  /* mil_x */
  fw1 = page->right;
  fw0 = page->left;
  fw  = page->width;
  page->to_world_x_constant = (fw1 - fw0) / fw;

  /* mil_y */
  fw1 = page->bottom;
  fw0 = page->top;
  fw  = page->height;
  page->to_world_y_constant = (fw1 - fw0) / fw;
}

/*! \brief Convert a x coordinate to mils.
 *  \par Function Description
 *  Convert a x coordinate to mils.
 *
 *  \param [in] page       The PAGE object
 *  \param [in] val        The x coordinate to convert
 *  \return The coordinate value in mils.
 */
int mil_x(PAGE *page, int val)
{
  double i;
  double fval;
  int j;

  fval = val;
  i = fval * page->to_world_x_constant + page->left;

#ifdef HAS_RINT
  j = rint(i);
#else
  j = i;
#endif

  return(j);
}

/*! \brief Convert a y coordinate to mils
 *  \par Function Description
 *  Convert a y coordinate to mils
 *
 *  \param [in] page       The PAGE object.
 *  \param [in] val        The y coordinate to convert.
 *  \return The coordinate value in mils.
 */
int mil_y(PAGE *page, int val)
{
  double i;
  double fval;
  int j;

  fval = page->height - val;
  i = fval * page->to_world_y_constant + page->top;

#ifdef HAS_RINT
  j = rint(i);
#else
  j = i;
#endif

  return(j);
}

/*! \brief Convert a x coordinate to pixels.
 *  \par Function Description
 *  Convert a x coordinate to pixels.
 *
 *  \param [in] page       The PAGE object
 *  \param [in] val        The x coordinate to convert
 *  \return The coordinate value in pixels.
 */
int pix_x(PAGE *page, int val)
{

  double i;
  int j;

  i = page->to_screen_x_constant * (double) (val - page->left);

#ifdef HAS_RINT
  j = rint(i);
#else
  j = i;
#endif

  /* this is a temp solution to fix the wrapping associated with */
  /* X coords being greated/less than than 2^15 */
  if (j >= 32768) {
    j = 32767;
  }
  if (j <= -32768) {
    j = -32767;
  }

  return(j);
}

/*! \brief Convert a y coordinate to pixels.
 *  \par Function Description
 *  Convert a y coordinate to pixels.
 *
 *  \param [in] page       The PAGE object
 *  \param [in] val        The y coordinate to convert
 *  \return The coordinate value in pixels.
 */
int pix_y(PAGE *page, int val)
{
  double i;
  int j;

  i = page->height - (page->to_screen_y_constant * (double) (val - page->top));

#ifdef HAS_RINT
  j = rint(i);
#else
  j = i;
#endif

  /* this is a temp solution to fix the wrapping associated with */
  /* X coords being greated/less than than 2^15 */
  if (j >= 32768) {
    j = 32767;
  }
  if (j <= -32768) {
    j = -32767;
  }

  return(j);
}

/*! \brief Transform WORLD coordinates to SCREEN coordinates
 *  \par Function Description
 *  This function takes in WORLD x/y coordinates and
 *  transforms them to SCREEN x/y coordinates.
 *
 *  \param [in]  page       The PAGE object.
 *  \param [in]  x          The x coordinate in WORLD units.
 *  \param [in]  y          The y coordinate in WORLD units.
 *  \param [out] screen_x   The x coordinate in SCREEN units.
 *  \param [out] screen_y   The y coordinate in SCREEN units.
 *  \note Question: why are we returning in screen_x and screen_y
 *                  if this is WORLD to SCREEN shouldn't SCREEN
 *                  coordinates be returned in x and y?
 */
void WORLDtoSCREEN(PAGE *page, int x, int y, int *screen_x, int *screen_y)
{
  *screen_x = pix_x(page, x);
  *screen_y = pix_y(page, y);
}

/*! \brief Transform WORLD coordinates to WORLD coordinates
 *  \par Function Description
 *  This function takes in SCREEN x/y coordinates and
 *  transforms them to WORLD x/y coordinates.
 *
 *  \param [in]  page       The PAGE object.
 *  \param [in]  mx         The x coordinate in SCREEN units.
 *  \param [in]  my         The y coordinate in SCREEN units.
 *  \param [out] x          The x coordinate in WORLD units.
 *  \param [out] y          The y coordinate in WORLD units.
 *  \note Question: why are we returning in x and y
 *                  if this is SCREEN to WORLD shouldn't WORLD
 *                  coordinates be returned in mx and my?
 */
void SCREENtoWORLD(PAGE *page, int mx, int my, int *x, int *y)
{
  *x = mil_x(page, mx);
  *y = mil_y(page, my);
}

/*! \brief Get absolute SCREEN coordinate.
 *  \par Function Description
 *  Get absolute SCREEN coordinate.
 *
 *  \param [in] page       The PAGE object.
 *  \param [in] val        The coordinate to convert.
 *  \return The converted SCREEN coordinate.
 */
int SCREENabs(PAGE *page, int val)
{
  double f0, f1, f;

  double i;
  int j;

  f0 = page->left;
  f1 = page->right;
  f = page->width / (f1 - f0);
  i = f * (double)(val);

#ifdef HAS_RINT
  j = rint(i);
#else
  j = i;
#endif

  return(j);
}

/*! \brief Get absolute WORLD coordinate.
 *  \par Function Description
 *  Get absolute WORLD coordinate.
 *
 *  \param [in] page       The PAGE object.
 *  \param [in] val        The coordinate to convert.
 *  \return The converted WORLD coordinate.
 */
int WORLDabs(PAGE *page, int val)
{
  double fw0,fw1,fw,fval;

  double i;
  int j;

  fw1 = page->right;
  fw0 = page->left;
  fw  = page->width;
  fval = val;
  i = fval * (fw1 - fw0) / fw;

#ifdef HAS_RINT
  j = rint(i);
#else
  j = i;
#endif

  return(j);
}

/* \note
 * encode_halfspace and clip are part of the cohen-sutherland clipping
 * algorithm.  They are used to determine if an object is visible or not
 */
/*! \brief Encode SCREEN coordinates as halfspace matrix.
 *  \par Function Description
 *  This function takes a point and checks if it is in the bounds
 *  of the current page coordinates. It
 *  handles points with SCREEN coordinates.
 *
 *  \param [in]  page       The PAGE object.
 *  \param [in]  point      The point in SCREEN coordinates to be checked.
 *  \param [out] halfspace  The created HALFSPACE structure.
 *
 *  \warning halfspace must be allocated before this function is called
 */
static void SCREENencode_halfspace(PAGE *page, sPOINT *point, HALFSPACE *halfspace)
{
  halfspace->left = point->x < 0;
  halfspace->right = point->x > page->width;
  halfspace->bottom = point->y > page->height;
  halfspace->top = point->y < 0;
}


/*! \brief Calculate the cliping region for a set of coordinates.
 *  \par Function Description
 *  This function will check the provided set of coordinates to see if
 *  they fall within a clipping region.  If they do the coordinates will
 *  be changed to reflect only the region no covered by the clipping window.
 *  All coordinates should be in SCREEN units.
 *
 *  \param [in] page       The current PAGE object.
 *  \param [in,out] x1     x coordinate of the first screen point.
 *  \param [in,out] y1     y coordinate of the first screen point.
 *  \param [in,out] x2     x coordinate of the second screen point.
 *  \param [in,out] y2     y coordinate of the second screen point.
 *  \return TRUE if coordinates are now visible, FALSE otherwise.
 */
int SCREENclip_change(PAGE *page, int *x1, int *y1, int *x2, int *y2)
{
  HALFSPACE half1, half2;
  HALFSPACE tmp_half;
  sPOINT tmp_point;
  sPOINT point1, point2;
  float slope;
  int in1, in2, done;
  int known_visible;
  int w_l, w_t, w_r, w_b;

  point1.x = *x1;
  point1.y = *y1;
  point2.x = *x2;
  point2.y = *y2;


  w_l = 0;
  w_t = 0;
  w_r = page->width;
  w_b = page->height;

  done = FALSE;
  known_visible = FALSE;

  do {
    SCREENencode_halfspace(page, &point1, &half1);
    SCREENencode_halfspace(page, &point2, &half2);

    if (GEDA_DEBUG) {
      printf("starting loop\n");
      printf("1 %d %d %d %d\n", half1.left, half1.top, half1.right, half1.bottom);
      printf("2 %d %d %d %d\n", half2.left, half2.top, half2.right, half2.bottom);
    }

    in1 = (!half1.left) &&
      (!half1.top) &&
      (!half1.right) &&
      (!half1.bottom);

    in2 = (!half2.left) &&
      (!half2.top) &&
      (!half2.right) &&
      (!half2.bottom);


    if (in1 && in2) { /* trivially accept */
      done = TRUE;
      known_visible = TRUE;
    } else if ( ((half1.left && half2.left) ||
                 (half1.right && half2.right)) ||
                ((half1.top && half2.top) ||
                 (half1.bottom && half2.bottom)) ) {
      done = TRUE; /* trivially reject */
      known_visible = FALSE;
    } else { /* at least one point outside */
      if (in1) {
        tmp_half = half1;
        half1 = half2;
        half2 = tmp_half;

        tmp_point = point1;
        point1 = point2;
        point2 = tmp_point;
      }

      if (point2.x == point1.x) { /* vertical line */
        if (half1.top) {
          point1.y = w_t;
        } else if (half1.bottom) {
          point1.y = w_b;
        }
      } else { /* not a vertical line */

				/* possible fix for alpha core dumping */
				/* assume the object is visible */
        if ((point2.x - point1.x) == 0) {
          return(TRUE);
        }

        slope = (float) (point2.y - point1.y) /
          (float) (point2.x - point1.x);

				/* possible fix for alpha core dumping */
				/* assume the object is visible */
        if (slope == 0.0) {
          return(TRUE);
        }

        if (half1.left) {
          point1.y = point1.y +
            (w_l - point1.x) * slope;
          point1.x = w_l;
        } else if (half1.right) {
          point1.y = point1.y +
            (w_r - point1.x) * slope;
          point1.x = w_r;
        } else if (half1.bottom) {
          point1.x = point1.x +
            (w_b - point1.y) / slope;
          point1.y = w_b;
        } else if (half1.top) {
          point1.x = point1.x +
            (w_t - point1.y) / slope;
          point1.y = w_t;
        }
      } /* end of not a vertical line */
    } /* end of at least one outside */
  } while (!done);

  /*printf("after: %d %d %d %d\n", point1.x, point1.y, point2.x, point2.y);*/
  *x1 = point1.x;
  *y1 = point1.y;
  *x2 = point2.x;
  *y2 = point2.y;
  return known_visible;
}
