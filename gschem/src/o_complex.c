/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#include <math.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include "gschem.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_complex_draw(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current)
{
  g_return_if_fail (o_current != NULL); 
  g_return_if_fail (o_current->complex != NULL);
  g_return_if_fail (o_current->complex->prim_objs != NULL);

  if (o_current->complex->x == -1 && o_current->complex->y -- -1) {
    /* Off-schematic components have their own dialog. */
    return;
  }

  if (!w_current->DONT_REDRAW) {
    o_redraw(w_current, o_current->complex->prim_objs, LIST_KIND_HEAD, TRUE);
  }
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_complex_draw_xor(GSCHEM_TOPLEVEL *w_current, int dx, int dy, OBJECT *object)
{
  g_assert( (object->type == OBJ_COMPLEX ||
             object->type == OBJ_PLACEHOLDER) );

  o_list_draw_xor( w_current, dx, dy, object->complex->prim_objs);
}

static enum visit_result
prepare_place_update_one(OBJECT *o_current, void *userdata)
{
  PAGE *page = userdata;

  s_conn_update_object(page, o_current);

  return VISIT_RES_OK;
}

static enum visit_result
prepare_place_prepend_one(OBJECT *o_current, void *userdata)
{
  GList **prim_objs = userdata;

  *prim_objs = g_list_prepend(*prim_objs, o_current);

  return VISIT_RES_OK;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_complex_prepare_place(GSCHEM_TOPLEVEL *w_current, const char *sym_name)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = safe_page_current(toplevel);
  OBJECT *o_start;
  char *buffer;
  const CLibSymbol *sym;
  int dont_redraw;

  /* remove the old place list if it exists */
  s_page_replace_place_list(toplevel, page, NULL);

  /* Insert the new object into the buffer at world coordinates (0,0).
   * It will be translated to the mouse coordinates during placement. */

  w_current->first_wx = 0;
  w_current->first_wy = 0;

  if (w_current->include_complex) {
    GList *prim_objs = NULL;

    o_start = new_head(toplevel);

    toplevel->ADDING_SEL=1;
    buffer = s_clib_symbol_get_data_by_name (sym_name);
    o_read_buffer(toplevel, o_start, buffer, -1, sym_name);
    g_free (buffer);
    toplevel->ADDING_SEL=0;

    /* XXX The objects aren't on the page yet; is it right to update connections? */
    s_visit_list(o_start, LIST_KIND_HEAD,
		 &prepare_place_update_one, page, VISIT_ANY, 1);

    s_visit_list(o_start, LIST_KIND_HEAD,
		 &prepare_place_prepend_one, &prim_objs, VISIT_LINEAR, 1);
    prim_objs = g_list_reverse(prim_objs);

    /* Delete the HEAD node */
    s_basic_unlink_object(o_start);
    s_delete_list_fromstart (toplevel, o_start);

    s_page_add_place_list(page, prim_objs);
  } else { /* if (w_current->include_complex) {..} else { */
    OBJECT *new_object;
    GList *promoted;

    toplevel->ADDING_SEL = 1; /* reuse this flag, rename later hack */
    sym = s_clib_get_symbol_by_name (sym_name);
    new_object = o_complex_new (toplevel, OBJ_COMPLEX, WHITE, 0, 0, 0, 0,
                                sym, sym_name, 1);
    promoted = o_complex_get_promotable (toplevel, new_object, TRUE);

    /* Attach promoted attributes to the original complex object */
    o_attrib_attach_list (toplevel, promoted, new_object);

    s_page_add_place_list(page, promoted);
    s_page_add_place_list(page, g_list_prepend(NULL, new_object));

    /* Link the place list OBJECTs together for good measure */
    /* XXX Is linking the right thing? */
    o_glist_relink_objects(s_page_borrow_place_list(page));

    toplevel->ADDING_SEL = 0;

    /* Flag the symbol as embedded if necessary */
    (*new_object->embed_func)(toplevel, new_object, w_current->embed_complex);
  }

  /* Run the complex place list changed hook without redrawing */
  /* since the place list is going to be redrawn afterwards */
  dont_redraw = w_current->DONT_REDRAW;
  w_current->DONT_REDRAW = 1;
  o_complex_place_changed_run_hook (w_current);
  w_current->DONT_REDRAW = dont_redraw;

  w_current->inside_action = 1;
  i_set_state(w_current, DRAWCOMP);
}


/*! \todo Finish function documentation!!!
*  \brief
*  \par Function Description
*
*/
void o_complex_start(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y)
{
  o_place_start (w_current, w_x, w_y);
}


/*! \brief Run the complex place list changed hook. 
 *  \par Function Description
 *  The complex place list is usually used when placing new components
 *  in the schematic. This function should be called whenever that list
 *  is modified.
 *  \param [in] w_current GSCHEM_TOPLEVEL structure.
 *
 */
void o_complex_place_changed_run_hook(GSCHEM_TOPLEVEL *w_current) {
  TOPLEVEL *toplevel = w_current->toplevel;
  GList const *ptr = NULL;

  /* Run the complex place list changed hook */
  if (scm_hook_empty_p(complex_place_list_changed_hook) == SCM_BOOL_F &&
      s_page_borrow_place_list(toplevel->page_current) != NULL) {
    ptr = s_page_borrow_place_list(toplevel->page_current);
    while (ptr) {
      scm_run_hook(complex_place_list_changed_hook, 
		   scm_cons (g_make_object_smob
			     (toplevel, ptr->data), SCM_EOL));
      ptr = g_list_next(ptr);
    }
  }
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_complex_end(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y, int continue_placing)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  GList *new_objects;
  GList *iter;
  OBJECT *o_current;

  o_place_end (w_current, w_x, w_y, continue_placing, &new_objects);

  if (w_current->include_complex) {
    g_list_free (new_objects);
    return;
  }

  /* Run the add component hook for the new component */
  for (iter = new_objects;
       iter != NULL;
       iter = g_list_next (iter)) {
    o_current = iter->data;

    if (scm_hook_empty_p(add_component_hook) == SCM_BOOL_F) {
      scm_run_hook(add_component_hook,
                   scm_cons(g_make_attrib_smob_list(o_current), SCM_EOL));
    }

    if (scm_hook_empty_p(add_component_object_hook) == SCM_BOOL_F) {
      scm_run_hook(add_component_object_hook,
                   scm_cons(g_make_object_smob(toplevel, o_current),
                            SCM_EOL));
    }
  }

  g_list_free (new_objects);
}

static enum visit_result
translate_disconnect_one(OBJECT *o_current, void *userdata)
{
  if (o_current->type != OBJ_COMPLEX && o_current->type != OBJ_PLACEHOLDER) {
    s_conn_remove(o_current);
  } else {
    s_conn_remove_complex(o_current);
  }

  return VISIT_RES_OK;
}

static enum visit_result
translate_reconnect_one(OBJECT *o_current, void *userdata)
{
  PAGE *page = userdata;

  s_conn_update_object(page, o_current);

  return VISIT_RES_OK;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *  \note
 *  don't know if this belongs yet
 */
void o_complex_translate_all(GSCHEM_TOPLEVEL *w_current, int offset)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = safe_page_current(toplevel);
  int w_rleft, w_rtop, w_rright, w_rbottom;
  int x, y;

  /* first zoom extents */
  a_zoom_extents(w_current, page, A_PAN_DONT_REDRAW);
  o_redraw_all(w_current);

  world_get_object_list_bounds(page->object_head, LIST_KIND_HEAD,
                               &w_rleft,
                               &w_rtop,
                               &w_rright,
                               &w_rbottom);

  /*! \todo do we want snap grid here? */
  x = snap_grid( toplevel, w_rleft );
  /* WARNING: w_rtop isn't the top of the bounds, it is the smaller
   * y_coordinate, which represents in the bottom in world coords.
   * These variables are as named from when screen-coords (which had 
   * the correct sense) were in use . */
  y = snap_grid( toplevel, w_rtop );

  s_visit_page(page, &translate_disconnect_one, NULL, VISIT_ANY, 1);
        
  if (offset == 0) {
    s_log_message(_("Translating schematic [%d %d]\n"), -x, -y);
    o_list_translate_world(-x, -y, page->object_head);
  } else {
    s_log_message(_("Translating schematic [%d %d]\n"),
                  offset, offset);
    o_list_translate_world(offset, offset, page->object_head);
  }

  s_visit_page(page, &translate_reconnect_one, page, VISIT_ANY, 1);

  /* this is an experimental mod, to be able to translate to all
   * places */
  a_zoom_extents(w_current, page, A_PAN_DONT_REDRAW);
  if (!w_current->SHIFTKEY) o_select_unselect_all(w_current);
  o_redraw_all(w_current);
  page->CHANGED=1;
  o_undo_savestate(w_current, UNDO_ALL);
  i_update_menus(w_current);
}
