/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdarg.h>
#include <stdio.h>
#include <sys/stat.h>
#include <math.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include "gschem.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

static enum visit_result text_draw_one(OBJECT *o, void *userdata)
{
  GSCHEM_TOPLEVEL *w_current = userdata;

  o_redraw_single(w_current, o);

  return VISIT_RES_OK;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_text_draw_lowlevel(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current)
{
  int left, right, top, bottom;

  g_return_if_fail (o_current != NULL);
  g_return_if_fail (o_current->text != NULL);

  s_visit_object(o_current, &text_draw_one, w_current,
		 VISIT_ANY, VISIT_DEPTH_FOREVER);

  world_get_single_object_bounds(o_current, &left, &top, &right, &bottom);
  o_current->w_left   = left;
  o_current->w_top    = top;
  o_current->w_right  = right;
  o_current->w_bottom = bottom;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_text_draw_rectangle(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  int left=0, right=0, top=0, bottom=0;
  GdkColor *color;

  g_return_if_fail(o_current->type == OBJ_TEXT);

  o_recalc_single_object(o_current);
  if (!o_current->w_bounds_valid) {
    return;
  }

  /* text is too small so go through and draw a rectangle in
     it's place */

  /* NOTE THAT THE TOP AND BOTTOM ARE REVERSED THROUGHOUT THE WHOLE OF GEDA FOR WORLD COORDS */
  WORLDtoSCREEN(page, o_current->w_left, o_current->w_bottom, &left, &top);
  WORLDtoSCREEN(page, o_current->w_right, o_current->w_top, &right, &bottom);

  if (toplevel->override_color != -1 ) {  /* Override */
    color = x_get_color(toplevel->override_color);
  } else {
    color = x_get_color(o_current->color);
  }
  gdk_gc_set_foreground(w_current->gc, color);

  if (w_current->DONT_REDRAW == 0) {
    gdk_draw_rectangle( w_current->backingstore,
                        w_current->gc,
                        FALSE,
                        left, 
                        top,
                        right - left,
                        bottom - top );
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_text_draw(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  int screen_x1, screen_y1;
  int small_dist, offset;
  int x, y;

  g_return_if_fail (o_current != NULL);
  g_return_if_fail (o_current->type == OBJ_TEXT);
  g_return_if_fail (o_current->text != NULL);

  o_text_get_xy(o_current, &x, &y);
  if (x == -1 && y == -1) {
    /* Off-schematic text has its own dialog. */
    return;
  }

  if (w_current->DONT_REDRAW == 1 || !o_text_display_p(o_current)) {
    /* Not drawing the text at all. */
    return;
  }

  if (!w_current->fast_mousepan || !w_current->doing_pan) {
    o_text_draw_lowlevel(w_current, o_current);

    /* Indicate on the schematic that the text is invisible by */
    /* drawing a little I on the screen at the origin */
    if (!o_text_printing_p(o_current) && o_text_display_p(o_current)) {
      if (toplevel->override_color != -1 ) {
        gdk_gc_set_foreground(w_current->gc, 
                              x_get_color(toplevel->override_color));
      } else {
        gdk_gc_set_foreground(w_current->gc, 
                              x_get_color(w_current->lock_color));
      }

      offset = SCREENabs(page, 10);
      small_dist = SCREENabs(page, 20);
      WORLDtoSCREEN(page, x, y, &screen_x1, &screen_y1);
      screen_x1 += offset;
      screen_y1 += offset;
      if (w_current->DONT_REDRAW == 0) {
	/* Top part of the I */
	gdk_draw_line(w_current->backingstore, w_current->gc, 
		      screen_x1,
		      screen_y1,
		      screen_x1+small_dist,
		      screen_y1);
	/* Middle part of the I */
	gdk_draw_line(w_current->backingstore, w_current->gc, 
		      screen_x1+small_dist/2,
		      screen_y1,
		      screen_x1+small_dist/2,
		      screen_y1+small_dist);
	/* Bottom part of the I */
	gdk_draw_line(w_current->backingstore, w_current->gc, 
		      screen_x1,
		      screen_y1+small_dist,
		      screen_x1+small_dist,
		      screen_y1+small_dist);
      }	
    }
  } else {
    if (w_current->doing_pan) {
      o_text_draw_rectangle(w_current, o_current);
      return;
    }
  }
	
  /* return if text origin marker displaying is disabled */ 
  if (w_current->text_origin_marker == FALSE) {
    return;
  }

  small_dist = SCREENabs(page, 10);

  WORLDtoSCREEN(page, x, y, &screen_x1, &screen_y1);

  /* this is not really a fix, but a lame patch */
  /* not having this will cause a bad draw of things when coords */
  /* get close to the 2^15 limit of X */
  if (screen_x1+small_dist > 32767 || screen_y1+small_dist > 32767) {
    return;
  }

  if (toplevel->override_color != -1 ) {
    gdk_gc_set_foreground(w_current->gc, 
                          x_get_color(toplevel->override_color));
  } else {

    gdk_gc_set_foreground(w_current->gc, 
                          x_get_color(w_current->lock_color));
  }

  if (w_current->DONT_REDRAW == 0) {
    gdk_draw_line(w_current->backingstore, w_current->gc, 
		  screen_x1-small_dist, 
		  screen_y1+small_dist, 
		  screen_x1+small_dist, 
		  screen_y1-small_dist);
    
    gdk_draw_line(w_current->backingstore, w_current->gc, 
		  screen_x1+small_dist, 
		  screen_y1+small_dist, 
		  screen_x1-small_dist, 
		  screen_y1-small_dist);
  }
}

struct draw_context {
  GSCHEM_TOPLEVEL *w_current;
  int dx, dy;
};

static enum visit_result text_draw_xor_one(OBJECT *o_current, void *userdata)
{
  struct draw_context *ctx = userdata;

  o_draw_xor(ctx->w_current, ctx->dx, ctx->dy, o_current);

  return VISIT_RES_OK;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_text_draw_xor(GSCHEM_TOPLEVEL *w_current, int dx, int dy, OBJECT *o_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  int top, bottom, left, right;
  int color, factor;

  g_return_if_fail(o_current->type == OBJ_TEXT);

  if (!o_text_display_p(o_current)) {
    return;
  }

  /* always display text which is 156 mils or larger */
  factor = (int) page->to_world_x_constant;
  if ((factor < w_current->text_display_zoomfactor) ||
      w_current->text_feedback == ALWAYS ||
      o_text_height(o_current, "a") >= 156) {
    struct draw_context ctx = {
      .w_current = w_current,
      .dx = dx, .dy = dy,
    };
    s_visit_object(o_current, &text_draw_xor_one, &ctx,
		   VISIT_DETERMINISTIC, VISIT_DEPTH_FOREVER);
  } else {
    /* text is too small so go through and draw a line in
       it's place */
    WORLDtoSCREEN(page, o_current->w_left + dx, o_current->w_bottom + dy, &left, &top);
    WORLDtoSCREEN(page, o_current->w_right + dx, o_current->w_top + dy, &right, &bottom);

    if (o_current->saved_color != -1) {
      color = o_current->saved_color;
    } else {
      color = o_current->color;
    }

    gdk_gc_set_foreground(w_current->outline_xor_gc,
                          x_get_darkcolor(color));

    gdk_draw_rectangle( w_current->backingstore,
                        w_current->outline_xor_gc,
                        FALSE,
                        left,
                        top,
                        right - left,
                        bottom - top );
  }
}

void o_text_notice_changed(OBJECT *subject, GSCHEM_TOPLEVEL *w_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  char const *string = o_text_get_string(subject);

  /* erase old object */
  /* XXX This won't work since o_erase_single already regenerates prim_objs. */
  o_erase_single(w_current, subject);
  o_text_draw(w_current, subject);

  /* handle slot= attribute, it's a special case */
  if (string && g_ascii_strncasecmp (string, "slot=", 5) == 0) {
    o_slot_end (w_current, string, strlen (string));
  }

  if (subject->attached_to) {
    OBJECT *owner;

    owner = subject->attached_to;

    /* Another slotting special case. */
    if (g_ascii_strncasecmp(string, "slotname=", 9) == 0 ||
	g_ascii_strncasecmp(string, "slotowner=", 10) == 0) {
      s_slot_reparent_from_attribs(toplevel, owner);
    }

    /* Rewrite symbols in slots, to pick up new values. */
    if (owner->type == OBJ_COMPLEX) {
      s_slot_rewrite_symbols(owner);
    }

    if (owner->type == OBJ_SLOT && owner->slot->owner) {
      s_slot_rewrite_symbols(owner->slot->owner);
    }
  }

  toplevel->page_current->CHANGED = 1;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_text_prepare_place(GSCHEM_TOPLEVEL *w_current, char *text)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  OBJECT *new_text;

  /* Insert the new object into the buffer at world coordinates (0,0).
   * It will be translated to the mouse coordinates during placement. */

  w_current->first_wx = 0;
  w_current->first_wy = 0;

  w_current->last_drawb_mode = LAST_DRAWB_MODE_NONE;

  new_text = o_text_new(toplevel, OBJ_TEXT, w_current->text_color,
			0, 0, LOWER_LEFT, 0, /* zero is angle */
			text,
			w_current->text_size,
			/* has to be visible so you can place it */
			/* visibility is set when you create the object */
			VISIBLE, SHOW_NAME_VALUE);

  s_page_replace_place_list(toplevel, toplevel->page_current,
			    g_list_prepend(NULL, new_text));

  w_current->inside_action = 1;
  i_set_state(w_current, DRAWTEXT);
}


/*! \todo Finish function documentation!!!
 *  \brief
*  \par Function Description
*
*/
void o_text_start (GSCHEM_TOPLEVEL *w_current, int w_x, int w_y)
{
  o_place_start (w_current, w_x, w_y);
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_text_edit(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current)
{
  /* you need to check to make sure only one object is selected */
  /* no actually this is okay... not here in o_edit */
  text_edit_dialog(w_current, o_text_get_string(o_current),
		   o_text_get_size(o_current),
		   o_text_get_alignment(o_current));
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_text_edit_end(GSCHEM_TOPLEVEL *w_current, char *string, int len, int text_size,
		     int text_alignment)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  OBJECT *object;
  GList *s_current;
  int numselect;

  /* skip over head */
  s_current = geda_list_get_glist( toplevel->page_current->selection_list );
  numselect = g_list_length( geda_list_get_glist( toplevel->page_current->selection_list ));
  
  while(s_current != NULL) {
    object = s_current->data;

    if (object) {
      if (object->type == OBJ_TEXT) {
        o_erase_single(w_current, object);

	o_text_set_size(object, text_size);
	o_text_set_alignment(object, text_alignment);

        /* probably the text object should be extended to carry a color */
        /* and we should pass it here with a function parameter (?) */
        object->saved_color = w_current->edit_color;

        /* only change text string if there is only ONE text object selected */
        if (numselect == 1 && string) {
          o_text_set_string(object, string);
	  /* handle slot= attribute, it's a special case */
	  if (g_ascii_strncasecmp (string, "slot=", 5) == 0) {
	    o_slot_end (w_current, string, strlen (string));
	  }
        }

	g_signal_emit_by_name(G_OBJECT(object), "changed");
      } 
    }
    
    s_current = g_list_next(s_current);
  }
  
  toplevel->page_current->CHANGED = 1;
  o_undo_savestate(w_current, UNDO_ALL);
}

void o_text_notice_create(GSCHEM_TOPLEVEL *w_current, OBJECT *o)
{
  g_signal_connect(o, "changed", G_CALLBACK(o_text_notice_changed), w_current);
}
