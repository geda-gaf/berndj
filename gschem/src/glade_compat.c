#include "config.h"

#include <stdarg.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#include <gtk/gtk.h>
#include <glade/glade.h>

#include "glade_compat.h"
#include "support.h"

GtkWidget *x_lookup_widget(GladeXML *xml, char const *name)
{
  GtkWidget *widget;

  widget = glade_xml_get_widget(xml, name);

  return widget;
}

/*! \brief Set properties in a named widget.
 *  \par Function Description
 *  Find a child widget by name and set its properties.
 *  \param [in] xml     The GladeXML object.
 *  \param [in] name    The name of the child widget.
 *  \param [in] prop    The name of the first property to set.
 *  \param [in] ...     Value of the first property, followed by more
 *                      name/value pairs, terminated by NULL.
 *  \return A pointer to the child widget.
 */
GtkWidget *x_lookup_widget_set(GladeXML *xml,
			       char const *name, char const *prop, ...)
{
  GtkWidget *widget;
  va_list args;

  widget = x_lookup_widget(xml, name);
  if (widget == NULL) {
    return widget;
  }

  va_start(args, prop);
  g_object_set_valist(G_OBJECT(widget), prop, args);
  va_end(args);

  return widget;
}

GtkWidget *x_glade_get_widget(GladeXML **xml, char const *widget_basename, ...)
{
  GtkWidget *widget;
  char *gladefile, *widgetname;
  va_list rest;
  char const *signal_handler_name;
  GCallback signal_handler;

  gladefile = getenv("GSCHEM_GLADES");
  if (gladefile == NULL) {
    gladefile = GEDADATADIR "/glade";
  }
  gladefile = g_strconcat(gladefile, "/", widget_basename, ".glade", NULL);
  fprintf(stderr, "Building new xml from %s\n", gladefile);
  *xml = glade_xml_new(gladefile, NULL, NULL);
  g_free(gladefile);
  if (*xml == NULL) {
    g_critical(_("Can't build widget from Glade file [%s]\n"), gladefile);
    return NULL;
  }

  /* Find the toplevel widget in the tree. */
  widgetname = g_strconcat(widget_basename, "_dialog", NULL);
  widget = glade_xml_get_widget(*xml, widgetname);
  g_free(widgetname);

  /* Connect signal handlers manually.  Libglade could do it too... */
  /* XXX - we could just use glade_xml_signal_autoconnect(). */
  va_start(rest, widget_basename);
  while (1) {
    signal_handler_name = va_arg(rest, char const *);
    if (signal_handler_name == NULL) {
      break;
    }
    signal_handler = va_arg(rest, GCallback);
    glade_xml_signal_connect(*xml, signal_handler_name, signal_handler);
  }
  va_end(rest);

  return widget;
}
/* vim: set sw=2: */
