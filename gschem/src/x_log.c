/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#ifdef HAVE_STRING_H
#include <string.h>
#endif

#include "gschem.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

#ifdef HAVE_G_REGEX_NEW
#define USE_OBJECT_HYPERLINKS
#endif

struct global_page_search {
  /* Look for this string. */
  char const *key;

  /* Record the search progress here. */
  gboolean found;
  GSCHEM_TOPLEVEL *window;
  PAGE *page;
  OBJECT *o;

  /* Carry some widget context too. */
  Log *log;
  GtkTextTag *link_tag;
};

static void x_log_callback_response (GtkDialog *dialog,
                                     gint arg1,
                                     gpointer user_data);
static void log_message (Log *log, 
                         const gchar *message, 
                         const gchar *style);

static void log_base_init(LogClass *klass);
static void log_base_finalize(LogClass *klass);
static void log_class_init (LogClass *class);
static void log_init       (Log *log);

static GtkWidget *log_dialog = NULL;
static GObjectClass *log_parent_class = NULL;

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 * 
 */
void x_log_open ()
{
  if (log_dialog == NULL) {
    gchar *contents;
    
    log_dialog = GTK_WIDGET (g_object_new (TYPE_LOG,
                                           /* GschemDialog */
                                           "settings-name", "log",
                                           /* "toplevel", TOPLEVEL * */
					   /* GtkDialog */
					   "type", GTK_WINDOW_TOPLEVEL,
                                           NULL));

    g_signal_connect (log_dialog,
                      "response",
                      G_CALLBACK (x_log_callback_response),
                      NULL);

    /* make it read the content of the current log file */
    /* and add its contents to the dialog */
    contents = s_log_read ();

    /* s_log_read can return NULL if the log file cannot be written to */
    if (contents == NULL)
    {
      return;
    }

    log_message (LOG (log_dialog), contents, "old");
    g_free (contents);

    x_log_update_func = x_log_message;
   
    if( auto_place_mode )
	gtk_widget_set_uposition( log_dialog, 10, 10); 
    gtk_widget_show (log_dialog);
  } else {
    g_assert (IS_LOG (log_dialog));
    gtk_window_present ((GtkWindow*)log_dialog);
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 * 
 */
void x_log_close ()
{
  if (log_dialog) {
    g_assert (IS_LOG (log_dialog));
    gtk_widget_destroy (log_dialog);
    x_log_update_func = NULL;
    log_dialog = NULL;
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 * 
 */
void x_log_message (const gchar *log_domain, GLogLevelFlags log_level,
                    const gchar *message)
{
  gchar *style;
  g_return_if_fail (log_dialog != NULL);

  if (log_level & (G_LOG_LEVEL_CRITICAL | G_LOG_LEVEL_ERROR)) {
    style = "critical";
  } else if (log_level & G_LOG_LEVEL_WARNING) {
    style = "warning";
  } else {
    style = "message";
  }

  log_message (LOG(log_dialog), message, style);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 * 
 */
static void x_log_callback_response (GtkDialog *dialog,
				     gint arg1,
				     gpointer user_data)
{
  switch (arg1) {
    case GTK_RESPONSE_DELETE_EVENT:
    case LOG_RESPONSE_CLOSE:
    g_assert (GTK_WIDGET (dialog) == log_dialog);
    x_log_close ();
    break;
    default:
    g_assert_not_reached ();
  }
}

#ifdef USE_OBJECT_HYPERLINKS
static enum visit_result
object_has_name(OBJECT *o_current, void *userdata)
{
  struct global_page_search *search = userdata;

  g_return_val_if_fail(!search->found, VISIT_RES_ERROR);

  if (strcmp(o_current->name, search->key) == 0) {
    search->o = o_current;
    search->found = TRUE;
    return VISIT_RES_EARLY;
  }

  return VISIT_RES_OK;
}

static void page_has_object(PAGE *page, void *userdata)
{
  struct global_page_search *search = userdata;

  if (search->found) {
    return;
  }

  search->page = page;
  s_visit_page(page, &object_has_name, search, VISIT_ANY, 1);
}

static void window_has_object(void *list_data, void *userdata)
{
  GSCHEM_TOPLEVEL *window = list_data;
  struct global_page_search *search = userdata;

  if (search->found) {
    return;
  }

  search->window = window;
  s_toplevel_foreach_page(window->toplevel, &page_has_object, search);
}

static void check_text_tags(GtkTextView *text_view G_GNUC_UNUSED,
			    GtkTextIter *iter)
{
  /* Copied from GTK+'s demos/gtk-demo/hypertext.c then modified. */
  GSList *tags = NULL, *tagp = NULL;

  tags = gtk_text_iter_get_tags(iter);
  for (tagp = tags; tagp != NULL; tagp = tagp->next) {
    GtkTextTag *tag = tagp->data;
    char *name = g_object_get_data(G_OBJECT(tag), "object_name");
    struct global_page_search search;

    if (!name) {
      /* Can't be an object hyperlink tag. */
      continue;
    }

    search.key = name;
    search.found = FALSE;
    g_list_foreach(global_window_list, &window_has_object, &search);

    if (search.found) {
      o_select_object_simple(search.window, search.window->toplevel,
			     search.page, search.o, TRUE);
    }
  }

  if (tags) {
    g_slist_free (tags);
  }
}

static void x_log_textview_event_after(GtkWidget *text_view,
				       GdkEvent *ev)
{
  /* Copied from GTK+'s demos/gtk-demo/hypertext.c then modified. */
  GtkTextIter start, end, iter;
  GtkTextBuffer *buffer;
  GdkEventButton *event;
  gint x, y;

  if (ev->type != GDK_BUTTON_RELEASE) {
    return;
  }

  event = (GdkEventButton *) ev;

  if (event->button != 1) {
    return;
  }

  buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view));

  /*
   * We get button-release-event even when it's a click-drag-release sequence
   * to select some text. Tracking motion events between click and release
   * seems like a lot of effort, so rather just use the presence of a text
   * selection as a proxy for motion. TODO: Track motion events.
   */
  gtk_text_buffer_get_selection_bounds(buffer, &start, &end);
  if (gtk_text_iter_get_offset(&start) != gtk_text_iter_get_offset(&end)) {
    return;
  }

  gtk_text_view_window_to_buffer_coords(GTK_TEXT_VIEW(text_view),
					GTK_TEXT_WINDOW_WIDGET,
					event->x, event->y, &x, &y);

  gtk_text_view_get_iter_at_location(GTK_TEXT_VIEW(text_view), &iter, x, y);

  check_text_tags(GTK_TEXT_VIEW(text_view), &iter);
}

static void link_tag_finalized(void *userdata, GObject *ex_tag G_GNUC_UNUSED)
{
  char *name = userdata;

  g_free(name);
}

/*! \brief Insert text into the log, with hyperlinks.
 *  \par Function Description
 *  Inserts text into \a buffer, applying tags to correctly formatted object
 *  names where they appear. Object names need only have the correct format
 *  in order to become hyperlinks.
 *
 *  \note I can't easily tell if it's safe to cause log output from inside
 *  this function. Hence fprintf to stderr seems like a reasonable
 *  alternative.
 */
static void insert_with_links(Log *log, GtkTextBuffer *buffer,
			      GtkTextIter *iter, gchar const *message,
			      gchar const *tag_name, gchar const *style)
{
  LogClass *klass = LOG_CLASS(LOG_GET_CLASS(log));
  int last_end;
  GtkTextTagTable *tag_table;
  GtkTextTag *base_tag, *style_tag;

  tag_table = gtk_text_buffer_get_tag_table(buffer);
  base_tag = gtk_text_tag_table_lookup(tag_table, tag_name);
  style_tag = gtk_text_tag_table_lookup(tag_table, style);

  last_end = 0;
  while (1) {
    int start_pos, end_pos;
    GtkTextTag *link_tag;
    GMatchInfo *object_match = NULL;
    char *name;

    if (!g_regex_match_full(klass->link_re, message,
			    -1, last_end, 0, &object_match, NULL)) {
      /* No more object names. */
      if (object_match) {
	g_free(object_match);
      }
      break;
    }

    g_match_info_fetch_pos(object_match, 0, &start_pos, &end_pos);
    if (start_pos == -1 || end_pos == -1) {
      fprintf(stderr, "Can't get offsets for matched string \"%s\"\n",
	      message + last_end);
      /* Just pretend there was no match after all. */
      g_free(object_match);
      continue;
    }

    name = g_match_info_fetch(object_match, 1);
    if (object_match) {
      g_free(object_match);
    }
    if (!name) {
      fprintf(stderr,
	      "Can't extract object name from matched string \"%.*s\"\n",
	      end_pos - start_pos, message + start_pos);
      continue;
    }

    if (start_pos - last_end) {
      /* Push out normal text. */
      gtk_text_buffer_insert_with_tags(buffer, iter,
				       message + last_end, start_pos - last_end,
				       base_tag, style_tag, NULL);
    }

    link_tag = gtk_text_buffer_create_tag(buffer, NULL,
					  "foreground", "navy blue",
					  "underline", PANGO_UNDERLINE_SINGLE,
					  NULL);
    g_object_weak_ref(G_OBJECT(link_tag), &link_tag_finalized, name);

    /*
     * Store name instead of a pointer to the object, otherwise we need a
     * lot of weak reference spaghetti, and then also some signal spaghetti
     * in order to notice new objects whose names may already appear in the
     * log.
     */
    g_object_set_data(G_OBJECT(link_tag), "object_name", name);
    /* TODO: Connect to "event" on link_tag. */

    /* Push out the link. */
    gtk_text_buffer_insert_with_tags(buffer, iter,
				     message + start_pos, end_pos - start_pos,
				     base_tag, style_tag, link_tag, NULL);

    last_end = end_pos;
  }

  /* Push out any remaining text. */
  gtk_text_buffer_insert_with_tags(buffer, iter, message + last_end, -1,
				   base_tag, style_tag, NULL);
}
#endif /* USE_OBJECT_HYPERLINKS */

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 * 
 */
static void log_message (Log *log, const gchar *message, 
                         const gchar *style)
{
  GtkTextBuffer *buffer;
  GtkTextIter iter;
  GtkTextMark *mark;
  
  g_return_if_fail (IS_LOG (log));

  buffer = gtk_text_view_get_buffer (log->textview);
  gtk_text_buffer_get_end_iter (buffer, &iter);

  /* Apply the "plain" tag before the level-specific tag in order to
   * reset the formatting */
  if (g_utf8_validate (message, -1, NULL)) {
#ifdef USE_OBJECT_HYPERLINKS
    insert_with_links(log, buffer, &iter, message, "plain", style);
#else /* !USE_OBJECT_HYPERLINKS */
    gtk_text_buffer_insert_with_tags_by_name (buffer, &iter, message, -1,
                                              "plain", style, NULL);
#endif /* !USE_OBJECT_HYPERLINKS */
  } else {
    /* If UTF-8 wasn't valid (due to a system locale encoded filename or
     * other string being included by mistake), log a warning, and print
     * the original message to stderr, where it may be partly intelligible */
    gtk_text_buffer_insert_with_tags_by_name (buffer, &iter,
      _("** Invalid UTF-8 in log message. See stderr or gschem.log.\n"),
                                              -1, "plain", style, NULL);
    fprintf (stderr, "%s", message);
  }

  mark = gtk_text_buffer_create_mark(buffer, NULL, &iter, FALSE);
  gtk_text_view_scroll_to_mark (log->textview, mark, 0, TRUE, 0, 1);
  gtk_text_buffer_delete_mark (buffer, mark);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 * 
 */
GType log_get_type ()
{
  static GType log_type = 0;
  
  if (!log_type) {
    static const GTypeInfo log_info = {
      sizeof(LogClass),
      (GBaseInitFunc) &log_base_init,
      (GBaseFinalizeFunc) &log_base_finalize,
      (GClassInitFunc) log_class_init,
      NULL, /* class_finalize */
      NULL, /* class_data */
      sizeof(Log),
      0,    /* n_preallocs */
      (GInstanceInitFunc) log_init,
    };
		
    log_type = g_type_register_static (GSCHEM_TYPE_DIALOG,
                                       "Log",
                                       &log_info, 0);
  }
  
  return log_type;
}

static void log_base_init(LogClass *klass)
{
#ifdef USE_OBJECT_HYPERLINKS
  klass->link_re = g_regex_new("#<object\\s+(\\w+\\.\\d+)>", 0, 0, NULL);
#endif /* USE_OBJECT_HYPERLINKS */
}

static void log_base_finalize(LogClass *klass)
{
#ifdef USE_OBJECT_HYPERLINKS
  g_regex_unref(klass->link_re);
#endif /* USE_OBJECT_HYPERLINKS */
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 * 
 */
static void log_class_init (LogClass *klass)
{
  log_parent_class = g_type_class_peek_parent(klass);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 * 
 */
static void log_init (Log *log)
{
  GtkWidget *scrolled_win, *text_view;
  GtkTextBuffer *text_buffer;
  GtkTextMark *mark;

  /* dialog initialization */
  g_object_set (G_OBJECT (log),
                /* GtkContainer */
                "border-width",    0,
                /* GtkWindow */
                "title",           _("Status"),
                "default-width",   600,
                "default-height",  200,
                "modal",           FALSE,
                "window-position", GTK_WIN_POS_NONE,
                "type-hint",       GDK_WINDOW_TYPE_HINT_NORMAL,
                /* GtkDialog */
                "has-separator",   TRUE,
                NULL);

  /* create a scrolled window for the textview */
  scrolled_win = GTK_WIDGET (
    g_object_new (GTK_TYPE_SCROLLED_WINDOW,
                  /* GtkContainer */
                  "border-width",      5,
                  /* GtkScrolledWindow */
                  "hscrollbar-policy", GTK_POLICY_AUTOMATIC,
                  "vscrollbar-policy", GTK_POLICY_AUTOMATIC,
                  "shadow-type",       GTK_SHADOW_ETCHED_IN,
                  NULL));
  /* create the text buffer */
  text_buffer = GTK_TEXT_BUFFER (g_object_new (GTK_TYPE_TEXT_BUFFER,
                                               NULL));

  /* Add some tags for highlighting log messages to the buffer */
  gtk_text_buffer_create_tag (text_buffer, "plain",
                              "foreground", "black",
                              "foreground-set", TRUE,
                              "weight", PANGO_WEIGHT_NORMAL,
                              "weight-set", TRUE,
                              NULL);
  /* The default "message" style is plain */
  gtk_text_buffer_create_tag (text_buffer, "message", NULL);
  /* "old" messages are in dark grey */
  gtk_text_buffer_create_tag (text_buffer, "old",
                              "foreground", "#404040",
                              "foreground-set", TRUE,
			      NULL);
  /* "warning" messages are printed in red */
  gtk_text_buffer_create_tag (text_buffer, "warning",
                              "foreground", "red",
                              "foreground-set", TRUE,
                              NULL);
  /* "critical" messages are bold red */
  gtk_text_buffer_create_tag (text_buffer, "critical",
                              "foreground", "red",
                              "foreground-set", TRUE,
                              "weight", PANGO_WEIGHT_BOLD,
                              "weight-set", TRUE,
                              NULL);

  /* create the text view and attach the buffer to it */
  text_view = GTK_WIDGET (g_object_new (GTK_TYPE_TEXT_VIEW,
                                        /* GtkTextView */
/* unknown property in GTK 2.2, use gtk_text_view_set_buffer() instead */
/*                                         "buffer",   text_buffer, */
                                        "editable", FALSE,
                                        NULL));
  gtk_text_view_set_buffer (GTK_TEXT_VIEW (text_view), text_buffer);

#ifdef USE_OBJECT_HYPERLINKS
  /* Use event-after so that GtkTextView can censor button-release-event. */
  g_signal_connect(text_view, "event-after",
		   G_CALLBACK(&x_log_textview_event_after), NULL);
#endif /* USE_OBJECT_HYPERLINKS */

  /* add the text view to the scrolled window */
  gtk_container_add (GTK_CONTAINER (scrolled_win), text_view);
  /* set textview of log */
  log->textview = GTK_TEXT_VIEW (text_view);

  /* add the scrolled window to the dialog vbox */
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (log)->vbox), scrolled_win,
                      TRUE, TRUE, 0);
  gtk_widget_show_all (scrolled_win);

  /* now add the close button to the action area */
  gtk_dialog_add_button (GTK_DIALOG (log),
                         GTK_STOCK_CLOSE, LOG_RESPONSE_CLOSE);

  /* scroll to the end of the buffer */
  mark = gtk_text_buffer_get_insert (text_buffer);
  gtk_text_view_scroll_to_mark (GTK_TEXT_VIEW (text_view), mark, 0.0, TRUE, 0.0, 1.0);

  g_object_unref(G_OBJECT(text_buffer));
}
