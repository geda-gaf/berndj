/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#include <math.h>

#include "gschem.h"

#include "../include/o_multinet.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif


/* magnetic options */
/* half size of the magnetic marker on the screen. */
#define MAGNETIC_HALFSIZE 6

/* define how far the cursor could be to activate magnetic */
#define MAGNETIC_PIN_REACH 50 
#define MAGNETIC_NET_REACH 20
#define MAGNETIC_BUS_REACH 30

/* weighting factors to tell that a pin is more important than a net */
#define MAGNETIC_PIN_WEIGHT 5.0
#define MAGNETIC_NET_WEIGHT 2.0
#define MAGNETIC_BUS_WEIGHT 3.0

/* Bit definitions for the four quardrants of the direction guessing */
#define QUADRANT1  0x01
#define QUADRANT2  0x02
#define QUADRANT3  0x04
#define QUADRANT4  0x08


/*! \brief Reset all variables used for net drawing
 *  \par Function Description
 *  This function resets all variables from GSCHEM_TOPLEVEL that are used
 *  for net drawing. This function should be called when escaping from
 *  a net drawing action or before entering it.
 */
void o_net_reset(GSCHEM_TOPLEVEL *w_current) 
{
  if (w_current->which_object) {
    s_delete_object(w_current->toplevel, w_current->which_object);
    w_current->which_object = NULL;
    w_current->which_grip = GRIP_NONE;
  }

  w_current->magnetic_wx = w_current->magnetic_wy = -1;
  w_current->magnetic_visible = w_current->rubber_visible = 0;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_net_draw(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  int size;
  int x1, y1, x2, y2; /* screen coords */

#if NET_DEBUG /* debug */
  char *tempstring;
  GdkFont *font;
#endif

  if (o_current == NULL) {
    return;
  }

  if (o_current->line == NULL) {
    return;
  }

  /* reuse line's routine */
  if (w_current->DONT_REDRAW == 1 ||
      !o_line_visible(toplevel, o_current, &x1, &y1, &x2, &y2)) {
    return;
  }

  if (GEDA_DEBUG) {
    printf("drawing net\n\n");
  }

  if (toplevel->net_style == THICK ) {
    size = SCREENabs(page, NET_WIDTH);

    if (size < 0)
      size=0;

    gdk_gc_set_line_attributes(w_current->gc, size, 
                               GDK_LINE_SOLID,
                               GDK_CAP_BUTT,
                               GDK_JOIN_MITER);

    gdk_gc_set_line_attributes(w_current->bus_gc, size, 
                               GDK_LINE_SOLID,
                               GDK_CAP_BUTT,
                               GDK_JOIN_MITER);
  }

  if (toplevel->override_color != -1 ) {
    gdk_gc_set_foreground(w_current->gc,
                          x_get_color(toplevel->override_color));

    gdk_draw_line(w_current->backingstore, w_current->gc,
                  x1, y1, x2, y2);
  } else {
    gdk_gc_set_foreground(w_current->gc,
                          x_get_color(o_current->color));
    gdk_draw_line(w_current->backingstore, w_current->gc,
                  x1, y1, x2, y2);

#if NET_DEBUG
    /* temp debug only */
    font = gdk_fontset_load ("10x20");
    tempstring = g_strdup_printf("%s", o_current->name);
    gdk_draw_text (w_current->backingstore,
                   font,
                   w_current->gc,
                   x1+20, y1+20,
                   tempstring,
                   strlen(tempstring));
    gdk_font_unref(font);
    g_free(tempstring);
#endif
  }

  if (GEDA_DEBUG) {
    printf("drew net\n\n");
  }

  /* yes zero is right for the width -> use hardware lines */
  if (toplevel->net_style == THICK ) {
    gdk_gc_set_line_attributes(w_current->gc, 0, 
                               GDK_LINE_SOLID,
                               GDK_CAP_NOT_LAST,
                               GDK_JOIN_MITER);

    gdk_gc_set_line_attributes(w_current->bus_gc, 0, 
                               GDK_LINE_SOLID,
                               GDK_CAP_NOT_LAST,
                               GDK_JOIN_MITER);
  }
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_net_draw_xor(GSCHEM_TOPLEVEL *w_current, int dx, int dy, OBJECT *o_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  int size;
  int color;
  int w_x1, w_y1, w_x2, w_y2;
  int sx[2], sy[2];

  if (o_current->line == NULL) {
    return;
  }

  if (o_current->saved_color != -1) {
    color = o_current->saved_color;
  } else {
    color = o_current->color;
  }

  gdk_gc_set_foreground(w_current->outline_xor_gc,
			x_get_darkcolor(color));

  if (toplevel->net_style == THICK ) {
    size = SCREENabs(page, NET_WIDTH);
    gdk_gc_set_line_attributes(w_current->outline_xor_gc, size+1,
                               GDK_LINE_SOLID,
                               GDK_CAP_NOT_LAST,
                               GDK_JOIN_MITER);
  }

  s_basic_get_grip(o_current, GRIP_1, &w_x1, &w_y1);
  s_basic_get_grip(o_current, GRIP_2, &w_x2, &w_y2);

  WORLDtoSCREEN(page, w_x1 + dx, w_y1 + dy, &sx[0], &sy[0]);
  WORLDtoSCREEN(page, w_x2 + dx, w_y2 + dy, &sx[1], &sy[1]);

  gdk_draw_line(w_current->backingstore, w_current->outline_xor_gc,
                sx[0], sy[0], sx[1], sy[1]);

  if (toplevel->net_style == THICK ) {
    gdk_gc_set_line_attributes(w_current->outline_xor_gc, 0,
                               GDK_LINE_SOLID,
                               GDK_CAP_NOT_LAST,
                               GDK_JOIN_MITER);
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_net_draw_xor_single(GSCHEM_TOPLEVEL *w_current, int dx, int dy, int whichone,
			   OBJECT *o_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  int color;
  int dx1 = -1, dx2 = -1, dy1 = -1,dy2 = -1;
  int w_x1, w_y1, w_x2, w_y2;
  int sx[2], sy[2];

  if (o_current->line == NULL) {
    return;
  }

  if (o_current->saved_color != -1) {
    color = o_current->saved_color;
  } else {
    color = o_current->color;
  }

  gdk_gc_set_foreground(w_current->outline_xor_gc,
			x_get_darkcolor(color));

  if (whichone == 0) {
    dx1 = dx;
    dy1 = dy;
    dx2 = 0;
    dy2 = 0;
  } else if (whichone == 1) {
    dx2 = dx;
    dy2 = dy;
    dx1 = 0;
    dy1 = 0;
  } else {
    fprintf(stderr, _("Got an invalid which one in o_net_draw_xor_single\n"));
  }

  s_basic_get_grip(o_current, GRIP_1, &w_x1, &w_y1);
  s_basic_get_grip(o_current, GRIP_2, &w_x2, &w_y2);

  WORLDtoSCREEN(page, w_x1 + dx1, w_y1 + dy1, &sx[0], &sy[0]);
  WORLDtoSCREEN(page, w_x2 + dx2, w_y2 + dy2, &sx[1], &sy[1]);

  gdk_draw_line(w_current->backingstore, w_current->outline_xor_gc,
                sx[0], sy[0], sx[1], sy[1]);
  o_invalidate_rect(w_current,
                    sx[0], sy[0], sx[1], sy[1]);

}


static void o_net_guess_direction_atom(OBJECT *object, int wx, int wy,
				       int *up, int *down,
				       int *left, int *right)
{
  int x1, y1, x2, y2;
  int xmin, ymin, xmax, ymax;
  int orientation;

  int *current_rules;
  /* badness values       {OVERWRITE, ORTHO, CONTINUE} */
  const int pin_rules[] = {100, 50, 0};
  const int bus_rules[] = {90, 0, 40};
  const int net_rules[] = {80, 30, 0};

  if ((orientation = o_net_orientation(object)) == NEITHER)
    return;

  switch (object->type) {
  case OBJ_NET:
    current_rules = (int*) net_rules;
    break;
  case OBJ_PIN:
    current_rules = (int*) pin_rules;
    break;
  case OBJ_BUS:
    current_rules = (int*) bus_rules;
    break;
  default:
    g_assert_not_reached ();
  }

  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);

  xmin = min(x1, x2);
  ymin = min(y1, y2);
  xmax = max(x1, x2);
  ymax = max(y1, y2);

  if (orientation == HORIZONTAL && wy == y1) {
    if (wx == xmin) {
      *up = max(*up, current_rules[1]);
      *down = max(*down, current_rules[1]);
      *right = max(*right, current_rules[0]);
      *left = max(*left, current_rules[2]);
    } else if (wx == xmax) {
      *up = max(*up, current_rules[1]);
      *down = max(*down, current_rules[1]);
      *right = max(*right, current_rules[2]);
      *left = max(*left, current_rules[0]);
    } else if (xmin < wx && wx < xmax) {
      *up = max(*up, current_rules[1]);
      *down = max(*down, current_rules[1]);
      *right = max(*right, current_rules[0]);
      *left = max(*left, current_rules[0]);
    }
  }
  if (orientation == VERTICAL && wx == x1) {
    if (wy == ymin) {
      *up = max(*up, current_rules[0]);
      *down = max(*down, current_rules[2]);
      *right = max(*right, current_rules[1]);
      *left = max(*left, current_rules[1]);
    } else if (wy == ymax) {
      *up = max(*up, current_rules[2]);
      *down = max(*down, current_rules[0]);
      *right = max(*right, current_rules[1]);
      *left = max(*left, current_rules[1]);
    } else if (ymin < wy && wy < ymax) {
      *up = max(*up, current_rules[0]);
      *down = max(*down, current_rules[0]);
      *right = max(*right, current_rules[1]);
      *left = max(*left, current_rules[1]);
    }
  }
}

struct guess_direction_context {
  int wx, wy;
  int *up, *down, *left, *right;
};

static enum visit_result
o_net_guess_direction_recurse(OBJECT *object, void *userdata)
{
  struct guess_direction_context *guess = userdata;

  switch (object->type) {
  case OBJ_COMPLEX:
  case OBJ_PLACEHOLDER:
    s_visit_object(object, &o_net_guess_direction_recurse, guess,
		   VISIT_ANY, 1);
    break;
  case OBJ_BUS:
  case OBJ_NET:
  case OBJ_PIN:
    o_net_guess_direction_atom(object, guess->wx, guess->wy,
			       guess->up, guess->down,
			       guess->left, guess->right);
    break;
  }

  return VISIT_RES_OK;
}

/*! \brief guess the best direction for the next net drawing action
 *  \par Function Description
 *  This function checks all connectable objects at a starting point.
 *  It determines the best drawing direction for each quadrant of the
 *  possible net endpoint.
 *
 *  The directions are stored in the GSCHEM_TOPLEVEL->net_direction variable
 *  as a bitfield.
 */
void o_net_guess_direction(GSCHEM_TOPLEVEL *w_current,
			   int wx, int wy)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  int up=0, down=0, left=0, right=0;
  PAGE *page = safe_page_current(toplevel);
  struct guess_direction_context guess_context = {
    .wx = wx, .wy = wy,
    .up = &up, .down = &down, .left = &left, .right = &right,
  };

  g_return_if_fail(page != NULL);

  s_visit_page(page, &o_net_guess_direction_recurse, &guess_context, VISIT_ANY, 1);

  w_current->net_direction = 0;
  w_current->net_direction |= up >= right ? 0 : QUADRANT1;
  w_current->net_direction |= up >= left ? 0 : QUADRANT2;
  w_current->net_direction |= down >= left ? 0 : QUADRANT3;
  w_current->net_direction |= down >= right ? 0 : QUADRANT4;

#if 0
  printf("o_net_guess_direction_recurse: up=%d down=%d left=%d right=%d direction=%d\n",
	 up, down, left, right, w_current->net_direction);
#endif
}

static void o_net_find_magnetic_atom(OBJECT *object, OBJECT const **o_magnetic,
				     double *mindist, double *min_weight,
				     int *magnetic_x, int *magnetic_y,
				     int w_x, int w_y)
{
  double dist1, dist2;
  int min_x, min_y;
  int x1, y1, x2, y2;
  double distance;
  double weight;

  switch (object->type) {
  case OBJ_PIN:
    s_basic_get_grip(object, object->whichend, &min_x, &min_y);

    distance = sqrt((double) (w_x - min_x)*(w_x - min_x)
		    + (double) (w_y - min_y)*(w_y - min_y));
    weight = distance / MAGNETIC_PIN_WEIGHT;
    break;
  case OBJ_BUS:
  case OBJ_NET:
    /* we have 3 possible points to connect:
       2 endpoints and 1 midpoint point */
    s_basic_get_grip(object, GRIP_1, &x1, &y1);
    s_basic_get_grip(object, GRIP_2, &x2, &y2);
    /* endpoint tests */
    dist1 = sqrt((double) (w_x - x1)*(w_x - x1)
		 + (double) (w_y - y1)*(w_y - y1));
    dist2 = sqrt((double) (w_x - x2)*(w_x - x2)
		 + (double) (w_y - y2)*(w_y - y2));
    if (dist1 < dist2) {
      min_x = x1;
      min_y = y1;
      distance = dist1;
    }
    else {
      min_x = x2;
      min_y = y2;
      distance = dist2;
    }

    /* midpoint tests */
    if ((x1 == x2)  /* vertical net */
	&& ((y1 >= w_y && w_y >= y2)
	    || (y2 >= w_y && w_y >= y1))) {
      if (abs(w_x - x1) < distance) {
	distance = abs(w_x - x1);
	min_x = x1;
	min_y = w_y;
      }
    }
    if ((y1 == y2)  /* horizontal net */
	&& ((x1 >= w_x && w_x >= x2)
	    || (x2 >= w_x && w_x >= x1))) {
      if (abs(w_y - y1) < distance) {
	distance = abs(w_y - y1);
	min_x = w_x;
	min_y = y1;
      }
    }

    if (object->type == OBJ_BUS)
      weight = distance / MAGNETIC_BUS_WEIGHT;
    else /* OBJ_NET */
      weight = distance / MAGNETIC_NET_WEIGHT;
    break;
  default:
    g_assert_not_reached ();
  }

  if (*o_magnetic == NULL || weight < *min_weight) {
    *mindist = distance;
    *min_weight = weight;
    *o_magnetic = object;
    *magnetic_x = min_x;
    *magnetic_y = min_y;
  }
}

struct find_magnetic_context {
  PAGE const *page;
  OBJECT const *exclude;
  int w_x, w_y;
  OBJECT const *o_magnetic;
  double *mindist, *min_weight;
  int *magnetic_x, *magnetic_y;
};

static enum visit_result
o_net_find_magnetic_recurse(OBJECT *object, void *userdata)
{
  struct find_magnetic_context *find_magnetic = userdata;

  if (!s_conn_connectible_p(object)) {
    return VISIT_RES_OK;
  }
  if (!visible(find_magnetic->page, object->w_left, object->w_top,
	       object->w_right, object->w_bottom))
    return VISIT_RES_OK; /* skip invisible objects */

  /* Skip the net being drawn. */
  if (find_magnetic->exclude &&
      o_multinet_contains(find_magnetic->exclude, object)) {
    return VISIT_RES_OK;
  }

  switch (object->type) {
  case OBJ_COMPLEX:
    s_visit_object(object, &o_net_find_magnetic_recurse, find_magnetic,
		   VISIT_DETERMINISTIC, 1);
    break;

  case OBJ_BUS:
  case OBJ_NET:
  case OBJ_PIN:
    o_net_find_magnetic_atom(object, &find_magnetic->o_magnetic,
			     find_magnetic->mindist, find_magnetic->min_weight,
			     find_magnetic->magnetic_x, find_magnetic->magnetic_y,
			     find_magnetic->w_x, find_magnetic->w_y);
    break;
  default:
    /* neither pin nor net nor bus */
    break;
  }

  return VISIT_RES_OK;
}

/*! \brief find the closest possible location to connect to
 *  \par Function Description
 *  This function calculates the distance to all connectible objects
 *  and searches the closest connection point.
 *  It searches for pins, nets and busses.
 *
 *  The connection point is stored in GSCHEM_TOPLEVEL->magnetic_wx and
 *  GSCHEM_TOPLEVEL->magnetic_wy. If no connection is found. Both variables
 *  are set to -1.
 */
void o_net_find_magnetic(GSCHEM_TOPLEVEL *w_current,
			 int w_x, int w_y)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  int min_x, min_y;
  double minbest;
  double min_weight;
  struct find_magnetic_context find_magnetic = {
    .page = page,
    .exclude = w_current->which_object,
    .w_x = w_x, .w_y = w_y,
    .o_magnetic = NULL,
    .mindist = &minbest, .min_weight = &min_weight,
    .magnetic_x = &w_current->magnetic_wx,
    .magnetic_y = &w_current->magnetic_wy,
  };

  g_return_if_fail(page != NULL);

  minbest = min_x = min_y = 0;
  min_weight = 0;

  s_visit_page(page, &o_net_find_magnetic_recurse, &find_magnetic, VISIT_ANY, 1);

  /* check whether we found an object and if it's close enough */
  if (find_magnetic.o_magnetic != NULL) {
    int magnetic_reach = 0;

    switch (find_magnetic.o_magnetic->type) {
    case (OBJ_PIN): magnetic_reach = MAGNETIC_PIN_REACH; break;
    case (OBJ_NET): magnetic_reach = MAGNETIC_NET_REACH; break;
    case (OBJ_BUS): magnetic_reach = MAGNETIC_BUS_REACH; break;
    }
    if (minbest > WORLDabs(page, magnetic_reach)) {
      w_current->magnetic_wx = -1;
      w_current->magnetic_wy = -1;
    }
  }
  else {
    w_current->magnetic_wx = -1;
    w_current->magnetic_wy = -1;
  }
}

/*! \brief calculates the net route to the magnetic marker
 *  \par Function Description
 *  Depending on the two rubbernet lines from start to last and from
 *  last to second, the 3 coordinates are manipulated to find
 *  a way to the magnetic marker.
 */
void o_net_finishmagnetic(GSCHEM_TOPLEVEL *w_current)
{
  OBJECT *multinet = w_current->which_object;

  s_basic_move_grip(multinet, w_current->which_grip,
		    w_current->magnetic_wx, w_current->magnetic_wy);
}

/*! \brief callback function to draw a net marker in magnetic mode
 *  \par Function Description
 *  If the mouse is moved, this function is called to update the 
 *  position of the magnetic marker.
 *  If the controllkey is pressed the magnetic marker follows the mouse.
 */
void o_net_start_magnetic(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y)
{
  o_net_eraserubber(w_current);
  /* the dc is completely clean now, reset inside_action. */
  w_current->inside_action = 0;

  if (w_current->CONTROLKEY) {
    w_current->magnetic_wx = w_x;
    w_current->magnetic_wy = w_y;
  }
  else {
    o_net_find_magnetic(w_current, w_x, w_y);
  }

  o_net_drawrubber(w_current);
}

/*! \brief set the start point of a new net
 *  \par Function Description
 *  This function sets the start point of a new net at the position of the 
 *  cursor. If we have a visible magnetic marker, we use that instead of 
 *  the cursor position
 */
void o_net_start(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  OBJECT *multinet;
  int start_x, start_y;

  if (w_current->magnetic_visible) {
    start_x = w_current->magnetic_wx;
    start_y = w_current->magnetic_wy;
  } else {
    start_x = w_x;
    start_y = w_y;
  }

  multinet = o_multinet_new_at_xy(w_current->toplevel, OBJ_EXTENSION,
				  w_current->net_color, 0, 0);

  w_current->which_grip = s_basic_move_grip(multinet, GRIP_NONE,
					    start_x, start_y);
  w_current->which_object = multinet;
  w_current->subject_is_new = 1;

  o_draw_rubbersubject_xor(w_current);

  if (start_x != snap_grid(toplevel, start_x)
      || start_y != snap_grid(toplevel, start_y))
      s_log_message(_("Warning: Starting net at off grid coordinate\n"));

  if (w_current->net_direction_mode)
    o_net_guess_direction(w_current, start_x, start_y);
}

/*! \brief finish a net drawing action 
 * \par Function Description 
 * This function finishes the drawing of a net. If we have a visible
 * magnetic marker, we use that instead of the current cursor
 * position.
 *
 * The rubber nets are removed, the nets and cues are drawn and the
 * net is added to the TOPLEVEL structure.  
 *
 * The function returns TRUE if it has drawn a net, FALSE otherwise.
 */
int o_net_end(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  OBJECT *multinet = w_current->which_object;
  OBJECT *primary, *secondary;
  GList *found_primary_connection = NULL;
  int save_magnetic, save_wx, save_wy;

  GList *other_objects = NULL;
  OBJECT *new_net = NULL;

  g_assert( w_current->inside_action != 0 );

  /* I've to store that because o_net_eraserubber would delete it
     but I need it for o_net_finish_magnetic */
  save_magnetic = w_current->magnetic_visible;

  gdk_gc_set_foreground(w_current->xor_gc,
			x_get_darkcolor(w_current->select_color) );

  o_net_eraserubber(w_current);

  if (save_magnetic)
    o_net_finishmagnetic(w_current);

  /* Drop the degenerate net segments. */
  o_multinet_normalize(toplevel, multinet);

  /* See if either of the nets are zero length.  We'll only add */
  /* the non-zero ones */
  primary = o_multinet_steal_primary(multinet);
  secondary = o_multinet_steal_secondary(multinet);

  /* If both nets are zero length... */
  /* this ends the net drawing behavior */
  if (primary == NULL && secondary == NULL) {
    s_delete_object(toplevel, multinet);
    w_current->which_object = NULL;
    w_current->which_grip = GRIP_NONE;
    return FALSE;
  }

  s_basic_get_grip(multinet, w_current->which_grip, &save_wx, &save_wy);

  /* We're done with the multinet but still have its net segments. */
  s_delete_object(w_current->toplevel, multinet);
  w_current->which_object = NULL;
  w_current->which_grip = GRIP_NONE;

  if (save_wx != snap_grid(toplevel, save_wx)
      || save_wy != snap_grid(toplevel, save_wy))
      s_log_message(_("Warning: Ending net at off grid coordinate\n"));

  if (primary) {
    /* create primary net */
    new_net = primary;
    s_page_append(page, new_net);
    primary = NULL;
  
      /* conn stuff */
      /* LEAK CHECK 1 */
      other_objects = s_conn_return_others(other_objects, page->object_tail);

      if (o_net_add_busrippers(w_current, new_net, other_objects)) {
	  g_list_free(other_objects);
	  other_objects = NULL;
	  other_objects = s_conn_return_others(other_objects, new_net);
      }

      if (GEDA_DEBUG) {
	printf("primary:\n");
	s_conn_print(new_net->conn_list);
      }
  
      o_cue_undraw_list(w_current, other_objects);
      o_cue_draw_list(w_current, other_objects);
      o_cue_draw_single(w_current, new_net);

      g_list_free(other_objects);
      other_objects = NULL;

      /* Go off and search for valid connection on this newly created net */
      found_primary_connection = s_conn_net_search(new_net, GRIP_2,
                                                   new_net->conn_list);
      found_primary_connection = g_list_remove_all(found_primary_connection,
						   secondary);
      if (found_primary_connection)
      {
      	/* if a net connection is found, reset start point of next net */
	s_basic_get_grip(new_net, GRIP_2, &save_wx, &save_wy);
      }

      /* you don't want to consolidate nets which are drawn non-ortho */
      if (toplevel->net_consolidate == TRUE && !w_current->CONTROLKEY) {
        /* CAUTION: Object list will change when nets are consolidated, don't
         *          keep pointers to other objects than new_net after this. */
        o_net_consolidate_segments(toplevel, page, new_net);
      }
  }

  /* If the second net is not zero length, add it as well */
  /* Also, a valid net connection from the primary net was not found */
  if (secondary && !found_primary_connection) {
    /* Add secondary net */
    new_net = secondary;
    s_page_append(page, new_net);
    secondary = NULL;
  
      /* conn stuff */
      /* LEAK CHECK 2 */
      other_objects = s_conn_return_others(other_objects, page->object_tail);

      if (o_net_add_busrippers(w_current, new_net, other_objects)) {
	  g_list_free(other_objects);
	  other_objects = NULL;
	  other_objects = s_conn_return_others(other_objects, new_net);
      }
      if (GEDA_DEBUG) {
	s_conn_print(new_net->conn_list);
      }

      o_cue_undraw_list(w_current, other_objects);
      o_cue_draw_list(w_current, other_objects);
      o_cue_draw_single(w_current, new_net);

      g_list_free(other_objects);
      other_objects = NULL;

      /* you don't want to consolidate nets which are drawn non-ortho */
      if (toplevel->net_consolidate == TRUE && !w_current->CONTROLKEY) {
        /* CAUTION: Object list will change when nets are consolidated, don't
         *          keep pointers to other objects than new_net after this. */
        o_net_consolidate_segments(toplevel, page, new_net);
      }
  }

  g_list_free(found_primary_connection);

  /* Delete any leftover net segments.  There can be no leftover primary. */
  if (secondary) {
    other_objects = s_conn_return_others(NULL, secondary);
    o_cue_undraw_list(w_current, other_objects);
    o_cue_erase_single(w_current, secondary);
    o_erase_single(w_current, secondary);
    s_delete_object(toplevel, secondary);
    o_cue_draw_list(w_current, other_objects);
  }

  page->CHANGED = 1;
  w_current->first_wx = save_wx;
  w_current->first_wy = save_wy;
  o_undo_savestate(w_current, UNDO_ALL);

  return (TRUE);
}

/*! \brief erase and redraw the rubber lines when drawing a net
 *  \par Function Description
 *  This function draws the rubbernet lines when drawing a net.
 */
void o_net_rubbernet(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y)
{
  OBJECT *multinet = w_current->which_object;
  int ortho, horizontal, quadrant;

  g_assert( w_current->inside_action != 0 );

  /* Orthognal mode enabled when Control Key is NOT pressed or
     if we are using magnetic mode */
  ortho = !w_current->CONTROLKEY || w_current->magneticnet_mode;

  o_net_eraserubber(w_current);

  if (w_current->magneticnet_mode) {
    if (w_current->CONTROLKEY) {
      /* set the magnetic marker position to current xy if the
	 controlkey is pressed. Thus the net will not connect to 
	 the closest net if we finish the net drawing */
      w_current->magnetic_wx = w_x;
      w_current->magnetic_wy = w_y;
    }
    else {
      o_net_find_magnetic(w_current, w_x, w_y);
    }
  }

  s_basic_move_grip(multinet, w_current->which_grip, w_x, w_y);

  /* In orthogonal mode secondary line is the same as the first */
  if (!ortho) {
    o_multinet_make_ortho(w_current->toplevel->page_current, multinet); /* XXX ??? */
  }
  /* If you press the control key then you can draw non-ortho nets */
  else {
    int preferred_direction;
    int net_start[2], net_end[2];

    s_basic_get_grip(multinet, GRIP_NET_START, &net_start[0], &net_start[1]);
    s_basic_get_grip(multinet, GRIP_NET_END, &net_end[0], &net_end[1]);

    if (net_end[1] > net_start[1])
      quadrant = net_end[0] > net_start[0] ? QUADRANT1: QUADRANT2;
    else
      quadrant = net_end[0] > net_start[0] ? QUADRANT4: QUADRANT3;

    horizontal = w_current->net_direction & quadrant;

    /* Pressing the shift key will cause the vertical and horizontal lines to switch places */
    if (!w_current->SHIFTKEY)
      horizontal = !horizontal;

    preferred_direction = horizontal ? DIRECTION_H_THEN_V : DIRECTION_V_THEN_H;
    o_multinet_set_direction(w_current->toplevel->page_current,
			     multinet, preferred_direction);
  }

  o_net_drawrubber(w_current);
}

/*! \brief draw rubbernet lines to the gc
 *  \par Function Description
 *  This function draws the rubbernets to the graphic context
 */
void o_net_drawrubber(GSCHEM_TOPLEVEL *w_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  int size=0, magnetic_halfsize;
  int magnetic_x, magnetic_y;

  WORLDtoSCREEN(page, w_current->magnetic_wx, w_current->magnetic_wy,
		&magnetic_x, &magnetic_y);

  w_current->rubber_visible = 1;

  if (toplevel->net_style == THICK) {
    size = SCREENabs(page, NET_WIDTH);
    gdk_gc_set_line_attributes(w_current->xor_gc, size,
			       GDK_LINE_SOLID,
			       GDK_CAP_NOT_LAST, GDK_JOIN_MITER);
  }
  size = max(size, 0);

  gdk_gc_set_foreground(w_current->xor_gc,
			x_get_darkcolor(w_current->select_color));

  if (w_current->magneticnet_mode) {
    if (w_current->magnetic_wx != -1 && w_current->magnetic_wy != -1) {
      w_current->magnetic_visible = 1;
      w_current->inside_action = 1;
      magnetic_halfsize = max(4*size, MAGNETIC_HALFSIZE);
      gdk_draw_arc(w_current->backingstore, w_current->xor_gc, FALSE,
		   magnetic_x - magnetic_halfsize,
		   magnetic_y - magnetic_halfsize,
		   2*magnetic_halfsize, 2*magnetic_halfsize,
		   0, FULL_CIRCLE);
      o_invalidate_rect(w_current, 
			magnetic_x - magnetic_halfsize - magnetic_halfsize,
			magnetic_y - magnetic_halfsize - magnetic_halfsize,
			magnetic_x + magnetic_halfsize + magnetic_halfsize,
			magnetic_y + magnetic_halfsize + magnetic_halfsize);
    }
  }

  if (toplevel->net_style == THICK) {
    gdk_gc_set_line_attributes(w_current->xor_gc, 0,
			       GDK_LINE_SOLID,
			       GDK_CAP_NOT_LAST, GDK_JOIN_MITER);
  }

  o_draw_rubbersubject_xor(w_current);
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *  \note
 *  used in button cancel code in x_events.c
 */
void o_net_eraserubber(GSCHEM_TOPLEVEL *w_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  int size=0, magnetic_halfsize;
  int magnetic_x, magnetic_y;

  if (! w_current->rubber_visible)
    return;

  WORLDtoSCREEN(page, w_current->magnetic_wx, w_current->magnetic_wy,
		&magnetic_x, &magnetic_y);

  w_current->rubber_visible = 0;

  if (toplevel->net_style == THICK) {
    size = SCREENabs(page, NET_WIDTH);
    gdk_gc_set_line_attributes(w_current->xor_gc, size,
			       GDK_LINE_SOLID,
			       GDK_CAP_NOT_LAST, GDK_JOIN_MITER);
  }
  size = max(size, 0);

  gdk_gc_set_foreground(w_current->xor_gc,
			x_get_darkcolor(w_current->select_color));

  if (w_current->magnetic_visible) {
    w_current->magnetic_visible = 0;
    magnetic_halfsize = max(4*size, MAGNETIC_HALFSIZE);
    gdk_draw_arc(w_current->backingstore, w_current->xor_gc, FALSE,
		 magnetic_x - magnetic_halfsize,
		 magnetic_y - magnetic_halfsize,
		 2*magnetic_halfsize, 2*magnetic_halfsize,
		 0, FULL_CIRCLE);
      o_invalidate_rect(w_current, 
			magnetic_x - magnetic_halfsize - magnetic_halfsize,
			magnetic_y - magnetic_halfsize - magnetic_halfsize,
			magnetic_x + magnetic_halfsize + magnetic_halfsize,
			magnetic_y + magnetic_halfsize + magnetic_halfsize);
  }

  if (toplevel->net_style == THICK) {
    gdk_gc_set_line_attributes(w_current->xor_gc, 0,
			       GDK_LINE_SOLID,
			       GDK_CAP_NOT_LAST, GDK_JOIN_MITER);
  }
  o_draw_rubbersubject_xor(w_current);
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
int o_net_add_busrippers(GSCHEM_TOPLEVEL *w_current, OBJECT *net_obj,
			 GList *other_objects)

{
  TOPLEVEL *toplevel = w_current->toplevel;
  OBJECT *new_obj;
  int color;
  GList *cl_current = NULL;
  OBJECT *bus_object = NULL;
  CONN *found_conn = NULL;
  int done;
  int otherone;
  BUS_RIPPER rippers[2];
  int ripper_count = 0;
  int i;
  double length;
  int sign;
  double distance1, distance2;
  int first, second;
  int made_changes = FALSE;
  const int ripper_size = w_current->bus_ripper_size;
  int complex_angle = 0;
  const CLibSymbol *rippersym = NULL;
  
  length = o_line_length(net_obj);

  if (!other_objects) {
    return(FALSE);
  }
  
  if (length <= ripper_size) {
    return(FALSE);
  }

  if (toplevel->override_net_color == -1) {
    color = w_current->net_color;
  } else {
    color = toplevel->override_net_color;
  }

  
  /* check for a bus connection and draw rippers if so */
  cl_current = other_objects;
  while (cl_current != NULL) {
    int bus_x[2], bus_y[2];
    
    bus_object = cl_current->data;

    s_basic_get_grip(bus_object, GRIP_1, &bus_x[0], &bus_y[0]);
    s_basic_get_grip(bus_object, GRIP_2, &bus_x[1], &bus_y[1]);

    if (bus_object && bus_object->type == OBJ_BUS) {
      /* yes, using the net routine is okay */
      int bus_orientation = o_net_orientation(bus_object);
      int net_orientation = o_net_orientation(net_obj);
      int net_x[2], net_y[2];
      int whichone;

      /* find the CONN structure which is associated with this object */
      GList *cl_current2 = net_obj->conn_list;
      done = FALSE;
      while (cl_current2 != NULL && !done) {
	CONN *tmp_conn = (CONN *) cl_current2->data;

	if (tmp_conn && tmp_conn->other_object &&
	    tmp_conn->other_object == bus_object) {

	  found_conn = tmp_conn;
	  done = TRUE;
	}

	cl_current2 = g_list_next(cl_current2);
      }

      if (!found_conn) {
        return(FALSE);
      }
      
      whichone = found_conn->whichone; /* Take copy for shorter lines. */

      switch (whichone) {
      case GRIP_1:
	otherone = GRIP_2;
	break;
      case GRIP_2:
	otherone = GRIP_1;
	break;
      default:
	/*
	 * There should be no connections between nets and busses other than
	 * endpoints of nets with the bus.
	 */
	g_error("Insane state: connection object has whichone %d\n", whichone);
	return FALSE; /* In case g_error is nonfatal. */
      }
      
      s_basic_get_grip(net_obj, whichone, &net_x[0], &net_y[0]);
      s_basic_get_grip(net_obj, otherone, &net_x[1], &net_y[1]);

      /* now deal with the found connection */
      if (bus_orientation == HORIZONTAL && net_orientation == VERTICAL) {
	/* printf("found horiz bus %s %d!\n", bus_object->name, whichone);*/

        sign = bus_object->bus_ripper_direction;
        if (!sign) {

          if (bus_x[0] < bus_x[1]) {
            first = 0;
            second = 1;
          } else {
            first = 1;
            second = 0;
          }
              
          distance1 = abs(bus_x[first] - net_x[0]);
          distance2 = abs(bus_x[second] - net_x[0]);
          
          if (distance1 <= distance2) {
            sign = 1;
          } else {
            sign = -1;
          }
          bus_object->bus_ripper_direction = sign;
        }
        /* printf("hor sign: %d\n", sign); */

        if (net_y[1] < bus_y[0]) {
          /* new net is below bus */
          /*printf("below\n");*/

          if (ripper_count >= 2) {
            /* try to exit gracefully */
            fprintf(stderr, _("Tried to add more than two bus rippers. Internal gschem error.\n"));
            made_changes = FALSE;
            break;
          }

          if (w_current->bus_ripper_rotation == NON_SYMMETRIC) {
            /* non-symmetric */
            if (sign == 1) {
              complex_angle = 0;
            } else {
              complex_angle = 90;
            }
          } else {
            /* symmetric */
            complex_angle = 0;
          }

	  net_y[0] -= ripper_size;
	  s_basic_move_grip(net_obj, whichone, net_x[0], net_y[0]);
          o_recalc_single_object(net_obj);
          rippers[ripper_count].x[0] = net_x[0];
          rippers[ripper_count].y[0] = net_y[0];
          rippers[ripper_count].x[1] = net_x[0] + sign*ripper_size;
          rippers[ripper_count].y[1] = net_y[0] + ripper_size;
          ripper_count++;
          /* printf("done\n"); */
          made_changes++;
          
        } else {
          /* new net is above bus */
          /* printf("above\n"); */

          if (ripper_count >= 2) {
            /* try to exit gracefully */
            fprintf(stderr, _("Tried to add more than two bus rippers. Internal gschem error.\n"));
            made_changes = FALSE;
            break;
          }

          if (w_current->bus_ripper_rotation == NON_SYMMETRIC) {
            /* non-symmetric */
            if (sign == 1) {
              complex_angle = 270;
            } else {
              complex_angle = 180;
            }
          } else {
            /* symmetric */
            complex_angle = 180;
          }
          
	  net_y[0] += ripper_size;
	  s_basic_move_grip(net_obj, whichone, net_x[0], net_y[0]);
          o_recalc_single_object(net_obj);
          rippers[ripper_count].x[0] = net_x[0];
          rippers[ripper_count].y[0] = net_y[0];
          rippers[ripper_count].x[1] = net_x[0] + sign*ripper_size;
          rippers[ripper_count].y[1] = net_y[0] - ripper_size;
            ripper_count++;
            
            /* printf("done\n"); */
          made_changes++;
        }
        
        
      } else if (bus_orientation == VERTICAL &&
		 net_orientation == HORIZONTAL) {

	/* printf("found vert bus %s %d!\n", bus_object->name, whichone); */

        sign = bus_object->bus_ripper_direction;
        if (!sign) {
          if (bus_y[0] < bus_y[1]) {
            first = 0;
            second = 1;
          } else {
            first = 1;
            second = 0;
          }

          distance1 = abs(bus_y[first] - net_y[0]);
          distance2 = abs(bus_y[second] - net_y[0]);
          
          if (distance1 <= distance2) {
            sign = 1;
          } else {
            sign = -1;
          }
          bus_object->bus_ripper_direction = sign;
        } 
        /* printf("ver sign: %d\n", sign); */

        
        if (net_x[1] < bus_x[0]) {
          /* new net is to the left of the bus */
          /* printf("left\n"); */
          
          if (ripper_count >= 2) {
            /* try to exit gracefully */
            fprintf(stderr, _("Tried to add more than two bus rippers. Internal gschem error.\n"));
            made_changes = FALSE;
            break;
          }

          if (w_current->bus_ripper_rotation == NON_SYMMETRIC) {
            /* non-symmetric */
            if (sign == 1) {
              complex_angle = 0;
            } else {
              complex_angle = 270;
            }
          } else {
            /* symmetric */
            complex_angle = 270;
          }

	  net_x[0] -= ripper_size;
	  s_basic_move_grip(net_obj, whichone, net_x[0], net_y[0]);
          o_recalc_single_object(net_obj);
          rippers[ripper_count].x[0] = net_x[0];
          rippers[ripper_count].y[0] = net_y[0];
          rippers[ripper_count].x[1] = net_x[0] + ripper_size;
          rippers[ripper_count].y[1] = net_y[0] + sign*ripper_size;
          ripper_count++;
                    
          made_changes++;
        } else {
          /* new net is to the right of the bus */
          /* printf("right\n"); */

          if (ripper_count >= 2) {
            /* try to exit gracefully */
            fprintf(stderr, _("Tried to add more than two bus rippers. Internal gschem error.\n"));
            made_changes = FALSE;
            break;
          }

          if (w_current->bus_ripper_rotation == NON_SYMMETRIC) {
            /* non-symmetric */
            if (sign == 1) {
              complex_angle = 90;
            } else {
              complex_angle = 180;
            }
          } else {
            /* symmetric */
            complex_angle = 90;
          }

	  net_x[0] += ripper_size;
	  s_basic_move_grip(net_obj, whichone, net_x[0], net_y[0]);
          o_recalc_single_object(net_obj);
          rippers[ripper_count].x[0] = net_x[0];
          rippers[ripper_count].y[0] = net_y[0];
          rippers[ripper_count].x[1] = net_x[0] - ripper_size;
          rippers[ripper_count].y[1] = net_y[0] + sign*ripper_size;
          ripper_count++;

          made_changes++;
        }
      }
    }


    cl_current = g_list_next(cl_current);
  }
 
  if (made_changes) {
    s_conn_remove(net_obj);

    if (w_current->bus_ripper_type == COMP_BUS_RIPPER) {
      GList *symlist = 
	s_clib_search (toplevel->bus_ripper_symname, CLIB_EXACT);
      if (symlist != NULL) {
        rippersym = (CLibSymbol *) symlist->data;
      }
      g_list_free (symlist);
    }
    
    for (i = 0; i < ripper_count; i++) {
      if (w_current->bus_ripper_type == NET_BUS_RIPPER) {
        new_obj = o_net_new(toplevel, OBJ_NET, color,
                  rippers[i].x[0], rippers[i].y[0],
                  rippers[i].x[1], rippers[i].y[1]);
	s_page_append(toplevel->page_current, new_obj);
      } else {

        if (rippersym != NULL) {
          new_obj = o_complex_new (toplevel, OBJ_COMPLEX, WHITE,
                                   rippers[i].x[0], rippers[i].y[0],
                                   complex_angle, 0,
                                   rippersym,
                                   toplevel->bus_ripper_symname, 1);
	  s_page_append(toplevel->page_current, new_obj);
          o_complex_promote_attribs(toplevel, toplevel->page_current, new_obj);

          o_complex_draw (w_current, new_obj);
        } else {
          s_log_message(_("Bus ripper symbol [%s] was not found in any component library\n"),
                        toplevel->bus_ripper_symname);
        }
      }
    }
    
    s_conn_update_object(toplevel->page_current, net_obj);
    return(TRUE);
  }

  return(FALSE);
}
