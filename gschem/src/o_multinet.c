/* gEDA - GPL Electronic Design Automation
 * o_multinet.c - multi-net meta-object.
 * Copyright (C) 2008 Bernd Jendrissek
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include "gschem.h"

#include "../include/o_multinet.h"

struct st_multinet {
  TOPLEVEL *toplevel;
  PAGE *page;
  OBJECT *primary;
  OBJECT *secondary;
  int last_x;
  int last_y;
  enum multinet_direction_t direction;
};

typedef struct st_multinet MULTINET;

static void o_multinet_destroy(OBJECT *o);
static enum visit_result
o_multinet_visit(OBJECT *o,
		 enum visit_result (*fn)(OBJECT *, void *), void *context,
		 enum visit_order order, int depth);
static void o_multinet_grip_foreach(OBJECT *o,
				    gboolean (*fn)(OBJECT *o,
						   int grip_x, int grip_y,
						   enum grip_t whichone,
						   void *userdata),
				    void *userdata);
static int o_multinet_grip_move(OBJECT *o, int whichone, int x, int y);
static void o_multinet_draw(GSCHEM_TOPLEVEL *w_current, OBJECT *o);

static void o_multinet_recalc_bounds(OBJECT *o) {
  MULTINET *multinet = o->type_state;
  OBJECT *primary = multinet->primary;
  OBJECT *secondary = multinet->secondary;

  o->w_left = min(primary->w_left, secondary->w_left);
  o->w_right = max(primary->w_right, secondary->w_right);
  o->w_bottom = max(primary->w_bottom, secondary->w_bottom);
  o->w_top = min(primary->w_top, secondary->w_top);
  o->w_bounds_valid = TRUE;
}

static void o_multinet_recalc(PAGE *page, OBJECT *o) {
  MULTINET *multinet = o->type_state;
  OBJECT *primary = multinet->primary;
  OBJECT *secondary = multinet->secondary;
  CONN *new_conn;
  int x, y;

  o_multinet_recalc_bounds(o);

  s_conn_remove(primary);
  s_conn_update_object(page, primary);
  s_conn_remove(secondary);
  s_conn_update_object(page, secondary);

  s_basic_get_grip(multinet->primary, GRIP_2, &x, &y);

  new_conn = s_conn_return_new(secondary, CONN_ENDPOINT,
			       x, y, GRIP_2, GRIP_1);
  if (s_conn_uniq(primary->conn_list, new_conn)) {
    primary->conn_list = g_list_append(primary->conn_list, new_conn);
  } else {
    g_free(new_conn);
  }

  new_conn = s_conn_return_new(primary, CONN_ENDPOINT,
			       x, y, GRIP_1, GRIP_2);
  if (s_conn_uniq(secondary->conn_list, new_conn)) {
    secondary->conn_list = g_list_append(secondary->conn_list, new_conn);
  } else {
    g_free(new_conn);
  }
}

OBJECT *o_multinet_new_at_xy(TOPLEVEL *toplevel, char type, int color, int x, int y)
{
  OBJECT *new_node;
  MULTINET *multinet;

  /* create the object */
  new_node = s_toplevel_new_object(toplevel, type, "multinet");
  new_node->color = color;

  multinet = g_malloc(sizeof (MULTINET));
  new_node->type_state = multinet;

  multinet->toplevel = toplevel;
  multinet->page = toplevel->page_current;
  multinet->primary = o_net_new_at_xy(toplevel, OBJ_NET, color, x, y);
  multinet->secondary = o_net_new_at_xy(toplevel, OBJ_NET, color, x, y);
  /* Just choose any direction arbitrarily. */
  multinet->direction = DIRECTION_H_THEN_V;

  new_node->bounds_recalc_func = o_multinet_recalc_bounds;
  new_node->draw_func = &o_multinet_draw;
  new_node->visit_func = &o_multinet_visit;
  new_node->grip_foreach_func = &o_multinet_grip_foreach;
  new_node->grip_move_func = &o_multinet_grip_move;
  new_node->destroy_func = &o_multinet_destroy;

  return new_node;
}

static void o_multinet_destroy(OBJECT *o)
{
  MULTINET *multinet = o->type_state;
  TOPLEVEL *toplevel = multinet->toplevel;
  OBJECT *primary = multinet->primary;
  OBJECT *secondary = multinet->secondary;

  if (primary) {
    s_delete_object(toplevel, primary);
  }
  if (secondary) {
    s_delete_object(toplevel, secondary);
  }
}

enum multinet_direction_t o_multinet_get_direction(OBJECT *o)
{
  MULTINET *multinet = o->type_state;

  return multinet->direction;
}

void o_multinet_set_direction(PAGE *page, OBJECT *o,
			      enum multinet_direction_t direction)
{
  MULTINET *multinet = o->type_state;
  int x[2], y[2];
  int knee_x, knee_y;

  if (multinet->direction == direction) {
    /* No change, nothing to do. */
    return;
  }

  if (direction == DIRECTION_NEXT) {
    switch (multinet->direction) {
    case DIRECTION_H_THEN_V:
      direction = DIRECTION_V_THEN_H;
      break;
    case DIRECTION_V_THEN_H:
      direction = DIRECTION_H_THEN_V;
      break;
    default:
      break;
    }
  }

  s_basic_get_grip(multinet->primary, GRIP_1, &x[0], &y[0]);
  s_basic_get_grip(multinet->secondary, GRIP_2, &x[1], &y[1]);

  switch (direction) {
  case DIRECTION_H_THEN_V:
    knee_x = x[1];
    knee_y = y[0];
    break;
  case DIRECTION_V_THEN_H:
    knee_x = x[0];
    knee_y = y[1];
    break;
  default:
    return;
  }

  s_basic_move_grip(multinet->primary, GRIP_2, knee_x, knee_y);
  s_basic_move_grip(multinet->secondary, GRIP_1, knee_x, knee_y);

  multinet->direction = direction;

  o_multinet_recalc(page, o);
}

void o_multinet_make_ortho(PAGE *page, OBJECT *o)
{
  MULTINET *multinet = o->type_state;
  int knee_x, knee_y;

  s_basic_get_grip(multinet->secondary, GRIP_1, &knee_x, &knee_y);
  s_basic_move_grip(multinet->secondary, GRIP_2, knee_x, knee_y);

  o_multinet_recalc(page, o);
}

static enum visit_result
o_multinet_visit(OBJECT *o,
		 enum visit_result (*fn)(OBJECT *, void *), void *context,
		 enum visit_order order, int depth)
{
  MULTINET *multinet = o->type_state;

  if (multinet->primary) {
    (*fn)(multinet->primary, context);
  }
  if (multinet->secondary) {
    (*fn)(multinet->secondary, context);
  }

  return VISIT_RES_OK;
}

static void o_multinet_grip_foreach(OBJECT *o,
				    gboolean (*fn)(OBJECT *o,
						   int grip_x, int grip_y,
						   enum grip_t whichone,
						   void *userdata),
				    void *userdata)
{
  MULTINET *multinet = o->type_state;
  OBJECT *primary = multinet->primary;
  OBJECT *secondary = multinet->secondary;
  GRIP multinet_grips[] = {
    { .x = 0, .y = 0, .whichone = GRIP_NET_START },
    { .x = 0, .y = 0, .whichone = GRIP_NET_END },
    { .whichone = GRIP_NONE }
  };

  if (primary || secondary) {
    s_basic_get_grip(primary ? primary : secondary, GRIP_1,
		     &multinet_grips[0].x, &multinet_grips[0].y);
    s_basic_get_grip(secondary ? secondary : primary, GRIP_2,
		     &multinet_grips[1].x, &multinet_grips[1].y);
  } else {
    multinet_grips[0].x = multinet_grips[1].x = multinet->last_x;
    multinet_grips[0].y = multinet_grips[1].y = multinet->last_y;
  }

  s_basic_grip_foreach_helper(o, multinet_grips, fn, userdata);
}

static int o_multinet_grip_move(OBJECT *o, int whichone, int x, int y)
{
  MULTINET *multinet = o->type_state;
  int retval = GRIP_NONE;
  int knee_x, knee_y;

  switch (whichone) {
  case GRIP_NONE:
    s_basic_move_grip(multinet->primary, GRIP_1, x, y);
    s_basic_move_grip(multinet->primary, GRIP_2, x, y);
    s_basic_move_grip(multinet->secondary, GRIP_1, x, y);
    s_basic_move_grip(multinet->secondary, GRIP_2, x, y);
    retval = GRIP_NET_END;
    break;

  case GRIP_NET_START:
    s_basic_move_grip(multinet->primary, GRIP_1, x, y);
    s_basic_get_grip(multinet->primary, GRIP_2, &knee_x, &knee_y);
    switch (multinet->direction) {
    case DIRECTION_H_THEN_V:
      knee_y = y;
      break;
    case DIRECTION_V_THEN_H:
      knee_x = x;
      break;
    default:
      break;
    }
    s_basic_move_grip(multinet->primary, GRIP_2, knee_x, knee_y);
    s_basic_move_grip(multinet->secondary, GRIP_1, knee_x, knee_y);
    break;

  case GRIP_NET_END:
    s_basic_move_grip(multinet->secondary, GRIP_2, x, y);
    s_basic_get_grip(multinet->secondary, GRIP_1, &knee_x, &knee_y);
    switch (multinet->direction) {
    case DIRECTION_H_THEN_V:
      knee_x = x;
      break;
    case DIRECTION_V_THEN_H:
      knee_y = y;
      break;
    default:
      break;
    }
    s_basic_move_grip(multinet->secondary, GRIP_1, knee_x, knee_y);
    s_basic_move_grip(multinet->primary, GRIP_2, knee_x, knee_y);
    break;
  }

  multinet->last_x = x;
  multinet->last_y = y;

  /*
   * XXX Need to recompute connections; it doesn't suffice just to recompute
   * object bounds. While a multinet object may never exist in a page's object
   * list, its constituent nets will.
   */
  o_multinet_recalc(multinet->page, o);

  return retval;
}

static void o_multinet_draw(GSCHEM_TOPLEVEL *w_current, OBJECT *o)
{
  MULTINET *multinet = o->type_state;

  (*multinet->primary->draw_func)(w_current, multinet->primary);
  (*multinet->secondary->draw_func)(w_current, multinet->secondary);
}

void o_multinet_get_lengths(OBJECT *o, int *primary, int *secondary)
{
  MULTINET *multinet = o->type_state;
  int net_start[2], knee[2], net_end[2];

  s_basic_get_grip(multinet->primary, GRIP_1, &net_start[0], &net_start[1]);
  s_basic_get_grip(multinet->primary, GRIP_2, &knee[0], &knee[1]);
  s_basic_get_grip(multinet->secondary, GRIP_2, &net_end[0], &net_end[1]);

  /* Segments are either horizontal or vertical, so just add coord diffs. */
  if (primary) {
    *primary = abs(knee[0] - net_start[0]) + abs(knee[1] - net_start[1]);
  }
  if (secondary) {
    *secondary = abs(net_end[0] - knee[0]) + abs(net_end[1] - knee[1]);
  }
}

void o_multinet_normalize(TOPLEVEL *toplevel, OBJECT *o)
{
  MULTINET *multinet = o->type_state;

  multinet->primary = o_net_normalize(toplevel, multinet->primary);
  multinet->secondary = o_net_normalize(toplevel, multinet->secondary);
}

gboolean o_multinet_contains(OBJECT const *haystack, OBJECT const *needle)
{
  MULTINET *multinet = haystack->type_state;

  return (multinet->primary == needle || multinet->secondary == needle);
}

OBJECT *o_multinet_steal_primary(OBJECT *o)
{
  MULTINET *multinet = o->type_state;
  OBJECT *primary = multinet->primary;

  multinet->primary = NULL;

  return primary;
}

OBJECT *o_multinet_steal_secondary(OBJECT *o)
{
  MULTINET *multinet = o->type_state;
  OBJECT *secondary = multinet->secondary;

  multinet->secondary = NULL;

  return secondary;
}
