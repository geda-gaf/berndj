/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#include <math.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include "gschem.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

static int undo_file_index=0;
static int prog_pid=0;

static char* tmp_path = NULL;

/* this is additional number of levels (or history) at which point the */
/* undo stack will be trimmed, it's used a safety to prevent running out */ 
/* of entries to free */
#define UNDO_PADDING  5

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_undo_init(void)
{
  prog_pid = getpid();

  tmp_path = g_strdup (getenv("TMP"));
  if (tmp_path == NULL) {
     tmp_path = g_strdup ("/tmp");
  }
  if (GEDA_DEBUG) {
    printf("%s\n", tmp_path);
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *  
 *  <B>flag</B> can be one of the following values:
 *  <DL>
 *    <DT>*</DT><DD>UNDO_ALL
 *    <DT>*</DT><DD>UNDO_VIEWPORT_ONLY
 *  </DL>
 */
void o_undo_savestate(GSCHEM_TOPLEVEL *w_current, int flag)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  char *filename = NULL;
  OBJECT *object_head = NULL;
  int levels;

  /* save autosave backups if necessary */
  o_autosave_backups(w_current);

  if (w_current->undo_control == FALSE) {
    return;
  }

  if (w_current->undo_type == UNDO_DISK && flag == UNDO_ALL) {
    /* Increment the number of operations since last backup if 
       auto-save is enabled */
    if (toplevel->auto_save_interval != 0) {
      page->ops_since_last_backup++;
    }

    filename = g_strdup_printf("%s%cgschem.save%d_%d.sch",
                               tmp_path, G_DIR_SEPARATOR,
                               prog_pid, undo_file_index++);

    /* Changed from f_save to o_save when adding backup copy creation. */
    /* f_save manages the creation of backup copies.
       This way, f_save is called only when saving a file, and not when
       saving an undo backup copy */
    o_save(toplevel, page, filename);
  } else if (w_current->undo_type == UNDO_MEMORY && flag == UNDO_ALL) {
    /* Increment the number of operations since last backup if 
       auto-save is enabled */
    if (toplevel->auto_save_interval != 0) {
      page->ops_since_last_backup++;
    }

    object_head = s_toplevel_new_object(toplevel, OBJ_HEAD, "undo_head");

    o_list_copy_all(toplevel, page->object_head,
                    object_head, NORMAL_FLAG);
  }

  s_undo_add(toplevel, page, flag, filename, object_head,
             page->left, page->top, page->right, page->bottom,
             page->page_control, page->up_page);

  if (GEDA_DEBUG) {
    printf("\n\n---Undo----\n");
    s_undo_print_all(page);
    printf("----\n");
  }

  g_free(filename);

  /* Now go through and see if we need to free/remove some undo levels */ 
  /* so we stay within the limits */

  /* only check history every 10 undo savestates */
  if (undo_file_index % 10) {
    return;
  }

  if (GEDA_DEBUG) {
    levels = s_undo_levels(page);

    printf("levels: %d\n", levels);
  }

  s_undo_trim(toplevel, page,
	      w_current->undo_levels + UNDO_PADDING);

  if (GEDA_DEBUG) {
    printf("\n\n---Undo----\n");
    s_undo_print_all(page);
    printf("----\n");
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *  <B>type</B> can be one of the following values:
 *  <DL>
 *    <DT>*</DT><DD>UNDO_ACTION
 *    <DT>*</DT><DD>REDO_ACTION
 *  </DL>
 */
void o_undo_callback(GSCHEM_TOPLEVEL *w_current, int type)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  PAGE *p_new;
  GList *old_state, *new_state;
  UNDO *u_new;
  UNDO *u_old;
  GList *save_newest;
  int save_logging;
  int find_prev_data=FALSE;
  int prev_status;

  char *save_filename;

  if (w_current->undo_control == FALSE) {
    s_log_message(_("Undo/Redo disabled in rc file\n"));
    return;
  }

  if (page->undo_current == NULL) {
    return;
  }

  switch (type) {
  case UNDO_ACTION:
    new_state = page->undo_current->next;
    break;
  case REDO_ACTION:
    new_state = page->undo_current->prev;
    break;
  default:
    return;
  }

  old_state = page->undo_current;

  if (new_state == NULL) {
    /* There is no state to undo/redo to. */
    return;
  }

  u_new = new_state->data;
  u_old = old_state->data;

  switch (w_current->undo_type) {
  case UNDO_DISK:
    if (u_new->filename == NULL) {
      s_log_message(_("File-based undo requested with no file\n"));
      return;
    }
    break;
  case UNDO_MEMORY:
    if (u_new->object_head == NULL) {
      s_log_message(_("In-memory undo requested with no object list\n"));
      return;
    }
    break;
  default:
    s_log_message(_("Unknown undo type\n"));
    return;
  }

  if (u_old->type == UNDO_ALL && u_new->type == UNDO_VIEWPORT_ONLY) {
    if (GEDA_DEBUG) {
      printf("Type: %d\n", u_new->type);
      printf("Current is an undo all, next is viewport only!\n");
    }
    find_prev_data = TRUE;

    o_undo_copy_prev_memento(new_state, w_current->undo_type);
  }

  /* save filename */
  save_filename = g_strdup (page->page_filename);

  /* save structure so it's not nuked */
  save_newest = page->undo_newest;
  page->undo_newest = NULL;
  page->undo_current = NULL;

  switch (w_current->undo_type) {
  case UNDO_DISK:
    p_new = s_page_new(toplevel, u_new->filename);
    break;
  case UNDO_MEMORY:
    p_new = s_page_new (toplevel, save_filename);
    break;
  default:
    /* We handled the default case gracefully earlier. */
    g_assert(0);
  }
  p_new->width = w_current->win_width;
  p_new->height = w_current->win_height;
  s_toplevel_goto_page(toplevel, p_new);
  set_window(p_new,
	     toplevel->init_left, toplevel->init_right,
	     toplevel->init_top, toplevel->init_bottom);

  s_hierarchy_replace_page(toplevel, page, p_new);

  /* Finally delete the page now that the new page is linked. */
  s_page_delete (toplevel, page);

  /* temporarily disable logging */
  save_logging = do_logging;
  prev_status = w_current->DONT_REDRAW;
  w_current->DONT_REDRAW = 1;
  do_logging = FALSE;

  if (w_current->undo_type == UNDO_DISK && u_new->filename) {
    f_open(toplevel, p_new, u_new->filename, NULL);

    x_manual_resize(w_current);
    p_new->page_control = u_new->page_control;
    p_new->up_page = u_new->up_page;
    p_new->CHANGED=1;
  } else if (w_current->undo_type == UNDO_MEMORY && u_new->object_head) {
    s_delete_list_fromstart(toplevel, p_new->object_head);

    p_new->object_head = s_toplevel_new_object(toplevel, OBJ_HEAD, "object_head");

    o_list_copy_all(toplevel, u_new->object_head,
                    p_new->object_head,
                    NORMAL_FLAG);

    p_new->object_tail = return_tail(p_new->object_head);
    x_manual_resize(w_current);
    p_new->page_control = u_new->page_control;
    p_new->up_page = u_new->up_page;
    p_new->CHANGED=1;
  }

  /* do misc setups */
  set_window(p_new,
             u_new->left, u_new->right,
             u_new->top, u_new->bottom);
  x_hscrollbar_update(w_current);
  x_vscrollbar_update(w_current);

  /* restore logging */
  do_logging = save_logging;

  /* set filename right */
  g_free(p_new->page_filename);
  p_new->page_filename = save_filename;

  /* final redraw */
  x_pagesel_update (w_current);
  x_multiattrib_update(w_current, p_new->selection_list);

  /* Let the caller to decide if redraw or not */
  w_current->DONT_REDRAW = prev_status;

  if (!w_current->DONT_REDRAW) {
    o_redraw_all(w_current);
  }
  i_update_menus(w_current);

  /* restore saved undo structures */
  p_new->undo_newest = save_newest;

  p_new->undo_current = new_state;

  /* don't have to free data here since filename, object_head are */
  /* just pointers to the real data (lower in the stack) */
  if (find_prev_data) {
    u_new->filename = NULL;
    u_new->object_head = NULL;
  }

  if (GEDA_DEBUG) {
    printf("\n\n---Undo----\n");
    s_undo_print_all(p_new);
    printf("----\n");
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_undo_cleanup(void)
{
  int i;
  char *filename;

  for (i = 0 ; i < undo_file_index; i++) {
    filename = g_strdup_printf("%s%cgschem.save%d_%d.sch", tmp_path,
                               G_DIR_SEPARATOR, prog_pid, i);
    unlink(filename);
    g_free(filename);
  }

  g_free(tmp_path);
  tmp_path = NULL;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_undo_remove_last_undo(GSCHEM_TOPLEVEL *w_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  if (toplevel->page_current->undo_current == NULL) {
    return;
  }

  toplevel->page_current->undo_current =
    toplevel->page_current->undo_current->next;
}
