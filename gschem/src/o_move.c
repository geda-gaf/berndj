/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>

#include "gschem.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_move_start(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  STRETCH *st_current;

  if (o_select_selected (w_current)) {

    /* Save the current state. When rotating the selection when moving,
       we have to come back to here */
    o_undo_savestate(w_current, UNDO_ALL);
    w_current->last_drawb_mode = LAST_DRAWB_MODE_NONE;
    w_current->event_state = MOVE;

    w_current->first_wx = w_current->second_wx = w_x;
    w_current->first_wy = w_current->second_wy = w_y;

    o_erase_selected(w_current);

    if (w_current->netconn_rubberband) {
      o_move_prep_rubberband(w_current);

      /* Set the dont_redraw flag on rubberbanded objects.
       * This ensures that they are not drawn (in their
       * un-stretched position) during screen updates. */
      st_current = toplevel->page_current->stretch_head->next;
      while (st_current != NULL) {
        st_current->object->dont_redraw = TRUE;
        st_current = st_current->next;
      }
    }

    o_select_move_to_place_list(w_current);
    w_current->inside_action = 1;

    o_move_rubbermove_xor (w_current, TRUE);
  }
}

#define SINGLE     0
#define COMPLEX    1

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *  \note
 *  type can be SINGLE or COMPLEX
 *  which basically controls if this is a single object or a complex
 */
void o_move_end_lowlevel(GSCHEM_TOPLEVEL *w_current, OBJECT * list, int type,
			 int diff_x, int diff_y,
			 GList** other_objects, GList** connected_objects)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  OBJECT *o_current;

  o_current = list;
  while (o_current != NULL) {
    switch (o_current->type) {
      case (OBJ_NET):
      case (OBJ_BUS):
      case (OBJ_PIN):
        /* save the other objects and remove o_current's connections */
        *other_objects = s_conn_return_others(*other_objects, o_current);
        s_conn_remove(o_current);

        /* do the actual translation */
        o_translate_world(diff_x, diff_y, o_current);
        s_conn_update_object(toplevel->page_current, o_current);
        *connected_objects = s_conn_return_others(*connected_objects, o_current);
        break;

      default:
        o_translate_world(diff_x, diff_y, o_current);
        break;
    }

    if (type == COMPLEX) {
      o_current = o_current->next;
    } else {
      o_current = NULL;
    }
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_move_end(GSCHEM_TOPLEVEL *w_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  STRETCH *st_current;
  GList *s_current = NULL;
  OBJECT *object;
  int diff_x, diff_y;
  int left, top, right, bottom;
  GList *other_objects = NULL;
  GList *connected_objects = NULL;
  GList *rubbernet_objects = NULL; 
  GList *rubbernet_other_objects = NULL;
  GList *rubbernet_connected_objects = NULL;

  object = o_select_return_first_object(w_current);

  if (!object) {
    /* actually this is an error condition hack */
    w_current->inside_action = 0;
    i_set_state(w_current, SELECT);
    return;
  }

  diff_x = w_current->second_wx - w_current->first_wx;
  diff_y = w_current->second_wy - w_current->first_wy;

  o_move_rubbermove_xor (w_current, FALSE);

  if (w_current->netconn_rubberband) {
    o_move_end_rubberband(w_current, diff_x, diff_y,
                          &rubbernet_objects, &rubbernet_other_objects,
                          &rubbernet_connected_objects);
  }

  /* Unset the dont_redraw flag on rubberbanded objects.
   * We set this above, in o_move_start(). */
  st_current = toplevel->page_current->stretch_head->next;
  while (st_current != NULL) {
    st_current->object->dont_redraw = FALSE;
    st_current = st_current->next;
  }

  s_current = geda_list_get_glist( toplevel->page_current->selection_list );

  while (s_current != NULL) {

    object = s_current->data;

    if (object == NULL) {
      fprintf(stderr, _("ERROR: NULL object in o_move_end!\n"));
      exit(-1);
    }


    switch (object->type) {
      case (OBJ_COMPLEX):
      case (OBJ_PLACEHOLDER):

        if (scm_hook_empty_p(move_component_hook) == SCM_BOOL_F &&
            object != NULL) {
          scm_run_hook(move_component_hook,
                       scm_cons(g_make_attrib_smob_list(object), SCM_EOL));
        }

        /* TODO: Fix so we can just pass the complex to o_move_end_lowlevel,
         * IE.. by falling through the bottom of this case statement. */

        /* this next section of code is from */
        /* o_complex_world_translate_world */
        object->complex->x = object->complex->x + diff_x;
        object->complex->y = object->complex->y + diff_y;

        o_move_end_lowlevel(w_current, object->complex->prim_objs,
                            COMPLEX, diff_x, diff_y,
                            &other_objects, &connected_objects);


        world_get_object_list_bounds(object->complex->prim_objs, LIST_KIND_HEAD,
			       &left, &top, &right, &bottom);

        object->w_left = left;
        object->w_top = top;
        object->w_right = right;
        object->w_bottom = bottom;

        break;

      default:
        o_move_end_lowlevel(w_current, object, SINGLE, diff_x, diff_y,
                            &other_objects, &connected_objects);
        break;
    }

    s_current = g_list_next(s_current);
  }

  /* Remove the undo saved in o_move_start */
  o_undo_remove_last_undo(w_current);

  /* Draw the objects that were moved (and connected/disconnected objects) */
  o_draw_selected(w_current);
  o_cue_undraw_list(w_current, other_objects);
  o_cue_draw_list(w_current, other_objects);
  o_cue_undraw_list(w_current, connected_objects);
  o_cue_draw_list(w_current, connected_objects);

  /* Draw the connected nets/buses that were also changed */
  o_draw_list(w_current, rubbernet_objects);
  o_cue_undraw_list(w_current, rubbernet_objects);
  o_cue_draw_list(w_current, rubbernet_objects);
  o_cue_undraw_list(w_current, rubbernet_other_objects);
  o_cue_draw_list(w_current, rubbernet_other_objects);
  o_cue_undraw_list(w_current, rubbernet_connected_objects);
  o_cue_draw_list(w_current, rubbernet_connected_objects);
 
  toplevel->page_current->CHANGED = 1;
  o_undo_savestate(w_current, UNDO_ALL);

  g_list_free(other_objects);
  g_list_free(connected_objects);
  g_list_free(rubbernet_objects);
  g_list_free(rubbernet_other_objects);
  g_list_free(rubbernet_connected_objects);

  /* XXX Not s_delete_object_glist because the objects are on-page. */
  s_page_forget_place_list(toplevel->page_current, FALSE);

  s_stretch_remove_most(toplevel->page_current->stretch_head);
  toplevel->page_current->stretch_tail = toplevel->page_current->stretch_head;
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_move_cancel (GSCHEM_TOPLEVEL *w_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  STRETCH *st_current;

  /* Unset the dont_redraw flag on rubberbanded objects.
   * We set this above, in o_move_start(). */
  st_current = toplevel->page_current->stretch_head->next;
  while (st_current != NULL) {
    st_current->object->dont_redraw = FALSE;
    st_current = st_current->next;
  }
  /* XXX Not s_delete_object_glist because the objects are on-page. */
  s_page_forget_place_list(toplevel->page_current, FALSE);
  o_undo_callback(w_current, UNDO_ACTION);

  s_stretch_remove_most(toplevel->page_current->stretch_head);
  toplevel->page_current->stretch_tail = toplevel->page_current->stretch_head;
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_move_rubbermove(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y)
{
  o_move_rubbermove_xor (w_current, FALSE);
  w_current->second_wx = w_x;
  w_current->second_wy = w_y;
  o_move_rubbermove_xor (w_current, TRUE);
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_move_rubbermove_xor (GSCHEM_TOPLEVEL *w_current, int drawing)
{
  o_place_rubberplace_xor (w_current, drawing);
  if (w_current->netconn_rubberband)
    o_move_stretch_rubberband(w_current);
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
int o_move_return_whichone(OBJECT * object, int x, int y)
{
  int whichone;

  whichone = s_basic_search_grips_exact(object, x, y);
  switch (whichone) {
  case GRIP_1:
  case GRIP_2:
    return whichone;
  default:
    fprintf(stderr,
	    _("DOH! tried to find the whichone, but didn't find it!\n"));
    return GRIP_NONE;
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_move_check_endpoint(GSCHEM_TOPLEVEL *w_current, OBJECT * object)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  GList *cl_current;
  CONN *c_current;
  int whichone;

  if (!object)
  return;

  if (object->type != OBJ_NET && object->type != OBJ_PIN &&
      object->type != OBJ_BUS) {
    fprintf(stderr, _("Got a non line object in o_move_check_endpoint\n"));
    return;
  }

  for (cl_current = object->conn_list;
       cl_current != NULL;
       cl_current = g_list_next(cl_current)) {

    c_current = (CONN *) cl_current->data;

    if (c_current->other_object == NULL)
      continue;

    /* really make sure that the object is not selected */
    if (c_current->other_object->saved_color != -1 ||
        c_current->other_object->selected == TRUE)
      continue;

    if (c_current->type != CONN_ENDPOINT &&
        (c_current->type != CONN_MIDPOINT ||
         c_current->other_whichone == -1))
      continue;

    /* Only attempt to stretch nets and buses */
    if (c_current->other_object->type != OBJ_NET &&
        c_current->other_object->type != OBJ_BUS)
      continue;

    whichone = o_move_return_whichone(c_current->other_object,
                                      c_current->x,
                                      c_current->y);

    if (GEDA_DEBUG) {
      printf ("FOUND: %s type: %d, whichone: %d, x,y: %d %d\n",
	      c_current->other_object->name, c_current->type,
	      whichone, c_current->x, c_current->y);

      printf("other x,y: %d %d\n", c_current->x, c_current->y);
      printf("type: %d return: %d real: [ %d %d ]\n",
	     c_current->type, whichone, c_current->whichone,
	     c_current->other_whichone);
    }

    if (whichone == GRIP_1 || whichone == GRIP_2) {
      toplevel->page_current->stretch_tail =
        s_stretch_add(toplevel->page_current->stretch_head,
                      c_current->other_object,
                      c_current, whichone);

      o_erase_single(w_current, c_current->other_object);
    }
  }

}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_move_prep_rubberband(GSCHEM_TOPLEVEL *w_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  GList *s_current;
  OBJECT *object;
  OBJECT *o_current;

  if (GEDA_DEBUG) {
    printf("\n\n\n");
    s_stretch_print_all(toplevel->page_current->stretch_head);
    printf("\n\n\n");
  }

  s_current = geda_list_get_glist( toplevel->page_current->selection_list );
  while (s_current != NULL) {
    object = s_current->data;
    if (object) {
      switch (object->type) {
        case (OBJ_NET):
        case (OBJ_PIN):
        case (OBJ_BUS):
          o_move_check_endpoint(w_current, object);
          break;

        case (OBJ_COMPLEX):
        case (OBJ_PLACEHOLDER):
          o_current = object->complex->prim_objs;
          while (o_current != NULL) {

            if (o_current->type == OBJ_PIN) {
              o_move_check_endpoint(w_current, o_current);
            }

            o_current = o_current->next;
          }

          break;

      }
    }
    s_current = g_list_next(s_current);
  }

  if (GEDA_DEBUG) {
    printf("\n\n\n\nfinished building scretch list:\n");
    s_stretch_print_all(toplevel->page_current->stretch_head);
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
int o_move_zero_length(OBJECT * object)
{
  int x1, y1, x2, y2;

  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);

  if (GEDA_DEBUG) {
    printf("x: %d %d y: %d %d\n", x1, x2, y1, y2);
  }

  if (x1 == x2 && y1 == y2) {
    return TRUE;
  } else {
    return FALSE;
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_move_end_rubberband(GSCHEM_TOPLEVEL *w_current, int world_diff_x,
			   int world_diff_y,
			   GList** objects,
			   GList** other_objects, GList** connected_objects)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  STRETCH *s_current;
  OBJECT *object;
  int x, y;
  int whichone;

  /* skip over head */
  s_current = toplevel->page_current->stretch_head->next;

  while (s_current != NULL) {
    
    object = s_current->object;
    if (object) {
      whichone = s_current->whichone;

      switch (object->type) {
        
        case (OBJ_NET):
          
          /* save the other objects and remove object's connections */
          *other_objects =
            s_conn_return_others(*other_objects, object);
          s_conn_remove(object);

	  s_basic_get_grip(object, whichone, &x, &y);

	  if (GEDA_DEBUG) {
	    printf("OLD: %d, %d\n", x, y);
	    printf("diff: %d, %d\n", world_diff_x, world_diff_y);
	  }

          x = x + world_diff_x;
          y = y + world_diff_y;

	  if (GEDA_DEBUG) {
	    printf("NEW: %d, %d\n", x, y);
	  }

	  s_basic_move_grip(object, whichone, x, y);

          if (o_move_zero_length(object)) {
            o_delete(w_current, object);
          } else {
            o_recalc_single_object(object);
            s_conn_update_object(toplevel->page_current, object);
            *connected_objects =
              s_conn_return_others(*connected_objects, object);
            *objects = g_list_append(*objects, object);
          }

          break;

        case (OBJ_PIN):

          /* not valid to do with pins */

          break;

        case (OBJ_BUS):

          /* save the other objects and remove object's connections */
          *other_objects =
            s_conn_return_others(*other_objects, object);
          s_conn_remove(object);

	  s_basic_get_grip(object, whichone, &x, &y);

	  if (GEDA_DEBUG) {
	    printf("OLD: %d, %d\n", x, y);
	    printf("diff: %d, %d\n", world_diff_x, world_diff_y);
	  }
          x = x + world_diff_x;
          y = y + world_diff_y;

	  if (GEDA_DEBUG) {
	    printf("NEW: %d, %d\n", x, y);
	  }

	  s_basic_move_grip(object, whichone, x, y);

          if (o_move_zero_length(object)) {
            o_delete(w_current, object);
          } else {
            o_recalc_single_object(object);
            s_conn_update_object(toplevel->page_current, object);
            *connected_objects =
              s_conn_return_others(*connected_objects, object);
            *objects = g_list_append(*objects, object);
          }

          break;
      }
    }
    
    s_current = s_current->next;
  }

  if (GEDA_DEBUG) {
    /*! \bug FIXME: moved_objects doesn't exist? */
    /*printf("%d\n", g_list_length(*moved_objects));*/
    printf("%d\n", g_list_length(*other_objects));
    printf("%d\n", g_list_length(*connected_objects));
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_move_stretch_rubberband(GSCHEM_TOPLEVEL *w_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  STRETCH *s_current;
  OBJECT *object;
  int diff_x, diff_y;
  int whichone;

  diff_x = w_current->second_wx - w_current->first_wx;
  diff_y = w_current->second_wy - w_current->first_wy;

  /* skip over head */
  s_current = toplevel->page_current->stretch_head->next;
  while (s_current != NULL) {

    object = s_current->object;
    if (object) {
      whichone = s_current->whichone;
      switch (object->type) {
        case (OBJ_NET):
          o_net_draw_xor_single(w_current,
                                diff_x, diff_y, whichone, object);
          break;

        case (OBJ_BUS):
          o_bus_draw_xor_single(w_current,
                                diff_x, diff_y, whichone, object);
          break;
      }
    }
    s_current = s_current->next;
  }
}
