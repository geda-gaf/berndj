/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <math.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif

#include "gschem.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

static GdkPoint points[5000];

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void x_grid_draw(GSCHEM_TOPLEVEL *w_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  int i, j;
  int x, y;
  int x_start, y_start;
  int count = 0;

  int incr = 100;
  int screen_incr = 0;

  if (!w_current->grid) {
    i_set_grid(w_current, -1);
    return;
  }

  if (w_current->grid_mode == GRID_VARIABLE_MODE)
  {
    /* In the variable mode around every 30th screenpixel will be grid-point */
    /* adding 0.1 for correct cast*/
    incr = round_5_2_1(page->to_world_x_constant *30)+0.1;

    /*limit grid to snap_size; only a idea of mine, hope you like it (hw) */
    if (incr < toplevel->snap_size) {
      incr = toplevel->snap_size;
    }
    /* usually this should never happen */
    if (incr < 1){
      incr = 1;
    }
  }
  else
  {
    incr = toplevel->snap_size;
    screen_incr = SCREENabs(page, incr);
    if (screen_incr < w_current->grid_fixed_threshold)
    {
      /* don't draw the grid if the screen incr spacing is less than the */
      /* threshold */
      return;
    }
  }
  
  /* update status bar */
  i_set_grid(w_current, incr);

  if (GEDA_DEBUG) {
    printf("---------x_grid_draw\n incr: %d\n",incr);

    printf("x1 %d\n", pix_x(page, 100));
    printf("x2 %d\n", pix_x(page, 200));
    printf("y1 %d\n", pix_y(page, 100));
    printf("y2 %d\n", pix_y(page, 200));
  }

  gdk_gc_set_foreground(w_current->gc,
                        x_get_color(w_current->grid_color));

  /* figure starting grid coordinates, work by taking the start
   * and end coordinates and rounding down to the nearest
   * increment */
  x_start = (page->left - (page->left % incr));
  y_start = (page->top - (page->top  % incr));

  for (i = x_start; i < page->right; i = i + incr) {
    for(j = y_start; j < page->bottom; j = j + incr) {
      WORLDtoSCREEN(page, i, j, &x, &y);
      if (inside_region(page->left,
                        page->top,
                        page->right,
                        page->bottom,
                        i, j)) {

	if (w_current->grid_dot_size == 1)
        {
          points[count].x = x;
          points[count].y = y;
          count++;

          /* get out of loop if more than 1000 points */
          if (count == 5000) {
            gdk_draw_points(
                            w_current->backingstore,
                            w_current->gc, points, count);
            count=0;
          }
        }
        else
        {
          gdk_draw_arc(w_current->backingstore, w_current->gc,
                       TRUE, x, y,
                       w_current->grid_dot_size,
                       w_current->grid_dot_size, 0, FULL_CIRCLE);
        }
      }
    }
  }

  /* now draw all the points in one step */
  if(count != 0) {
    gdk_draw_points(w_current->backingstore,
                    w_current->gc, points, count);
  }
}
