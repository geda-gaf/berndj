/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#include <math.h>

#include "gschem.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

#define GET_BOX_WIDTH(w)  abs((w)->second_wx - (w)->first_wx)
#define GET_BOX_HEIGHT(w) abs((w)->second_wy - (w)->second_wy)

#define GET_PICTURE_WIDTH(w)			\
  abs((w)->second_wx - (w)->first_wx) 
#define GET_PICTURE_HEIGHT(w)						\
  (w)->pixbuf_wh_ratio == 0 ? 0 : abs((w)->second_wx - (w)->first_wx)/(w)->pixbuf_wh_ratio
#define GET_PICTURE_LEFT(w)			\
  min((w)->first_wx, (w)->second_wx)
#define GET_PICTURE_TOP(w)						\
  (w)->first_wy > (w)->second_wy ? (w)->first_wy  :			\
  (w)->first_wy+abs((w)->second_wx - (w)->first_wx)/(w)->pixbuf_wh_ratio

struct grip_foreach_context {
  int mouse_x;
  int mouse_y;
  int grip_size;
  enum grip_t *whichone;
};

static gboolean o_grip_contains(OBJECT *o, int grip_x, int grip_y, enum grip_t whichone, void *userdata);

/*! \brief Check if point is inside grip.
 *  \par Function Description
 *  This function is used to determine if the (<B>x</B>,<B>y</B>) point is
 *  inside a grip of one of the selected object on the current sheet.
 *  The selected object are in a list starting at
 *  <B>w_current->toplevel->page_current->selection2_head</B>.
 *  The <B>x</B> and <B>y</B> parameters are in world units.
 *  If the point is inside one grip, a pointer on the object it belongs to is
 *  returned and <B>*whichone</B> is set according to the position of the grip
 *  on the object.
 *  Else, <B>*whichone</B> is unchanged and the function returns <B>NULL</B>.
 *
 *  A specific search function is provided for every kind of graphical object.
 *  The list of selected object is covered : each object is tested with the
 *  appropriate function.
 *
 *  \param [in]  w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in]  x          Current x coordinate of pointer in world units.
 *  \param [in]  y          Current y coordinate of pointer in world units.
 *  \param [out] whichone   Which grip point is selected.
 *  \return Pointer to OBJECT the grip is on, NULL otherwise.
 */
OBJECT *o_grips_search_world(GSCHEM_TOPLEVEL *w_current, int x, int y, int *whichone)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  OBJECT *object=NULL;
  GList *s_current;
  enum grip_t whichone_stronglytyped = GRIP_NONE;
  struct grip_foreach_context grips = {
    .mouse_x = x,
    .mouse_y = y,
    .whichone = &whichone_stronglytyped,
  };
  int size;
  int w_size;

  if (!whichone) {
    return(NULL);
  }

  /* get the size of the grip according to zoom level */
  size = o_grips_size(w_current);
  w_size = WORLDabs(page, size);
  grips.grip_size = w_size;

  s_current = geda_list_get_glist(page->selection_list);
  while (s_current != NULL) {
    object = s_current->data;
    if (object) {
      switch(object->type) {
        case(OBJ_PICTURE):
        case(OBJ_ARC):
        case(OBJ_CIRCLE):
        case(OBJ_BOX):
	case(OBJ_PATH):
        case(OBJ_LINE):
        case(OBJ_PIN):
        case(OBJ_NET):
        case(OBJ_BUS):
          /* check the grips of objects with grip iterators. */
	  object->grip_foreach_func(object, &o_grip_contains, &grips);
	  if (whichone_stronglytyped != GRIP_NONE) {
	    *whichone = whichone_stronglytyped;
	    return object;
	  }
          break;

        default:
          break;
      }
    }
    s_current = g_list_next(s_current);
  }

  return(NULL);
}


/*! \brief Check if pointer is inside the grip region.
 *
 *  \par Function Description
 *  This function checks if the point (<B>x</B>,<B>y</B>) is
 *  inside the grip centered at (<B>grip_x</B>,<B>grip_y</B>).
 *
 *  \param [in]  x          Current x coordinate of pointer in world units.
 *  \param [in]  y          Current y coordinate of pointer in world units.
 *  \param [in]  grip_x     Current x coordinate of grip center in world units.
 *  \param [in]  grip_y     Current y coordinate of grip center in world units.
 *  \param [in]  size       Half the width of the grip square in world units.
 *  \return True / False whether the mouse pointer is inside the grip.
 */
static gboolean inside_grip( int x, int y, int grip_x, int grip_y, int size )
{
  int xmin, ymin, xmax, ymax;

  xmin = grip_x - size;
  ymin = grip_y - size;
  xmax = xmin + 2 * size;
  ymax = ymin + 2 * size;

  return inside_region(xmin, ymin, xmax, ymax, x, y);
}

static gboolean o_grip_contains(OBJECT *o, int grip_x, int grip_y, enum grip_t whichone, void *userdata)
{
  struct grip_foreach_context *ctx = (struct grip_foreach_context *) userdata;

  if (whichone == GRIP_ARC_CENTER) {
    /*
     * We ignore GRIP_ARC_CENTER which we assume is coincident with
     * GRIP_ARC_RADIUS.  This may change later; a suitable grip for
     * constant-center radius adjustment might be halfway along the
     * arc.
     */
    return FALSE;
  }

  if (inside_grip(ctx->mouse_x, ctx->mouse_y,
		  grip_x, grip_y, ctx->grip_size)) {
    *ctx->whichone = whichone;
    return TRUE;
  }

  return FALSE;
}

/*! \brief Start process of modifying one grip.
 *  \par Function Description
 *  This function starts the process of modifying one grip of an object
 *  on the current sheet. The event occurred in (<B>w_x</B>,<B>w_y</B>) in world unit.
 *  If this position is related to a grip of an object, the function
 *  prepares the modification of this grip thanks to the user input.
 *
 *  The function returns <B>FALSE</B> if an error occurred or if no grip
 *  have been found under (<B>w_x</B>,<B>w_y</B>). It returns <B>TRUE</B> if a grip
 *  has been found and modification of the object has been started.
 *
 *  If a grip has been found, this function modifies the GSCHEM_TOPLEVEL
 *  variables <B>which_grip</B> and <B>which_object</B> with the identifier
 *  of the grip and the object it belongs to respectively.
 *
 *  \param [in]  w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in]  w_x        Current x coordinate of pointer in world units.
 *  \param [in]  w_y        Current y coordinate of pointer in world units.
 *  \return FALSE if an error occurred or no grip was found, TRUE otherwise.
 */
int o_grips_start(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y)
{
  OBJECT *object;
  int whichone;

  if (w_current->draw_grips == FALSE) {
    return(FALSE);
  }

  /* search if there is a grip on a selected object at (w_x,w_y) */
  object = o_grips_search_world(w_current, w_x, w_y, &whichone);

  if (object == NULL)
    return FALSE;

  w_current->which_grip = whichone;
  w_current->which_object = object;
  w_current->subject_is_new = 0;

  /* there is one */
  /* depending on its type, start the modification process */
  switch(object->type) {
    case(OBJ_ARC):
      /* start the modification of a grip on an arc */
      o_grips_start_arc(w_current, object, w_x, w_y, whichone);
      return(TRUE);

    case(OBJ_BOX):
      /* start the modification of a grip on a box */
      o_grips_start_box(w_current, object, w_x, w_y, whichone);
      return(TRUE);

    case(OBJ_PATH):
      /* start the modification of a grip on a path */
      o_grips_start_path(w_current, object, w_x, w_y, whichone);
      return(TRUE);

    case(OBJ_PICTURE):
      /* start the modification of a grip on a picture */
      o_grips_start_picture(w_current, object, w_x, w_y, whichone);
      return(TRUE);

    case(OBJ_CIRCLE):
      /* start the modification of a grip on a circle */
      o_grips_start_circle(w_current, object, w_x, w_y, whichone);
      return(TRUE);

    case(OBJ_LINE):
    case(OBJ_NET):
    case(OBJ_PIN):
    case(OBJ_BUS):
      /* identical for line/net/pin/bus */
      /* start the modification of a grip on a line */
      o_grips_start_line(w_current, object, w_x, w_y, whichone);
      return(TRUE);

    default:
      /* object type unknown : error condition */
      return(FALSE);
  }
  return(FALSE);
}

/*! \brief Initialize grip motion process for an arc.
 *  \par Function Description
 *  This function initializes the grip motion process for an arc.
 *  From the <B>o_current</B> pointed object, it stores into the
 *  GSCHEM_TOPLEVEL structure the coordinates of the center, the radius
 *  and the two angle that describes an arc. These variables are used in
 *  the grip process.
 *
 *  The coordinates of the center of the arc on x- and y-axis are stored
 *  into the <B>first_wx</B> and <B>first_wy</B> fields of the GSCHEM_TOPLEVEL
 *  structure in screen units.
 *
 *  The radius of the center is stored into the <B>distance</B> field of
 *  the GSCHEM_TOPLEVEL structure in screen units.
 *
 *  The two angles describing the arc on a circle are stored into the
 *  <B>second_wx</B> for the starting angle and <B>second_wy</B> for the ending angle.
 *  These angles are expressed in degrees.
 *
 *  \param [in]  w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in]  o_current  Arc OBJECT to check.
 *  \param [in]  x          (unused)
 *  \param [in]  y          (unused)
 *  \param [out] whichone   (unused)
 */
void o_grips_start_arc(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current,
                       int x, int y, int whichone)
{
  w_current->last_drawb_mode = LAST_DRAWB_MODE_NONE;

  /* TODO: Hoist this out of the per-object functions. */
  w_current->which_grip = whichone;

  /* erase the arc before */
  o_erase_single(w_current, o_current);

  s_basic_move_grip(o_current, whichone, x, y);

  /* draw the first temporary arc */
  o_draw_rubbersubject_xor(w_current);
  w_current->rubber_visible = 1;
}

/*! \brief Initialize grip motion process for a box.
 *  \par Function Description
 *  This function initializes the grip motion process for a box. From the
 *  <B>o_current</B> pointed object, it stores into the GSCHEM_TOPLEVEL
 *  structure the .... These variables are used in the grip process.
 *
 *  The function first erases the grips.
 *
 *  The coordinates of the selected corner are put in
 *  (<B>w_current->second_wx</B>,<B>w_current->second_wx</B>).
 *
 *  The coordinates of the opposite corner go in
 *  (<B>w_current->first_wx</B>,<B>w_current->first_wy</B>). They are not suppose
 *  to change during the action.
 *
 *  \param [in]  w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in]  o_current  Box OBJECT to check.
 *  \param [in]  x          (unused)
 *  \param [in]  y          (unused)
 *  \param [out] whichone   Which coordinate to check.
 */
void o_grips_start_box(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current,
                       int x, int y, int whichone)
{
  w_current->last_drawb_mode = LAST_DRAWB_MODE_NONE;

  /* TODO: Hoist this out of the per-object functions. */
  w_current->which_grip = whichone;

  /* erase the box before */
  o_erase_single(w_current, o_current);

  s_basic_move_grip(o_current, whichone, x, y);

  /* draw the first temporary box */
  o_draw_rubbersubject_xor(w_current);
  w_current->rubber_visible = 1;
}

/*! \brief Initialize grip motion process for a path.
 *  \par Function Description
 *  This function initializes the grip motion process for a path.
 *  From the <B>o_current</B> pointed object, it stores into the
 *  GSCHEM_TOPLEVEL structure the ....
 *  These variables are used in the grip process.
 *
 *  The function first erases the grips.
 *
 *  The coordinates of the selected corner are put in
 *  (<B>w_current->second_wx</B>,<B>w_current->second_wy</B>).
 *
 *  The coordinates of the opposite corner go in
 *  (<B>w_current->first_wx</B>,<B>w_current->first_wy</B>). They are not
 *  suppose to change during the action.
 *
 *  \param [in]  w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in]  o_current  Picture OBJECT to check.
 *  \param [in]  x          (unused)
 *  \param [in]  y          (unused)
 *  \param [out] whichone   Which coordinate to check.
 */
void o_grips_start_path(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current,
			int x, int y, int whichone)
{
  w_current->last_drawb_mode = LAST_DRAWB_MODE_NONE;

  /* TODO: Hoist this out of the per-object functions. */
  w_current->which_grip = whichone;

  /* erase the path before */
  o_erase_single(w_current, o_current);

  s_basic_move_grip(o_current, whichone, x, y);

  /* draw the first temporary path */
  o_draw_rubbersubject_xor(w_current);
  w_current->rubber_visible = 1;
}

/*! \brief Initialize grip motion process for a picture.
 *  \par Function Description
 *  This function initializes the grip motion process for a picture.
 *  From the <B>o_current</B> pointed object, it stores into the
 *  GSCHEM_TOPLEVEL structure the ....
 *  These variables are used in the grip process.
 *
 *  The function first erases the grips.
 *
 *  The coordinates of the selected corner are put in
 *  (<B>w_current->second_wx</B>,<B>w_current->second_wy</B>).
 *
 *  The coordinates of the opposite corner go in
 *  (<B>w_current->first_wx</B>,<B>w_current->first_wy</B>). They are not
 *  suppose to change during the action.
 *
 *  \param [in]  w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in]  o_current  Picture OBJECT to check.
 *  \param [in]  x          (unused)
 *  \param [in]  y          (unused)
 *  \param [out] whichone   Which coordinate to check.
 */
void o_grips_start_picture(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current,
                           int x, int y, int whichone)
{
  enum grip_t corner, opposite;

  w_current->last_drawb_mode = LAST_DRAWB_MODE_NONE;

  /* erase the picture before */
  o_erase_single(w_current, o_current);
  w_current->current_pixbuf = o_picture_original_pixbuf(o_current);
  w_current->pixbuf_filename = o_picture_filename(o_current);
  w_current->pixbuf_wh_ratio = o_picture_ratio(o_current);

  /* (second_wx,second_wy) is the selected corner */
  /* (first_wx, first_wy) is the opposite corner */
  switch(whichone) {
  case GRIP_UPPER_LEFT:
    corner = GRIP_UPPER_LEFT;
    opposite = GRIP_LOWER_RIGHT;
    break;
  case GRIP_LOWER_RIGHT:
    corner = GRIP_LOWER_RIGHT;
    opposite = GRIP_UPPER_LEFT;
    break;
  case GRIP_UPPER_RIGHT:
    corner = GRIP_UPPER_RIGHT;
    opposite = GRIP_LOWER_LEFT;
    break;
  case GRIP_LOWER_LEFT:
    corner = GRIP_LOWER_LEFT;
    opposite = GRIP_UPPER_RIGHT;
    break;
  default:
    return; /* error */
  }

  s_basic_get_grip(o_current, corner, &w_current->second_wx, &w_current->second_wy);
  s_basic_get_grip(o_current, opposite, &w_current->first_wx, &w_current->first_wy);

  /* draw the first temporary picture */
  o_picture_rubberbox_xor(w_current);
  w_current->rubber_visible = 1;
}

/*! \brief Initialize grip motion process for a circle.
 *  \par Function Description
 *  This function initializes the grip motion process for a circle.
 *  From the <B>o_current</B> pointed object, it stores into the
 *  GSCHEM_TOPLEVEL structure the coordinate of the center and the radius.
 *  These variables are used in the grip process.
 *
 *  The function first erases the grips.
 *
 *  The coordinates of the center are put in
 *  (<B>w_current->first_wx</B>,<B>w_current->first_wy</B>). They are not suppose
 *  to change during the action.
 *
 *  The radius of the circle is stored in <B>w_current->distance<B>.
 *
 *  \param [in]  w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in]  o_current  Circle OBJECT to check.
 *  \param [in]  x          (unused)
 *  \param [in]  y          (unused)
 *  \param [out] whichone   Which coordinate to check.
 */
void o_grips_start_circle(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current,
                          int x, int y, int whichone)
{
  w_current->last_drawb_mode = LAST_DRAWB_MODE_NONE;

  /* TODO: Hoist this out of the per-object functions. */
  w_current->which_grip = whichone;

  /* erase the circle before */
  o_erase_single(w_current, o_current);

  s_basic_move_grip(o_current, whichone, x, y);

  /* draw the first temporary circle */
  o_draw_rubbersubject_xor(w_current);
  w_current->rubber_visible = 1;
}

/*! \brief Initialize grip motion process for a line.
 *  This function starts the move of one of the two grips of the line
 *  object <B>o_current</B>.
 *  The line and its grips are first erased. The move of the grips is
 *  materialized with a temporary line in selection color drawn over the
 *  sheet with an xor-function.
 *
 *  During the move of the grip, the line is described by
 *  (<B>w_current->first_wx</B>,<B>w_current->first_wy</B>) and
 *  (<B>w_current->second_wx</B>,<B>w_current->second_wy</B>).
 *
 *  The line end that corresponds to the moving grip is in
 *  (<B>w_current->second_wx</B>,<B>w_current->second_wy</B>).
 *
 *  \param [in]  w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in]  o_current  Line OBJECT to check.
 *  \param [in]  x          (unused)
 *  \param [in]  y          (unused)
 *  \param [out] whichone   Which coordinate to check.
 */
void o_grips_start_line(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current,
                        int x, int y, int whichone)
{
  w_current->last_drawb_mode = LAST_DRAWB_MODE_NONE;

  /* TODO: Hoist this out of the per-object functions. */
  w_current->which_grip = whichone;

  /* erase the line before */
  o_erase_single(w_current, o_current);

  s_basic_move_grip(o_current, whichone, x, y);

  /* draw the first temporary line */
  o_draw_rubbersubject_xor(w_current);
  w_current->rubber_visible = 1;
}

/*! \brief Modify previously selected object according to mouse position.
 *  \par Function Description
 *  This function modify the previously selected
 *  object according to the mouse position in <B>w_x</B> and <B>w_y</B>.
 *  The grip under modification is updated and the temporary object displayed.
 *
 *  The object under modification is <B>w_current->which_object</B> and
 *  the grip concerned is <B>w_current->which_grip</B>.
 *
 *  Depending on the object type, a specific function is used.
 *  It erases the temporary object, updates its internal representation,
 *  and draws it again.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] w_x        Current x coordinate of pointer in world units.
 *  \param [in] w_y        Current y coordinate of pointer in world units.
 */
void o_grips_motion(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y)
{
  int grip = w_current->which_grip;

  g_assert( w_current->inside_action != 0 );
  g_return_if_fail( w_current->which_object != NULL );

  switch(w_current->which_object->type) {
    case(OBJ_ARC):
    /* erase, update and draw an arc */
    o_grips_motion_arc (w_current, w_x, w_y, grip);
    break;

    case(OBJ_BOX):
    /* erase, update and draw a box */
    o_grips_motion_box (w_current, w_x, w_y, grip);
    break;

    case(OBJ_PATH):
    /* erase, update and draw a box */
    o_grips_motion_path(w_current, w_x, w_y, grip);
    break;

    case(OBJ_PICTURE):
    /* erase, update and draw a box */
    o_grips_motion_picture (w_current, w_x, w_y, grip);
    break;

    case(OBJ_CIRCLE):
    /* erase, update and draw a circle */
    o_grips_motion_circle(w_current, w_x, w_y, grip);
    break;

    case(OBJ_LINE):
    case(OBJ_NET):
    case(OBJ_PIN):
    case(OBJ_BUS):
    /* erase, update and draw a line */
    /* same for net, pin and bus as they share the same internal rep. */
    o_grips_motion_line(w_current, w_x, w_y, grip);
    break;

    default:
    return; /* error condition */
  }
}

/*! \brief Modify previously selected arc according to mouse position.
 *  \par Function Description
 *  This function is the refreshing part of the grip motion process.
 *  It is called whenever the position of the pointer is changed,
 *  therefore requiring the GSCHEM_TOPLEVEL variables to be updated.
 *  Depending on the grip selected and moved, the temporary GSCHEM_TOPLEVEL
 *  variables are changed according to the current position of the pointer.
 *
 *  If the grip at the center of the arc has been moved - modifying the
 *  radius of the arc -, the <B>w_current->distance</B> field is updated.
 *  To increase the radius of the arc, the user must drag the grip to the
 *  right of the center. To decrease the radius of the arc, the user must
 *  drag the grip to the left of the center. Negative radius can not be
 *  obtained.
 *
 *  If one of the end of arc grip has been moved - modifying the arc
 *  describing the arc -, the <B>w_current->second_wx</B> or
 *  <B>w_current->second_wy</B> are updated according to which of the grip
 *  has been selected.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] w_x        Current x coordinate of pointer in world units.
 *  \param [in] w_y        Current y coordinate of pointer in world units.
 *  \param [in] whichone   Which grip to start motion with.
 */
void o_grips_motion_arc(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y, int whichone)
{
  o_arc_rubberarc(w_current, w_x, w_y, whichone);
}

/*! \brief Modify previously selected box according to mouse position.
 *  \par Function Description
 *  This function is the refreshing part of the grip motion process. It is
 *  called whenever the position of the pointer is changed, therefore
 *  requiring the GSCHEM_TOPLEVEL variables to be updated.
 *  Depending on the grip selected and moved, the temporary GSCHEM_TOPLEVEL
 *  variables are changed according to the current position of the pointer
 *  and the modifications temporary drawn.
 *
 *  This function only makes a call to #o_box_rubberbox() that updates
 *  the GSCHEM_TOPLEVEL variables, erase the previous temporary box and draw
 *  the new temporary box.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] w_x        Current x coordinate of pointer in world units.
 *  \param [in] w_y        Current y coordinate of pointer in world units.
 *  \param [in] whichone   Which grip to start motion with.
 */
void o_grips_motion_box(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y, int whichone)
{
  /* erase, update and draw the temporary box */
  o_box_rubberbox(w_current, w_x, w_y);
}

/*! \brief Modify previously selected path according to mouse position.
 *  \par Function Description
 *  This function is the refreshing part of the grip motion process. It is
 *  called whenever the position of the pointer is changed, therefore
 *  requiring the GSCHEM_TOPLEVEL variables to be updated.
 *  Depending on the grip selected and moved, the temporary GSCHEM_TOPLEVEL
 *  variables are changed according to the current position of the pointer
 *  and the modifications temporary drawn.
 *
 *  This function only makes a call to #o_path_rubberbox() that
 *  updates the GSCHEM_TOPLEVEL variables, erase the previous temporary
 *   path and draw the new temporary path.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] w_x        Current x coordinate of pointer in world units.
 *  \param [in] w_y        Current y coordinate of pointer in world units.
 *  \param [in] whichone   Which grip to start motion with.
 */
void o_grips_motion_path (GSCHEM_TOPLEVEL *w_current,
                          int w_x, int w_y, int whichone)
{
  /* erase, update and draw the temporary path */
  o_path_rubberpath(w_current, w_x, w_y);
}

/*! \brief Modify previously selected picture according to mouse position.
 *  \par Function Description
 *  This function is the refreshing part of the grip motion process. It is
 *  called whenever the position of the pointer is changed, therefore
 *  requiring the GSCHEM_TOPLEVEL variables to be updated.
 *  Depending on the grip selected and moved, the temporary GSCHEM_TOPLEVEL
 *  variables are changed according to the current position of the pointer
 *  and the modifications temporary drawn.
 *
 *  This function only makes a call to #o_picture_rubberbox() that
 *  updates the GSCHEM_TOPLEVEL variables, erase the previous temporary
 *   picture and draw the new temporary picture.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] w_x        Current x coordinate of pointer in world units.
 *  \param [in] w_y        Current y coordinate of pointer in world units.
 *  \param [in] whichone   Which grip to start motion with.
 */
void o_grips_motion_picture(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y, int whichone)
{
  /* erase, update and draw the temporary picture */
  o_picture_rubberbox(w_current, w_x, w_y);
}

/*! \brief Modify previously selected circle according to mouse position.
 *  \par Function Description
 *  This function is the refreshing part of the grip motion process. It is
 *  called whenever the position of the pointer is changed, therefore
 *  requiring the GSCHEM_TOPLEVEL variables to be updated.
 *  Depending on the grip selected and moved, the temporary GSCHEM_TOPLEVEL
 *  variables are changed according to the current position of the pointer
 *  and the modifications temporary drawn.
 *
 *  This function only makes a call to #o_circle_rubbercircle() that updates
 *  the GSCHEM_TOPLEVEL variables, erase the previous temporary circle and
 *  draw the new temporary circle.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] w_x        Current x coordinate of pointer in world units.
 *  \param [in] w_y        Current y coordinate of pointer in world units.
 *  \param [in] whichone   Which grip to start motion with.
 */
void o_grips_motion_circle(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y, int whichone)
{
  /* erase, update and draw the temporary circle */
  o_circle_rubbercircle(w_current, w_x, w_y);
}

/*! \brief Modify previously selected line according to mouse position.
 *  \par Function Description
 *  This function is called during the move of the grip to update the
 *  temporary line drawn under the mouse pointer.
 *  The current position of the mouse is in <B>w_x</B> and <B>w_y</B> in world coords.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] w_x        Current x coordinate of pointer in world units.
 *  \param [in] w_y        Current y coordinate of pointer in world units.
 *  \param [in] whichone   Which grip to start motion with.
 */
void o_grips_motion_line(GSCHEM_TOPLEVEL *w_current, int w_x, int w_y, int whichone)
{
  /* erase, update and draw the temporary line */
  o_line_rubberline(w_current, w_x, w_y);
}

/*! \brief End process of modifying object with grip.
 *  \par Function Description
 *  This function ends the process of modifying a parameter of an object
 *  with a grip.
 *  The temporary representation of the object is erased, the object is
 *  modified and finally drawn.
 *
 *  The object under modification is <B>w_current->which_object</B> and
 *  the grip concerned is <B>w_current->which_grip</B>.
 *
 *  Depending on the object type, a specific function is used. It erases
 *  the temporary object, updates the object and draws the modified object
 *  normally.
 *
 *  \param [in,out] w_current  The GSCHEM_TOPLEVEL object.
 */
void o_grips_end(GSCHEM_TOPLEVEL *w_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  OBJECT *object;
  int grip;

  object = w_current->which_object;
  grip = w_current->which_grip;

  if (!object) {
    /* actually this is an error condition hack */
    w_current->inside_action = 0;
    i_set_state(w_current, SELECT);
    return;
  }

  switch(object->type) {

    case(OBJ_ARC):
    /* modify an arc object */
    o_grips_end_arc(w_current, object, grip);
    break;

    case(OBJ_BOX):
    /* modify a box object */
    o_grips_end_box(w_current, object, grip);
    break;

    case(OBJ_PATH):
    /* modify a path object */
    o_grips_end_path(w_current, object, grip);
    break;

    case(OBJ_PICTURE):
    /* modify a picture object */
    o_grips_end_picture(w_current, object, grip);
    break;

    case(OBJ_CIRCLE):
    /* modify a circle object */
    o_grips_end_circle(w_current, object, grip);
    break;

    case(OBJ_LINE):
    /* modify a line object */
    o_grips_end_line(w_current, object, grip);
    break;

    case(OBJ_NET):
      /* modify a net object */
      o_grips_end_net(w_current, object, grip);
      break;

    case(OBJ_PIN):
      /* modify a pin object */
      o_grips_end_pin(w_current, object, grip);
      break;

    case(OBJ_BUS):
      /* modify a bus object */
      o_grips_end_bus(w_current, object, grip);
      break;

    default:
    return;
  }

  /* reset global variables */
  w_current->which_grip = GRIP_NONE;
  w_current->which_object = NULL;

  w_current->rubber_visible = 0;

  toplevel->page_current->CHANGED=1;
  o_undo_savestate(w_current, UNDO_ALL);
}

/*! \brief End process of modifying arc object with grip.
 *  \par Function Description
 *  This function ends the grips process specific to an arc object. It erases
 *  the old arc and write back to the object the new parameters of the arc.
 *  Depending on the grip selected and moved, the right fields are updated.
 *  The function handles the conversion from screen unit to world unit before
 *  updating and redrawing.
 *
 *  If the grip at the center of the arc has been moved - modifying the radius
 *  of the arc -, the new radius is calculated expressed in world unit
 *  (the center is unchanged). It is updated with the function #o_arc_modify().
 *
 *  If one of the end of arc grip has been moved - modifying one of the
 *  angles describing the arc -, this angle is updated with the
 *  #o_arc_modify() function.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] o_current  Arc OBJECT to end modification on.
 *  \param [in] whichone   Which grip is pointed to.
 */
void o_grips_end_arc(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current, int whichone)
{
  /* erase the temporary arc */
  o_draw_rubbersubject_xor(w_current);
  w_current->rubber_visible = 0;

  /* display the new arc */
  o_redraw_single(w_current, o_current);
}

/*! \todo Finish function documentation!!!
 *  \brief End process of modifying box object with grip.
 *  \par Function Description
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] o_current  Box OBJECT to end modification on.
 *  \param [in] whichone   Which grip is pointed to.
 */
void o_grips_end_box(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current, int whichone)
{
  int box_width, box_height;

  box_width  = GET_BOX_WIDTH (w_current);
  box_height = GET_BOX_HEIGHT(w_current);

  /* don't allow zero width/height boxes
   * this ends the box drawing behavior
   * we want this? hack */
  if ((box_width == 0) && (box_height == 0)) {
    o_redraw_single(w_current, o_current);
    return;
  }

  /* erase the temporary box */
  o_draw_rubbersubject_xor(w_current);

  /* draw the modified box */
  o_redraw_single(w_current, o_current);
}

/*! \todo Finish function documentation!!!
 *  \brief End process of modifying path object with grip.
 *  \par Function Description
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] o_current  Picture OBJECT to end modification on.
 *  \param [in] whichone   Which grip is pointed to.
 */
void o_grips_end_path(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current, int whichone)
{
  /* erase the temporary path */
  o_draw_rubbersubject_xor(w_current);

  /* draw the modified path */
  o_redraw_single(w_current, o_current);
}

/*! \todo Finish function documentation!!!
 *  \brief End process of modifying picture object with grip.
 *  \par Function Description
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] o_current  Picture OBJECT to end modification on.
 *  \param [in] whichone   Which grip is pointed to.
 */
void o_grips_end_picture(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current, int whichone)
{
  /* erase the temporary picture */
  o_picture_rubberbox_xor(w_current);

  /* don't allow zero width/height pictures
   * this ends the picture drawing behavior
   * we want this? hack */
  if ((GET_PICTURE_WIDTH(w_current) == 0) || (GET_PICTURE_HEIGHT(w_current) == 0)) {
    o_redraw_single(w_current, o_current);
    return;
  }

  o_picture_modify(o_current,
		   w_current->second_wx, w_current->second_wy, whichone);

  /* draw the modified picture */
  o_redraw_single(w_current, o_current);

  w_current->current_pixbuf = NULL;
  w_current->pixbuf_filename = NULL;
  w_current->pixbuf_wh_ratio = 0;
}

/*! \brief End process of modifying circle object with grip.
 *  \par Function Description
 *  This function ends the process of modifying the radius of the circle
 *  object <B>*o_current</B>.
 *  The modified circle is finally normally drawn.
 *
 *  A circle with a null radius is not allowed. In this case, the process
 *  is stopped and the circle is left unchanged.
 *
 *  The last value of the radius is in <B>w_current->distance</B> in screen units.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] o_current  Circle OBJECT to end modification on.
 *  \param [in] whichone   Which grip is pointed to.
 */
void o_grips_end_circle(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current, int whichone)
{
  /* erase the temporary circle */
  o_draw_rubbersubject_xor(w_current);

  /* display the new circle */
  o_redraw_single(w_current, o_current);
}

/*! \brief End process of modifying line object with grip.
 *  \par Function Description
 *  This function ends the process of modifying one end of the line
 *  object <B>*o_current</B>.
 *  This end is identified by <B>whichone</B>. The line object is modified
 *  according to the <B>whichone</B> parameter and the last position of the
 *  line end.
 *  The modified line is finally normally drawn.
 *
 *  A line with a null width, i.e. when both ends are identical, is not
 *  allowed. In this case, the process is stopped and the line unchanged.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] o_current  Line OBJECT to end modification on.
 *  \param [in] whichone   Which grip is pointed to.
 */
void o_grips_end_line(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current, int whichone)
{
  /* erase the temporary line */
  o_draw_rubbersubject_xor(w_current);

  /* display the new line */
  o_redraw_single(w_current, o_current);
}


/*! \brief End process of modifying net object with grip.
 *  \par Function Description
 *  This function ends the process of modifying one end of the net
 *  object <B>*o_current</B>.
 *  This end is identified by <B>whichone</B>. The line object is modified
 *  according to the <B>whichone</B> parameter and the last position of the
 *  line end.
 *  The connections to the modified net are checked and recreated if necessary.
 *
 *  A net with zero length, i.e. when both ends are identical, is not
 *  allowed. In this case, the process is stopped and the line unchanged.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] o_current  Net OBJECT to end modification on.
 *  \param [in] whichone   Which grip is pointed to.
 */
void o_grips_end_net(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current, int whichone)
{
  GList *other_objects = NULL;
  GList *connected_objects = NULL;

  /* erase the temporary line */
  o_draw_rubbersubject_xor(w_current);

  /* don't allow zero length net
   * this ends the net drawing behavior
   * we want this? hack */
  if (!o_line_length(o_current)) {
    o_redraw_single(w_current, o_current);
    return;
  }

  /* remove the old net */
  o_cue_undraw(w_current, o_current);

  other_objects = s_conn_return_others(other_objects, o_current);

  /* get the other connected objects and redraw them */
  connected_objects = s_conn_return_others(connected_objects,
					   o_current);
  /* add bus rippers if necessary */
  if (o_net_add_busrippers(w_current, o_current, connected_objects)) {
    o_erase_single(w_current, o_current);
    o_cue_undraw(w_current, o_current);

    o_net_draw(w_current, o_current);
    o_cue_draw_single(w_current, o_current);
  }

  /* draw the object objects */
  o_cue_undraw_list(w_current, other_objects);
  o_cue_draw_list(w_current, other_objects);
  
  o_redraw_single(w_current, o_current);
  
  g_list_free(connected_objects);
  connected_objects = NULL;
  
  /* get the other connected objects and redraw them */
  connected_objects = s_conn_return_others(connected_objects,
					   o_current);
  
  o_cue_undraw_list(w_current, connected_objects);
  o_cue_draw_list(w_current, connected_objects);
  /* finally draw this objects cues */
  o_cue_draw_single(w_current, o_current);

  g_list_free(other_objects);
  other_objects = NULL;
  g_list_free(connected_objects);
  connected_objects = NULL;
}

/*! \brief End process of modifying pin object with grip.
 *  \par Function Description
 *  This function ends the process of modifying one end of the pin
 *  object <B>*o_current</B>.
 *  This end is identified by <B>whichone</B>. The pin object is modified
 *  according to the <B>whichone</B> parameter and the last position of the
 *  pin end.
 *  The connections to the modified pin are checked and recreated if necessary.
 *
 *  A pin with zero length, i.e. when both ends are identical, is not
 *  allowed. In this case, the process is stopped and the line unchanged.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] o_current  Net OBJECT to end modification on.
 *  \param [in] whichone   Which grip is pointed to.
 */
void o_grips_end_pin(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current, int whichone)
{
  GList *other_objects = NULL;
  GList *connected_objects = NULL;

  /* erase the temporary line */
  o_draw_rubbersubject_xor(w_current);

  /* don't allow zero length pin
   * this ends the pin changing behavior
   * we want this? hack */
  if (!o_line_length(o_current)) {
    o_redraw_single(w_current, o_current);
    return;
  }

  /* erase old pin object */
  o_cue_undraw(w_current, o_current);

  other_objects = s_conn_return_others(other_objects, o_current);

  /* redraw the object connections */
  o_cue_undraw_list(w_current, other_objects);
  o_cue_draw_list(w_current, other_objects);

  /* get the other connected objects and redraw them */
  connected_objects = s_conn_return_others(connected_objects,
					   o_current);
  o_cue_undraw_list(w_current, connected_objects);
  o_cue_draw_list(w_current, connected_objects);

  /* finally draw this objects cues */
  o_cue_draw_single(w_current, o_current);

  /* free the two lists */
  g_list_free(other_objects);
  other_objects = NULL;
  g_list_free(connected_objects);
  connected_objects = NULL;
}

/*! \brief End process of modifying bus object with grip.
 *  \par Function Description
 *  This function ends the process of modifying one end of the bus
 *  object <B>*o_current</B>.
 *  This end is identified by <B>whichone</B>. The line object is modified
 *  according to the <B>whichone</B> parameter and the last position of the
 *  bus end.
 *  The connections to the modified bus are checked and recreated if necessary.
 *
 *  A bus with zero length, i.e. when both ends are identical, is not
 *  allowed. In this case, the process is stopped and the bus unchanged.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] o_current  bus OBJECT to end modification on.
 *  \param [in] whichone   Which grip is pointed to.
 */
void o_grips_end_bus(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current, int whichone)
{
  GList *other_objects = NULL;
  GList *connected_objects = NULL;

  /* erase the temporary line */
  o_draw_rubbersubject_xor(w_current);

  /* don't allow zero length bus
   * this ends the bus changing behavior
   * we want this? hack */
  if (!o_line_length(o_current)) {
    o_redraw_single(w_current, o_current);
    return;
  }

  /* erase the old bus and it's cues */
  o_cue_undraw(w_current, o_current);

  other_objects = s_conn_return_others(other_objects, o_current);

  /* redraw the connected objects */
  o_cue_undraw_list(w_current, other_objects);
  o_cue_draw_list(w_current, other_objects);

  /* get the other connected objects and redraw them */
  connected_objects = s_conn_return_others(connected_objects,
					   o_current);
  o_cue_undraw_list(w_current, connected_objects);
  o_cue_draw_list(w_current, connected_objects);

  /* finally draw this objects cues */
  o_cue_draw_single(w_current, o_current);

  /* free the two lists */
  g_list_free(other_objects);
  other_objects = NULL;
  g_list_free(connected_objects);
  connected_objects = NULL;
}


/*! \brief Get half the width and height of grip in screen units.
 *  \par Function Description
 *  According to the current zoom level, the function returns half the width
 *  and height of a grip in screen units.
 *
 *  <B>GRIP_SIZE1</B> and <B>GRIP_SIZE2</B> and <B>GRIP_SIZE3</B> are macros defined
 *  in libgeda #defines.h. They are the half width/height of a grip in
 *  world unit for a determined range of zoom factors.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \return Half grip size in screen units.
 */
int o_grips_size(GSCHEM_TOPLEVEL *w_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  int factor, size;
  
  factor = (int) page->to_world_x_constant;
  if (factor > SMALL_ZOOMFACTOR1) {
    /* big zoom factor : small size converted to screen unit */
    size = SCREENabs(page, GRIP_SIZE1);
  } else if (factor > SMALL_ZOOMFACTOR2) {
    /* medium zoom factor : medium size converted to screen unit */
    size = SCREENabs(page, GRIP_SIZE2);
  } else {
    /* small zoom factor : big size converted to screen unit */
    size = SCREENabs(page, GRIP_SIZE3);
  }
  
  return size;
}

/*! \brief Draw grip centered at <B>grip_x</B>, <B>grip_y</B>
 *  \par Function Description
 *  This function draws a grip centered at (<B>grip_x</B>,<B>grip_y</B>).
 *  Its color is either the selection color or the overriding color from
 *  <B>toplevel->override_color</B>.
 *
 *  The size of the grip depends on the current zoom factor.
 *
 *  <B>x</B> and <B>y</B> are in screen unit.
 *
 *  \param [in] o          The OBJECT to which this grip belongs.
 *  \param [in] grip_x     Center x screen coordinate for drawing grip.
 *  \param [in] grip_y     Center y screen coordinate for drawing grip.
 *  \param [in] whichone   Which grip to draw.
 *  \param [in] userdata   The hidden GSCHEM_TOPLEVEL object.
 */
static gboolean o_grips_draw_single_one(OBJECT *o,
					int grip_x, int grip_y,
					enum grip_t whichone,
					void *userdata)
{
  GSCHEM_TOPLEVEL *w_current = userdata;
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  GdkColor *color;
  int size, x2size;
  int x, y;

  /* Ignore GRIP_ARC_CENTER.  See note in o_grip_contains(). */
  if (whichone == GRIP_ARC_CENTER) {
    return FALSE;
  }

  WORLDtoSCREEN(page, grip_x, grip_y, &x, &y);

  /*
   * Depending on the current zoom level, the size of the grip is
   * determined. <B>size</B> is half the width and height of the grip
   * and <B>x2size</B> is the full width and height of the grip.
   */
  /* size is half the width of grip */
  size = o_grips_size(w_current);
  /* x2size is full width */
  x2size = 2 * size;

  /*
   * The grip can be displayed or erased : if <B>toplevel->override_color</B>
   * is not set the grip is drawn with the selection color ; if
   * <B>toplevel->override_color</B> is set then the color it refers it
   * is used. This way the grip can be erased if this color is the
   * background color.
   */
  if (toplevel->override_color != -1 ) {
    /* override : use the override_color instead */
    color = x_get_color(toplevel->override_color);
  } else {
    /* use the normal selection color */
    color = x_get_color(w_current->select_color);
  }
  /* set the color for the grip */
  gdk_gc_set_foreground(w_current->gc, color);

  /* set the line options for grip : solid, 1 pix wide */
  gdk_gc_set_line_attributes(w_current->gc, 0, GDK_LINE_SOLID,
                             GDK_CAP_BUTT, GDK_JOIN_MITER);

  /*
   * A grip is a hollow square centered at (<B>x</B>,<B>y</B>)
   * with a width/height of <B>x2size</B>.
   */
  if (w_current->DONT_REDRAW == 0) {
    /* draw the grip in backingstore */
    gdk_draw_rectangle(w_current->backingstore, w_current->gc, FALSE,
                       x - size, y - size, x2size, x2size);
    o_invalidate_rect(w_current, x - size, y - size, x + size, y + size);
  }

  return FALSE;
}

/*! \brief Draw a single object's grips.
 *  \par Function Description
 *  Traverses all grips that the object exposes, drawing a grip at each point.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] o_current  The object whose grips we want to draw.
 */
static void o_grips_draw_single(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current)
{
  (*o_current->grip_foreach_func)(o_current, &o_grips_draw_single_one, w_current);
}

/*! \brief Erase a single object's grips.
 *  \par Function Description
 *  Traverses all grips that the object exposes, erasing a grip at each point.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] o_current  The object whose grips we want to draw.
 */
void o_grips_erase_single(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  /* set overriding color */
  toplevel->override_color = toplevel->background_color;
  /* draw grips with backgound color : erase grip */
  o_grips_draw_single(w_current, o_current);
  /* return to default */
  toplevel->override_color = -1;
}

/*! \brief Draw or erase a single object's grips.
 *  \par Function Description
 *  Traverses all grips that the object exposes, updating the canvas at each.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] o_current  The object whose grips we want to draw.
 */
void o_grips_redraw_single(GSCHEM_TOPLEVEL *w_current, OBJECT *o_current)
{
  if ((o_current->draw_grips == TRUE) && (w_current->draw_grips == TRUE)) {
    if (!o_current->selected) {
      /* object is no more selected, erase the grips */
      o_current->draw_grips = FALSE;
      if (w_current->DONT_REDRAW == 0) {
	o_grips_erase_single(w_current, o_current);
      }
    } else {
      /* object is selected, draw the grips on the picture */
      if (w_current->DONT_REDRAW == 0) {
	o_grips_draw_single(w_current, o_current);
      }
    }
  }
}
