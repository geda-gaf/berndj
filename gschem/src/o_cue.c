/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#include <string.h>

#include "gschem.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

struct redraw_context {
  GSCHEM_TOPLEVEL *w_current;
  gboolean draw_selected;
};

static enum visit_result
cue_redraw_one(OBJECT *o_current, void *userdata)
{
  struct redraw_context *ctx = userdata;
  GSCHEM_TOPLEVEL *w_current = ctx->w_current;
  gboolean draw_selected = ctx->draw_selected;

  switch (o_current->type) {
  case OBJ_BUS:
  case OBJ_NET:
  case OBJ_PIN:
    if (o_current->dont_redraw ||
	(o_current->selected && !draw_selected)) {
    } else {
      o_cue_draw_single(w_current, o_current);
    }
    return VISIT_RES_NORECURSE;
  case OBJ_COMPLEX:
  case OBJ_PLACEHOLDER:
    return VISIT_RES_OK;
  default:
    return VISIT_RES_NORECURSE;
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_cue_redraw_all(GSCHEM_TOPLEVEL *w_current, OBJECT *head,
		      enum list_kind kind, gboolean draw_selected)
{
  struct redraw_context ctx = {
    .w_current = w_current,
    .draw_selected = draw_selected,
  };

  s_visit_list(head, kind, &cue_redraw_one, &ctx,
	       VISIT_DETERMINISTIC, VISIT_DEPTH_FOREVER);
}

/*! 
 *  \brief Set the color on the gc depending on the passed in color id
 */
static void o_cue_set_color(GSCHEM_TOPLEVEL *w_current, int color)
{
  if (w_current->toplevel->override_color != -1 ) {
    gdk_gc_set_foreground(w_current->gc,
                          x_get_color(w_current->toplevel->override_color));
  } else {
    gdk_gc_set_foreground(w_current->gc, x_get_color(color));
  }
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_cue_draw_lowlevel(GSCHEM_TOPLEVEL *w_current, OBJECT *object, int whichone)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  int x[2], y[2], screen_x, screen_y;
  GList *cl_current;
  CONN *conn;
  int type, count = 0;
  int done = FALSE;
  int size, x2size, pinsize;
  int otherone;
  int bus_involved=FALSE;

  if (whichone != GRIP_1 && whichone != GRIP_2) return;
  
  s_basic_get_grip(object, whichone, &x[0], &y[0]);

  type = CONN_ENDPOINT;
  
  cl_current = object->conn_list;
  while(cl_current != NULL && !done) {
    conn = (CONN *) cl_current->data;
   
    if (conn->x == x[0] && conn->y == y[0]) {
      switch(conn->type) {
        case(CONN_ENDPOINT):
          count++;
          if (conn->other_object &&
              ((object->type == OBJ_NET &&
               conn->other_object->type == OBJ_BUS) ||
              (object->type == OBJ_BUS &&
               conn->other_object->type == OBJ_NET))) {
            bus_involved=TRUE;
          }
          break;

        case(CONN_MIDPOINT):
          type = CONN_MIDPOINT;
          done = TRUE;
          count = 0;
          if (conn->other_object &&
              ((object->type == OBJ_NET &&
                conn->other_object->type == OBJ_BUS) ||
               (object->type == OBJ_BUS &&
               conn->other_object->type == OBJ_NET))) {
            bus_involved=TRUE;
          }
          break;
      }
    }

    cl_current = g_list_next(cl_current);
  }

  if (GEDA_DEBUG) {
    printf("type: %d count: %d\n", type, count);
  }
  
  size = SCREENabs(page, CUE_BOX_SIZE);
  x2size = 2 * size;

  WORLDtoSCREEN(page, x[0], y[0], &screen_x, &screen_y);
  
  switch(type) {
    case(CONN_ENDPOINT):
      if (object->type == OBJ_NET) { /* only nets have these cues */
        if (count < 1) { /* Didn't find anything connected there */
	  if (w_current->DONT_REDRAW == 0) {
	    o_cue_set_color(w_current, toplevel->net_endpoint_color);
	    gdk_draw_rectangle(w_current->backingstore,
			       w_current->gc, TRUE,
			       screen_x - size,
			       screen_y - size,
			       x2size,
			       x2size);
	    o_invalidate_rect(w_current, screen_x - size, screen_y - size,
	                                 screen_x + size, screen_y + size);
	  }
        } else if (count >= 2) {
          /* draw circle */
          if (bus_involved) {
            size = SCREENabs(page, CUE_CIRCLE_SMALL_SIZE) / 2;
          } else {
            size = SCREENabs(page, CUE_CIRCLE_LARGE_SIZE) / 2;
          }
          x2size = 2 * size;
	  if (w_current->DONT_REDRAW == 0) {
	    o_cue_set_color(w_current, toplevel->junction_color);
	    gdk_draw_arc(w_current->backingstore,
			 w_current->gc, TRUE,
                       screen_x - size,
			 screen_y - size,
			 x2size, x2size, 0, FULL_CIRCLE);
	    o_invalidate_rect(w_current, screen_x - size, screen_y - size,
                                   screen_x + size, screen_y + size);
	  }
        }
      } else if (object->type == OBJ_PIN) {
        /* Didn't find anything connected there */
        if (count < 1 && object->whichend == whichone) {
          otherone = (whichone == GRIP_1 ? GRIP_2 : GRIP_1);

          pinsize = SCREENabs(page, 10);
          if (toplevel->pin_style == THICK ) {
            gdk_gc_set_line_attributes(w_current->gc, pinsize,
                                       GDK_LINE_SOLID,
                                       GDK_CAP_NOT_LAST,
                                       GDK_JOIN_MITER);
          }

    /* No need to invalidate the PIN end CUE, as the
     * whole pin region is already invalidated. */
	  if (w_current->DONT_REDRAW == 0) {
	    s_basic_get_grip(object, otherone, &x[1], &y[1]);

	    o_cue_set_color(w_current, toplevel->net_endpoint_color);

	    if (y[0] == y[1]) {
	      /* horizontal line */
	      if (x[0] <= x[1]) {
		gdk_draw_line(w_current->backingstore, w_current->gc,
			      screen_x, screen_y, screen_x + size, screen_y);
	      } else {
		gdk_draw_line(w_current->backingstore, w_current->gc,
			      screen_x, screen_y, screen_x - size, screen_y);
	      }
	    } else if (x[0] == x[1]) {
	      /* vertical line */
	      if (y[0] <= y[1]) {
		gdk_draw_line(w_current->backingstore, w_current->gc,
			      screen_x, screen_y, screen_x, screen_y - size);
	      } else {
		gdk_draw_line(w_current->backingstore, w_current->gc,
			      screen_x, screen_y, screen_x, screen_y + size);
	      }
	    } else {
	      /* angled line */
	      /* not supporting rendering of cue for angled pin for now. hack */
	    }
	  }

          if (toplevel->pin_style == THICK ) {
            gdk_gc_set_line_attributes(w_current->gc, 0,
                                       GDK_LINE_SOLID,
                                       GDK_CAP_NOT_LAST,
                                       GDK_JOIN_MITER);
          }
        }
      }
      break;

    case(CONN_MIDPOINT):
      /* draw circle */
      if (bus_involved) {
        size = SCREENabs(page, CUE_CIRCLE_SMALL_SIZE) / 2;
      } else {
        size = SCREENabs(page, CUE_CIRCLE_LARGE_SIZE) / 2;
      }
      x2size = size * 2;

      if (w_current->DONT_REDRAW == 0) {
	o_cue_set_color(w_current, toplevel->junction_color);
	gdk_draw_arc(w_current->backingstore,
		     w_current->gc, TRUE,
		     screen_x - size,
		     screen_y - size,
		     x2size, x2size, 0, FULL_CIRCLE);
	    o_invalidate_rect(w_current, screen_x - size, screen_y - size,
                                   screen_x + size, screen_y + size);
      }
      break;

      /* here is where you draw bus rippers */
  }
}

/*! \brief Lowlevel endpoint erase.
 *  \par Function Description
 *  This function erases OBJECT endpoints forcibly.
 *
 *  \param [in] w_current  The GSCHEM_TOPLEVEL object.
 *  \param [in] object     OBJECT to forcibly erase endpoint from.
 *  \param [in] whichone   Which endpoint to erase from OBJECT.
 */
void o_cue_erase_lowlevel(GSCHEM_TOPLEVEL *w_current, OBJECT *object, int whichone)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  int x, y, screen_x, screen_y;
  int size, x2size;
  
  s_basic_get_grip(object, whichone, &x, &y);

  size = SCREENabs(page, CUE_BOX_SIZE);
  x2size = 2 * size;

  gdk_gc_set_foreground(w_current->gc,
                        x_get_color(toplevel->background_color));
 
  WORLDtoSCREEN(page, x, y, &screen_x, &screen_y);
  
  if (w_current->DONT_REDRAW == 0) {
    gdk_draw_rectangle(w_current->backingstore,
		       w_current->gc, TRUE,
		       screen_x - size,
		       screen_y - size,
		       x2size,
		       x2size);
	    o_invalidate_rect(w_current, screen_x - size, screen_y - size,
                                   screen_x + size, screen_y + size);
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_cue_draw_lowlevel_midpoints(GSCHEM_TOPLEVEL *w_current, OBJECT *object)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  PAGE *page = toplevel->page_current;
  int x, y, screen_x, screen_y;
  GList *cl_current;
  CONN *conn;
  int size, x2size;

  if (toplevel->override_color != -1 ) {
    gdk_gc_set_foreground(w_current->gc,
                          x_get_color(toplevel->override_color));
  } else {
    gdk_gc_set_foreground(w_current->gc,
                          x_get_color(toplevel->junction_color));
  }
  
  cl_current = object->conn_list;
  while(cl_current != NULL) {
    conn = (CONN *) cl_current->data;

    switch(conn->type) {        
      case(CONN_MIDPOINT):

        x = conn->x;
        y = conn->y;
          
        WORLDtoSCREEN(page, x, y, &screen_x, &screen_y);
 
        /* draw circle */
        if (conn->other_object &&
            ( (object->type == OBJ_BUS &&
               conn->other_object->type == OBJ_NET) ||
              (object->type == OBJ_NET &&
               conn->other_object->type == OBJ_BUS))) {
          size = SCREENabs(page, CUE_CIRCLE_SMALL_SIZE) / 2;
        } else {
          size = SCREENabs(page, CUE_CIRCLE_LARGE_SIZE) / 2;
        }
        x2size = size * 2;

	if (w_current->DONT_REDRAW == 0) {
	  gdk_draw_arc(w_current->backingstore,
		       w_current->gc, TRUE,
		       screen_x - size,
		       screen_y - size,
		       x2size, x2size, 0, FULL_CIRCLE);
	  o_invalidate_rect(w_current, screen_x - size, screen_y - size,
                                 screen_x + size, screen_y + size);
	}
        break;
    }
   

    cl_current = g_list_next(cl_current);
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_cue_draw_single(GSCHEM_TOPLEVEL *w_current, OBJECT *object)
{
  if (!object) {
    return;
  }

  if (object->type != OBJ_NET && object->type != OBJ_PIN &&
      object->type != OBJ_BUS) {
        return;
      }

  if (object->type != OBJ_PIN) {
    o_cue_draw_lowlevel(w_current, object, GRIP_1);
    o_cue_draw_lowlevel(w_current, object, GRIP_2);
    o_cue_draw_lowlevel_midpoints(w_current, object);
  } else {
    o_cue_draw_lowlevel(w_current, object, object->whichend);
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_cue_erase_single(GSCHEM_TOPLEVEL *w_current, OBJECT *object)
{
  TOPLEVEL *toplevel = w_current->toplevel;
  if (!object) {
    return;
  }

  if (object->type != OBJ_NET && object->type != OBJ_PIN &&
      object->type != OBJ_BUS)
  {
    return;
  }

  if (object->type != OBJ_PIN) {
    o_cue_erase_lowlevel(w_current, object, GRIP_1);
    o_cue_erase_lowlevel(w_current, object, GRIP_2);
    toplevel->override_color = toplevel->background_color;
    o_cue_draw_lowlevel_midpoints(w_current, object);
    toplevel->override_color = -1;
  } else {
    o_cue_erase_lowlevel(w_current, object, object->whichend);
  }
}

/*! \brief Erase the cues of an object and redraw connected objects.
 *  \par Function Description
 *  This function erases the cues on object \a object. It then redraws
 *  the cues of its connected objects.
 *
 *  \param [in] w_current The GSCHEM_TOPLEVEL object.
 *  \param [in] object    The object to redraw with no cue.
 */
static void o_cue_undraw_lowlevel(GSCHEM_TOPLEVEL *w_current, OBJECT *object)
{
  GList *cl_current;
  CONN *conn;

  o_cue_erase_single(w_current, object);

  cl_current = object->conn_list;
  while(cl_current != NULL) {
    conn = (CONN *) cl_current->data;

    if (conn->other_object) {
      o_redraw_single(w_current, conn->other_object);
    }

    cl_current = g_list_next(cl_current);
  }
}

static enum visit_result cue_undraw_one(OBJECT *o_current, void *userdata)
{
  GSCHEM_TOPLEVEL *w_current = userdata;

  if (o_current->type == OBJ_PIN ||
      o_current->type == OBJ_NET ||
      o_current->type == OBJ_BUS) {
    o_cue_undraw_lowlevel (w_current, o_current);
  }

  return VISIT_RES_OK;
}

/*! \brief Hide the cues of an object.
 *  \par Function Description
 *  This function takes an object and redraws it without its cues. It
 *  also manages the redraw of connected objects.
 *
 *  \note
 *  The connections of the object are unchanged by this function.
 *
 *  \param [in] w_current The GSCHEM_TOPLEVEL object.
 *  \param [in] object    The object to redraw with no cue.
 */
void o_cue_undraw(GSCHEM_TOPLEVEL *w_current, OBJECT *object)
{
  switch (object->type) {
      case OBJ_PIN:
      case OBJ_NET:
      case OBJ_BUS:
        o_cue_undraw_lowlevel (w_current, object);
        break;
  case OBJ_COMPLEX:
  case OBJ_PLACEHOLDER:
    s_visit_object(object, &cue_undraw_one, w_current, VISIT_DETERMINISTIC, 1);
  }

  o_redraw_single(w_current, object);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_cue_draw_list(GSCHEM_TOPLEVEL *w_current, GList const *object_list)
{
  OBJECT *object;
  GList const *ol_current;

  ol_current = object_list;
  while(ol_current != NULL) {
    object = ol_current->data;

    o_cue_draw_single(w_current, object);
    
    ol_current = g_list_next(ol_current);
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_cue_undraw_list(GSCHEM_TOPLEVEL *w_current, GList const *object_list)
{
  OBJECT *object;
  GList const *ol_current;

  ol_current = object_list;
  while(ol_current != NULL) {
    object = ol_current->data;

    o_cue_undraw(w_current, object);
    
    ol_current = g_list_next(ol_current);
  }
}
