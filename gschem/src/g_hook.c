/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>
#include <stdio.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif
#include <math.h>

#include "gschem.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
/*
 * Adds an attribute <B>scm_attrib_name</B> with value <B>scm_attrib_value</B> to the given <B>object</B>.
The attribute has the visibility <B>scm_vis</B> and show <B>scm_show</B> flags.
The possible values are:
  - <B>scm_vis</B>: scheme boolean. Visible (TRUE) or hidden (FALSE).
  - <B>scm_show</B>: a list containing what to show: "name", "value", or both.
The return value is always TRUE.
 */
SCM g_add_attrib(SCM object, SCM scm_attrib_name, 
		 SCM scm_attrib_value, SCM scm_vis, SCM scm_show)
{
  GSCHEM_TOPLEVEL *w_current=global_window_current;
  OBJECT *o_current=NULL;
  gboolean vis;
  int show=0;
  gchar *attrib_name=NULL;
  gchar *attrib_value=NULL;
  gchar *value=NULL;
  gchar *newtext=NULL;
  SCM rest;

  SCM_ASSERT (scm_is_string(scm_attrib_name), scm_attrib_name,
	      SCM_ARG2, "add-attribute-to-object");
  SCM_ASSERT (scm_is_string(scm_attrib_value), scm_attrib_value,
	      SCM_ARG3, "add-attribute-to-object");
  SCM_ASSERT (scm_boolean_p(scm_vis), scm_vis,
	      SCM_ARG4, "add-attribute-to-object");
  SCM_ASSERT (scm_list_p(scm_show), scm_show,
	      SCM_ARG5, "add-attribute-to-object");
  
  /* Get o_current */
  SCM_ASSERT(g_get_data_from_object_smob(object, NULL, &o_current),
	     object, SCM_ARG1, "add-attribute-to-object");

  scm_dynwind_begin(0);

  /* Get parameters */
  attrib_name = scm_to_locale_string(scm_attrib_name);
  attrib_value = scm_to_locale_string(scm_attrib_value);
  scm_dynwind_free(attrib_name);
  scm_dynwind_free(attrib_value);
  vis = SCM_NFALSEP(scm_vis);

  for (rest = scm_show; !scm_is_null(rest); rest = SCM_CDR(rest)) {
    /* Check every element in the list. It should be a string! */
    SCM_ASSERT(scm_is_string(SCM_CAR(rest)), SCM_CAR(rest), SCM_ARG5,
	       "add-attribute-to-object");
    
    value = scm_to_locale_string(SCM_CAR(rest));
    scm_dynwind_free(value);
    
    /* Only "name" or "value" strings are allowed */
    SCM_ASSERT(!((strcasecmp(value, "name") != 0) &&
		 (strcasecmp(value, "value") != 0) ), SCM_CAR(rest),
	       SCM_ARG5, "add-attribute-to-object");
    
    /* show = 1 => show value; 
       show = 2 => show name; 
       show = 3 => show both */
    if (strcasecmp(value, "value") == 0) {
      show |= 1;
    }
    else if (strcasecmp(value, "name") == 0) {
      show |= 2;
    }	  
  }
  /* Show name and value (show = 3) => show=0 for gschem */
  if (show == 3) {
    show = 0;
  }
  
  newtext = g_strdup_printf("%s=%s", attrib_name, attrib_value);
  o_attrib_add_attrib (w_current, newtext, vis, show, o_current);
  g_free(newtext);

  scm_dynwind_end();
  return SCM_BOOL_T;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
/*
 * Sets several text properties of the given <B>attribute smob</B>:
  - <B>coloridx</B>: The index of the text color, or -1 to keep previous color.
  - <B>size</B>: Size (numeric) of the text, or -1 to keep the previous size.
  - <B>alignment</B>: String with the alignment of the text. Possible values are:
    * ""           : Keep the previous alignment.
    * "Lower Left"
    * "Middle Left"
    * "Upper Left"
    * "Lower Middle"
    * "Middle Middle"
    * "Upper Middle"
    * "Lower Right"
    * "Middle Right"
    * "Upper Right"
  - <B>rotation</B>: Angle of the text, or -1 to keep previous angle.
  - <B>x</B>, <B>y</B>: Coords of the text.
 */
SCM g_set_attrib_text_properties(SCM attrib_smob, SCM scm_coloridx,
				 SCM scm_size, SCM scm_alignment,
				 SCM scm_rotation, SCM scm_x, SCM scm_y)
{
  struct st_attrib_smob *attribute = 
    (struct st_attrib_smob *) SCM_SMOB_DATA(attrib_smob);
  OBJECT *object;
  GSCHEM_TOPLEVEL *w_current = global_window_current;

  int color = -1;
  int size = -1;
  char *alignment_string;
  int alignment = -2;
  int rotation = 0;
  int x = -1, y = -1;

  SCM_ASSERT (scm_is_integer(scm_coloridx), scm_coloridx,
	      SCM_ARG2, "set-attribute-text-properties!");
  SCM_ASSERT ( scm_is_integer(scm_size),
               scm_size, SCM_ARG3, "set-attribute-text-properties!");
  SCM_ASSERT (scm_is_string(scm_alignment), scm_alignment,
	      SCM_ARG4, "set-attribute-text-properties!");
  SCM_ASSERT ( scm_is_integer(scm_rotation),
               scm_rotation, SCM_ARG5, "set-attribute-text-properties!");
  SCM_ASSERT ( scm_is_integer(scm_x),
               scm_x, SCM_ARG6, "set-attribute-text-properties!");
  SCM_ASSERT ( scm_is_integer(scm_y),
               scm_y, SCM_ARG7, "set-attribute-text-properties!");

  color = scm_to_int(scm_coloridx);

  SCM_ASSERT (!(color < -1 || color >= MAX_COLORS),
              scm_coloridx, SCM_ARG2, "set-attribute-text-properties!");

  size = scm_to_int(scm_size);
  rotation = scm_to_int(scm_rotation);
  x = scm_to_int(scm_x);
  y = scm_to_int(scm_y);
  
  scm_dynwind_begin(0);
  alignment_string = scm_to_locale_string(scm_alignment);
  scm_dynwind_free(alignment_string);

  if (strlen(alignment_string) == 0) {
    alignment = -1;
  }
  if (strcmp(alignment_string, "Lower Left") == 0) {
    alignment = 0;
  }
  if (strcmp(alignment_string, "Middle Left") == 0) {
    alignment = 1;
  }
  if (strcmp(alignment_string, "Upper Left") == 0) {
    alignment = 2;
  }
  if (strcmp(alignment_string, "Lower Middle") == 0) {
    alignment = 3;
  }
  if (strcmp(alignment_string, "Middle Middle") == 0) {
    alignment = 4;
  }
  if (strcmp(alignment_string, "Upper Middle") == 0) {
    alignment = 5;
  }
  if (strcmp(alignment_string, "Lower Right") == 0) {
    alignment = 6;
  }
  if (strcmp(alignment_string, "Middle Right") == 0) {
    alignment = 7;
  }
  if (strcmp(alignment_string, "Upper Right") == 0) {
    alignment = 8;
  }
  if (alignment == -2) {
    /* Bad specified */
    SCM_ASSERT (scm_is_string(scm_alignment), scm_alignment,
		SCM_ARG4, "set-attribute-text-properties!");
  }
  scm_dynwind_end();

  if (attribute &&
      attribute->attribute) {
    object = attribute->attribute;
    if (object &&
	object->text) {
      int new_x, new_y;

      o_erase_single(w_current, object);

      o_text_get_xy(object, &new_x, &new_y);
      if (x != -1) {
	new_x = x;
      }
      if (y != -1) {
	new_y = y;
      }
      o_text_set_xy(object, new_x, new_y);
      if (size != -1) {
	o_text_set_size(object, size);
      }
      if (alignment != -1) {
	o_text_set_alignment(object, alignment);
      }
      if (rotation != -1) {
	o_text_set_angle(object, rotation);
      }
      if (!w_current->DONT_REDRAW) {
	o_text_draw(w_current, object);
      }
    }
  }
  return SCM_BOOL_T;
}

/*! \brief Return a list of selected objects in a page.
 *  \par Function Description
 *  Returns a list containing the selected objects in the given page.
 *
 *  \param [in] page_smob  Page to look at.
 *
 *  \return a list of objects in the selection list.
 */
SCM g_get_selected_objects(SCM page_smob)
{
  TOPLEVEL *toplevel;
  PAGE *page;
  SCM return_list = SCM_EOL;
  GList *iter;

  /* Get toplevel and the page */
  SCM_ASSERT(g_get_data_from_page_smob(page_smob, &toplevel, &page),
	     page_smob, SCM_ARG1, "get-selected-objects");

  for (iter = geda_list_get_glist(page->selection_list); iter != NULL; iter = iter->next) {
    return_list = scm_cons(g_make_object_smob(toplevel, iter->data),
			   return_list);
  }

  return return_list;
}


SCM g_get_current_page(void)
{
  return (g_make_page_smob(global_window_current->toplevel,
			   global_window_current->toplevel->page_current));
}
