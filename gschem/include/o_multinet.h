#ifndef H_MULTINET_H
#define H_MULTINET_H

#include <libgeda/libgeda.h>

enum multinet_grips_t {
  GRIP_NET_START = GRIP_FIRST_OPAQUE,
  GRIP_NET_END
};

#endif
