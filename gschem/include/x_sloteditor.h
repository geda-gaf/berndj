/* gEDA - GPL Electronic Design Automation
 * gschem - gEDA Schematic Capture
 * x_sloteditor.h - A dialog to edit slots in a schematic.
 * Copyright (C) 2008 Bernd Jendrissek <bernd.jendrissek@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#ifndef H_X_SLOTEDITOR_H
#define H_X_SLOTEDITOR_H

#include <gtk/gtk.h>

#include <libgeda/libgeda.h>

#include "glade_compat.h"
#include "gschem_struct.h"

#define TYPE_SLOT_EDITOR         (slot_editor_get_type())
#define SLOT_EDITOR(obj)         (G_TYPE_CHECK_INSTANCE_CAST((obj), TYPE_SLOT_EDITOR, SlotEditor))
#define SLOT_EDITOR_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), TYPE_SLOT_EDITOR, SlotEditorClass))
#define IS_SLOT_EDITOR(obj)      (G_TYPE_CHECK_INSTANCE_TYPE((obj), TYPE_SLOT_EDITOR))

typedef struct {
  GObjectClass parent_class;
} SlotEditorClass;

typedef struct {
  GObject parent_instance;
  GSCHEM_TOPLEVEL *w_current;
  PAGE *page;
  GtkWindow *sewindow;
  GtkTreeView *treeview;
  GtkTreeStore *treestore;
  gboolean dispose_has_run;
} SlotEditor;

#endif
