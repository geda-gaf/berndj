#ifndef H_GLADE_COMPAT_H
#define H_GLADE_COMPAT_H

#include "config.h"

#include <gtk/gtk.h>
#include <glade/glade.h>

GtkWidget *x_lookup_widget(GladeXML *xml, char const *name);
GtkWidget *x_lookup_widget_set(GladeXML *xml,
			       char const *name, char const *prop, ...);
GtkWidget *x_glade_get_widget(GladeXML **xml, char const *gladefile, ...);

#endif
