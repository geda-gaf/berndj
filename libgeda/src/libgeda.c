/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998, 1999, 2000 Kazu Hirata / Ales Hvezda
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif
#ifdef HAVE_STRARG_H
#include <stdarg.h>
#endif
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/* Some long-lived objects that should die only when exiting. */
GList *libgeda_caches = NULL;

/*! \brief Perform runtime initialization of libgeda library.
 *  \par Function Description
 *  This function is responsible for making sure that any runtime
 *  initialization is done for all the libgeda routines.  It should
 *  be called before any other libgeda functions are called.
 *
 */
void libgeda_init(gboolean use_guile)
{
  char *geda_data = getenv("GEDADATA");

  if (geda_data == NULL) {
    g_setenv ("GEDADATA", GEDADATADIR, FALSE);
  }

  /* Initialise gettext */
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");

  /* Initialise gobject */
  g_type_init ();

  s_clib_init();
  s_slib_init();
  s_menu_init();
  s_attrib_init();
  s_color_init();

  o_text_init(); 

  if (use_guile) {
    g_register_libgeda_funcs();
    g_register_libgeda_vars();

    g_init_object_smob();
    g_init_attrib_smob();
    g_init_page_smob();
    g_init_toplevel_smob();
  }
}

static void fini_unref(gpointer victim, gpointer user_data G_GNUC_UNUSED)
{
  g_object_unref(victim);
}

void libgeda_fini(void)
{
  /*
   * XXX Explicitly flushing all ports should not be necessary. libguile
   * should be doing that from within its own atexit handlers. But apparently
   * very occasionally (once every few hundred runs) scm_flush_all_ports seems
   * to fail to run. When this happens, strace suggests some GC is happening
   * that normally doesn't, at the point where the gnetlist process exits.
   * This behaviour may be ASLR-related, as it seems to disappear when using a
   * fixed memory layout (using setarch). Observed with libguile 2.0.11.
   */
  scm_flush_all_ports();

  g_list_foreach(libgeda_caches, &fini_unref, NULL);

  /* Deallocate string descriptions of objects. */
  s_describe_object(NULL, 0);
}
