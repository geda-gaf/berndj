/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#include <math.h>

#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/*! \file o_pin_basic.c
 *  \brief functions for the pin object
 */

/*! Default setting for pin draw function. */
void (*pin_draw_func)() = NULL;

/*! \brief calculate and return the boundaries of a pin object
 *  \par Function Description
 *  This function calculates the object boundaries of a pin \a object.
 *
 *  \param [in]  toplevel  The TOPLEVEL object.
 *  \param [in]  object    a pin object
 *  \param [out] left      the left world coord
 *  \param [out] top       the top world coord
 *  \param [out] right     the right world coord
 *  \param [out] bottom    the bottom world coord
 */
void world_get_pin_bounds(OBJECT *object, int *left, int *top,
			  int *right, int *bottom)
{
  world_get_line_bounds(object, left, top, right, bottom);
}

OBJECT *o_pin_new_at_xy(TOPLEVEL *toplevel, char type, int color, int x, int y)
{
  OBJECT *pin = o_line_new_at_xy(toplevel, type, color, x, y);

  return pin;
}

/*! \brief create a new pin object
 *  \par Function Description
 *  This function creates and returns a new pin object.
 *  
 *  \param [in]     toplevel    The TOPLEVEL object.
 *  \param [in]     type        The OBJECT type (usually OBJ_PIN)
 *  \param [in]     color       The color of the pin
 *  \param [in]     x1          x-coord of the first point
 *  \param [in]     y1          y-coord of the first point
 *  \param [in]     x2          x-coord of the second point
 *  \param [in]     y2          y-coord of the second point
 *  \param [in]     pin_type    type of pin (PIN_TYPE_NET or PIN_TYPE_BUS)
 *  \param [in]     whichend    The connectable end of the pin
 *  \return A new pin OBJECT
 */
OBJECT *o_pin_new(TOPLEVEL *toplevel,
		  char type, int color,
		  int x1, int y1, int x2, int y2, int pin_type, int whichend)
{
  OBJECT *new_node;

  new_node = o_pin_new_at_xy(toplevel, type, color, x1, y1);
  
  s_basic_move_grip(new_node, GRIP_1, x1, y1);
  s_basic_move_grip(new_node, GRIP_2, x2, y2);

  o_pin_recalc(new_node);

  new_node->pin_type = pin_type;
  new_node->whichend = whichend;
  
  return new_node;
}

/*! \brief recalc the visual properties of a pin object
 *  \par Function Description
 *  This function updates the visual coords of the \a o_current object.
 *  
 *  \param [in]     toplevel    The TOPLEVEL object.
 *  \param [in]     o_current   a pin object.
 *
 */
void o_pin_recalc(OBJECT *o_current)
{
  int left, right, top, bottom;

  if (o_current->line == NULL) {
    return;
  }

  world_get_pin_bounds(o_current, &left, &top, &right, &bottom);

  o_current->w_left = left;
  o_current->w_top = top;
  o_current->w_right = right;
  o_current->w_bottom = bottom;
  o_current->w_bounds_valid = TRUE;
}

/*! \brief read a pin object from a char buffer
 *  \par Function Description
 *  This function reads a pin object from the buffer \a buf.
 *  
 *  \param [in] toplevel     The TOPLEVEL object
 *  \param [in] buf          a text buffer (usually a line of a schematic file)
 *  \param [in] release_ver  The release number gEDA
 *  \param [in] fileformat_ver a integer value of the file format
 *  \return The object list
 */
OBJECT *o_pin_read(TOPLEVEL *toplevel, char buf[],
		   unsigned int release_ver, unsigned int fileformat_ver)
{
  OBJECT *new_obj;
  char type; 
  int x1, y1;
  int x2, y2;
  int d_x1, d_y1;
  int d_x2, d_y2;
  int color;
  int pin_type;
  int whichend;

  if(release_ver <= VERSION_20020825) {
    sscanf(buf, "%c %d %d %d %d %d\n", &type, &x1, &y1, &x2, &y2, &color);
    pin_type = PIN_TYPE_NET;
    whichend = -1;     
  } else {
    sscanf(buf, "%c %d %d %d %d %d %d %d\n", &type, &x1, &y1, &x2, &y2,
           &color, &pin_type, &whichend);
  }

  if (whichend == -1) {
    s_log_message(_("Found a pin which did not have the whichone field set.\n"
                    "Verify and correct manually.\n"));
  } else if (whichend < -1 || whichend > 1) {
    s_log_message(_("Found an invalid whichend on a pin (reseting to zero): %d\n"),
                  whichend);
    whichend = 0;
  }
  
  d_x1 = x1; 
  d_y1 = y1; 
  d_x2 = x2; 
  d_y2 = y2; 

  if (x1 == x2 && y1 == y2) {
    s_log_message(_("Found a zero length pin: [ %s ]\n"), buf);
  }

  if (color < 0 || color > MAX_COLORS) {
    s_log_message(_("Found an invalid color [ %s ]\n"), buf);
    s_log_message(_("Setting color to WHITE\n"));
    color = WHITE;
  }

  if (toplevel->override_pin_color != -1) {
    color = toplevel->override_pin_color;
  }

  /* Map file format magic numbers into grip constants. */
  switch (whichend) {
  case 0:
    whichend = GRIP_1;
    break;
  case 1:
    whichend = GRIP_2;
    break;
  case -1:
    whichend = GRIP_NONE;
    break;
  }
  new_obj = o_pin_new(toplevel, type, color, d_x1, d_y1,
                      d_x2, d_y2, pin_type, whichend);

  return new_obj;
}

/*! \brief Create a string representation of the pin object
 *  \par Function Description
 *  This function takes a pin \a object and return a string
 *  according to the file format definition.
 *
 *  \param [in] object  a pin OBJECT
 *  \return the string representation of the pin OBJECT
 */
char *o_pin_save(OBJECT *object)
{
  int x1, x2, y1, y2;
  int color;
  int pin_type, whichend;
  char *buf;
  
  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);
  
  /* Use the right color */
  if (object->saved_color == -1) {
    color = object->color;
  } else {
    color = object->saved_color;
  }

  pin_type = object->pin_type;
  whichend = object->whichend;
  
  buf = g_strdup_printf("%c %d %d %d %d %d %d %d", object->type,
		   x1, y1, x2, y2, color, pin_type, whichend);
  return(buf);
}

/*! \brief move a pin object
 *  \par Function Description
 *  This function changes the position of a pin \a object.
 *
 *  \param [in] dx           The x-distance to move the object
 *  \param [in] dy           The y-distance to move the object
 *  \param [in] object       The pin OBJECT to be moved
 */
void o_pin_translate_world(int dx, int dy, OBJECT *object)
{
  int x1, y1, x2, y2;

  if (object == NULL) printf("ptw NO!\n");

  /* Update world coords */
  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);
  s_basic_move_grip(object, GRIP_1, x1 + dx, y1 + dy);
  s_basic_move_grip(object, GRIP_2, x2 + dx, y2 + dy);

  /* Update bounding box */
  o_pin_recalc(object);
}

/*! \brief create a copy of a pin object
 *  \par Function Description
 *  This function creates a copy of the pin object \a o_current.
 *
 *  \param [in] toplevel     The TOPLEVEL object
 *  \param [in] o_current    The object that is copied
 *  \return a new pin object
 */
OBJECT *o_pin_copy(TOPLEVEL *toplevel, OBJECT *o_current)
{
  OBJECT *new_obj;
  int x1, y1, x2, y2;
  int color;

  if (o_current->saved_color == -1) {
    color = o_current->color;
  } else {
    color = o_current->saved_color;
  }

  s_basic_get_grip(o_current, GRIP_1, &x1, &y1);
  s_basic_get_grip(o_current, GRIP_2, &x2, &y2);

  new_obj = o_pin_new(toplevel, OBJ_PIN, color, x1, y1, x2, y2,
		      o_current->pin_type, o_current->whichend);

  /* XXX What is the purpose of this?  It seems redundant. */
  s_basic_move_grip(new_obj, GRIP_1, x1, y1);
  s_basic_move_grip(new_obj, GRIP_2, x2, y2);

  /*	new_obj->attribute = 0;*/

  return new_obj;
}

/*! \brief postscript print command for a pin object
 *  \par Function Description
 *  This function writes the postscript command of the pin object \a o_current
 *  into the FILE \a fp points to.
 *  
 *  \param [in] toplevel     The TOPLEVEL object
 *  \param [in] fp           pointer to a FILE structure
 *  \param [in] o_current    The OBJECT to print
 */
void o_pin_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current,
		 double scale, GArray *unicode_table)
{
  int pin_width;
  int x1, y1;
  int x2, y2;
  
  if (o_current == NULL) {
    printf("got null in o_pin_print\n");
    return;
  }

  if (toplevel->print_color) {
    f_print_set_color(fp, o_current->color);
  }

  s_basic_get_grip(o_current, GRIP_1, &x1, &y1);
  s_basic_get_grip(o_current, GRIP_2, &x2, &y2);
  pin_width = 2;
  if(toplevel->pin_style == THICK) {
    pin_width = PIN_WIDTH;
  }

  fprintf(fp, "%d %d %d %d %d line\n", x1, y1, x2, y2, pin_width);
}

/*! \brief rotate a pin object around a centerpoint
 *  \par Function Description
 *  This function rotates a pin \a object around the point
 *  (\a world_centerx, \a world_centery).
 *  
 *  \param [in] world_centerx x-coord of the rotation center
 *  \param [in] world_centery y-coord of the rotation center
 *  \param [in] angle         The angle to rotat the pin object
 *  \param [in] object        The pin object
 *  \note only steps of 90 degrees are allowed for the \a angle
 */
void o_pin_rotate_world(int world_centerx, int world_centery, int angle,
			OBJECT *object)
{
  int x1, y1, x2, y2;
  int newx, newy;
	
  if (angle == 0)
    return;

  /* translate object to origin */
  o_pin_translate_world(-world_centerx, -world_centery, object);

  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);

  rotate_point_90(x1, y1, angle, &newx, &newy);
  s_basic_move_grip(object, GRIP_1, newx, newy);

  rotate_point_90(x2, y2, angle, &newx, &newy);
  s_basic_move_grip(object, GRIP_2, newx, newy);

  o_pin_translate_world(world_centerx, world_centery, object);
}

/*! \brief mirror a pin object horizontally at a centerpoint
 *  \par Function Description
 *  This function mirrors a pin \a object horizontally at the point
 *  (\a world_centerx, \a world_centery).
 *  
 *  \param [in] world_centerx x-coord of the mirror position
 *  \param [in] world_centery y-coord of the mirror position
 *  \param [in] object        The pin object
 */
void o_pin_mirror_world(int world_centerx, int world_centery, OBJECT *object)
{
  int x1, y1, x2, y2;

  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);

  /* translate object to origin */
  o_pin_translate_world(-world_centerx, -world_centery, object);

  s_basic_move_grip(object, GRIP_1, -x1, y1);
  s_basic_move_grip(object, GRIP_2, -x2, y2);

  o_pin_translate_world(world_centerx, world_centery, object);
}

/*! \brief modify one point of a pin object
 *  \par Function Description
 *  This function modifies one point of a pin \a object. The point
 *  is specified by the \a whichone variable and the new coordinate
 *  is (\a x, \a y).
 *  
 *  \param toplevel   The TOPLEVEL object
 *  \param object     The pin OBJECT to modify
 *  \param x          new x-coord of the pin point
 *  \param y          new y-coord of the pin point
 *  \param whichone   pin point to modify
 *
 */
void o_pin_modify(TOPLEVEL *toplevel, OBJECT *object,
		  int x, int y, int whichone)
{
  s_basic_move_grip(object, whichone, x, y);

  o_pin_recalc(object);
}

/*! \brief guess the whichend of pins of object list
 *  \par Function Description
 *  This function determines the whichend of the pins in the \a object_list.
 *  In older libgeda file format versions there was no information about the 
 *  active end of pins.
 *  This function calculates the bounding box of all pins in the object list.
 *  The side of the pins that are closer to the boundary of the box are
 *  set as active ends of the pins.
 *  
 *  \param toplevel    The TOPLEVEL object
 *  \param object_list list of OBJECTs
 *  \param num_pins    pin count in the object list
 *
 */
void o_pin_update_whichend(TOPLEVEL *toplevel,
			   OBJECT *object_list, int num_pins)
{
  OBJECT *o_current;
  int top, left;
  int right, bottom;
  int d1, d2, d3, d4;
  int min0, min1;
  int min0_whichend, min1_whichend;
  int rleft, rtop, rright, rbottom;
  int found;

  if (object_list && num_pins) {
    if (num_pins == 1 || toplevel->force_boundingbox) {
      world_get_object_list_bounds(object_list, LIST_KIND_HEAD,
				   &left, &top, &right, &bottom);
    } else {
      found = 0;

      /* only look at the pins to calculate bounds of the symbol */
      o_current = object_list;
      while (o_current != NULL) {
        if (o_current->type == OBJ_PIN) {
          rleft = o_current->w_left;
          rtop = o_current->w_top;
          rright = o_current->w_right;
          rbottom = o_current->w_bottom;

          if ( found ) {
            left = min( left, rleft );
            top = min( top, rtop );
            right = max( right, rright );
            bottom = max( bottom, rbottom );
          } else {
            left = rleft;
            top = rtop;
            right = rright;
            bottom = rbottom;
            found = 1;
          }
        }
        o_current=o_current->next;
      }
    }
  } else {
    return;
  }

  o_current = object_list;
  while (o_current != NULL) {
    int x1, y1, x2, y2;

    s_basic_get_grip(o_current, GRIP_1, &x1, &y1);
    s_basic_get_grip(o_current, GRIP_2, &x2, &y2);

    /* Determine which end of the pin is on or nearest the boundary */
    if (o_current->type == OBJ_PIN && o_current->whichend == -1) {
      if (y1 == y2) {
        /* horizontal */
        
        if (x1 == left) {
          o_current->whichend = GRIP_1;
        } else if (x2 == left) {
          o_current->whichend = GRIP_2;
        } else if (x1 == right) {
          o_current->whichend = GRIP_1;
        } else if (x2 == right) {
          o_current->whichend = GRIP_2;
        } else {
            
          d1 = abs(x1 - left);
          d2 = abs(x2 - left);
          d3 = abs(x1 - right);
          d4 = abs(x2 - right);

          if (d1 <= d2) {
            min0 = d1;
            min0_whichend = GRIP_1;
          } else {
            min0 = d2;
            min0_whichend = GRIP_2;
          }

          if (d3 <= d4) {
            min1 = d3;
            min1_whichend = GRIP_1;
          } else {
            min1 = d4;
            min1_whichend = GRIP_2;
          }

          if (min0 <= min1) {
            o_current->whichend = min0_whichend;
          } else {
            o_current->whichend = min1_whichend;
          }
        }
           
      } else if (x1 == x2) {
        /* vertical */
        
        if (y1 == top) {
          o_current->whichend = GRIP_1;
        } else if (y2 == top) {
          o_current->whichend = GRIP_2;
        } else if (x1 == bottom) {
          o_current->whichend = GRIP_1;
        } else if (x2 == bottom) {
          o_current->whichend = GRIP_2;
        } else {
            
          d1 = abs(y1 - top);
          d2 = abs(y2 - top);
          d3 = abs(y1 - bottom);
          d4 = abs(y2 - bottom);

          if (d1 <= d2) {
            min0 = d1;
            min0_whichend = GRIP_1;
          } else {
            min0 = d2;
            min0_whichend = GRIP_2;
          }

          if (d3 <= d4) {
            min1 = d3;
            min1_whichend = GRIP_1;
          } else {
            min1 = d4;
            min1_whichend = GRIP_2;
          }

          if (min0 <= min1) {
            o_current->whichend = min0_whichend;
          } else {
            o_current->whichend = min1_whichend;
          }
        }
      }
    }
    o_current = o_current->next;
  }
}
