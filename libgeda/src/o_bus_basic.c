/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */

/*! \file o_bus_basic.c 
 *  \brief functions for the bus object
 */

#include <config.h>
#include <stdio.h>
#include <math.h>

#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/*! Default setting for bus draw function. */
void (*bus_draw_func)() = NULL;

/*! \brief calculate and return the boundaries of a bus object
 *  \par Function Description
 *  This function calculates the object boundaries of a bus \a object.
 *
 *  \param [in]  toplevel  The TOPLEVEL object.
 *  \param [in]  object    a bus object
 *  \param [out] left      the left world coord
 *  \param [out] top       the top world coord
 *  \param [out] right     the right world coord
 *  \param [out] bottom    the bottom world coord
 */
void world_get_bus_bounds(OBJECT *object, int *left, int *top,
			  int *right, int *bottom)
{
  world_get_line_bounds(object, left, top, right, bottom);
}

OBJECT *o_bus_new_at_xy(TOPLEVEL *toplevel, char type, int color, int x, int y)
{
  OBJECT *bus = o_line_new_at_xy(toplevel, type, color, x, y);

  return bus;
}

/*! \brief create a new bus object
 *  \par Function Description
 *  This function creates and returns a new bus object.
 *  
 *  \param [in]     toplevel    The TOPLEVEL object.
 *  \param [in]     type        The OBJECT type (usually OBJ_BUS)
 *  \param [in]     color       The color of the bus
 *  \param [in]     x1          x-coord of the first point
 *  \param [in]     y1          y-coord of the first point
 *  \param [in]     x2          x-coord of the second point
 *  \param [in]     y2          y-coord of the second point
 *  \param [in]  bus_ripper_direction direction of the bus rippers
 *  \return A new bus OBJECT
 */
OBJECT *o_bus_new(TOPLEVEL *toplevel,
		  char type, int color,
		  int x1, int y1, int x2, int y2,
		  int bus_ripper_direction)
{
  OBJECT *new_node;

  new_node = o_bus_new_at_xy(toplevel, type, color, x1, y1);
  
  s_basic_move_grip(new_node, GRIP_1, x1, y1);
  s_basic_move_grip(new_node, GRIP_2, x2, y2);

  new_node->bus_ripper_direction = bus_ripper_direction;

  o_bus_recalc(new_node);

  return new_node;
}

/*! \brief recalc the visual properties of a bus object
 *  \par Function Description
 *  This function updates the visual coords of the \a o_current object.
 *  
 *  \param [in]     toplevel    The TOPLEVEL object.
 *  \param [in]     o_current   a bus object.
 */
void o_bus_recalc(OBJECT *o_current)
{
  int left, right, top, bottom;

  if (o_current == NULL) {
    return;
  }

  if (o_current->line == NULL) {
    return;
  }

  world_get_bus_bounds(o_current, &left, &top, &right, &bottom);

  o_current->w_left = left;
  o_current->w_top = top;
  o_current->w_right = right;
  o_current->w_bottom = bottom;
  o_current->w_bounds_valid = TRUE;
}

/*! \brief read a bus object from a char buffer
 *  \par Function Description
 *  This function reads a bus object from the buffer \a buf.
 *  
 *  \param [in] toplevel     The TOPLEVEL object
 *  \param [in] buf          a text buffer (usually a line of a schematic file)
 *  \param [in] release_ver  The release number gEDA
 *  \param [in] fileformat_ver a integer value of the file format
 *  \return The object list
 */
OBJECT *o_bus_read(TOPLEVEL *toplevel, char buf[],
		   unsigned int release_ver, unsigned int fileformat_ver)
{
  OBJECT *new_obj;
  char type; 
  int x1, y1;
  int x2, y2;
  int d_x1, d_y1;
  int d_x2, d_y2;
  int color;
  int ripper_dir;

  if(release_ver <= VERSION_20020825) {
    sscanf(buf, "%c %d %d %d %d %d\n", &type, &x1, &y1, &x2, &y2, &color);
    ripper_dir = 0;
  } else {
    sscanf(buf, "%c %d %d %d %d %d %d\n", &type, &x1, &y1, &x2, &y2, &color,
           &ripper_dir);
  }
    
  d_x1 = x1; 
  d_y1 = y1; 
  d_x2 = x2; 
  d_y2 = y2; 

  if (x1 == x2 && y1 == y2) {
    s_log_message(_("Found a zero length bus [ %c %d %d %d %d %d ]\n"),
                    type, x1, y1, x2, y2, color);
  }

  if (toplevel->override_bus_color != -1) {
    color = toplevel->override_bus_color;
  }

  if (color < 0 || color > MAX_COLORS) {
    s_log_message(_("Found an invalid color [ %s ]\n"), buf);
    s_log_message(_("Setting color to WHITE\n"));
    color = WHITE;
  }

  if (ripper_dir < -1 || ripper_dir > 1) {
    s_log_message(_("Found an invalid bus ripper direction [ %s ]\n"), buf);
    s_log_message(_("Resetting direction to neutral (no direction)\n"));
    ripper_dir = 0;
  }

  new_obj = o_bus_new(toplevel, type, color,
                      d_x1, d_y1, d_x2, d_y2, ripper_dir);

  return new_obj;
}

/*! \brief Create a string representation of the bus object
 *  \par Function Description
 *  This function takes a bus \a object and return a string
 *  according to the file format definition.
 *
 *  \param [in] object  a bus OBJECT
 *  \return the string representation of the bus OBJECT
 */
char *o_bus_save(OBJECT *object)
{
  int x1, x2, y1, y2;
  int color;
  char *buf;

  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);

  /* Use the right color */
  if (object->saved_color == -1) {
    color = object->color;
  } else {
    color = object->saved_color;
  }

  buf = g_strdup_printf("%c %d %d %d %d %d %d", object->type,
          x1, y1, x2, y2, color, object->bus_ripper_direction);
  return(buf);
}
       
/*! \brief move a bus object
 *  \par Function Description
 *  This function changes the position of a bus \a object.
 *
 *  \param [in] dx           The x-distance to move the object
 *  \param [in] dy           The y-distance to move the object
 *  \param [in] object       The bus OBJECT to be moved
 */
void o_bus_translate_world(int dx, int dy, OBJECT *object)
{
  int x1, x2, y1, y2;

  if (object == NULL) printf("btw NO!\n");

  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);

  /* Update world coords */
  s_basic_move_grip(object, GRIP_1, x1 + dx, y1 + dy);
  s_basic_move_grip(object, GRIP_2, x2 + dx, y2 + dy);

  /* Update bounding box */
  o_bus_recalc(object);
}

/*! \brief create a copy of a bus object
 *  \par Function Description
 *  This function creates a copy of the bus object \a o_current.
 *
 *  \param [in] toplevel     The TOPLEVEL object
 *  \param [in] o_current    The object that is copied
 *  \return a new bus object
 */
OBJECT *o_bus_copy(TOPLEVEL *toplevel, OBJECT *o_current)
{
  OBJECT *new_obj;
  int x1, y1, x2, y2;
  int color;

  if (o_current->saved_color == -1) {
    color = o_current->color;
  } else {
    color = o_current->saved_color;
  }

  s_basic_get_grip(o_current, GRIP_1, &x1, &y1);
  s_basic_get_grip(o_current, GRIP_2, &x2, &y2);

  /* make sure you fix this in pin and bus as well */
  /* still doesn't work... you need to pass in the new values */
  /* or don't update and update later */
  /* I think for now I'll disable the update and manually update */
  new_obj = o_bus_new(toplevel, OBJ_BUS, color, x1, y1, x2, y2,
		      o_current->bus_ripper_direction);

  /* XXX What is the purpose of this?  It seems redundant. */
  s_basic_move_grip(new_obj, GRIP_1, x1, y1);
  s_basic_move_grip(new_obj, GRIP_2, x2, y2);

  return new_obj;
}

/*! \brief postscript print command for a bus object
 *  \par Function Description
 *  This function writes the postscript command of the bus object \a o_current
 *  into the FILE \a fp points to.
 *  
 *  \param [in] toplevel     The TOPLEVEL object
 *  \param [in] fp           pointer to a FILE structure
 *  \param [in] o_current    The OBJECT to print
 */
void o_bus_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current,
		 double scale, GArray *unicode_table)
{
  int bus_width;
  int x1, y1;
  int x2, y2;

  if (o_current == NULL) {
    printf("got null in o_bus_print\n");
    return;
  }

  if (toplevel->print_color) {
    f_print_set_color(fp, o_current->color);
  }

  bus_width = 2;
  if (toplevel->bus_style == THICK) {
    bus_width = BUS_WIDTH;	
  }

  s_basic_get_grip(o_current, GRIP_1, &x1, &y1);
  s_basic_get_grip(o_current, GRIP_2, &x2, &y2);

  fprintf(fp, "%d %d %d %d %d line\n", x1, y1, x2, y2, bus_width);
}


/*! \brief rotate a bus object around a centerpoint
 *  \par Function Description
 *  This function rotates a bus \a object around the point
 *  (\a world_centerx, \a world_centery).
 *  
 *  \param [in] world_centerx x-coord of the rotation center
 *  \param [in] world_centery y-coord of the rotation center
 *  \param [in] angle         The angle to rotate the bus object
 *  \param [in] object        The bus object
 *  \note only steps of 90 degrees are allowed for the \a angle
 */
void o_bus_rotate_world(int world_centerx, int world_centery, int angle,
			OBJECT *object)
{
  int x1, y1, x2, y2;
  int newx, newy;

  if (angle == 0)
  return;

  /* translate object to origin */
  o_bus_translate_world(-world_centerx, -world_centery, object);

  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);

  rotate_point_90(x1, y1, angle, &newx, &newy);
  s_basic_move_grip(object, GRIP_1, newx, newy);

  rotate_point_90(x2, y2, angle, &newx, &newy);
  s_basic_move_grip(object, GRIP_2, newx, newy);

  o_bus_translate_world(world_centerx, world_centery, object);
}

/*! \brief mirror a bus object horizontally at a centerpoint
 *  \par Function Description
 *  This function mirrors a bus \a object horizontally at the point
 *  (\a world_centerx, \a world_centery).
 *  
 *  \param [in] world_centerx x-coord of the mirror position
 *  \param [in] world_centery y-coord of the mirror position
 *  \param [in] object        The bus object
 */
void o_bus_mirror_world(int world_centerx, int world_centery, OBJECT *object)
{
  int x1, y1, x2, y2;

  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);

  /* translate object to origin */
  o_bus_translate_world(-world_centerx, -world_centery, object);

  s_basic_move_grip(object, GRIP_1, -x1, y1);
  s_basic_move_grip(object, GRIP_2, -x2, y2);

  o_bus_translate_world(world_centerx, world_centery, object);
}

/*! \brief modify one point of a bus object
 *  \par Function Description
 *  This function modifies one point of a bus \a object. The point
 *  is specified by the \a whichone variable and the new coordinate
 *  is (\a x, \a y).
 *  
 *  \param toplevel   The TOPLEVEL object
 *  \param object     The bus OBJECT to modify
 *  \param x          new x-coord of the bus point
 *  \param y          new y-coord of the bus point
 *  \param whichone   bus point to modify
 */
void o_bus_modify(TOPLEVEL *toplevel, OBJECT *object,
		  int x, int y, int whichone)
{
  s_basic_move_grip(object, whichone, x, y);

  o_bus_recalc(object);
}
