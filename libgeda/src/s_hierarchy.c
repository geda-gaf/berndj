/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/*! \brief */
static int page_control_counter=0;

/*!
 *  \brief Search for schematic associated source files and load them.
 *  \par Function Description
 *  This function searches the associated source file referred by the
 *  <B>filename</B> and loads it.  If the <B>flag</B> is set to
 *  <B>HIERARCHY_NORMAL_LOAD</B> and the page is already in the list of
 *  pages it will return the <B>pid</B> of that page.
 *  If the <B>flag</B> is set to <B>HIERARCHY_FORCE_LOAD</B> then this
 *  function will load the page again with a new page id. The second case
 *  is mainly used by gnetlist where pushed down schematics MUST be unique.
 *
 *  \param [in] toplevel      The TOPLEVEL object.
 *  \param [in] filename      Schematic file name.
 *  \param [in] parent        The parent page of the schematic.
 *  \param [in] page_control
 *  \param [in] flag          sets whether to force load
 *  \return The number of pages loaded, -1 otherwise.
 *
 *  \note
 *  This function finds the associated source files and
 *  loads all up
 *  It only works for schematic files though
 *  this is basically push
 *  flag can either be HIERARCHY_NORMAL_LOAD or HIERARCHY_FORCE_LOAD
 *  flag is mainly used by gnetlist where pushed down schematics MUST be unique
 */
int s_hierarchy_down_schematic_single(TOPLEVEL *toplevel,
				      const gchar *filename, PAGE *parent,
				      int page_control, int flag) 
{
  gchar *string;
  PAGE *found;
  PAGE *forbear;

  string = s_slib_search_single(filename);
  if (string == NULL) {
    return -1;
  }

  switch (flag) {
    case HIERARCHY_NORMAL_LOAD:
    {
      gchar *normal_filename = f_normalize_filename(string, NULL);
      found = s_page_search(toplevel, normal_filename);
      g_free(normal_filename);
      
      if (found) {
	/* check whether this page is in the parents list */
	for (forbear = parent; 
	     forbear != NULL && found != forbear && forbear->up_page != NULL;
	     forbear = forbear->up_page)
	  ; /* void */

	if (found == forbear) {
	  s_log_message(_("hierarchy loop detected while visiting page:\n"
                          "  \"%s\"\n"), found->page_filename);
	  return -1;  /* error signal */
	}
        s_toplevel_goto_page(toplevel, found);
        if (page_control != 0) {
          found->page_control = page_control;
        }
        found->up_page = parent;
        g_free (string);
        return found->page_control;
      }
      
      found = s_page_new (toplevel, string);
      s_toplevel_goto_page(toplevel, found);
      
      f_open(toplevel, found, found->page_filename, NULL);
    }
    break;

    case HIERARCHY_FORCE_LOAD:
    {
      PAGE *page = s_page_new (toplevel, string);
      s_toplevel_goto_page(toplevel, page);
      f_open(toplevel, page, page->page_filename, NULL);
    }
    break;
  }

  if (page_control == 0) {
    page_control_counter++;
    toplevel->page_current->page_control = page_control_counter;
  } else {
    toplevel->page_current->page_control = page_control;
  }

  toplevel->page_current->up_page = parent;

  s_toplevel_goto_page(toplevel, toplevel->page_current);

  g_free (string);

  return(page_control_counter);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void s_hierarchy_down_symbol (TOPLEVEL *toplevel,
			      const CLibSymbol *symbol, PAGE *parent)
{
  PAGE *page;
  gchar *filename;

  filename = s_clib_symbol_get_filename (symbol);

  page = s_page_search (toplevel, filename);
  if (page) {
    s_toplevel_goto_page(toplevel, page);
    g_free (filename);
    return;
  }

  page = s_page_new (toplevel, filename);
  g_free(filename);

  s_toplevel_goto_page(toplevel, page);

  f_open(toplevel, page, page->page_filename, NULL);

  page->up_page = parent;
  page_control_counter++;
  page->page_control = page_control_counter;
}

/*! \brief Search for the parent page of a page in hierarchy.
 *  \par Function Description
 *  This function searches the parent page of page \a page in the
 *  hierarchy. It checks all the pages in the list \a page_list.
 *
 *  It returns a pointer on the page if found, NULL otherwise.
 *
 *  \note
 *  The page \a current_page must be in the list \a page_list.
 *
 *  \param [in] page_list    The list of pages in which to search.
 *  \param [in] current_page The reference page for the search.
 *  \returns A pointer on the page found or NULL if not found.
 */
PAGE *s_hierarchy_find_up_page (GedaPageList *page_list, PAGE *current_page)
{
  if (current_page->up_page == NULL) {
    s_log_message(_("There are no schematics above the current one!\n"));
    return NULL;
  }

  return current_page->up_page;
}

/*! \brief Replace a page with another in other pages' up_page lines.
 *  \par Function Description
 *  This functions searches all other pages in \a toplevel that refer to
 *  \a p_old as their up_page link, and rewrites those up_page links to
 *  point to \a p_new.  Call this when re-reading a page from file, or
 *  from undo state.
 *
 *  \note
 *  This function can safely accept a stale \a p_old, as long as no new
 *  pages have taken that pointer value.
 *
 *  \param [in] toplevel  The TOPLEVEL to search for pages.
 *  \param [in] p_old     The obsoleted PAGE.
 *  \param [in] p_new     The PAGE replacing \a p_old.
 */
void s_hierarchy_replace_page(TOPLEVEL *toplevel, PAGE *p_old, PAGE *p_new)
{
  GList *p_iter;

  for (p_iter = geda_list_get_glist(toplevel->pages);
       p_iter != NULL;
       p_iter = g_list_next(p_iter)) {
    PAGE *p_current = p_iter->data;
    if (p_current->up_page == p_old) {
      p_current->up_page = p_new;
    }
  }
}

struct traverse_context {
  TOPLEVEL *toplevel;
  PAGE *page;
  gint flags;
};

static enum visit_result hierarchy_traverse_one(OBJECT *o, void *userdata)
{
  struct traverse_context *tree_cursor = userdata;
  TOPLEVEL *toplevel = tree_cursor->toplevel;
  PAGE *page = tree_cursor->page;
  gint page_control = 0;

  /* only complex things like symbols can contain attributes */
  if (o->type == OBJ_COMPLEX) {
    char *filename = o_attrib_search_name_single_count(o, "source", 0);

    /* if above is NULL, then look inside symbol */
    if (filename == NULL) {
      filename = o_attrib_search_object(o, "source", 0);
    }

    if (filename != NULL) {
      /* we got a schematic source attribute
	 lets load the page and dive into it */
      page_control = s_hierarchy_down_schematic_single(toplevel,
						       filename,
						       page,
						       0,
						       HIERARCHY_NORMAL_LOAD);
      if (page_control != -1) {
	/* call the recursive function */
	s_hierarchy_traversepages(toplevel, toplevel->page_current,
				  tree_cursor->flags | HIERARCHY_INNERLOOP);
	s_toplevel_goto_page(toplevel, page);
      } else {
	s_log_message(_("ERROR in s_hierarchy_traverse: "
			"schematic not found: %s\n"),
		      filename);
      }

      g_free(filename);
      filename = NULL;
    }
  }

  return VISIT_RES_OK;
}

/*! \todo Traverse the tree of pages rooted at a given one.
 *  \brief
 *  \par Function Description
 *  This function traverses the hierarchy tree of pages and returns a flat
 *  list of pages that are below the given page. There are two
 *  <B>flags</B>: <B>HIERARCHY_NODUPS</B>: returns a list without
 *  duplicate pages 
 *  <B>HIERARCHY_POSTORDER</B>: traverses the hierarchy tree and
 *  returns a postorder list instead of preorder.
 *
 *  \param [in] toplevel  The TOPLEVEL object.
 *  \param [in] page      The root of the hierarchy to traverse.
 *  \param [in] flags     A combination of HIERARCHY_NODUPS and
 *                        HIERARCHY_POSTORDER.
 *  \return A GList of PAGE pointers.
 *
 *  \warning
 *  Call must g_list_free returned GList.
 */
GList *s_hierarchy_traversepages(TOPLEVEL *toplevel,
				 PAGE *page,
				 gint flags)
{
  static GList *pages = NULL;
  struct traverse_context tree_cursor = {
    .toplevel = toplevel,
    .page = page,
    .flags = flags,
  };

  /* init static variables the first time*/
  if (!(flags & HIERARCHY_INNERLOOP)) {
    pages = NULL;
  }

  /* preorder traversing */
  if (!(flags & HIERARCHY_POSTORDER)) {
    /* check whether we already visited this page */
    if ((flags & HIERARCHY_NODUPS)
	&& (g_list_find(pages, page) != NULL)) {
      return pages;  /* drop the page subtree */
      }
    pages = g_list_append(pages, page);
  }

  /* walk through the page objects and search for underlying schematics */
  s_visit_page(page, &hierarchy_traverse_one, &tree_cursor,
	       VISIT_DETERMINISTIC, 1);

  /* postorder traversing */
  if (flags & HIERARCHY_POSTORDER) {
    /* check whether we already visited this page */
    if ((flags & HIERARCHY_NODUPS) && (g_list_find(pages, page) != NULL)) {
      return pages;  /* don't append it */
    }
    pages = g_list_append(pages, page);
  }

  return pages;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 *  \note
 *  Test function which only prints the name of a page and its number.
 */
gint s_hierarchy_print_page(PAGE *p_current, void * data)
{
  printf("pagefilename: %s\n", p_current->page_filename);
  return 0;
}

/*! \brief Search for a page preceding a given page in hierarchy.
 *  \par Function Description
 *  This function searches the previous sibling of page \a page in the
 *  hierarchy. It checks all the pages preceding \a page in the list
 *  \a page_list.
 *
 *  It returns a pointer on the page if found, NULL otherwise.
 *
 *  \note
 *  The page \a current_page must be in the list \a page_list.
 *
 *  \param [in] page_list    The list of pages in which to search.
 *  \param [in] current_page The reference page for the search.
 *  \returns A pointer on the page found or NULL if not found.
  */
PAGE *s_hierarchy_find_prev_page (GedaPageList *page_list, PAGE *current_page)
{
  const GList *iter;

  iter = g_list_find (geda_list_get_glist (page_list), current_page);
  for (iter = g_list_previous (iter);
       iter != NULL;
       iter = g_list_previous (iter)) {

    PAGE *page = (PAGE *)iter->data;
    if (page->page_control == current_page->page_control) {
      return page;
    }
  }

  return NULL;
}

/*! \brief Search for a page following a given page in hierarchy.
 *  \par Function Description
 *  This function searches the next sibling of page \a page in the
 *  hierarchy. It checks all the pages following \a page in the list
 *  \a page_list.
 *
 *  It returns a pointer on the page if found, NULL otherwise.
 *
 *  \note
 *  The page \a current_page must be in the list \a page_list.
 *
 *  \param [in] page_list    The list of pages in which to search.
 *  \param [in] current_page The reference page for the search.
 *  \returns A pointer on the page found or NULL if not found.
  */
PAGE *s_hierarchy_find_next_page (GedaPageList *page_list, PAGE *current_page)
{
  const GList *iter;

  iter = g_list_find (geda_list_get_glist (page_list), current_page);
  for (iter = g_list_next (iter);
       iter != NULL;
       iter = g_list_next (iter)) {

    PAGE *page = (PAGE *)iter->data;
    if (page->page_control == current_page->page_control) {
      return page;
    }
  }

  return NULL;
}
