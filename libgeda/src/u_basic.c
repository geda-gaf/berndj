/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998, 1999, 2000 Kazu Hirata / Ales Hvezda
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif
#ifdef HAVE_STDARG_H
#include <stdarg.h>
#endif
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef HAVE_LIBUUID
#include <uuid/uuid.h>
#endif

#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
/* the delimiter is what is passed in or spaces */
/* count starts at zero */
char *u_basic_breakup_string(char const *string, char delimiter, int count)
{
  char *next_delimiter;
  int i;

  g_return_val_if_fail ((string != NULL),
                        NULL);

  /* Skip all the preceding fields. */
  for (i = 0; i < count; i++) {
    string = strchr(string, delimiter);
    if (string == NULL) {
      return NULL;
    }
    string++;
  }

  /* skip over any leading white space */
  while (*string == ' ') {
    string++;
  }

  next_delimiter = strchr(string, delimiter);

  if (next_delimiter != NULL) {
    /* Copy up to the next delimiter character. */
    char *retval;

    retval = g_strdup_printf("%.*s", (int) (next_delimiter - string), string);

    return retval;
  } else {
    /* Just return the rest of the string. */
    return g_strdup(string);
  }
}

char *u_basic_split(char **s, char delimiter)
{
  char *first_delimiter, *retval;

  first_delimiter = strchr(*s, delimiter);
  if (!first_delimiter) {
    retval = g_strdup(*s);
    *s = NULL;
    return retval;
  }

  retval = g_strndup(*s, first_delimiter - *s);
  *s = first_delimiter + 1;

  return retval;
}

/*! \brief Populate a hash table with a string of name=value pairs.
 *  \param [inout] tab     The hash table to receive the name=value pairs.
 *  \param [in] s          The string to break up into name=value pairs.
 *  \param [in] comma      The character separating name=value pairs.
 *  \param [in] equals     The character separating name from value in pairs.
 */
void u_basic_populate_hash(GHashTable *tab, char *s, char comma, char equals)
{
  char *leftovers, *pair;

  if (s == NULL) {
    return;
  }

  for (leftovers = s; leftovers && leftovers[0]; ) {
    char *first_equals;

    /* Break out one name=value pair. */
    pair = u_basic_split(&leftovers, comma);

    first_equals = strchr(pair, equals);
    if (first_equals) {
      /* Break the pair into name and value, then add to the hash table. */
      *first_equals = 0;

      g_hash_table_replace(tab,
			   g_strdup(pair),
			   g_strdup(first_equals + 1));

      *first_equals = equals;
    } else {
      /* Just add a name=1 pair to the hash table. */
      g_hash_table_replace(tab, g_strdup(pair), g_strdup("1"));
    }

    g_free(pair);
  }
}

/*! \brief Return a universally unique identifier.
 *  \par Some objects need to be uniquely and persistently identifiable.
 *  Inside a running program, pointers meet this responsibility, but a saved
 *  file needs these "cookies".
 *  \return A dynamically allocated universally unique identifier
 */
char *u_basic_make_uuid(void)
{
  char *s = NULL;

#ifdef HAVE_LIBUUID
  {
    uuid_t u;

    /* XXX Ick, but <uuid/uuid.h> doesn't seem to have anything better. */
    s = g_malloc(36 + 1);

    uuid_generate(u);
    uuid_unparse(u, s);
  }
#else /* !HAVE_LIBUUID */
  /* FIXME: Do just a *little* bit better - at least try to read from
   * /proc/sys/kernel/random/uuid.
   */
  {
    static unsigned long uuid_diversifier;
    char *timestamp;

#ifdef HAVE_DECL_G_DATE_TIME_FORMAT
    {
      GDateTime *now = g_date_time_new_now_utc();
      timestamp = g_date_time_format(now, "-%s-%N");
      g_date_time_unref(now);
    }
#else /* !HAVE_DECL_G_DATE_TIME_FORMAT */
    timestamp = g_strdup("");
#endif /* !HAVE_DECL_G_DATE_TIME_FORMAT */

    s = g_strdup_printf("%04x%s-%08lx",
			((unsigned int) getpid()) & 0xffff,
			timestamp,
			uuid_diversifier++ & 0xffffffffUL);
    g_free(timestamp);
  }
#endif /* !HAVE_LIBUUID */

  return s;
}

int u_basic_casecmp(char const *s1, char const *s2)
{
  char *p1, *p2;
  int retval;

  p1 = g_utf8_casefold(s1, -1);
  p2 = g_utf8_casefold(s2, -1);

  retval = g_utf8_collate(p1, p2);

  g_free(p1);
  g_free(p2);

  return retval;
}
