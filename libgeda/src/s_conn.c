/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#include <ctype.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/*! \file s_conn.c
 *  \brief The connection system
 *  
 *  The connection system stores and tracks the connections between
 *  connected <b>OBJECTS</b>. The connected OBJECTS are either
 *  <b>pins</b>, <b>nets</b> and <b>busses</b>.
 *  
 *  Each connection object with the type <b>st_conn</b> represents a
 *  single unidirectional relation to another object.
 *  
 *  The following figure with two nets and a pin shows the relations
 *  between connections and OBJECTS:
 *  
 *  \image html s_conn_overview.png
 *  \image latex s_conn_overview.pdf "Connection overview" width=14cm
 */


gboolean s_conn_connectible_p(OBJECT const *object)
{
  switch (object->type) {
    OBJECT const *o_current;
  case OBJ_BUS:
  case OBJ_NET:
  case OBJ_PIN:
    return TRUE;
  case OBJ_PLACEHOLDER:
    return FALSE;
  case OBJ_COMPLEX:
    /*
     * XXX Maybe just return TRUE unconditionally?
     * Depends on the purpose of this function: if its purpose is to be
     * accurate, it needs to test each of prim_objs. If its purpose is to
     * provide a quick decision that may include false positives but no false
     * negatives, an unconditional TRUE is okay.
     */
    for (o_current = object->complex->prim_objs;
	 o_current;
	 o_current = o_current->next) {
      if (s_conn_connectible_p(o_current)) {
	return TRUE;
      }
    }
    return FALSE;
  default:
    return FALSE;
  }
}

static void s_conn_make_half(OBJECT *p, OBJECT *q,
			     int conn_type, int x, int y,
			     int p_whichone, int q_whichone)
{
  CONN *new_conn;

  new_conn = s_conn_return_new(p, conn_type, x, y, q_whichone, p_whichone);

  /* do uniqness check */
  if (s_conn_uniq(q->conn_list, new_conn)) {
    q->conn_list = g_list_append(q->conn_list, new_conn);
  } else {
    g_free(new_conn);
  }
}

static void s_conn_make_connection(int conn_type, OBJECT *p, OBJECT *q,
				   int p_whichone, int q_whichone,
				   int x, int y)
{
  s_conn_make_half(p, q, conn_type, x, y, p_whichone, q_whichone);
  s_conn_make_half(q, p, conn_type, x, y, q_whichone, p_whichone);
}

static void s_conn_break_connection(OBJECT *p, OBJECT *q)
{
  s_conn_remove_other(p, q);
  s_conn_remove_other(q, p);
}

/*! \brief create a new connection object
 *  \par Function Description
 *  create a single st_conn object and initialize it with the 
 *  given parameters.
 *  
 *  \return The new connection object
 */
CONN *s_conn_return_new(OBJECT * other_object, int type, int x, int y,
			int whichone, int other_whichone)
{
  CONN *new_conn;

  new_conn = (CONN *) g_malloc(sizeof(CONN));

  if (GEDA_DEBUG) {
    printf("** creating: %s %d %d\n", other_object->name, x, y);
  }

  new_conn->other_object = other_object;
  new_conn->type = type;
  new_conn->x = x;
  new_conn->y = y;
  new_conn->whichone = whichone;
  new_conn->other_whichone = other_whichone;

  return (new_conn);
}

/*! \brief check if a connection is uniq in a list
 *  \par Function Description
 *  This function checks if there's no identical connection
 *  in the list of connections.
 *  \param conn_list list of connection objects
 *  \param input_conn single connection object.
 *  \return TRUE if the CONN structure is unique, FALSE otherwise.
 */
int s_conn_uniq(GList const *conn_list, CONN const *input_conn)
{
  GList const *c_current;
  CONN const *conn;

  c_current = conn_list;
  while (c_current != NULL) {
    conn = c_current->data;

    if (conn->other_object == input_conn->other_object &&
        conn->x == input_conn->x && conn->y == input_conn->y &&
        conn->type == input_conn->type) {
      return (FALSE);
    }

    c_current = g_list_next(c_current);
  }

  return (TRUE);
}

/*! \brief remove a object from the connection list of another object
 *  \par Function Description
 *  This function removes the OBJECT <b>to_remove</b> from the connection
 *  list of the OBJECT <b>other_object</b>.
 *  \param other_object OBJECT from that the to_remove OBJECT needs to be removed
 *  \param to_remove OBJECT to remove
 *  \return TRUE if a connection has been deleted, FALSE otherwise
 */
int s_conn_remove_other(OBJECT *other_object,
			OBJECT * to_remove)
{
    GList *c_current = NULL;
    CONN *conn = NULL;

    c_current = other_object->conn_list;
    while (c_current != NULL) {
	conn = (CONN *) c_current->data;

	if (conn->other_object == to_remove) {
	    other_object->conn_list =
		g_list_remove(other_object->conn_list, conn);

	    if (GEDA_DEBUG) {
	      printf("Found other_object in remove_other\n");
	      printf("Freeing other: %s %d %d\n", conn->other_object->name,
		     conn->x, conn->y);
	    }

	    /* Do not write modify c_current like this, since this will cause */
	    /* very nasty data corruption and upset glib's memory slice */
	    /* allocator. */
	    /* c_current->data = NULL;   Do not comment in */

	    g_free(conn);

#if 0 /* this does not work right */
            if (other_object->type == OBJ_BUS &&
                other_object->conn_list == NULL) {
              other_object->bus_ripper_direction = 0;
            }
#endif
            
	    return (TRUE);
	}

	c_current = g_list_next(c_current);
    }

    return (FALSE);
}

/*! \brief remove an OBJECT from the connection system
 *  \par Function Description
 *  This function removes all connections from and to the OBJECT
 *  <b>to_remove</b>.
 *  \param to_remove OBJECT to unconnected from all other objects
 */
void s_conn_remove(OBJECT *to_remove)
{
  GList *c_current;
  CONN *conn;

  if (to_remove->type != OBJ_PIN && to_remove->type != OBJ_NET &&
      to_remove->type != OBJ_BUS) {
    return;
  }

  c_current = to_remove->conn_list;
  while (c_current != NULL) {
    conn = (CONN *) c_current->data;

    if (GEDA_DEBUG) {
      printf("Removing: %s\n", conn->other_object->name);
    }

    while (s_conn_remove_other(conn->other_object, to_remove)) {
      /* Keep removing until all references are gone. */
    }

    if (GEDA_DEBUG) {
      printf("returned from remove_other\n");
      printf("Freeing: %s %d %d\n", conn->other_object->name, conn->x,
	     conn->y);
    }
    c_current->data = NULL;
    g_free(conn);
    c_current = g_list_next(c_current);
  }

  if (GEDA_DEBUG) {
    printf("length: %d\n", g_list_length(to_remove->conn_list));
  }

  g_list_free(to_remove->conn_list);
  to_remove->conn_list = NULL; /*! \todo Memory leak? TODO hack */

#if 0 /* this does not work right either */
  if (to_remove->type == OBJ_BUS)
  {
    to_remove->bus_ripper_direction = 0;
  }
#endif
}

/*! \brief remove a complex OBJECT from the connection system
 *  \par Function Description
 *  This function removes all connections from and to the underlying 
 *  OBJECTS of the given complex OBJECT
 *  <b>to_remove</b>.
 *  \param to_remove OBJECT to unconnected from all other objects
 *
 */
void s_conn_remove_complex(OBJECT *to_remove)
{
  OBJECT *o_current;
  
  if (to_remove->type != OBJ_COMPLEX && to_remove->type != OBJ_PLACEHOLDER) {
    return;
  }

  o_current = to_remove->complex->prim_objs;
  while (o_current != NULL) {
    switch (o_current->type) {
      case (OBJ_NET):
      case (OBJ_PIN):
      case (OBJ_BUS):
        s_conn_remove(o_current);
        break;

    }
    o_current = o_current->next;
  }
}

int s_conn_test_endpoint(OBJECT *object, OBJECT *other_object,
			 int grip_x, int grip_y, enum grip_t other_whichone)
{
  int add_conn_endpoint = FALSE;
  int whichone;

  /* check for net / bus compatibility */
  /* you cannot connect the endpoint of a bus to a net */
  /* and the reverse is true as well */
  if ((object->type == OBJ_NET && other_object->type == OBJ_BUS) ||
      (object->type == OBJ_BUS && other_object->type == OBJ_NET)) {
    return GRIP_NONE;
  }

  whichone = s_basic_search_grips_exact(object, grip_x, grip_y);
  switch (whichone) {
  case GRIP_1:
  case GRIP_2:
    /* Only the active end of a pin can connect to other objects. */
    if (object->type == OBJ_PIN) {
      if (whichone == object->whichend) {
	if (other_object->type != OBJ_PIN ||
	    other_whichone == other_object->whichend ) {
	  add_conn_endpoint = TRUE;
	}
      }
    } else if (other_object->type == OBJ_PIN) {
      if (other_whichone == other_object->whichend) {
	if (object->type != OBJ_PIN ||
	    whichone == object->whichend ) {
	  add_conn_endpoint = TRUE;
	}
      }
    } else {
      add_conn_endpoint = TRUE;
    }
    break;
  case GRIP_NONE:
    return GRIP_NONE;
  default:
    /* Add switch cases if LINE objects get more grips. */
    g_warn_if_reached();
    return GRIP_NONE;
  }

  if (add_conn_endpoint) {
    if (GEDA_DEBUG) {
      printf("other_whichone: %d\n", other_whichone);
      printf("whichone: %d\n", whichone);
      printf("object: %d\n", object->whichend);
      printf("other: %d\n\n", other_object->whichend);
    }

    return whichone;
  }

  return GRIP_NONE;
}

gboolean s_conn_test_midpoint(OBJECT *object, OBJECT *other_object, int x, int y)
{
  switch (other_object->type) {
  case OBJ_PIN:
    /* pins are not allowed midpoint connections (on them) */
    return FALSE;
  case OBJ_NET:
    if (object->type == OBJ_BUS) {
      /* you cannot have the middle of net connect to buses */
      return FALSE;
    }
    break;
  case OBJ_BUS:
    break;
  default:
    return FALSE;
  }

  /* do object's endpoints touch other_object? */
  /* check for midpoint of other_object */
  if (o_line_subset_p(other_object, x, y)) {
    switch (s_basic_search_grips_exact(other_object, x, y)) {
    case GRIP_1:
    case GRIP_2:
      /* Endpoint connections are handled independently. */
      return FALSE;
    default:
      if (GEDA_DEBUG) {
	printf("%d endpoint to %s midpoint of: %s\n",
	       s_basic_search_grips_exact(object, x, y),
	       object->name,
	       other_object->name);
      }
      return TRUE;
    }
  }

  return FALSE;
}

static gboolean s_conn_check_grip(OBJECT *other_object,
				  int grip_x, int grip_y,
				  enum grip_t other_whichone, void *userdata)
{
  OBJECT *object = userdata;
  int whichone; /* For grip constants on object. */

  if (object == other_object) {
    /* Don't bother checking other grips; it'll still be the same object. */
    /* This block can execute only if a OBJ_COMPLEX is its own prim_obj. */
    g_warn_if_reached();
    return FALSE;
  }

  if (!s_conn_connectible_p(other_object)) {
    return FALSE;
  }

  whichone = s_conn_test_endpoint(object, other_object, grip_x, grip_y,
				  other_whichone);
  if (whichone != GRIP_NONE) {
    s_conn_make_connection(CONN_ENDPOINT, object, other_object,
			   whichone, other_whichone, grip_x, grip_y);
    return FALSE;
  }

  if (other_object->type != OBJ_PIN || other_object->whichend == other_whichone) {
    if (s_conn_test_midpoint(other_object, object, grip_x, grip_y)) {
      s_conn_make_connection(CONN_MIDPOINT, object, other_object,
			     GRIP_NONE, other_whichone, grip_x, grip_y);
      return FALSE;
    }
  }

  return FALSE;
}

static gboolean s_conn_veto(OBJECT *p, OBJECT *q)
{
  /* check for pin / bus compatibility */
  /* you cannot connect pins and buses at all */
  if ((p->type == OBJ_PIN && q->type == OBJ_BUS) ||
      (p->type == OBJ_BUS && q->type == OBJ_PIN)) {
    s_conn_remove_other(p, q);
    s_conn_remove_other(q, p);
    return TRUE;
  }

  return FALSE;
}

static void s_conn_update_pair(OBJECT *object, OBJECT *other_object)
{
  s_conn_break_connection(object, other_object);
  switch (object->type) {
    int x1, y1, x2, y2;
  case OBJ_BUS:
  case OBJ_NET:
  case OBJ_PIN:
    s_basic_get_grip(object, GRIP_1, &x1, &y1);
    if (object->type != OBJ_PIN || object->whichend == GRIP_1) {
      if (s_conn_test_midpoint(object, other_object, x1, y1)) {
	s_conn_make_connection(CONN_MIDPOINT, object, other_object,
			       GRIP_1, GRIP_NONE, x1, y1);
	return;
      }
    }
    s_basic_get_grip(object, GRIP_2, &x2, &y2);
    if (object->type != OBJ_PIN || object->whichend == GRIP_2) {
      if (s_conn_test_midpoint(object, other_object, x2, y2)) {
	s_conn_make_connection(CONN_MIDPOINT, object, other_object,
			       GRIP_2, GRIP_NONE, x2, y2);
	return;
      }
    }
    break;
  }
  other_object->grip_foreach_func(other_object, &s_conn_check_grip,
				  object);
}

static enum visit_result
s_conn_update_complex_atom(OBJECT *atom, void *context)
{
  OBJECT *object = context;

  if (object == atom) {
    /* object might be one of the COMPLEX's prim_objs. */
    return VISIT_RES_OK;
  }

  if (s_conn_veto(object, atom)) {
    return VISIT_RES_OK;
  }

  /* COMPLEX objects don't reasonably contain other COMPLEX objects. */
  /* if (atom->type == OBJ_COMPLEX) { recurse(); } */

  s_conn_update_pair(object, atom);

  return VISIT_RES_OK;
}

static enum visit_result
update_object_one_other(OBJECT *other_object, void *userdata)
{
  OBJECT *object = userdata;

  if (other_object == object) {
    /* Objects don't connect to themselves. */
    return VISIT_RES_OK;
  }

  if (!s_conn_connectible_p(other_object)) {
    /* Don't bother checking grips; other_object cannot connect. */
    return VISIT_RES_OK;
  }

  if (s_conn_veto(object, other_object)) {
    return VISIT_RES_OK;
  }

  switch (other_object->type) {
  case OBJ_COMPLEX:
    s_visit_object(other_object, &s_conn_update_complex_atom, object,
		   VISIT_DETERMINISTIC, 1);
    break;
  case OBJ_BUS:
  case OBJ_NET:
  case OBJ_PIN:
    s_conn_update_pair(object, other_object);
    break;
  default:
    /* s_conn_connectible_p filters out anything else already. */
    g_return_val_if_reached(VISIT_RES_OK);
    break;
  }

  return VISIT_RES_OK;
}

/*! \brief add an OBJECT to the connection system
 *  \par Function Description
 *  This function searches for all geometrical connections of the OBJECT
 *  \a object to all other connectable objects. It adds connections
 *  to the object and from all other
 *  objects to this one.
 *  \param [in] page   The PAGE on which \a object exists
 *  \param [in] object OBJECT to add into the connection system
 */
void s_conn_update_object(PAGE *page, OBJECT *object)
{
  if (!s_conn_connectible_p(object)) {
    /* Save work by returning early. NOT a contract. */
    return;
  }

  switch (object->type) {
  case OBJ_COMPLEX:
    /* Don't put the COMPLEX itself in the connection system, only its parts. */
    s_conn_update_complex(page, object);
    return;
  }

  s_visit_page(page, &update_object_one_other, object, VISIT_DETERMINISTIC, 1);

  if (GEDA_DEBUG) {
    s_conn_print(object->conn_list);
  }
}

/*! \brief add an complex OBJECT to the connection system
 *  \par Function Description
 *  This function adds all underlying OBJECTs of a complex OBJECT
 *  <b>complex</b> into the connection system.
 *  \param complex complex OBJECT to add into the connection system
 */
void s_conn_update_complex(PAGE *page, OBJECT *complex)
{
  OBJECT *o_current;

  o_current = complex->complex->prim_objs;
  while (o_current != NULL) {
    switch (o_current->type) {
      case (OBJ_PIN):
      case (OBJ_NET):
      case (OBJ_BUS):
        s_conn_update_object(page, o_current);
        break;
    }
    o_current = o_current->next;
  }
}

/*! \brief print all connections of a connection list
 *  \par Function Description
 *  This is a debugging function to print a List of connections.
 *  \param conn_list GList of connection objects
 */
void s_conn_print(GList const *conn_list)
{
  CONN const *conn;
  GList const *cl_current;

  printf("\nStarting s_conn_print\n");
  cl_current = conn_list;
  while (cl_current != NULL) {
    conn = (CONN *) cl_current->data;
    printf("-----------------------------------\n");
    printf("other object: %s\n", conn->other_object->name);
    printf("type: %d\n", conn->type);
    printf("x: %d y: %d\n", conn->x, conn->y);
    printf("whichone: %d\n", conn->whichone);
    printf("other_whichone: %d\n", conn->other_whichone);
    printf("-----------------------------------\n");

    cl_current = g_list_next(cl_current);
  }
}

/*! \brief Search for net in existing connections.
 *  \par Function Description
 *  This method searches the connection list for the first matching
 *  connection with the given x, y, and whichone endpoint.
 *
 *  \param [in] new_net    Net OBJECT to compare to.
 *  \param [in] whichone   The connection number to check.
 *  \param [in] conn_list  List of existing connections to compare
 *                         <B>new_net</B> to.
 *  \return A list of objects connected at the given point.
 */
GList *s_conn_net_search(OBJECT const *new_net, int whichone, GList const *conn_list)
{
  CONN const *conn;
  GList const *cl_current;
  GList *return_list = NULL;

  cl_current = conn_list;
  while (cl_current != NULL) {

    conn = (CONN *) cl_current->data;
    if (conn != NULL && conn->whichone == whichone && 
	s_basic_search_grips_exact(new_net, conn->x, conn->y) == whichone)
    {
      return_list = g_list_append(return_list, conn->other_object);
    }

    cl_current = g_list_next(cl_current);
  }
 
  return return_list;
}

/*! \brief get a list of all objects connected to this one
 *  \par Function Description
 *  This function gets all other_object from the connection
 *  list of the current object. If an <b>input_list</b> is given, the other
 *  objects are appended to that list. If the input list is <b>NULL</b>, a new
 *  list is returned
 *  \param input_list GList of OBJECT's or NULL
 *  \param object OBJECT to get other OBJECTs from
 *  \return A GList of objects
 *  \warning
 *  Caller must g_list_free returned GList pointer.
 *  Do not free individual data items in list.
 */
GList *s_conn_return_others(GList *input_list, OBJECT const *object)
{
  CONN const *conn;
  GList const *cl_current;
  GList *return_list=NULL;

  return_list = input_list;
  
  cl_current = object->conn_list;
  while (cl_current != NULL) {
    conn = cl_current->data;
    
    if (conn->other_object && conn->other_object != object) {
      /* FIXME: prepend + reverse */
      return_list = g_list_append(return_list, conn->other_object);
    }
        
    cl_current = g_list_next(cl_current);
  }

  return(return_list);
}

/*! \brief get a list of all objects connected to this complex one
 *  \par Function Description
 *  This function gets all other_object from the connection
 *  list of all underlying OBJECTs of the given complex OBJECT.
 *  If an <b>input_list</b> is given, the other
 *  objects are appended to that list. If the input list is <b>NULL</b>, a new
 *  list is returned
 *  \param input_list GList of OBJECT's or NULL
 *  \param object complex OBJECT to get other objects from
 *  \return A GList of objects
 *  \warning
 *  Caller must g_list_free returned GList pointer.
 *  Do not free individual data items in list.
 */
GList *s_conn_return_complex_others(GList *input_list, OBJECT const *object)
{
  OBJECT *o_current;
  CONN *conn;
  GList *cl_current;
  GList *return_list=NULL;

  if (object->type != OBJ_COMPLEX && object->type != OBJ_PLACEHOLDER) {
    return(NULL);
  }

  return_list = input_list;
  
  o_current = object->complex->prim_objs;
  while(o_current != NULL) {
    cl_current = o_current->conn_list;
    while (cl_current != NULL) {

      conn = (CONN *) cl_current->data;
    
      if (conn->other_object && conn->other_object != o_current) {
        return_list = g_list_append(return_list, conn->other_object);
      }
        
      cl_current = g_list_next(cl_current);
    }

    o_current = o_current->next;
  }
  
  return(return_list);
}
