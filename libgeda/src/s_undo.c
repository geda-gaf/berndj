/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#include <ctype.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

static void s_undo_prune(TOPLEVEL *toplevel, PAGE *page);

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
GList *s_undo_add(TOPLEVEL *toplevel, PAGE *page, int type,
		  char *filename, OBJECT *object_head,
		  int left, int top, int right, int bottom,
		  int page_control, PAGE *up_page)
{
  GList *new_state;
  UNDO *u_new;

  u_new = (UNDO *) g_malloc(sizeof(UNDO));

  u_new->filename = g_strdup (filename);
	
  if (object_head != NULL) {
    u_new->object_head = object_head;	
  } else {
    u_new->object_head = NULL;	
  }

  u_new->type = type;

  u_new->left = left;
  u_new->top = top;
  u_new->right = right;
  u_new->bottom = bottom;

  u_new->page_control = page_control;
  u_new->up_page = up_page;

  s_undo_prune(toplevel, page);
  new_state = g_list_prepend(page->undo_newest, u_new);
  page->undo_newest = new_state;
  page->undo_current = new_state;

  return new_state;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void s_undo_print_all(PAGE *page)
{
  GList *iter;

  printf("START printing undo ********************\n");
  printf("NEWEST\n");
  for (iter = page->undo_newest; iter != NULL; iter = iter->next) {
    UNDO *u_current = iter->data;

    if (u_current->filename) printf("%s\n", u_current->filename);
		
    if (u_current->object_head) {
      printf("%s\n", u_current->object_head->name);	
      print_struct_forw(u_current->object_head);
    }
		
    printf("\t%d %d %d %d%s\n", u_current->left, u_current->top,
           u_current->right, u_current->bottom,
	   iter == page->undo_current ? " (current)" : "");
  }
  printf("OLDEST\n");
  printf("Number of levels: %d\n", s_undo_levels(page));
  printf("DONE printing undo ********************\n");
  printf("\n");

}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void s_undo_free_all(TOPLEVEL *toplevel, PAGE *page)
{
  GList *iter;

  for (iter = page->undo_newest; iter != NULL; iter = iter->next) {
    UNDO *u_current = iter->data;

    g_free(u_current->filename);
		
    if (u_current->object_head) {
      s_delete_list_fromstart(toplevel,
                              u_current->object_head);
      u_current->object_head = NULL;
    }

    g_free(u_current);
  }

  g_list_free(page->undo_newest);
  page->undo_newest = NULL;
  page->undo_current = NULL;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
static void s_undo_prune(TOPLEVEL *toplevel, PAGE *page)
{
  while (page->undo_newest != page->undo_current) {
    UNDO *u_current = page->undo_newest->data;

    if (u_current->filename) {
      unlink(u_current->filename);
      g_free(u_current->filename);
    }

    if (u_current->object_head) {
      s_delete_list_fromstart(toplevel,
                              u_current->object_head);
      u_current->object_head = NULL;
    }

    g_free(u_current);

    page->undo_newest = g_list_delete_link(page->undo_newest,
					   page->undo_newest);
  }
}

void s_undo_trim(TOPLEVEL *toplevel, PAGE *page, int levels)
{
  GList *iter;
  int i;

  if (GEDA_DEBUG) {
    printf("Trimming: %d levels\n", levels);
  }

  for (iter = page->undo_newest, i = 0; iter != NULL; i++) {
    if (i >= levels) {
      UNDO *u_current = iter->data;
      GList *iter_next;

      if (u_current->filename) {
	if (GEDA_DEBUG) {
	  printf("Freeing: %s\n", u_current->filename);
	}
        unlink(u_current->filename);
        g_free(u_current->filename);
      }

      if (u_current->object_head) {
        s_delete_list_fromstart(toplevel,
                                u_current->object_head);
        u_current->object_head = NULL; 
      }

      g_free(u_current);

      iter_next = iter->next;
      page->undo_newest = g_list_delete_link(page->undo_newest, iter);
      iter = iter_next;
    } else {
      iter = iter->next;
    }
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
int s_undo_levels(PAGE const *page)
{
  int count = 0;
  GList const *iter;

  for (iter = page->undo_newest; iter != NULL; iter = iter->next) {
    UNDO const *u_current = iter->data;

    if (u_current->filename || u_current->object_head) {
      count++;
    }
  }

  return(count);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_undo_copy_prev_memento(GList *link, enum undo_type_t undo_type)
{
  UNDO *u_current;
  GList *iter;

  if (link == NULL) {
    return;
  }

  u_current = link->data;

  /* Dig back in history until we find what we want. */
  for (iter = link->next; iter != NULL; iter = iter->next) {
    UNDO *u_before = iter->data;

    if (undo_type == UNDO_DISK && u_before->filename) {
      u_current->filename = u_before->filename;
      return;
    } else if (undo_type == UNDO_MEMORY && u_before->object_head) {
      u_current->object_head = u_before->object_head;
      return;
    }
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void s_undo_init(PAGE *p_current)
{
	p_current->undo_newest = NULL;
	p_current->undo_current = NULL;
}
