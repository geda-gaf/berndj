/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#include <math.h>

#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/*! \brief Find the closest grid coordinate.
 *  \par Function Description
 *  This function snaps the current input coordinate to the
 *  closest grid coordinate.
 *
 *  \param [in] toplevel  The TOPLEVEL object.
 *  \param [in] input      The coordinate to snap.
 *  \return The closest grid coordinate to the input.
 */
int snap_grid(TOPLEVEL *toplevel, int input)
{
  int p, m, n;
  int sign, value, snap_size;
	
  if (!toplevel->snap || (toplevel->snap_size <= 0)) {
    return(input);
  }

		
  snap_size = toplevel->snap_size;

  /* this code was inspired from killustrator, it's much simpler than mine */
  sign = ( input < 0 ? -1 : 1 );
  value = abs(input);

  p = value / snap_size;
  m = value % snap_size;
  n = p * snap_size;
  if (m > snap_size / 2) {
    n += snap_size;
  }

  if (GEDA_DEBUG) {
    printf("p: %d\n", p);
    printf("m: %d\n", m);
    printf("m > snap_size / 2: %d\n", (m > snap_size / 2));
    printf("n: %d\n", n);
    printf("n*s: %d\n", n*sign);
  }

  return(sign*n);
}                               

/*! \brief Checks if a point is snapped.
 *  \par Function Description
 *  This function checks if a point is snapped.
 *
 *  \param [in] val  The point to check.
 *  \return 0 if point (x) is snapped, non-zero otherwise
 *
 *  \note This function is unused for now.
 */
int on_snap(int val)
{
  return( (val / 100)*100 - val);
}

/*! \brief Encode WORLD coordinates as halfspace matrix.
 *  \par Function Description
 *  This function takes a point and checks if it is in the bounds
 *  of the given page. It handles points with WORLD coordinates.
 *  
 *  \param [in]  page       The PAGE object.
 *  \param [in]  point      The point in WORLD coordinates to be checked.
 *  \param [out] halfspace  The created HALFSPACE structure.
 *
 *  \warning halfspace must be allocated before this function is called
 */
static void WORLDencode_halfspace(PAGE const *page, sPOINT *point, HALFSPACE *halfspace)
{
  halfspace->left = point->x < page->left;
  halfspace->right = point->x > page->right;
  halfspace->bottom = point->y > page->bottom;
  halfspace->top = point->y < page->top;
}

/*! \brief Check if a set of coordinates are within a clipping region
 *  \par Function Description
 *  This function will check if the given set of coordinates
 *  are within a clipping region. No action will be taken to change
 *  the coordinates.
 *
 *  \param [in]     page   The PAGE whose viewport defines the clipping region.
 *  \param [in,out] x1     x coordinate of the first screen point.
 *  \param [in,out] y1     y coordinate of the first screen point.
 *  \param [in,out] x2     x coordinate of the second screen point.
 *  \param [in,out] y2     y coordinate of the second screen point.
 *  \return TRUE if coordinates are now visible, FALSE otherwise.
 */
int clip_nochange(PAGE const *page, int x1, int y1, int x2, int y2)
{
  HALFSPACE half1, half2; 
  HALFSPACE tmp_half;
  sPOINT tmp_point;
  sPOINT point1, point2;
  float slope;
  int in1, in2, done;
  int known_visible;
  int w_l, w_t, w_r, w_b;

  point1.x = x1;
  point1.y = y1;
  point2.x = x2;
  point2.y = y2;

  /*printf("before: %d %d %d %d\n", x1, y1, x2, y2);*/

  w_l = page->left;
  w_t = page->top;
  w_r = page->right;
  w_b = page->bottom;

  done = FALSE;
  known_visible = FALSE;

  do {
    WORLDencode_halfspace(page, &point1, &half1);
    WORLDencode_halfspace(page, &point2, &half2);

    if (GEDA_DEBUG) {
      printf("starting loop\n");
      printf("1 %d %d %d %d\n", half1.left, half1.top, half1.right, half1.bottom);
      printf("2 %d %d %d %d\n", half2.left, half2.top, half2.right, half2.bottom);
    }

    in1 = (!half1.left) && 
      (!half1.top) && 
      (!half1.right) && 
      (!half1.bottom);

    in2 = (!half2.left) &&  
      (!half2.top) && 
      (!half2.right) && 
      (!half2.bottom);


    if (in1 && in2) { /* trivially accept */
      done = TRUE;
      known_visible = TRUE;
    } else if ( ((half1.left && half2.left) || 
                 (half1.right && half2.right)) ||
                ((half1.top && half2.top) || 
                 (half1.bottom && half2.bottom)) ) {
      done = TRUE; /* trivially reject */
      known_visible = FALSE;
    } else { /* at least one point outside */
      if (in1) {
        tmp_half = half1;
        half1 = half2; 
        half2 = tmp_half;

        tmp_point = point1; 
        point1 = point2; 
        point2 = tmp_point;
      }

      if (point2.x == point1.x) { /* vertical line */
        if (half1.top) {
          point1.y = w_t;
        } else if (half1.bottom) {
          point1.y = w_b;
        }
      } else { /* not a vertical line */

				/* possible fix for alpha core dumping */
				/* assume the object is visible */
        if ((point2.x - point1.x) == 0) {
          return(TRUE);
        }

        slope = (float) (point2.y - point1.y) / 
          (float) (point2.x - point1.x); 

				/* possible fix for alpha core dumping */
				/* assume the object is visible */
        if (slope == 0.0) {
          return(TRUE);
        }

        if (half1.left) {
          point1.y = point1.y + 
            (w_l - point1.x) * slope;
          point1.x = w_l;
        } else if (half1.right) {
          point1.y = point1.y + 
            (w_r - point1.x) * slope;
          point1.x = w_r;
        } else if (half1.bottom) {
          point1.x = point1.x +
            (w_b - point1.y) / slope;
          point1.y = w_b;
        } else if (half1.top) {
          point1.x = point1.x + 
            (w_t - point1.y) / slope;
          point1.y = w_t;
        }
      } /* end of not a vertical line */
    } /* end of at least one outside */
  } while (!done);

  return known_visible;
}

/*! \brief Check if a bounding box is visible on the screen.
 *  \par Function Description
 *  This function checks if a given bounding box is visible on the screen.
 *
 *  WARNING: top and bottom are mis-named in world-coords,
 *  top is the smallest "y" value, and bottom is the largest.
 *  Be careful! This doesn't correspond to what you'd expect.
 *
 *  \param [in] page       The PAGE object.
 *  \param [in] wleft      Left coordinate of the bounding box.
 *  \param [in] wtop       Top coordinate of the bounding box.
 *  \param [in] wright     Right coordinate of the bounding box.
 *  \param [in] wbottom    Bottom coordinate of the bounding box.
 *  \return TRUE if bounding box is visible, FALSE otherwise
 */
int visible(PAGE const *page, int wleft, int wtop, int wright, int wbottom)
{
  int known_visible=FALSE;

  known_visible = clip_nochange(page, wleft, wtop, wright, wtop);

  if (GEDA_DEBUG) {
    printf("vis1 %d\n", known_visible);
  }

  if (!known_visible) {
    known_visible = clip_nochange(page, wleft, wbottom, wright, wbottom);
  } else {
    return known_visible;
  } 

  if (GEDA_DEBUG) {
    printf("vis2 %d\n", known_visible);
  }

  if (!known_visible) {
    known_visible = clip_nochange(page, wleft, wtop, wleft, wbottom);
  } else {
    return known_visible;
  } 

  if (GEDA_DEBUG) {
    printf("vis3 %d\n", known_visible);
  }

  if (!known_visible) {
    known_visible = clip_nochange(page, wright, wtop, wright, wbottom);
  } else {
    return known_visible;
  } 

  if (GEDA_DEBUG) {
    printf("vis4 %d\n", known_visible);
    printf("%d %d %d\n", wleft, page->top, wright);
    printf("%d %d %d\n", wtop, page->top, wbottom);
    printf("%d %d %d\n", wleft, page->right, wright);
    printf("%d %d %d\n", wtop, page->bottom, wbottom);
  }

  /*
   * now check to see if bounding box encompasses the entire viewport.
   * We only need to test if one point on the screen clipping boundary
   * is inside the bounding box of the object.
   */
  if (page->left >= wleft  &&
      page->left <= wright &&
      page->top >= wtop    &&
      page->top <= wbottom ) {
    known_visible = 1;
  }

  if (GEDA_DEBUG) {
    printf("vis5 %d\n", known_visible);
  }

  return known_visible;
}

/*! \brief Rotate a point by an arbitrary angle.
 *  \par Function Description
 *  This function will rotate a point coordinate by an arbitrary angle
 *  and return the new coordinate in the newx and newy parameters.
 *
 *  \param [in]  x      Input point x coordinate.
 *  \param [in]  y      Input point y coordinate.
 *  \param [in]  angle  Angle to rotate in degrees.
 *  \param [out] newx   Output point x coordinate.
 *  \param [out] newy   Output point y coordinate.
 */
void rotate_point(int x, int y, int angle, int *newx, int *newy)
{
  double costheta, sintheta;
  double rad;

  rad = angle*M_PI/180;

  costheta = cos(rad);
  sintheta = sin(rad);

  *newx = x * costheta - y * sintheta;
  *newy = x * sintheta + y * costheta;
}

/*! \brief Rotate point in 90 degree increments only.
 *  \par Function Description
 *  This function takes a point coordinate and rotates it by
 *  90 degrees at a time.  The new point coordinate is returned
 *  in newx and newy.
 *
 *  \param [in]  x      Input point x coordinate.
 *  \param [in]  y      Input point y coordinate.
 *  \param [in]  angle  Angle to rotate by (90 degree increments only).
 *  \param [out] newx   Output point x coordinate.
 *  \param [out] newy   Output point y coordinate.
 */
void rotate_point_90(int x, int y, int angle, int *newx, int *newy)
{
  double costheta=1; 
  double sintheta=0;

  /* I could have used sine/cosine for this, but I want absolute 
   * accuracy */
  switch(angle) {

    case(0):
      *newx = x;
      *newy = y; 
      return;
      break;
		
    case(90):
      costheta = 0;
      sintheta = 1;
      break;
		
    case(180):
      costheta = -1;
      sintheta = 0;
      break;
		
    case(270):
      costheta = 0;
      sintheta = -1;
      break;
  }

  *newx = x * costheta - y * sintheta;
  *newy = x * sintheta + y * costheta;
}

/*! \brief Convert Paper size to World coordinates.
 *  \par Function Description
 *  This function takes the paper size and converts it to
 *  world coordinates. It supports landscape with a fixed aspect ratio.
 *
 *  \param [in]  width   Paper width. (units?)
 *  \param [in]  height  Paper height. (units?)
 *  \param [in]  border  Paper border size. (units?)
 *  \param [out] right   Right world coordinate. (units?)
 *  \param [out] bottom  Bottom world coordinate. (units?)
 *
 *  \todo Support more modes than just landscape only mode.
 */
void PAPERSIZEtoWORLD(int width, int height, int border, int *right, int *bottom)
{
  float aspect;

  aspect = (float) width / (float) height;

  if (GEDA_DEBUG) {
    printf("%f\n", aspect);
  }

  if (aspect < 1.333333333) {
    /* is this rint really needed? */
#ifdef HAS_RINT
    *right = (int) rint(width+border + 
			((height+border)*1.33333333 - (width+border)));
#else 
    *right = (int) width+border + 
      ((height+border)*1.33333333 - (width+border));
#endif
    *bottom = height+border;
  } else {
    *right = (int) width+border;	
    *bottom = (int) height+border + ((width+border)/1.33333333 - (height+border));
  }
	
  if (GEDA_DEBUG) {
    aspect = (float) *right / (float) *bottom;
    printf("%f\n", aspect);
  }
}


/*! \brief Rounds numbers by a power of 10.
 *  \par Function Description
 *  This function will round numbers using a power of 10 method.
 *  For example:
 *                1235 rounds to 1000
 *                 670 rounds to  500
 *               0.234 rounds to  0.2
 *  integer values would be enough if there are no numbers smaller than 1 (hw)
 *
 *  \param [in] unrounded  The number to be rounded.
 *  \return The rounded number.
 */
/* rounds for example 1235 to 1000, 670 to 500, 0.234 to 0.2 ...
int would be enough if there are no numbers smaller 1 (hw)*/
double round_5_2_1(double unrounded)
{
  int digits;
  double betw_1_10;
	
  /*only using the automatic cast */
  digits = log10(unrounded);
  /* creates numbers between 1 and 10 */
  betw_1_10 = unrounded / pow(10,digits);
	
  if (betw_1_10 < 1.5) {
    return(pow(10,digits));
  }
  if (betw_1_10 > 1.4 && betw_1_10 < 3.5 ) {
    return(2*pow(10,digits));
  }
  if (betw_1_10 > 3.4 && betw_1_10 < 7.5 ) {
    return(5*pow(10,digits));
  }
  else {
    return(10*pow(10,digits));
  }
}

double fact(int n)
{
  double retval;

  for (retval = 1.0; n > 1; n--) {
    retval *= n;
  }

  return retval;
}
