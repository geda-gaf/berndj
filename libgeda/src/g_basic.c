/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#include <sys/stat.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

struct catch_context {
  SCM s_stack;
  scm_t_catch_handler handler;
  void *userdata;
};

struct source_reference {
  SCM frame;
  SCM filename;
  SCM line_num;
  SCM col_num;
};

gchar *g_strdup_scm_string(SCM scm_s)
{
  char *s, *retval;

  s = scm_to_locale_string(scm_s);
  retval = g_strdup(s);
  free(s);

  return retval;
}

/* Pre-unwind handler called in the context in which the exception was
 * thrown. */
static SCM protected_pre_unwind_handler (void *data, SCM key G_GNUC_UNUSED,
					 SCM args G_GNUC_UNUSED)
{
  struct catch_context *context = data;

  /* Capture the stack trace */
  context->s_stack = scm_make_stack(SCM_BOOL_T, SCM_EOL);

  return SCM_BOOL_T;
}

static SCM frame_source_ref(void *data)
{
  struct source_reference *sourceref = data;
  SCM source;

  source = scm_frame_source(sourceref->frame);

  sourceref->filename = scm_source_property(source,
					    scm_from_locale_symbol("filename"));
  sourceref->line_num = scm_source_property(source,
					    scm_from_locale_symbol("line"));
  sourceref->col_num = scm_source_property(source,
					   scm_from_locale_symbol("column"));

  return SCM_BOOL_T;
}

/* Post-unwind handler called in the context of the catch expression.
 * This actually does the work of parsing the stack and generating log
 * messages. */
static SCM protected_post_unwind_handler (void *data, SCM key, SCM args)
{
  struct catch_context *context = data;
  SCM retval = SCM_BOOL_F;

  char *message = NULL;
  
  scm_dynwind_begin(0);

  /* Capture the error message */
  if (scm_is_pair(args) &&
      scm_is_pair(scm_cdr(args)) &&
      scm_is_pair(scm_cddr(args)) &&
      scm_is_string(scm_cadr(args)) &&
      scm_is_true(scm_list_p(scm_caddr(args)))) {
    /* Infer that scm-error raised the exception. */
    SCM s_msg = scm_simple_format (SCM_BOOL_F, 
                                   scm_cadr (args), 
                                   scm_caddr (args));
    message = scm_to_locale_string (s_msg);
  } else if (scm_is_pair(args) &&
	     scm_is_pair(scm_cdr(args)) &&
	     scm_is_string(scm_cadr(args))) {
    /* Not sure what raises these, but presumably args is '(SUBR MESSAGE ...). */
    message = scm_to_locale_string (scm_cadr (args));
  } else {
    SCM s_msg = scm_simple_format(SCM_BOOL_F,
				  scm_from_locale_string("Caught exception: ~S ~S\n"),
				  scm_list_2(key, args));
    message = scm_to_locale_string(s_msg);
  }
  scm_dynwind_free(message);

  /* If a stack was captured, extract debugging information */
  if (scm_stack_p(context->s_stack) == SCM_BOOL_T) {
    SCM stack_length = scm_stack_length(context->s_stack);
    SCM s_port, s_source;
    SCM frame_depth;
    struct source_reference sourceref;
    char *filename, *trace;
    int exception_caught;
    gboolean valid_source = FALSE;

    /* Capture & log backtrace */
    s_port = scm_open_output_string();
    scm_display_backtrace(context->s_stack, s_port, SCM_BOOL_F, SCM_BOOL_F);
    trace = scm_to_locale_string (scm_get_output_string (s_port));
    scm_dynwind_free(trace);
    scm_close_output_port (s_port);
    s_log_message ("\n%s\n", trace);

    /* Capture & log location of first frame with useful info. */
    for (frame_depth = scm_from_int(0);
	 scm_is_true(scm_less_p(frame_depth, stack_length));
	 frame_depth = scm_oneplus(frame_depth)) {
      sourceref.frame = scm_stack_ref(context->s_stack, frame_depth);
      s_source = scm_frame_source(sourceref.frame);

      /*
       * XXX s_source may be #f here, but libguile manual doesn't document
       * this condition. Wrap subsequent uses in a catch block.
       */

      /* Safely get source properties. */
      exception_caught = 0;
      scm_c_catch(SCM_BOOL_T,
		  frame_source_ref, &sourceref,
		  &g_scm_exception_witness, &exception_caught,
		  NULL, NULL);
      if (!exception_caught
	  && scm_is_string(sourceref.filename)
	  && scm_is_integer(sourceref.line_num)
	  && scm_is_integer(sourceref.col_num)) {
	/* Found a frame worth reporting. */
	valid_source = TRUE;

	filename = scm_to_locale_string(sourceref.filename);
	scm_dynwind_free(filename);
	s_log_message(_("%s:%i:%i #%i: %s\n"),
		      filename,
		      scm_to_int(sourceref.line_num) + 1,
		      scm_to_int(sourceref.col_num) + 1,
		      scm_to_int(scm_difference(scm_oneminus(stack_length),
						frame_depth)),
		      message);
	break;
      }
    }
    
    if (!valid_source) {
      s_log_message (_("Unknown file: %s\n"), message);
    }
  } else {
    /* No stack, so can't display debugging info */
    s_log_message (_("Evaluation failed: %s\n"), message);
    s_log_message (_("Enable debugging for more detailed information\n"));
  }

  if (context->handler) {
    retval = (*context->handler)(context->userdata, key, args);
  }

  scm_dynwind_end();

  return retval;
}

/* Actually carries out evaluation for protected eval */
static SCM protected_body_eval (void *data)
{
  SCM args = *((SCM *)data);
  return scm_eval (scm_car (args), scm_cadr (args));
}

/*! \brief Evaluate a Scheme expression safely.
 *  \par Function Description
 *
 *  Often a libgeda program (or libgeda itself) will need to call out
 *  to Scheme code, for example to load a Scheme configuration file.
 *  If an error or exception caused by such code goes uncaught, it
 *  locks up the Scheme interpreter, stopping any further Scheme code
 *  from being run until the program is restarted.
 *
 *  This function is equivalent to scm_eval (), with the important
 *  difference that any errors or exceptions caused by the evaluated
 *  expression \a exp are caught and reported via the libgeda logging
 *  mechanism.  If an error occurs during evaluation, this function
 *  returns SCM_BOOL_F.  If \a module_or_state is undefined, uses the
 *  current interaction environment.
 *
 *  \param exp             Expression to evaluate
 *  \param module_or_state Environment in which to evaluate \a exp
 *
 *  \returns Evaluation results or SCM_BOOL_F if exception caught.
 */
SCM g_scm_eval_protected (SCM exp, SCM module_or_state)
{
  struct catch_context exception_context = {
    .s_stack = SCM_BOOL_T,
    .handler = NULL,
    .userdata = NULL,
  };
  SCM body_data;
  SCM result;

  if (module_or_state == SCM_UNDEFINED) {
    body_data = scm_list_2 (exp, scm_interaction_environment ());
  } else {
    body_data = scm_list_2 (exp, module_or_state);
  }

  result = scm_c_catch (SCM_BOOL_T,
                        protected_body_eval,           /* catch body */
                        &body_data,                    /* body data */
                        protected_post_unwind_handler, /* post handler */
			&exception_context,            /* post data */
                        protected_pre_unwind_handler,  /* pre handler */
                        &exception_context             /* pre data */
                        );

  scm_remember_upto_here_2(body_data, exception_context.s_stack);

  return result;
}

/* Actually carries out evaluation for protected eval-string */
static SCM protected_body_eval_string (void *data)
{
  SCM str = *((SCM *)data);
  return scm_eval_string (str);
}

/*! \brief Evaluate a C string as a Scheme expression safely
 *  \par Function Description
 *
 *  Evaluates a C string like scm_c_eval_string().  Simple wrapper for
 *  g_scm_eval_string_protected().
 *
 *  \param str  String to evaluate.
 *
 *  \returns Evaluation results or SCM_BOOL_F if exception caught.
 */
SCM g_scm_c_eval_string_protected (const gchar *str) {
  SCM s_str;
  g_return_val_if_fail ((str != NULL), SCM_BOOL_F);
  s_str = scm_from_locale_string (str);
  return g_scm_eval_string_protected (s_str);
}

/*! \brief Evaluate a string as a Scheme expression safely
 *  \par Function Description
 *
 *  Evaluates a string similarly to scm_eval_string(), but catching
 *  any errors or exceptions and reporting them via the libgeda
 *  logging mechanism.
 *
 *  See also g_scm_eval_protected() and g_scm_c_eval_string_protected().
 *
 *  \param str  String to evaluate.
 *
 *  \returns Evaluation results or SCM_BOOL_F if exception caught.
 */
SCM g_scm_eval_string_protected (SCM str)
{
  struct catch_context exception_context = {
    .s_stack = SCM_BOOL_T,
    .handler = NULL,
    .userdata = NULL,
  };
  SCM result;

  result = scm_c_catch (SCM_BOOL_T,
                        protected_body_eval_string,    /* catch body */
                        &str,                          /* body data */
                        protected_post_unwind_handler, /* post handler */
                        &exception_context,            /* post data */
                        protected_pre_unwind_handler,  /* pre handler */
                        &exception_context             /* pre data */
                        );

  scm_remember_upto_here_1(exception_context.s_stack);

  return result;
}

static SCM protected_body_apply(void *data)
{
  SCM *function_call = data;
  SCM f, args;
  SCM retval;

  f = scm_car(*function_call);
  args = scm_cdr(*function_call);

  retval = scm_apply_0(f, args);

  return retval;
}

/*! \brief Apply a function to a list of arguments safely
 *  \par Function Description
 *
 *  Applies \a proc to a list of arguments \a args, catching any errors or
 *  exceptions, optionally calling a C function \a handler.
 *
 *  \param [in] proc      The function to apply
 *  \param [in] args      A list of arguments to pass to \a proc
 *  \param [in] handler   A C function to call for a value to return to the
 *                        calling code if an exception occurs.
 *  \param [in] userdata  Caller-supplied context to pass to \a handler.
 *
 *  \returns Results of apply \a proc, or SCM_BOOL_F if any errors occured.
 */
SCM g_scm_apply_protected(SCM proc, SCM args,
			  scm_t_catch_handler handler, void *userdata)
{
  struct catch_context exception_context = {
    .s_stack = SCM_BOOL_T,
    .handler = handler,
    .userdata = userdata,
  };
  SCM function_call;
  SCM retval;

  function_call = scm_cons(proc, args);

  retval = scm_c_catch(SCM_BOOL_T,
		       protected_body_apply,          /* catch body */
		       &function_call,                /* body data */
		       protected_post_unwind_handler, /* post handler */
		       &exception_context,            /* post data */
		       protected_pre_unwind_handler,  /* pre handler */
		       &exception_context             /* pre data */
		      );

  scm_remember_upto_here_1(exception_context.s_stack);

  return retval;
}

/*! \brief Helper function to notice exceptions
 *  \par Function Description
 *
 *  Use this function as the \a handler argument to g_scm_apply_protected,
 *  with an int initialized to zero as the \a userdata argument. This helper
 *  will set the witness integer to 1 if g_scm_apply_protected catches an
 *  exception.
 *
 *  \returns SCM_BOOL_F for compatibility with g_scm_eval*_protected.
 */
SCM g_scm_exception_witness(void *userdata, SCM key, SCM args)
{
  int *witness = userdata;

  *witness = 1;

  return SCM_BOOL_F;
}

static SCM g_scm_ref_lookup_body(void *userdata)
{
  char const *name = userdata;

  return (scm_variable_ref(scm_c_lookup(name)));
}

/*! \brief Safely look up a symbol and dereference it
 *  \par Function Description
 *
 *  Look up a symbol and dereference it to get its value, without throwing
 *  an exception. This is a convenience function intended to help make calls
 *  with g_scm_apply_protected to functions of a given name.
 *
 *  \param [in] name  The name of the function.
 *
 *  \returns The value of \a name, or SCM_UNDEFINED if an error occurred.
 */
SCM g_scm_safe_ref_lookup(char const *name)
{
  int exception_caught = 0;
  struct catch_context exception_context = {
    .s_stack = SCM_BOOL_T,
    .handler = g_scm_exception_witness,
    .userdata = &exception_caught,
  };
  SCM retval;

  retval = scm_c_catch(SCM_BOOL_T, &g_scm_ref_lookup_body,
		       (void *) name, /* Cast const-ness away. */
		       protected_post_unwind_handler, &exception_context,
		       protected_pre_unwind_handler, &exception_context);

  scm_remember_upto_here_1(exception_context.s_stack);

  return (exception_caught ? SCM_UNDEFINED : retval);
}

SCM g_scm_file_get_contents(SCM f)
{
  char *filename, *contents;
  gsize length;
  SCM retval = SCM_UNDEFINED;

  SCM_ASSERT(scm_is_string(f), f, SCM_ARG1, "file-contents");

  scm_dynwind_begin(0);

  filename = scm_to_locale_string(f);
  scm_dynwind_free(filename);

  if (g_file_get_contents(filename, &contents, &length, NULL)) {
    scm_dynwind_unwind_handler(&g_free, contents, SCM_F_WIND_EXPLICITLY);
    if (length > SIZE_MAX) {
      retval = SCM_BOOL_F;
    } else {
      /* This shouldn't really use any encoding, it's just bytes. */
      retval = scm_from_locale_stringn(contents, length);
    }
  } else {
    retval = SCM_BOOL_F;
  }

  scm_dynwind_end();
  return retval;
}

/*! \brief Start reading a scheme file
 *  \par Function Description
 *  Start reading a scheme file
 *
 *  \param [in] filename  The file name to start reading from.
 *  \return 0 on success, -1 on failure.
 */
int g_read_file(const gchar *filename)
{
	SCM eval_result = SCM_BOOL_F;
        SCM expr;
	char * full_filename;

	if (filename == NULL) {
		return(-1);
	}

	/* get full, absolute path to file */
	full_filename = f_normalize_filename (filename, NULL);
	if (full_filename == NULL) {
		return(-1);
	}
	
	if (access(full_filename, R_OK) != 0) {
          s_log_message(_("Could not find [%s] for interpretation\n"),
                        full_filename);
		return(-1);
  	}

        expr = scm_list_2 (scm_from_locale_symbol ("load"),
                           scm_from_locale_string (full_filename));
        eval_result = g_scm_eval_protected (expr,
                                            scm_interaction_environment ());

	g_free(full_filename);

	return (eval_result != SCM_BOOL_F);
}
