/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif

#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/*! global which is used in o_list_copy_all */
extern int global_sid;

/*! \todo Finish documentation!!!!
 *  \brief
 *  \par Function Description
 *  returns head !!!!!!!!!!!!!!!!!!!
 *  look at above.. this returns what was passed in!!!!
 *  copies selected to list_head (!! returns new list)
 *  flag is either NORMAL_FLAG or SELECTION_FLAG
 *
 *  \param [in]  toplevel   The TOPLEVEL object.
 *  \param [in]  list_head
 *  \param [in]  selected
 *  \param [in]  flag
 *  \param [out] return_end  
 *  \return OBJECT pointer.
 */
OBJECT *o_list_copy_to(TOPLEVEL *toplevel, OBJECT *list_head,
		       OBJECT *selected, int flag, OBJECT **return_end)
{
  OBJECT *new_obj;
  OBJECT *end;

  /* are we adding a selection or the real object list */
  toplevel->ADDING_SEL = flag;

  new_obj = s_basic_copy(toplevel, selected);
  g_return_val_if_fail(new_obj, list_head);

  /* Store a reference in the copied object to where it was copied.
   * Used to retain associations when copying attributes */
  selected->copied_to = new_obj;

  end = return_tail (list_head);
  end = s_basic_link_object (new_obj, end);

  if (list_head == NULL)
    list_head = end;

  /* I don't think this is a good idea at all */
  /* toplevel->ADDING_SEL = 0; */
        
  if (return_end) {
    *return_end = end;	
  }

  return(list_head);
}

struct list_copy_context {
  TOPLEVEL *toplevel;
  OBJECT *dest;
  int adding_sel;
};

static enum visit_result
list_copy_all_one_nontext(OBJECT *o_current, void *userdata)
{
  struct list_copy_context *ctx = userdata;

  if (o_current->type != OBJ_TEXT) {
    o_list_copy_to(ctx->toplevel, ctx->dest, o_current, ctx->adding_sel,
		   &ctx->dest);
    ctx->dest->sid = global_sid++;
  }

  return VISIT_RES_OK;
}

static enum visit_result
list_copy_all_one_text(OBJECT *o_current, void *userdata)
{
  struct list_copy_context *ctx = userdata;

  if (o_current->type == OBJ_TEXT) {
    o_list_copy_to(ctx->toplevel, ctx->dest, o_current, ctx->adding_sel,
		   &ctx->dest);
    ctx->dest->sid = global_sid++;

    if (o_current->attached_to /*&& !toplevel->ADDING_SEL*/) {
      if (o_current->attached_to->copied_to) {
	o_attrib_attach(ctx->toplevel, ctx->dest, o_current->attached_to->copied_to);
      }
    }
  }

  return VISIT_RES_OK;
}

static enum visit_result
list_copy_all_one_clear_copied(OBJECT *o_current, void *userdata)
{
  o_current->copied_to = NULL;

  return VISIT_RES_OK;
}

/*! \todo Finish function description!!!
 *  \brief
 *  \par Function Description
 *  you need to pass in a head_node for dest_list_head
 *  flag is either NORMAL_FLAG or SELECTION_FLAG
 *
 *  \param [in] toplevel       The TOPLEVEL object.
 *  \param [in] src_list_head   
 *  \param [in] dest_list_head  
 *  \param [in] flag
 *  \return OBJECT pointer.
 */
OBJECT *o_list_copy_all(TOPLEVEL *toplevel, OBJECT *src_list_head,
                        OBJECT *dest_list_head, int flag)
{
  struct list_copy_context ctx = {
    .toplevel = toplevel,
    .dest = dest_list_head,
    .adding_sel = flag,
  };
  int adding_sel_save;

  if (src_list_head == NULL || dest_list_head == NULL) {
    return(NULL);
  }

  /* Save ADDING_SEL as o_list_copy_to() sets it */
  adding_sel_save = toplevel->ADDING_SEL;

  /* first do all NON text items */
  s_visit_list(src_list_head, LIST_KIND_HEAD,
	       &list_copy_all_one_nontext, &ctx, VISIT_LINEAR, 1);

  /* then do all text items */
  s_visit_list(src_list_head, LIST_KIND_HEAD,
	       &list_copy_all_one_text, &ctx, VISIT_LINEAR, 1);

  /* Clean up dangling copied_to pointers */
  s_visit_list(src_list_head, LIST_KIND_HEAD,
	       &list_copy_all_one_clear_copied, NULL, VISIT_LINEAR, 1);

  toplevel->ADDING_SEL = adding_sel_save;

  return ctx.dest;
}

/*! \todo Finish function description!!!
 *  \brief
 *  \par Function Description
 *  you need to pass in a head_node for dest_list_head
 *  flag is either NORMAL_FLAG or SELECTION_FLAG
 *  this function copies the objects in the src GList src_list
 *  to the destination GList dest_list
 *  this routine assumes that objects in src_list are selected
 *  objects are unselected before they are copied and then reselected
 *  this is necessary to preserve the color info
 *
 *  \param [in] toplevel       The TOPLEVEL object.
 *  \param [in] src_list       The GList to copy from.
 *  \param [in] dest_list      The GList to copy to.
 *  \param [in] flag
 *  \return the dest_list GList with objects appended to it.
 */
GList *o_glist_copy_all_to_glist(TOPLEVEL *toplevel,
                                 GList const *src_list,
                                 GList *dest_list, int flag)
{
  GList const *src;
  GList *dest;
  OBJECT *src_object, *dst_object;
  int adding_sel_save;
  int selected_save;

  src = src_list;
  /* Reverse any existing items, as we will prepend, then reverse at the end */
  dest = g_list_reverse (dest_list);

  if (src == NULL) {
    return(NULL);
  }

  /* Save ADDING_SEL as o_list_copy_to() sets it */
  adding_sel_save = toplevel->ADDING_SEL;

  /* first do all NON text items */
  while(src != NULL) {
    src_object = src->data;

    /* unselect the object before the copy */
    selected_save = src_object->selected;
    if (selected_save)
      o_selection_unselect(src_object);

    if (src_object->type != OBJ_TEXT && src_object->type != OBJ_HEAD) {
      dst_object = o_list_copy_to (toplevel, NULL, src_object, flag, NULL);
      dst_object->sid = global_sid++;
      dest = g_list_prepend (dest, dst_object);
    }

    /* reselect it */
    if (selected_save) {
      o_selection_select(src_object);
    }

    src = g_list_next(src);
  }

  src = src_list;

  /* then do all text items */
  while(src != NULL) {
    src_object = src->data;

    /* unselect the object before the copy */
    selected_save = src_object->selected;
    if (selected_save)
      o_selection_unselect(src_object);

    if (src_object->type == OBJ_TEXT) {
      dst_object = o_list_copy_to (toplevel, NULL, src_object, flag, NULL);
      dst_object->sid = global_sid++;
      dest = g_list_prepend (dest, dst_object);

      if (src_object->attached_to != NULL &&
          src_object->attached_to->copied_to != NULL) {
        o_attrib_attach(toplevel, dst_object,
                        src_object->attached_to->copied_to);
      }
    }

    /* reselect it */
    if (selected_save) {
      o_selection_select(src_object);
    }

    src = g_list_next(src);
  }

  /* Clean up dangling copied_to pointers */
  src = src_list;
  while(src != NULL) {
    src_object = src->data;
    src_object->copied_to = NULL;
    src = g_list_next (src);
  }

  /* Reverse the list to be in the correct order */
  dest = g_list_reverse (dest);

  /* Link the copied objects together for good measure */
  o_glist_relink_objects (dest);

  toplevel->ADDING_SEL = adding_sel_save;

  return(dest);
}


/*! \brief Relink OBJECT next and prev pointers to match the passed GList
 *
 * \par Function Description
 * Updates the OBJECT next and prev pointers for OBJECTs in the passed
 * GList of objects to link in the same order as the passed GList.
 *
 * \param [in] o_glist   The GList of OBJECTs.
 */
void o_glist_relink_objects(GList const *o_glist)
{
  GList const *iter;
  OBJECT *list_tail = NULL;
  OBJECT *object;

  for (iter = o_glist; iter != NULL; iter = g_list_next (iter)) {
    object = iter->data;
    list_tail = s_basic_link_object(object, list_tail);
  }
}


/*! \todo Finish function description!!!
 *  \brief
 *  \par Function Description
 *  assuming list is head
 *  head will NOT be deleted
 *
 *  \param [in] toplevel  The TOPLEVEL object.
 *  \param [in] list
 */
void o_list_delete_rest(TOPLEVEL *toplevel, OBJECT *list)
{
  OBJECT *o_current=NULL;
	
  o_current = return_tail(list);

  /* remove list backwards */
  while(o_current != NULL) {
    if (o_current->type != OBJ_HEAD) {
      o_current = s_delete(toplevel, o_current);
    } else {
      o_current->next = NULL; /* set sel_head->next to be empty */
      o_current = NULL;
    }
  }
}

struct affine_context {
  int x;
  int y;
  int angle;
};

static enum visit_result translate_world_one(OBJECT *o, void *userdata)
{
  struct affine_context *ctx = userdata;

  o_translate_world(ctx->x, ctx->y, o);

  return VISIT_RES_OK;
}

/*! \todo Finish function description!!!
 *  \brief
 *  \par Function Description
 */
void o_list_translate_world(int dx, int dy, OBJECT *list)
{
  struct affine_context ctx = {
    .x = dx,
    .y = dy,
  };

  /* XXX o_text_create_string still uses partial lists. */
  s_visit_list(list, list->type == OBJ_HEAD ? LIST_KIND_HEAD : LIST_KIND_REST,
	       &translate_world_one, &ctx, VISIT_ANY, 1);
}


/*! \todo Finish function description!!!
 *  \brief
 *  \par Function Description
 */
void o_glist_translate_world(int dx, int dy, GList const *list)
{
  GList const *iter = list;
  OBJECT *o_current;

  while ( iter != NULL ) {
    o_current = iter->data;
    o_translate_world(dx, dy, o_current);
    iter = g_list_next (iter);
  }
}

static enum visit_result rotate_world_one(OBJECT *o, void *userdata)
{
  struct affine_context *ctx = userdata;

  o_rotate_world(ctx->x, ctx->y, ctx->angle, o);

  return VISIT_RES_OK;
}

/*! \todo Finish function description!!!
 *  \brief
 *  \par Function Description
 */
void o_list_rotate_world(int x, int y, int angle, OBJECT *list)
{
  struct affine_context ctx = {
    .x = x,
    .y = y,
    .angle = angle,
  };

  /* XXX o_text_create_string still uses partial lists. */
  s_visit_list(list, list->type == OBJ_HEAD ? LIST_KIND_HEAD : LIST_KIND_REST,
	       &rotate_world_one, &ctx, VISIT_ANY, 1);
}


/*! \todo Finish function description!!!
 *  \brief
 *  \par Function Description
 */
void o_glist_rotate_world(int x, int y, int angle, GList const *list)
{
  GList const *iter = list;
  OBJECT *o_current;

  while ( iter != NULL ) {
    o_current = iter->data;
    o_rotate_world(x, y, angle, o_current);
    iter = g_list_next (iter);
  }
}

static enum visit_result mirror_world_one(OBJECT *o, void *userdata)
{
  struct affine_context *ctx = userdata;

  o_mirror_world(ctx->x, ctx->y, o);

  return VISIT_RES_OK;
}


/*! \todo Finish function description!!!
 *  \brief
 *  \par Function Description
 */
void o_list_mirror_world(int x, int y, OBJECT *list)
{
  struct affine_context ctx = {
    .x = x,
    .y = y,
  };

  s_visit_list(list, LIST_KIND_HEAD, &mirror_world_one, &ctx, VISIT_ANY, 1);
}


/*! \todo Finish function description!!!
 *  \brief
 *  \par Function Description
 */
void o_glist_mirror_world(int x, int y, GList const *list)
{
  GList const *iter = list;
  OBJECT *o_current;

  while ( iter != NULL ) {
    o_current = iter->data;
    o_mirror_world(x, y, o_current);
    iter = g_list_next (iter);
  }
}
