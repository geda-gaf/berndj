/* gEDA - GPL Electronic Design Automation
 * factory.c - Abstract Factory pattern for libgeda objects
 * Copyright (C) 2007, 2008 Bernd Jendrissek
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glib.h>
#include <libguile.h>
#include "factory.h"
#include "o_types.h"
#include "s_object.h"
#include "s_page.h"
#include "struct.h"
#include "prototype.h"

OBJECT *s_factory_new_object(struct st_factory *factory, char kind,
			     char const *name);
PAGE *s_factory_new_page(struct st_factory *factory, TOPLEVEL *toplevel,
                         gchar const *filename);

struct st_factory libgeda_factory = {
.new_object = &s_factory_new_object,
.new_page = &s_factory_new_page,
};

OBJECT *s_factory_new_object(struct st_factory *factory G_GNUC_UNUSED,
			     char kind, char const *name)
{
  return (s_object_new(kind, name));
}

PAGE *s_factory_new_page(struct st_factory *factory G_GNUC_UNUSED,
			 TOPLEVEL *toplevel, gchar const *filename)
{
  return (g_object_new(GEDA_PAGE_TYPE,
                       "toplevel", toplevel,
		       "filename", filename,
		       NULL));
}

/*! \brief Derive a factory from the libgeda base class
 *  \par Function Description
 *  Inherits base class methods where the vtable has NULL entries.
 *
 *  \param [in] specialization  The factory specialization.
 *  \param [in] base            The base class from which to inherit.
 */
void s_factory_derive(struct st_factory *specialization,
		      struct st_factory const *base)
{
  if (specialization->new_object == NULL) {
    specialization->new_object = base->new_object;
  }
  if (specialization->new_page == NULL) {
    specialization->new_page = base->new_page;
  }
}
/* vim: set sw=2: */
