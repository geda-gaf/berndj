/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_STRING_H
#include <string.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

enum {
  PROPERTY_FILENAME = 1,
  PROPERTY_TOPLEVEL,
};

enum {
  GEDA_PAGE_SIGNAL_CHANGED,
  GEDA_PAGE_SIGNAL_ADD_OBJECT,
  GEDA_PAGE_SIGNAL_REMOVE_OBJECT,
  GEDA_PAGE_SIGNAL_LAST
};

/* Glue GObject naming conventions to those of gEDA. */
typedef PAGE GedaPage;
typedef GObjectClass GedaPageClass;

static guint geda_page_signals[GEDA_PAGE_SIGNAL_LAST];

G_DEFINE_TYPE(GedaPage, geda_page, G_TYPE_OBJECT)

static void s_page_set_property(GObject *object, guint property_id,
				const GValue *value, GParamSpec *pspec G_GNUC_UNUSED)
{
  PAGE *page = GEDA_PAGE(object);

  switch (property_id) {
    char const *filename;
  case PROPERTY_TOPLEVEL:
    g_object_set_data(G_OBJECT(page), "toplevel", g_value_get_object(value));
    break;
  case PROPERTY_FILENAME:
    filename = g_value_get_string(value);

    /* big assumption here that page_filename isn't null */
    if (g_path_is_absolute (filename)) {
      page->page_filename = g_strdup (filename);
    } else {
      gchar *pwd = g_get_current_dir ();
      page->page_filename = g_build_filename (pwd, filename, NULL);
      g_free (pwd);
    }
    break;
  }
}

static void geda_page_class_init(GedaPageClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  GParamSpec *pspec;

  gobject_class->set_property = s_page_set_property;

  /*
   * PROPERTY_TOPLEVEL should be temporary, until we decouple objects from
   * global state.  Until then, there are many functions which expect a
   * TOPLEVEL * argument to get at program state.
   */
  pspec = g_param_spec_object("toplevel", "Page parent TOPLEVEL",
			      "Set page's parent TOPLEVEL",
                              S_TOPLEVEL_TYPE,
			      G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
  g_object_class_install_property(gobject_class, PROPERTY_TOPLEVEL, pspec);

  pspec = g_param_spec_string("filename", "Page file name",
			      "Set page's file name", "",
			      G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
  g_object_class_install_property(gobject_class, PROPERTY_FILENAME, pspec);

  geda_page_signals[GEDA_PAGE_SIGNAL_CHANGED] =
    g_signal_new("changed", G_TYPE_FROM_CLASS(gobject_class),
		 G_SIGNAL_RUN_LAST, 0, NULL, NULL,
		 &g_cclosure_marshal_VOID__VOID,
		 G_TYPE_NONE, 0);
  geda_page_signals[GEDA_PAGE_SIGNAL_ADD_OBJECT] =
    g_signal_new("add-object", G_TYPE_FROM_CLASS(gobject_class),
		 G_SIGNAL_RUN_LAST, 0, NULL, NULL,
		 &g_cclosure_marshal_VOID__OBJECT,
		 G_TYPE_NONE, 1, G_TYPE_OBJECT);
  geda_page_signals[GEDA_PAGE_SIGNAL_REMOVE_OBJECT] =
    g_signal_new("remove-object", G_TYPE_FROM_CLASS(gobject_class),
		 G_SIGNAL_RUN_LAST, 0, NULL, NULL,
		 &g_cclosure_marshal_VOID__OBJECT,
		 G_TYPE_NONE, 1, G_TYPE_OBJECT);
}

GType s_page_get_type(void)
{
  static GType type = 0;

  if (type == 0) {
    static const GTypeInfo info = {
      .class_size = sizeof (GObjectClass),
      .class_init = (GClassInitFunc) &geda_page_class_init,
      .instance_size = sizeof (PAGE),
      .instance_init = (GInstanceInitFunc) &geda_page_init,
    };

    type = g_type_register_static(G_TYPE_OBJECT, "GEDA_PAGE", &info, 0);
  }

  return type;
}

static void geda_page_init(PAGE *page)
{
  page->CHANGED = 0;

  page->up_page = NULL;
  page->page_control = 0;

  /* new selection mechanism */
  page->selection_list = o_selection_new();

  /* net/pin/bus stretch when doing moves */
  page->stretch_head = page->stretch_tail = s_stretch_new_head();

  page->page_private.place_list = NULL;

  /* init undo struct pointers */
  s_undo_init(page);
  
  page->object_lastfound = NULL;
  
  /* Backup variables */
  g_get_current_time (&page->last_load_or_save_time);
  page->ops_since_last_backup = 0;
  page->saved_since_first_loaded = 0;
  page->do_autosave_backup = 0;

  page->load_newer_backup_func = load_newer_backup_func;
}

/*! \brief create a new page object
 *  \par Function Description
 *  Creates a new page and add it to <B>toplevel</B>'s list of pages.
 *
 *  It initializes the #PAGE structure and set its <B>page_filename</B>
 *  to <B>filename</B>. <B>toplevel</B>'s current page is not changed by
 *  this function.
 */
PAGE *s_page_new (TOPLEVEL *toplevel, const gchar *filename)
{
  PAGE *page;

  page = toplevel->factory->new_page(toplevel->factory, toplevel, filename);

  /* First one to setup head */
  page->object_head = s_toplevel_new_object(toplevel, OBJ_HEAD, "object_head");
  /* do this just to be sure that object tail is truly correct */
  page->object_tail = return_tail(page->object_head);

  /* now append page to page list of toplevel */
  geda_list_add( toplevel->pages, page );

  return page;
}

/*! \brief Add objects to the place list.
 *  \par Function Description
 *  Adds a list of objects to the place list.
 *  \param [in] page     The page holding the objects and the place list.
 *  \param [in] objects  A list of objects to add to the place list.
 *
 *  \note This function takes ownership of \a new_list. Do not free it.
 */
void s_page_add_place_list(PAGE *page, GList *objects)
{
  page->page_private.place_list = g_list_concat(page->page_private.place_list,
						objects);
}

/*! \brief Gets the place list from a page.
 *  \par Function Description
 *  Gets the list of objects in the place list.
 *  \param [in] page  The page whose place list to get.
 *
 *  \return A GList of objects in the place list.
 *
 *  \note The returned GList belongs to the page. Do not free it.
 */
GList const *s_page_borrow_place_list(PAGE const *page)
{
  return page->page_private.place_list;
}

/*! \brief Replace the place list.
 *  \par Function Description
 *  Replaces the place list of a page with the items in \a new_list.
 *  \param [in] toplevel  The TOPLEVEL in which the page exists.
 *  \param [in] page      The page whose place list to replace.
 *  \param [in] new_list  A list of new items to put in the place list.
 *
 *  \note This function takes ownership of \a new_list. Do not free it.
 */
void s_page_replace_place_list(TOPLEVEL *toplevel, PAGE *page, GList *new_list)
{
  s_delete_object_glist (toplevel, page->page_private.place_list);
  page->page_private.place_list = new_list;
}

/*! \brief Forget the place list.
 *  \par Function Description
 *  Forgets the list of items in the place list without deleting the objects.
 *  \param [in] page  The page whose place list to forget.
 *
 *  \return The old place list if \a return_old is set, otherwise undefined.
 *
 *  \note This function frees the place list unless \a return_old is set.
 */
GList *s_page_forget_place_list(PAGE *page, gboolean return_old)
{
  GList *old_list = page->page_private.place_list;

  if (!return_old) {
    g_list_free(old_list);
  }
  page->page_private.place_list = NULL;

  return old_list;
}

/*! \brief delete a page and it's contents
 *  \par Function Description
 *  Deletes a single page <B>page</B> from <B>toplevel</B>'s list of pages.
 *
 *  See #s_page_delete_list() to delete all pages of a <B>toplevel</B>
 *
 *  If the current page of toplevel is given as parameter <B>page</B>,
 *  the function sets the field <B>page_current</B> of the TOPLEVEL
 *  struct to NULL.
 */
void s_page_delete (TOPLEVEL *toplevel, PAGE *page)
{
  PAGE *return_to_page;
  gchar *backup_filename;
  gchar *real_filename;

  /* We need to temporarily make the page being deleted current because
   * various functions called below (some indirectly) assume they are
   * deleting objects from the current page.
   *
   * These functions are known to include:
   *   s_delete_object ()
   */

  /* save page_current and switch to page */
  if (page == toplevel->page_current) {
    return_to_page = NULL;
  } else {
    return_to_page = toplevel->page_current;
    s_toplevel_goto_page(toplevel, page);
  }

  /* Get the real filename and file permissions */
  real_filename = follow_symlinks (page->page_filename, NULL);
  
  if (real_filename == NULL) {
    s_log_message (_("s_page_delete: Can't get the real filename of %s."),
                   page->page_filename);
  }
  else {
    backup_filename = f_get_autosave_filename (real_filename);

    /* Delete the backup file */
    if ( (g_file_test (backup_filename, G_FILE_TEST_EXISTS)) && 
	 (!g_file_test(backup_filename, G_FILE_TEST_IS_DIR)) )
    {
      if (unlink(backup_filename) != 0) {
	s_log_message(_("s_page_delete: Unable to delete backup file %s."),
                      backup_filename);
      }
    }
    g_free (backup_filename);
  }
  g_free(real_filename);

  /* Free the selection object */
  g_object_unref( page->selection_list );

  /* then delete objects of page */
  s_delete_list_fromstart (toplevel, page->object_head);

  /* Free the objects in the place list. */
  s_page_replace_place_list(toplevel, page, NULL);

  if (GEDA_DEBUG) {
    printf("Freeing page: %s\n", page->page_filename);
  }

  s_stretch_destroy_all (page->stretch_head);

  /* free current page undo structs */
  s_undo_free_all (toplevel, page); 

  /* ouch, deal with parents going away and the children still around */
  page->up_page = NULL;
  g_free (page->page_filename);

  geda_list_remove( toplevel->pages, page );

  g_object_unref(G_OBJECT(page));

  s_toplevel_goto_page(toplevel, return_to_page);
}


/*! \brief Deletes the list of pages of <B>toplevel</B>.
 *  \par Function Description
 *  Deletes the list of pages of <B>toplevel</B>.
 *  This function should only be called when you are finishing up.
 *
 *  \param toplevel  The TOPLEVEL object.
 */
void s_page_delete_list(TOPLEVEL *toplevel)
{
  GList *list_copy, *iter;
  PAGE *page;

  /* s_page_delete removes items from the page list, so make a copy */
  list_copy = g_list_copy (geda_list_get_glist (toplevel->pages));

  for (iter = list_copy; iter != NULL; iter = g_list_next (iter)) {
    page = (PAGE *)iter->data;

    s_page_delete (toplevel, page);
  }

  g_list_free (list_copy);

  s_toplevel_goto_page(toplevel, NULL);
}

/*! \brief Prepares a page to become the current one.
 *  \par Function Description
 *  Prepares a page to become the current one, changing the current directory.
 *  \param p_new     The PAGE to go to
 *  \note Do not call this function directly. Rather use s_toplevel_goto_page.
 */
void s_page_goto(PAGE *p_new)
{
  gchar *dirname;

  dirname = g_dirname (p_new->page_filename);
  if (chdir (dirname)) {
    /* An error occurred with chdir */
#warning FIXME: What do we do?
  }
  g_free (dirname);
}

/*! \brief Search for pages by filename.
 *  \par Function Description
 *  Searches in \a toplevel's list of pages for a page with a filename
 *  equal to \a filename.
 *
 *  \param toplevel  The TOPLEVEL object
 *  \param filename  The filename string to search for
 *  
 *  \return PAGE pointer to a matching page, NULL otherwise.
 */
PAGE *s_page_search (TOPLEVEL *toplevel, const gchar *filename)
{
  const GList *iter;
  PAGE *page;

  for ( iter = geda_list_get_glist( toplevel->pages );
        iter != NULL;
        iter = g_list_next( iter ) ) {

    page = (PAGE *)iter->data;
    if (u_basic_casecmp(page->page_filename, filename) == 0) {
      return page;
    }
  }
  return NULL;
}

/*! \brief Print full TOPLEVEL structure.
 *  \par Function Description
 *  This function prints the internal structure of <B>toplevel</B>'s
 *  list of pages.
 *
 *  \param [in] toplevel  The TOPLEVEL object to print.
 */
void s_page_print_all(TOPLEVEL const *toplevel)
{
  const GList *iter;
  PAGE const *page;

  for ( iter = geda_list_get_glist( toplevel->pages );
        iter != NULL;
        iter = g_list_next( iter ) ) {

    page = iter->data;
    printf ("FILENAME: %s\n", page->page_filename);
    print_struct_forw (page->object_head);
  }
}

/*! \brief Saves one page to file
 *  \par Function Description
 *  Saves one page to file, optionally setting the filename.
 *
 *  \param [in] toplevel      The TOPLEVEL context of the page.
 *  \param [in] page          The page to save to file.
 *  \param [in] new_filename  The new filename, or NULL to make no change.
 *
 *  \return FALSE if saving to file fails.
 */
gboolean s_page_save(TOPLEVEL *toplevel, PAGE *page, char *new_filename)
{
  PAGE *old_page;
  char *savename;

  savename = new_filename ? new_filename : page->page_filename;

  old_page = toplevel->page_current; /* XXX Remove when s_object_delete takes a PAGE * */
  s_toplevel_goto_page(toplevel, page);

  if (f_save(toplevel, page, savename)) {
    if (new_filename) {
      /* replace page filename with new one, do not free filename */
      g_free(page->page_filename);
      page->page_filename = new_filename;
      s_log_message("Saved As [%s]\n", savename);
    } else {
      s_log_message("Saved [%s]\n", savename);
    }

    /* reset the changed flag of current page*/
    page->CHANGED = 0;

    s_toplevel_goto_page(toplevel, old_page);
    return TRUE;
  } else {
    s_toplevel_goto_page(toplevel, old_page);
    return FALSE;
  }
}

/*! \brief Saves all the pages of a TOPLEVEL object.
 *  \par Function Description
 *  Saves all the pages in the <B>toplevel</B> parameter.
 *
 *  \param [in] toplevel  The TOPLEVEL to save pages from.
 *  \return The number of failed tries to save a page.
 */
gint s_page_save_all (TOPLEVEL *toplevel)
{
  const GList *iter;
  gint status = 0;

  for ( iter = geda_list_get_glist( toplevel->pages );
        iter != NULL;
        iter = g_list_next( iter ) ) {
    PAGE *p_current = iter->data;

    if (!s_page_save(toplevel, p_current, NULL)) {
      s_log_message(_("Could NOT save [%s]\n"), p_current->page_filename);
      /* increase the error counter */
      status++;
    }
  }

  return status;
}

/*! \brief Autosave initialization function.
 *  \par Function Description
 *  This function sets up the autosave callback function.
 *
 *  \param [in] toplevel  The TOPLEVEL object.
 */
void s_page_autosave_init(TOPLEVEL *toplevel)
{
  if (toplevel->auto_save_interval != 0) {

    /* 1000 converts seconds into milliseconds */
    toplevel->auto_save_timeout = 
      g_timeout_add(toplevel->auto_save_interval*1000,
                    (GSourceFunc) s_page_autosave,
                    toplevel);
  }
}

/*! \brief Autosave callback function.
 *  \par Function Description
 *  This function is a callback of the glib g_timeout functions.
 *  It is called every "interval" milliseconds and it sets a flag to save
 *  a backup copy of the opened pages.
 *
 *  \param [in] toplevel  The TOPLEVEL object.
 *  \return The length in milliseconds to set for next interval.
 */
gint s_page_autosave (TOPLEVEL *toplevel) 
{
  const GList *iter;
  PAGE *p_current;

  if (toplevel == NULL) {
    return 0;
  }

  /* Do nothing if the interval is 0 */
  if (toplevel->auto_save_interval == 0) {
    return toplevel->auto_save_interval;
  }

  /* Should we just disable the autosave timeout returning 0 or
     just wait for more pages to be added? */
  if ( toplevel->pages == NULL)
    return toplevel->auto_save_interval;

  for ( iter = geda_list_get_glist( toplevel->pages );
        iter != NULL;
        iter = g_list_next( iter ) ) {

    p_current = (PAGE *)iter->data;

    if (p_current->ops_since_last_backup != 0) {
      /* Real autosave is done in o_undo_savestate */
      p_current->do_autosave_backup = 1;
    }
  }

  return toplevel->auto_save_interval;
}

/*! \brief Append an OBJECT to the PAGE
 *
 *  \par Function Description
 *  Links the passed OBJECT to the end of the PAGE's
 *  linked list of objects.
 *
 *  \param [in] page    The PAGE the object is being added to.
 *  \param [in] object  The OBJECT being added to the page.
 */
void s_page_append(PAGE *page, OBJECT *object)
{
  page->object_tail = s_basic_link_object(object, page->object_tail);
}

static enum visit_result s_page_objects_prepend(OBJECT *o, void *userdata)
{
  GList **l = userdata;

  *l = g_list_prepend(*l, o);

  return VISIT_RES_OK;
}

/*! \brief Return a GList of OBJECTs on the PAGE
 *
 *  \par Function Description
 *  An accessor for the PAGE's GList of objects.
 *
 *  NB: This GList is owned by the PAGE, and must not be
 *      free'd or modified by the caller.
 *      XXX: This is a lie for now.  Free it yourself.
 *
 *  \param [in] page      The PAGE to get objects on.
 *  \returns A pointer to the PAGE's GList of objects
 */
GList *s_page_objects (PAGE *page)
{
  OBJECT *o;
  GList *l = NULL;

  s_visit_page(page, &s_page_objects_prepend, &l, VISIT_LINEAR, 1);

  /* Return items in the same order in which they appear in the page list. */
  l = g_list_reverse(l);

  return l;
}
