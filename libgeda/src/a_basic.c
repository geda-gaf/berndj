/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */

/*! \file a_basic.c
 *  \brief basic libgeda read and write functions
 */
#include <config.h>

#include <stdio.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif
#include <sys/stat.h>
#include <sys/types.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include "libgeda_priv.h"
#include <glib/gstdio.h>

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/*! \brief Get the file header string.
 *  \par Function Description
 *  This function simply returns the DATE_VERSION and
 *  FILEFORMAT_VERSION formatted as a gEDA file header.
 *
 *  \warning <em>Do not</em> free the returned string.
 */
const gchar *o_file_format_header()
{
  static gchar *header = NULL;

  if (header == NULL)
    header = g_strdup_printf("v %s %u\n", DATE_VERSION, FILEFORMAT_VERSION);

  return header;
}

/*! \brief "Save" a file into a string buffer
 *  \par Function Description
 *  This function saves a whole schematic into a buffer in libgeda
 *  format. The buffer should be freed when no longer needed.
 *
 *  \param [in] toplevel  The data to save.
 *  \param [in] page      The page to save.
 *  \returns a buffer containing schematic data or NULL on failure.
 */
gchar *o_save_buffer(TOPLEVEL *toplevel, PAGE *page)
{
  GString *acc;
  gchar *buffer;

  g_return_val_if_fail(toplevel != NULL, NULL);
  g_return_val_if_fail(page != NULL, NULL);

  /* make sure you init net_consolide to false (default) in all */
  /* programs */
  if (toplevel->net_consolidate == TRUE) {
    o_net_consolidate(toplevel, page);
  }

  acc = g_string_new (o_file_format_header());

  buffer = o_save_objects(page->object_head);
  g_string_append (acc, buffer);
  g_free (buffer);

  return g_string_free (acc, FALSE);
}

/*! \brief Save a series of objects into a string buffer
 *  \par Function Description
 *  This function recursively saves a set of objects into a buffer in
 *  libgeda format.  User code should not normally call this function;
 *  they should call o_save_buffer() instead.
 *
 *  \param [in] object_list  Head of list of objects to save.
 *  \returns a buffer containing schematic data or NULL on failure.
 */
gchar *o_save_objects (OBJECT *object_list)
{
  OBJECT *o_current = object_list;
  gchar *out;
  GString *acc;
  gboolean already_wrote = FALSE;

  g_return_val_if_fail ((object_list != NULL), NULL);

  acc = g_string_new("");

  while ( o_current != NULL ) {

    if (o_current->type != OBJ_HEAD &&
        o_current->attached_to == NULL) {

      switch (o_current->type) {

        case(OBJ_LINE):
          out = o_line_save(o_current);
          break;

        case(OBJ_NET):
          out = o_net_save(o_current);
          break;

        case(OBJ_BUS):
          out = o_bus_save(o_current);
          break;

        case(OBJ_BOX):
          out = o_box_save(o_current);
          break;

        case(OBJ_CIRCLE):
          out = o_circle_save(o_current);
          break;

        case(OBJ_COMPLEX):
          out = o_complex_save(o_current);
          g_string_append_printf(acc, "%s\n", out);
          already_wrote = TRUE;
          g_free(out); /* need to free here because of the above flag */

          if (o_complex_is_embedded(o_current)) {
            g_string_append(acc, "[\n");

            out = o_save_objects(o_current->complex->prim_objs);
            g_string_append (acc, out);
            g_free(out);

            g_string_append(acc, "]\n");
          }
          break;

        case(OBJ_PLACEHOLDER):  /* new type by SDB 1.20.2005 */
          out = o_complex_save(o_current);
          break;

	case(OBJ_SLOT):
	  out = s_slot_save(o_current);
	  break;

        case(OBJ_TEXT):
          out = o_text_save(o_current);
          break;

        case(OBJ_PATH):
          out = o_path_save(o_current);
          break;

        case(OBJ_PIN):
          out = o_pin_save(o_current);
          break;

        case(OBJ_ARC):
          out = o_arc_save(o_current);
          break;

        case(OBJ_PICTURE):
          out = o_picture_save(o_current);
          break;

        default:
          /*! \todo Maybe we can continue instead of just failing
           *  completely? In any case, failing gracefully is better
           *  than killing the program, which is what this used to
           *  do... */
          g_critical (_("o_save_objects: object %p has unknown type '%c'\n"),
                      o_current, o_current->type);
          /* Dump string built so far */
          g_string_free (acc, TRUE);
          return NULL;
      }

      /* output the line */
      if (!already_wrote) {
        g_string_append_printf(acc, "%s\n", out);
        g_free(out);
      } else {
        already_wrote = FALSE;
      }

      /* save any attributes */
      if (o_current->attribs != NULL) {
        out = o_save_attribs(o_current->attribs);
        g_string_append(acc, out);
        g_free (out);
      }

    }
    o_current = o_current->next;
  }

  return g_string_free (acc, FALSE);
}

/*! \brief Save a file
 *  \par Function Description
 *  This function saves the data in a libgeda format to a file
 *  \param [in] toplevel  The data to save to file.
 *  \param [in] page      The page to save.
 *  \param [in] filename   The filename to save the data to.
 *  \return 1 on success, 0 on failure.
 */
int o_save(TOPLEVEL *toplevel, PAGE *page, const char *filename)
{
  struct stat s;
  char *buffer;
  gboolean metadata_valid;
  gboolean exists;
  gboolean saved;
	
  exists = g_file_test(filename, G_FILE_TEST_EXISTS);

  metadata_valid = g_stat(filename, &s);
  if (exists && !metadata_valid) {
    s_log_message(_("o_save: Could not get original file permissions [%s]\n"),
		  filename);
  }

  buffer = o_save_buffer(toplevel, page);

  saved = g_file_set_contents(filename, buffer, strlen(buffer), NULL);
  if (!saved) {
    s_log_message(_("o_save: Could not save [%s]\n"), filename);
  }

  g_free(buffer);

  if (exists && metadata_valid && saved) {
    if (!g_chmod(filename, s.st_mode)) {
      s_log_message(_("o_save: Could not restore file permissions [%s]\n"),
		    filename);
    }
#ifdef HAVE_CHOWN
    if (!chown(filename, s.st_uid, s.st_gid)) {
      s_log_message(_("o_save: Could not restore file ownership [%s]\n"),
		    filename);
    }
#endif
  }

  return saved;
}

/*! \brief Create a simple OBJECT from a string
 *  \par Function Description
 *  \a o_basic_read reads a line of text and creates an object from it, of
 *  a type specified in the line itself.
 *
 *  \param [in] toplevel  The TOPLEVEL object.
 *  \param [in]  buf             Character string with circle description.
 *  \param [in]  release_ver     libgeda release version number.
 *  \param [in]  fileformat_ver  libgeda file format version number.
 *  \return A pointer to the new object.
 */
OBJECT *o_basic_read(TOPLEVEL *toplevel, char buf[],
		     unsigned int release_ver, unsigned int fileformat_ver)
{
  OBJECT *new_obj;

  switch (buf[0]) {
  case OBJ_LINE:
    new_obj = o_line_read(toplevel, buf, release_ver, fileformat_ver);
    break;
  case OBJ_NET:
    new_obj = o_net_read(toplevel, buf, release_ver, fileformat_ver);
    break;
  case OBJ_BUS:
    new_obj = o_bus_read(toplevel, buf, release_ver, fileformat_ver);
    break;
  case OBJ_BOX:
    new_obj = o_box_read(toplevel, buf, release_ver, fileformat_ver);
    break;
  case OBJ_CIRCLE:
    new_obj = o_circle_read(toplevel, buf, release_ver, fileformat_ver);
    break;
  case OBJ_ARC:
    new_obj = o_arc_read(toplevel, buf, release_ver, fileformat_ver);
    break;
  default:
    new_obj = NULL;
    break;
  }

  return new_obj;
}

/*! \brief Read a memory buffer
 *  \par Function Description
 *  This function reads data in libgeda format from a memory buffer.
 *
 *  If the size argument is negative, the buffer is assumed to be
 *  null-terminated.
 *
 *  The name argument is used for debugging, and should be set to a
 *  meaningful string (e.g. the name of the file the data is from).
 *
 *  \param [in,out] toplevel    The current TOPLEVEL structure.
 *  \param [in]     object_list  The object_list to read data to.
 *  \param [in]     buffer       The memory buffer to read from.
 *  \param [in]     size         The size of the buffer.
 *  \param [in]     name         The name to describe the data with.
 *  \return object_list if successful read, or NULL on error.
 */
OBJECT *o_read_buffer(TOPLEVEL *toplevel, OBJECT *object_list,
		      char const *buffer, const int size,
		      const char *name)
{
  char *line = NULL;
  TextBuffer *tb = NULL;

  char objtype;
  GList *complex_parents = NULL;
  OBJECT *object_before_attr=NULL;
  unsigned int release_ver;
  unsigned int fileformat_ver;
  unsigned int current_fileformat_ver;
  int found_pin = 0;
  OBJECT* last_complex = NULL;
  int itemsread = 0;

  /* fill version with default file format version (the current one) */
  current_fileformat_ver = FILEFORMAT_VERSION;

  g_return_val_if_fail ((buffer != NULL), NULL);

  tb = s_textbuffer_new (buffer, size);

  while (1) {
    OBJECT *new_obj=NULL;

    line = s_textbuffer_next_line(tb);
    if (line == NULL) break;

    sscanf(line, "%c", &objtype);

    /* Do we need to check the symbol version?  Yes, but only if */
    /* 1) the last object read was a complex and */
    /* 2) the next object isn't the start of attributes.  */
    /* If the next object is the start of attributes, then check the */
    /* symbol version after the attributes have been read in, see the */
    /* STARTATTACH_ATTR case */
    if (last_complex && objtype != STARTATTACH_ATTR)
    {
        /* yes */
        /* verify symbol version (not file format but rather contents) */
        o_complex_check_symversion(toplevel, last_complex);
        last_complex = NULL;  /* no longer need to check */
    }

    switch (objtype) {
      case(OBJ_LINE):
      case(OBJ_NET):
      case(OBJ_BUS):
      case(OBJ_BOX):
      case(OBJ_CIRCLE):
      case(OBJ_ARC):
        new_obj = o_basic_read(toplevel, line, release_ver, fileformat_ver);
        break;
		
      case(OBJ_PICTURE):
	line = g_strdup(line);
        new_obj = o_picture_read(toplevel, line, tb,
				 release_ver, fileformat_ver);
	g_free (line);
        break;
		
      case(OBJ_COMPLEX):
      case(OBJ_PLACEHOLDER):
        new_obj = o_complex_read(toplevel, line, release_ver, fileformat_ver);

        /* last_complex is used for verifying symversion attribute */
        last_complex = new_obj;
        break;

      case(OBJ_SLOT):
	new_obj = s_slot_read(toplevel, line);
	break;

      case(OBJ_TEXT):
	/* Take a copy of the text buffer since o_text_read makes calls to
	 * s_textbuffer_next_line too. */
	line = g_strdup(line);
        new_obj = o_text_read(toplevel, line, tb, release_ver, fileformat_ver);
	g_free(line);
        break;

      case(OBJ_PATH):
        line = g_strdup(line);
        new_obj = o_path_read(toplevel, line, tb, release_ver, fileformat_ver);
        g_free (line);
        break;

      case(OBJ_PIN):
        new_obj = o_pin_read(toplevel, line, release_ver, fileformat_ver);
        found_pin++;
        break;

      case(STARTATTACH_ATTR): 
        object_before_attr = object_list;
	/* first is the fp */
	/* 2nd is the object to get the attributes */ 
        object_list = o_read_attribs(toplevel, object_list,
				     tb, release_ver, fileformat_ver);

        /* by now we have finished reading all the attributes */
        /* did we just finish attaching to a complex object? */
        if (last_complex)
        {
          /* yes */
          /* verify symbol version (not file format but rather contents) */
          o_complex_check_symversion(toplevel, last_complex);
          last_complex = NULL;
        }
        
	g_signal_emit_by_name(G_OBJECT(object_before_attr), "attribs-changed");

	/* slots only apply to complex objects */
        if (object_before_attr->type == OBJ_COMPLEX || 
	    object_before_attr->type == OBJ_PLACEHOLDER) {
          o_attrib_slot_update(toplevel, object_before_attr);
        }

	/* need this? nope */
	/*object_list = return_tail(object_list);*/
        object_before_attr = NULL;
        break;

      case(START_EMBEDDED): 
	/* XXX How can the buffer contain OBJ_PLACEHOLDER? */
	if (object_list->type == OBJ_COMPLEX ||
	    object_list->type == OBJ_PLACEHOLDER) {
	  complex_parents = g_list_prepend(complex_parents, object_list);
	  object_list = object_list->complex->prim_objs;
	} else {
          fprintf(stderr, _("Read unexpected embedded "
                            "symbol start marker in [%s] :\n>>\n%s<<\n"),
                  name, line);
	}
       	break;

      case(END_EMBEDDED): 
	if (complex_parents != NULL) {
	  object_list = complex_parents->data;
	  complex_parents = g_list_delete_link(complex_parents,
					       complex_parents);
	  /* don't do this since objects are already
	   * stored/read translated
	   * o_complex_translate_world(toplevel, object_list->x,
	   *                   object_list->y, object_list->complex);
	   */

          o_recalc_single_object(object_list);
	} else {
          fprintf(stderr, _("Read unexpected embedded "
                            "symbol end marker in [%s] :\n>>\n%s<<\n"),
                  name, line);
	}
	
        break;

      case(ENDATTACH_ATTR):
        /* this case is never hit, since the } is consumed by o_read_attribs */
        break;	

      case(INFO_FONT): 
        o_text_set_info_font(line);
        break;	

      case(COMMENT):
	/* do nothing */
        break;

      case(VERSION_CHAR):
        itemsread = sscanf(line, "v %u %u\n", &release_ver, &fileformat_ver);

	/* 20030921 was the last version which did not have a fileformat */
	/* version.  The below latter test should not happen, but it is here */
	/* just in in case. */
	if (release_ver <= VERSION_20030921 || itemsread == 1)
        { 
          fileformat_ver = 0;
	}
        
	if (fileformat_ver < current_fileformat_ver)
        {
          s_log_message(_("Read an old format sym/sch file!\n"
                          "Please run g[sym|sch]update on:\n[%s]\n"), name);
	}
        break;

      default:
        fprintf(stderr, _("Read garbage in [%s] :\n>>\n%s<<\n"),
                name, line);
        break;
    }

    if (new_obj) {
      object_list = s_basic_link_object(new_obj, object_list);
      if (complex_parents) {
	new_obj->complex_parent = complex_parents->data;
      }
    }
  }

  /* Was the very last thing we read a complex and has it not been checked */
  /* yet?  This would happen if the complex is at the very end of the file  */
  /* and had no attached attributes */
  if (last_complex)
  {
        o_complex_check_symversion(toplevel, last_complex);
        last_complex = NULL;  /* no longer need to check */
  }

  if (found_pin) {
    if (release_ver <= VERSION_20020825) {
      o_pin_update_whichend(toplevel, return_head(object_list), found_pin);
    }
  }

  tb = s_textbuffer_free(tb);
  
  return(object_list);

}

/*! \brief Read a file
 *  \par Function Description
 *  This function reads a file in libgeda format.
 *
 *  \param [in,out] toplevel    The current TOPLEVEL structure.
 *  \param [in]     object_list  The object_list to read data to.
 *  \param [in]     filename     The filename to read from.
 *  \param [in,out] err          #GError structure for error reporting, or
 *                               NULL to disable error reporting
 *  \return object_list if successful read, or NULL on error.
 */
OBJECT *o_read(TOPLEVEL *toplevel, OBJECT *object_list, char const *filename,
               GError **err)
{
  char *buffer = NULL;
  size_t size;
  OBJECT *result = NULL;

  /* Return NULL if error reporting is enabled and the return location
   * for an error isn't NULL. */
  g_return_val_if_fail (err == NULL || *err == NULL, NULL);

  if (!g_file_get_contents(filename, &buffer, &size, err)) {
    return NULL;
  } 

  /* Parse file contents */
  result = o_read_buffer(toplevel, object_list, buffer, size, filename);
  g_free (buffer);
  return result;
}

/*! \brief Scale a set of lines.
 *  \par Function Description
 *  This function takes a list of lines and scales them
 *  by the values of x_scale and y_scale.
 *
 *  \param [in,out]  list  The list with lines to scale.
 *  \param [in]   x_scale  The x scale value for the lines.
 *  \param [in]   y_scale  The y scale value for the lines.
 *
 *  \todo this really doesn't belong here. you need more of a core routine
 *        first. yes.. this is the core routine, just strip out the drawing
 *        stuff
 *        move it to o_complex_scale
 */
void o_scale(OBJECT *list, int x_scale, int y_scale)
{
  OBJECT *o_current;

  /* this is okay if you just hit scale and have nothing selected */
  if (list == NULL) {
    return;
  }

  o_current = list;
  while (o_current != NULL) {
    switch(o_current->type) {
      case(OBJ_LINE):
        o_line_scale_world(x_scale, y_scale, o_current);
        break;
    }
    o_current = o_current->next;
  }
}
