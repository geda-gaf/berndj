/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */

/*! \file o_complex_basic.c
 *  \brief Functions for complex objects
 *
 *  Complex objects are collections of primary objects.
 */

#include <config.h>

#include <stdio.h>
#include <math.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include "factory.h"
#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

static int world_get_complex_bounds(OBJECT *complex,
				    int *left, int *top, int *right, int *bottom);
static void o_complex_recalc(OBJECT *o_current);
static void o_complex_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current,
			    double scale, GArray *unicode_table);
static void o_complex_grip_foreach(OBJECT *o,
				   gboolean (*fn)(OBJECT *o,
						  int grip_x, int grip_y,
						  enum grip_t whichone,
						  void *userdata),
				   void *userdata);
static void o_complex_embed(TOPLEVEL *toplevel, OBJECT *o, char yes);
static enum visit_result
o_complex_visit(OBJECT *o_current,
		enum visit_result (*fn)(OBJECT *, void *), void *context,
		enum visit_order order, int depth);
static OBJECT *o_complex_copy(TOPLEVEL *toplevel, OBJECT *o_current);
static OBJECT *o_complex_copy_embedded(TOPLEVEL *toplevel, OBJECT *o_current);

/*! Default setting for complex draw function. */
void (*complex_draw_func)() = NULL;

/*! \brief Return the bounds of the given object.
 *  \par Given an object, calculate the bounds coordinates.
 *  \param [in] toplevel The toplevel structure.
 *  \param [in] o_current The object to look the bounds for.
 *  \param [out] rleft   pointer to the left coordinate of the object.
 *  \param [out] rtop    pointer to the top coordinate of the object.
 *  \param [out] rright  pointer to the right coordinate of the object.
 *  \param [out] rbottom pointer to the bottom coordinate of the object.
 *  \return If any bounds were found for the object
 *  \retval 0 No bound was found
 *  \retval 1 Bound was found
 */
int world_get_single_object_bounds(OBJECT *o_current,
                                   int *rleft, int *rtop, int *rright, int *rbottom)
{
  g_return_val_if_fail(o_current, 0);

  if (!o_current->w_bounds_valid) {
    o_recalc_single_object(o_current);
    if (!o_current->w_bounds_valid) {
      return 0;
    }
  }
  *rleft = o_current->w_left;
  *rtop = o_current->w_top;
  *rright = o_current->w_right;
  *rbottom = o_current->w_bottom;
  return 1;
}

struct get_bounds_context {
  int found;
  int *left, *top, *right, *bottom;
};

static enum visit_result get_bounds_one(OBJECT *o_current, void *userdata)
{
  struct get_bounds_context *ctx = userdata;
  int rleft, rtop, rright, rbottom;

  if (world_get_single_object_bounds(o_current, &rleft, &rtop, &rright, &rbottom)) {
    if (ctx->found) {
      *ctx->left = min(*ctx->left, rleft);
      *ctx->top = min(*ctx->top, rtop);
      *ctx->right = max(*ctx->right, rright);
      *ctx->bottom = max(*ctx->bottom, rbottom);
    } else {
      *ctx->left = rleft;
      *ctx->top = rtop;
      *ctx->right = rright;
      *ctx->bottom = rbottom;
      ctx->found = 1;
    }
  }

  return VISIT_RES_OK;
}

/*! \brief Return the bounds of the given list of objects.
 *  \par Given a list of objects, calculate the bounds coordinates.
 *  \param [in] complex   The list of objects to look the bounds for.
 *  \param [out] left   pointer to the left coordinate of the object.
 *  \param [out] top    pointer to the top coordinate of the object.
 *  \param [out] right  pointer to the right coordinate of the object.
 *  \param [out] bottom pointer to the bottom coordinate of the object.
 *  \return If any bounds were found for the list of objects
 *  \retval 0 No bounds were found
 *  \retval 1 Bound was found
 */
int
world_get_object_list_bounds(OBJECT *complex, enum list_kind kind,
		       int *left, int *top, int *right, int *bottom)
{
  struct get_bounds_context context = {
    .found = 0,
    .left = left,
    .top = top,
    .right = right,
    .bottom = bottom,
  };

  s_visit_list(complex, kind, &get_bounds_one, &context, VISIT_ANY, 1);

  return context.found;
}

/*! \brief Return the bounds of the given GList of objects.
 *  \par Given a list of objects, calculate the bounds coordinates.
 *  \param [in]  head   The list of objects to look the bounds for.
 *  \param [out] left   pointer to the left coordinate of the object.
 *  \param [out] top    pointer to the top coordinate of the object.
 *  \param [out] right  pointer to the right coordinate of the object.
 *  \param [out] bottom pointer to the bottom coordinate of the object.
 *  \return If any bounds were found for the list of objects
 *  \retval 0 No bounds were found
 *  \retval 1 Bound was found
 */
int world_get_object_glist_bounds(GList const *head,
                                  int *left, int *top, int *right, int *bottom)
{
  GList const *s_current=NULL;
  OBJECT *o_current=NULL;
  int rleft, rtop, rright, rbottom;
  int found = 0;

  s_current = head;

  /* Find the first object with bounds, and set the bounds variables, then expand as necessary. */
  while ( s_current != NULL ) {
    o_current = s_current->data;
    g_assert (o_current != NULL);
    if (world_get_single_object_bounds(o_current, &rleft, &rtop, &rright, &rbottom)) {
      if ( found ) {
        *left = min( *left, rleft );
        *top = min( *top, rtop );
        *right = max( *right, rright );
        *bottom = max( *bottom, rbottom );
      } else {
        *left = rleft;
        *top = rtop;
        *right = rright;
        *bottom = rbottom;
        found = 1;
      }
    }
    s_current = g_list_next (s_current);
  }
  return found;
}

/*! \brief Queries the bounds of a complex object.
 *  \par Function Description
 *  This function returns the bounding box of the complex object
 *  <B>object</B>.
 *
 *  \param [in]  complex   The complex object.
 *  \param [out] left      The leftmost edge of the bounding box (in
 *                         world units).
 *  \param [out] top       The upper edge of the bounding box (in
 *                         world units).
 *  \param [out] right     The rightmost edge of the bounding box (in
 *                         world units).
 *  \param [out] bottom    The bottom edge of the bounding box (in
 *                         screen units).
 *  \return If any bounds were found for the list of objects
 *  \retval 0 No bounds were found
 *  \retval 1 Bound was found
 */
static int world_get_complex_bounds(OBJECT *complex,
				    int *left, int *top, int *right, int *bottom)
{
  g_return_if_fail (complex != NULL &&
                    (complex->type == OBJ_COMPLEX ||
                     complex->type == OBJ_PLACEHOLDER) &&
                    complex->complex != NULL);

  return world_get_object_list_bounds(complex->complex->prim_objs, LIST_KIND_HEAD,
				      left, top, right, bottom);
}

/*! \brief create a new head object
 *  \par Function Description
 *  This function creates a <b>complex_head</b> OBJECT. This OBJECT
 *  is just a special empty object. This object is never modified.
 *  
 *  \return new head OBJECT
 */
OBJECT *new_head(TOPLEVEL *toplevel)
{
  OBJECT *new_node=NULL;

  new_node = s_toplevel_new_object(toplevel, OBJ_HEAD, "complex_head"); 

  return new_node;
}

char *o_complex_get_refdes(OBJECT const *o_current,
			   char const *default_value)
{
  static GHashTable *objects_warned = NULL;
  char *refdes;

  /* Casting away const is okay because we don't use return_objects. */
  refdes = o_attrib_search_name_single((OBJECT *) o_current, "refdes", NULL);
  if (refdes == NULL) {
    refdes = o_attrib_search_name_single((OBJECT *) o_current, "uref", NULL);
    if (refdes) {
      /* Warn once about a symbol using uref instead of refdes. */
      if (!objects_warned) {
	objects_warned = g_hash_table_new(&g_direct_hash, &g_direct_equal);
	libgeda_caches = g_list_prepend(libgeda_caches, objects_warned);
      }
      if (!g_hash_table_lookup_extended(objects_warned,
					GINT_TO_POINTER(o_current->sid),
					NULL, NULL)) {
	g_warning(_("Found uref=%s; please use refdes=\n"), refdes);
	g_hash_table_replace(objects_warned,
			     GINT_TO_POINTER(o_current->sid),
			     GINT_TO_POINTER(o_current->sid));
      }
    } else {
      refdes = default_value ? g_strdup(default_value) : NULL;
    }
  }

  return refdes;
}

/*! \brief check whether an object is a attributes
 *  \par Function Description
 *  This function checks if an object should be promoted.
 *  An attribute object is promotable if it's promoted by default, or the user
 *  has configured it to promote an attribute.
 *
 *  \param [in] toplevel  The TOPLEVEL object
 *  \param [in] object    The attribute object to check
 *  \return TRUE if the object is a eligible attribute, FALSE otherwise
 */
static int o_complex_is_eligible_attribute (TOPLEVEL *toplevel, OBJECT *object)
{
  char *name = NULL;
  int promotableAttribute = FALSE;

  g_return_val_if_fail(object->type == OBJ_TEXT, FALSE);

  /* check list against attributes which can be promoted */
  if (toplevel->always_promote_attributes != NULL) {
    if (o_attrib_get_name_value(o_text_get_string(object), &name, NULL)) {
      if (g_list_find_custom(toplevel->always_promote_attributes,
			     name, (GCompareFunc) strcmp) != NULL) {
        /* Name of the attribute was in the always promote attributes list */
        promotableAttribute = TRUE;
      }

      g_free(name);
      if (promotableAttribute)
        return TRUE;
    }
  }

  /* object is invisible and we do not want to promote invisible text */
  if (!o_text_printing_p(object) && toplevel->promote_invisible == FALSE)
    return FALSE; /* attribute not eligible for promotion */

  /* yup, attribute can be promoted */
  return TRUE;
}

/*! \brief get the embedded state of an complex object
 *  \par Function Description
 *  Checks and returns the status of the complex object.
 *
 *  \param o_current  The object to check
 *  \return 1 if embedded, 0 otherwise
 */
int o_complex_is_embedded(OBJECT const *o_current)
{
  g_return_val_if_fail(o_current != NULL, 0);

  if(o_current->complex == NULL)
    return 0;

  if (o_current->complex->is_embedded) {
    return 1;
  } else {
    return 0;
  }
}

static enum visit_result get_toplevel_attribs_one(OBJECT *o_current,
						  void *userdata)
{
  GList **o_list = userdata;

  if (o_current->type == OBJ_TEXT &&
      o_current->attached_to == NULL &&
      o_attrib_get_name_value(o_text_get_string(o_current), NULL, NULL)) {
    *o_list = g_list_prepend(*o_list, o_current);
  }

  return VISIT_RES_OK;
}

/*! \brief Get a list of all toplevel attributes of and object list
 *
 *  \par Function Description
 *  Returns a GList of all attribute OBJECTs
 *
 *  \param [in]  o_head   The head of the object list
 *  \returns              A GList of attribute OBJECTs
 */
GList *o_complex_get_toplevel_attribs(OBJECT *o_head)
{
  GList *o_list = NULL;

  s_visit_list(o_head, LIST_KIND_HEAD, &get_toplevel_attribs_one, &o_list,
	       VISIT_DETERMINISTIC, 1);
  o_list = g_list_reverse (o_list);

  return o_list;
}


/*! \brief Get attributes eligible for promotion from inside a complex
 *
 *  \par Function Description
 *  Returns a GList of OBJECTs which are eligible for promotion from
 *  within the passed complex OBJECT.
 *
 *  If detach is TRUE, the function removes these attribute objects from
 *  the prim_objs of the complex. It detached, the returned OBJECTs are
 *  isolated from each other, having their next and prev pointers set to NULL.
 *
 *  If detach is FALSE, the OBJECTs are left in place. Their next and prev
 *  pointers form part of the complex's prim_objs linked list.
 *
 *  \param [in]  toplevel The toplevel environment.
 *  \param [in]  object   The complex object being modified.
 *  \param [in]  detach   Should the attributes be detached?
 *  \returns              A linked list of OBJECTs to promote.
 */
GList *o_complex_get_promotable (TOPLEVEL *toplevel, OBJECT *object, int detach)
{
  GList *promoted = NULL;
  GList *attribs;
  GList *iter;
  OBJECT *tmp;

  if (!toplevel->attribute_promotion) /* controlled through rc file */
    return NULL;

  attribs = o_complex_get_toplevel_attribs(object->complex->prim_objs);

  for (iter = attribs; iter != NULL; iter = g_list_next (iter)) {
    tmp = iter->data;

    /* Is it an attribute we want to promote? */
    if (!o_complex_is_eligible_attribute(toplevel, tmp))
      continue;

    if (detach) {
      /* Remove and isolate it from the complex list */
      s_basic_unlink_object(tmp);
      tmp->complex_parent = NULL;
    }

    promoted = g_list_prepend (promoted, tmp);
  }

  g_list_free (attribs);

  promoted = g_list_reverse (promoted);
  return promoted;
}

static enum visit_result signal_add_object_one(OBJECT *o, void *userdata)
{
  PAGE *page = userdata;

  g_signal_emit_by_name(page, "add-object", o);

  return VISIT_RES_OK;
}

/*! \brief Promote attributes from the passed OBJECT
 *
 *  \par Function Description
 *  Promotes attributes from the passed OBJECT, linking them into the
 *  OBJECT linked list immediately prior to the passed OBJECT.
 *
 *  \param [in]  toplevel The toplevel environment.
 *  \param [in]  page     The PAGE on which \a object exists.
 *  \param [in]  object   The complex object whose attributes are being promoted.
 */
void o_complex_promote_attribs(TOPLEVEL *toplevel, PAGE *page, OBJECT *object)
{
  GList *promoted;
  OBJECT *first_promoted, *last_promoted;

  promoted = o_complex_get_promotable (toplevel, object, TRUE);

  if (promoted == NULL)
    return;

  /* Link the promoted OBJECTs together */
  o_glist_relink_objects (promoted);

  first_promoted = promoted->data;
  last_promoted = g_list_last (promoted)->data;

  /* Insert promoted attributes before the complex in the object list */
  s_basic_splice(object, first_promoted, last_promoted);
  if (page) {
    /* XXX Emit signal only after the objects are on the page. */
    s_visit_list(first_promoted, LIST_KIND_REST, &signal_add_object_one, page,
		 VISIT_ANY, 1);
  }

  /* Attach promoted attributes to the original complex object */
  o_attrib_attach_list (toplevel, promoted, object);

  g_list_free (promoted);
}


/*! \brief Delete or hide promotable from the passed OBJECT
 *
 *  \par Function Description
 *  Deletes or hides promotable attributes from the passed OBJECT.
 *  This is used when loading symbols during the load of a schematic from
 *  disk. The schematic will already contain local copies of symbol's
 *  promotable objects, so we delete or hide the symbol's copies.
 *
 *  Deletion / hiding is dependant on the setting of
 *  toplevel->keep_invisible. If true, attributes eligible for
 *  promotion are kept in memory but flagged as invisible.
 *
 *  \param [in]  toplevel The toplevel environment.
 *  \param [in]  object   The complex object being altered.
 */
void o_complex_remove_promotable_attribs (TOPLEVEL *toplevel, OBJECT *object)
{
  GList *promotable, *iter;

  promotable = o_complex_get_promotable (toplevel, object, FALSE);

  if (promotable == NULL)
    return;

  for (iter = promotable; iter != NULL; iter = g_list_next (iter)) {
    OBJECT *a_object = iter->data;
    g_warn_if_fail(a_object->type == OBJ_TEXT);
    if (toplevel->keep_invisible == TRUE) {
      /* Hide promotable attributes */
      o_text_change(a_object, NULL, INVISIBLE, LEAVE_NAME_VALUE_ALONE);
    } else {
      s_delete (toplevel, a_object);    /* Delete promotable attributes */
    }
  }

  g_list_free (promotable);
}

static void o_complex_notice_destroy(OBJECT *subject, void *userdata)
{
  TOPLEVEL *toplevel = userdata;

  /* Evict all slotted symbols. */
  s_slot_destroy_complex(toplevel, subject);

  /* Detach from any owning slot. */
  s_slot_unlink(subject);
}

static enum visit_result set_complex_parent_one(OBJECT *tmp, void *userdata)
{
  OBJECT *new_node = userdata;

  tmp->complex_parent = new_node;

  return VISIT_RES_OK;
}

/* Done */
/*! \brief
 *  \par Function Description
 *
 */
OBJECT *o_complex_new(TOPLEVEL *toplevel,
		      char type,
		      int color, int x, int y, int angle,
		      int mirror, const CLibSymbol *clib,
		      const gchar *basename,
		      int selectable)
{
  OBJECT *new_node=NULL;
  OBJECT *prim_objs=NULL;
  OBJECT *new_prim_obj;
  int save_adding_sel = 0;
  int loaded_normally = FALSE;

  gchar *buffer;

  new_node = s_toplevel_new_object(toplevel, type, "complex");
  g_signal_connect(G_OBJECT(new_node), "remove",
		   G_CALLBACK(o_complex_notice_destroy), toplevel);

  if (clib != NULL) {
    new_node->complex_basename = g_strdup (s_clib_symbol_get_name (clib));
  } else {
    new_node->complex_basename = g_strdup (basename);
  }

  new_node->color = color;
  new_node->selectable = selectable;
	
  new_node->complex = g_malloc(sizeof (COMPLEX));
	
  new_node->complex->angle = angle;
  new_node->complex->mirror = mirror;
  new_node->complex->is_embedded = FALSE;

  new_node->complex->x = x;
  new_node->complex->y = y;

  new_node->copy_func = &o_complex_copy;
  new_node->bounds_recalc_func = o_complex_recalc;
  new_node->draw_func = complex_draw_func;
  new_node->psprint_func = &o_complex_print;
  new_node->grip_foreach_func = &o_complex_grip_foreach;
  new_node->embed_func = &o_complex_embed;
  new_node->visit_func = &o_complex_visit;

  /* this was at the beginning and p_complex was = to complex */
  prim_objs = new_head(toplevel);

  /* get the symbol data */
  if (clib != NULL) {
    buffer = s_clib_symbol_get_data (clib);
  }

  save_adding_sel = toplevel->ADDING_SEL;
  toplevel->ADDING_SEL = 1;	/* name is hack, don't want to */

  if (clib == NULL || buffer == NULL) {
    OBJECT *save_prim_objs;
    char *not_found_text = NULL;
    int left, right, top, bottom;
    int x_offset, y_offset;

    /* filename was NOT found */
    loaded_normally = FALSE;

    /* save the prim_objs pointer, since the following code modifies it */
    save_prim_objs = prim_objs;

    /* Put placeholder into object list.  Changed by SDB on
     * 1.19.2005 to fix problem that symbols were silently
     * deleted by gattrib when RC files were messed up.  */
    new_node->type = OBJ_PLACEHOLDER;

    /* Mark the origin of the missing component */
    new_prim_obj = o_line_new(toplevel, OBJ_LINE,
                           DETACHED_ATTRIBUTE_COLOR,
                           x - 50, y, x + 50, y);
    prim_objs = s_basic_link_object(new_prim_obj, prim_objs);
    new_prim_obj = o_line_new(toplevel, OBJ_LINE,
                           DETACHED_ATTRIBUTE_COLOR,
                           x, y + 50, x, y - 50); 
    prim_objs = s_basic_link_object(new_prim_obj, prim_objs);

    /* Add some useful text */
    not_found_text = 
      g_strdup_printf (_("Component not found:\n %s"),
		       new_node->complex_basename);
    new_prim_obj = o_text_new(toplevel,
                           OBJ_TEXT, DETACHED_ATTRIBUTE_COLOR, 
                           x + NOT_FOUND_TEXT_X, 
                           y + NOT_FOUND_TEXT_Y, LOWER_LEFT, 0, 
                           not_found_text, 8,
                           VISIBLE, SHOW_NAME_VALUE);
    prim_objs = s_basic_link_object(new_prim_obj, prim_objs);
    g_free(not_found_text);

    /* figure out where to put the hazard triangle */
    world_get_text_bounds(prim_objs, &left, &top, &right, &bottom);
    x_offset = (right - left) / 4;  
    y_offset = bottom - top + 100;  /* 100 is just an additional offset */

    /* TODO: set buffer to string constant that defines the triangle. */
    /* add hazard triangle */
    new_prim_obj = o_line_new(toplevel, OBJ_LINE,
                           DETACHED_ATTRIBUTE_COLOR,
                           x + NOT_FOUND_TEXT_X + x_offset, 
                           y + NOT_FOUND_TEXT_Y + y_offset, 
                           x + NOT_FOUND_TEXT_X + x_offset + 600, 
                           y + NOT_FOUND_TEXT_Y + y_offset); 
    o_set_line_options(new_prim_obj, END_ROUND, TYPE_SOLID, 50, -1, -1);
    prim_objs = s_basic_link_object(new_prim_obj, prim_objs);
    new_prim_obj = o_line_new(toplevel, OBJ_LINE,
                           DETACHED_ATTRIBUTE_COLOR,
                           x + NOT_FOUND_TEXT_X + x_offset, 
                           y + NOT_FOUND_TEXT_Y + y_offset, 
                           x + NOT_FOUND_TEXT_X + x_offset + 300, 
                           y + NOT_FOUND_TEXT_Y + y_offset + 500); 
    o_set_line_options(new_prim_obj, END_ROUND, TYPE_SOLID, 50, -1, -1);
    prim_objs = s_basic_link_object(new_prim_obj, prim_objs);
    new_prim_obj = o_line_new(toplevel, OBJ_LINE,
                           DETACHED_ATTRIBUTE_COLOR,
                           x + NOT_FOUND_TEXT_X + x_offset + 300, 
                           y + NOT_FOUND_TEXT_Y + y_offset + 500, 
                           x + NOT_FOUND_TEXT_X + x_offset + 600, 
                           y + NOT_FOUND_TEXT_Y + y_offset); 
    o_set_line_options(new_prim_obj, END_ROUND, TYPE_SOLID, 50, -1, -1);
    prim_objs = s_basic_link_object(new_prim_obj, prim_objs);
    new_prim_obj = o_text_new(toplevel,
                           OBJ_TEXT, DETACHED_ATTRIBUTE_COLOR, 
                           x + NOT_FOUND_TEXT_X + x_offset + 270, 
                           y + NOT_FOUND_TEXT_Y + y_offset + 90, 
                           LOWER_LEFT, 0, "!", 18,
                           VISIBLE, SHOW_NAME_VALUE);
    prim_objs = s_basic_link_object(new_prim_obj, prim_objs);
    prim_objs = save_prim_objs;
  } else {
    /* filename was found */
    loaded_normally = TRUE;

    /* add connections till translated */
    o_read_buffer(toplevel, prim_objs, buffer, -1, new_node->complex_basename);

    g_free (buffer);
  }
  toplevel->ADDING_SEL = save_adding_sel;

  /* do not mirror/rotate/translate/connect the primitive objects if the
   * component was not loaded via o_read 
   */
  if (loaded_normally == TRUE) {
    if (mirror) {
      o_list_mirror_world(0, 0, prim_objs);
    }

    o_list_rotate_world(0, 0, angle, prim_objs);
    o_list_translate_world(x, y, prim_objs);
  }

  new_node->complex->prim_objs = prim_objs;

  /* XXX s_visit_list skips the OBJ_HEAD node. */
  set_complex_parent_one(prim_objs, new_node);
  s_visit_list(prim_objs, LIST_KIND_HEAD, &set_complex_parent_one, new_node,
	       VISIT_ANY, 1);

  o_complex_recalc(new_node);

  s_slot_setup_complex(toplevel, new_node);

  return new_node;
}

/*! \brief create a new embedded object
 *  \par Function Description
 *  This function creates a new embedded object.
 *
 *  \param [in]  toplevel  The TOPLEVEL object
 *  \param [in]  type      The type of the object (usually OBJ_COMLEX)
 *  \param [in]  color     The color of the object
 *  \param [in]  x         The x location of the complex object
 *  \param [in]  y         The y location of the complex object
 *  \param [in]  angle     The rotation angle
 *  \param [in]  mirror    The mirror status
 *  \param [in]  basename  The basic name the embedded was created of
 *  \param [in]  selectable whether the object can be selected with the mouse
 *  \return a new complex object
 */
OBJECT *o_complex_new_embedded(TOPLEVEL *toplevel,
			       char type, int color, int x, int y, int angle, int mirror,
			       const gchar *basename, int selectable)
{
  OBJECT *prim_objs=NULL;
  OBJECT *new_node=NULL;

  new_node = s_toplevel_new_object(toplevel, type, "complex");

  new_node->complex = g_malloc(sizeof (COMPLEX));
  new_node->complex->x = x;
  new_node->complex->y = y;

  new_node->complex->angle = angle;
  new_node->complex->mirror = mirror;
  new_node->complex->is_embedded = TRUE;
	
  new_node->complex_basename = g_strdup(basename);

  new_node->color = color;
  new_node->selectable = selectable;

  new_node->copy_func = &o_complex_copy_embedded;
  new_node->bounds_recalc_func = o_complex_recalc;
  new_node->draw_func = complex_draw_func;
  new_node->psprint_func = &o_complex_print;
  new_node->grip_foreach_func = &o_complex_grip_foreach;
  new_node->embed_func = &o_complex_embed;
  new_node->visit_func = &o_complex_visit;

  /* this was at the beginning and p_complex was = to complex */
  prim_objs = new_head(toplevel);
  new_node->complex->prim_objs = prim_objs;

  set_complex_parent_one(prim_objs, new_node);
  /* XXX Embedded prim_objs will get reparented in o_read_buffer. */

  /* don't have to translate/rotate/mirror here at all since the */
  /* object is in place */
  return new_node;
}

/*! \brief update the visual boundaries of the complex object
 *  \par Function Description
 *  This function updates the boundaries of the object \a o_current.
 *
 *  \param [in]  toplevel  The TOPLEVEL object
 *  \param [in]  o_current The OBJECT to update
 */
static void o_complex_recalc(OBJECT *o_current)
{
  int left, right, top, bottom;

  /* recalc routine Add this somewhere */
  /* libhack */
  /* o_recalc(o_current->complex);*/

  if ((!o_current) || (o_current->type != OBJ_COMPLEX && o_current->type != OBJ_PLACEHOLDER))
    return;

  if (o_current->complex->x == -1 && o_current->complex->y == -1) {
    /* Off-schematic components have no bounding box. */
    return;
  }

  if (world_get_complex_bounds(o_current, &left, &top, &right, &bottom)) {
    o_current->w_left = left;
    o_current->w_top = top;
    o_current->w_right = right;
    o_current->w_bottom = bottom;
    o_current->w_bounds_valid = TRUE;
  }
}

/*! \brief read a complex object from a char buffer
 *  \par Function Description
 *  This function reads a complex object from the buffer \a buf.
 *  
 *  \param [in] toplevel     The TOPLEVEL object
 *  \param [in] buf          a text buffer (usually a line of a schematic file)
 *  \param [in] release_ver  The release number gEDA
 *  \param [in] fileformat_ver a integer value of the file format
 *  \return The new object
 *
 *  \todo Don't use fixed-length string for symbol basename
 */
OBJECT *o_complex_read(TOPLEVEL *toplevel,
		       char buf[], unsigned int release_ver,
		       unsigned int fileformat_ver)
{
  OBJECT *new_obj;
  char type; 
  int x1, y1;
  int angle;

  char basename[256]; /* FIXME This is a hack */
	
  int selectable;
  int mirror;

  sscanf(buf, "%c %d %d %d %d %d %s\n",
         &type, &x1, &y1, &selectable, &angle, &mirror, basename);

  switch(angle) {
    case(0):
    case(90):
    case(180):
    case(270):
      break;

    default:
      s_log_message(_("Found a component with an invalid rotation [ %c %d %d %d %d %d %s ]\n"), type, x1, y1, selectable, angle, mirror, basename);
      break;
  }

  switch(mirror) {
    case(0):
    case(1):
      break;
		
    default:
      s_log_message(_("Found a component with an invalid mirror flag [ %c %d %d %d %d %d %s ]\n"), type, x1, y1, selectable, angle, mirror, basename);
      break;
  }
  if (strncmp(basename, "EMBEDDED", 8) == 0) {
    new_obj = o_complex_new_embedded(toplevel, type,
                                     WHITE, x1, y1, angle, mirror,
                                     basename + 8,
                                     selectable);
  } else {
    const CLibSymbol *clib = s_clib_get_symbol_by_name (basename);

    new_obj = o_complex_new(toplevel, type,
                                WHITE, 
                                x1, y1, 
                                angle, mirror, clib,
                                basename, selectable);
    /* Delete or hide attributes eligible for promotion inside the complex */
     o_complex_remove_promotable_attribs (toplevel, new_obj);
  }

  return new_obj;
}

/*! \brief Create a string representation of the complex object
 *  \par Function Description
 *  This function takes a complex \a object and return a string
 *  according to the file format definition.
 *
 *  \param [in] object  a complex OBJECT
 *  \return the string representation of the complex OBJECT
 */
char *o_complex_save(OBJECT *object)
{
  char *buf = NULL;
  char *basename;

  g_return_val_if_fail (object != NULL, NULL);

  if ((object->type == OBJ_COMPLEX) || (object->type == OBJ_PLACEHOLDER)) {
    basename = g_strdup_printf ("%s%s",
				object->complex->is_embedded ? "EMBEDDED" : "",
				object->complex_basename);
    /* We force the object type to be output as OBJ_COMPLEX for both
     * these object types. */
    buf = g_strdup_printf("%c %d %d %d %d %d %s", OBJ_COMPLEX,
                          object->complex->x, object->complex->y,
                          object->selectable, object->complex->angle,
                          object->complex->mirror, basename);
    g_free (basename);
  }

  return(buf);
}

/*! \brief move a complex object
 *  \par Function Description
 *  This function changes the position of a complex \a object.
 *
 *  \param [in] dx           The x-distance to move the object
 *  \param [in] dy           The y-distance to move the object
 *  \param [in] object       The complex OBJECT to be moved
 */
void o_complex_translate_world(int dx, int dy, OBJECT *object)
{
  g_return_if_fail (object != NULL &&
                    (object->type == OBJ_COMPLEX ||
                     object->type == OBJ_PLACEHOLDER));

  object->complex->x = object->complex->x + dx;
  object->complex->y = object->complex->y + dy;

  o_list_translate_world(dx, dy, object->complex->prim_objs);

  o_complex_recalc(object);
}

static void o_complex_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current,
			    double scale, GArray *unicode_table)
{
  fprintf(fp, "gsave\n");
  f_print_objects(toplevel, fp,
		  o_current->complex->prim_objs,
		  0, 0, scale,
		  unicode_table);
  fprintf(fp, "grestore\n");
}

static void o_complex_grip_foreach(OBJECT *o,
				   gboolean (*fn)(OBJECT *o,
						  int grip_x, int grip_y,
						  enum grip_t whichone,
						  void *userdata),
				   void *userdata)
{
  const GRIP complex_grips[] = {
    {
      .x = o->complex->x,
      .y = o->complex->y,
      .whichone = GRIP_COMPLEX_ORIGIN,
    },
    { .whichone = GRIP_NONE }
  };

  s_basic_grip_foreach_helper(o, complex_grips, fn, userdata);
}

static void o_complex_embed(TOPLEVEL *toplevel, OBJECT *o, char yes)
{

  g_assert(o->type == OBJ_COMPLEX || o->type == OBJ_PLACEHOLDER);

  if (!o_complex_is_embedded(o) == !yes) {
    return;
  }

  if (yes) {
    /* Set the embedded flag. */
    o->complex->is_embedded = TRUE;
    o->copy_func = &o_complex_copy_embedded;

    s_log_message("Component [%s] has been embedded\n",
		  o->complex_basename);
  } else {
    const CLibSymbol *sym;

    /* search for the symbol in the library */
    sym = s_clib_get_symbol_by_name(o->complex_basename);

    if (sym == NULL) {
      /* symbol not found in the symbol library: signal an error */
      s_log_message(_("Could not find component [%s], while trying to "
		      "unembed. Component is still embedded\n"),
		    o->complex_basename);
      return;
    }

    /* clear the embedded flag */
    o->complex->is_embedded = FALSE;
    o->copy_func = &o_complex_copy;

    s_log_message(_("Component [%s] has been successfully unembedded\n"),
		  o->complex_basename);
  }

  toplevel->page_current->CHANGED = 1; /* FIXME: Do this from an observer. */
}

static enum visit_result
o_complex_visit(OBJECT *o_current,
		enum visit_result (*fn)(OBJECT *, void *), void *context,
		enum visit_order order, int depth)
{
  g_return_if_fail(o_current->type == OBJ_COMPLEX);
  g_return_if_fail(o_current->complex->prim_objs != NULL);

  return (s_visit_list(o_current->complex->prim_objs, LIST_KIND_HEAD, fn,
		       context, order, depth));
}

/*! \brief create a copy of a complex object
 *  \par Function Description
 *  This function creates a copy of the complex object \a o_current.
 *
 *  \param [in] toplevel     The TOPLEVEL object
 *  \param [in] o_current    The object that is copied
 *  \return a new complex object
 */
static OBJECT *o_complex_copy(TOPLEVEL *toplevel, OBJECT *o_current)
{
  OBJECT *new_obj=NULL;
  int color;
  const CLibSymbol *clib = NULL;

  g_return_val_if_fail(o_current != NULL, NULL);

  if (o_current->saved_color == -1) {
    color = o_current->color;
  } else {
    color = o_current->saved_color;
  }
	
  /*
   * FIXME If the component library path changes at runtime, then a copy
   * might not be an identical one. Maybe rather copy prim_objs directly?
   */
  clib = s_clib_get_symbol_by_name (o_current->complex_basename);

  new_obj = o_complex_new (toplevel, o_current->type, color,
                           o_current->complex->x, o_current->complex->y,
                           o_current->complex->angle,
                           o_current->complex->mirror,
                           clib, o_current->complex_basename,
                           o_current->selectable);

  /* Delete or hide attributes eligible for promotion inside the complex */
   o_complex_remove_promotable_attribs (toplevel, new_obj);

  o_attrib_slot_copy(toplevel, o_current, new_obj);

  /* deal with stuff that has changed */

  /* here you need to create a list of attributes which need to be 
   * connected to the new list, probably make an attribute list and
   * fill it with sid's of the attributes */

  return new_obj;
}

/*! \brief create a copy of a embedded complex object
 *  \par Function Description
 *  This function creates a copy of an embedded complex object \a o_current.
 *
 *  \param [in] toplevel     The TOPLEVEL object
 *  \param [in] o_current    The object that is copied
 *  \return a new complex object
 */
static OBJECT *o_complex_copy_embedded(TOPLEVEL *toplevel, OBJECT *o_current)
{
  OBJECT *new_obj=NULL;
  OBJECT *temp_list;
  int color;

  g_return_val_if_fail(o_current != NULL, NULL);

  if (o_current->saved_color == -1) {
    color = o_current->color;
  } else {
    color = o_current->saved_color;
  }

  new_obj = o_complex_new_embedded (toplevel, o_current->type, color,
                                    o_current->complex->x, o_current->complex->y,
                                    o_current->complex->angle,
                                    o_current->complex->mirror,
                                    o_current->complex_basename,
                                    o_current->selectable);

  /* deal with stuff that has changed */
	
  temp_list = o_list_copy_all(toplevel,
                              o_current->complex->prim_objs,
                              new_obj->complex->prim_objs, 
                              toplevel->ADDING_SEL);
	
  new_obj->complex->prim_objs = return_head(temp_list);

  /* XXX s_visit_list skips the OBJ_HEAD node. */
  set_complex_parent_one(new_obj->complex->prim_objs, new_obj);
  s_visit_list(new_obj->complex->prim_objs, LIST_KIND_HEAD,
	       &set_complex_parent_one, new_obj, VISIT_ANY, 1);

  o_complex_recalc(new_obj);

  /* here you need to create a list of attributes which need to be 
   * connected to the new list, probably make an attribute list and
   * fill it with sid's of the attributes */

  return new_obj;
}

static enum visit_result set_color_one(OBJECT *o_current, void *userdata)
{
  int *color = userdata;

  o_current->color = *color;

  return VISIT_RES_OK;
}

/*! \brief Change the color of an object
 *  \par Function Description
 *  This function changes the the color of an object
 *
 *  \param [in] object     The object
 *  \param [in] color      The new color
 *
 *  \note This function is mainly used to change the color of text objects
 */
void o_complex_set_color(OBJECT *object, int color)
{
  s_visit(object, &set_color_one, &color, VISIT_ANY, VISIT_DEPTH_FOREVER);
}

static enum visit_result set_color_save_one(OBJECT *o_current, void *userdata)
{
  int const *color = userdata;

  o_current->saved_color = o_current->color;
  o_current->color = *color;

  return VISIT_RES_OK;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_complex_set_color_save(OBJECT *o_current, int color)
{
  s_visit(o_current, &set_color_save_one, &color, VISIT_ANY,
	  VISIT_DEPTH_FOREVER);
}

static enum visit_result unset_color_one(OBJECT *o_current, void *userdata)
{
  o_current->color = o_current->saved_color;
  o_current->saved_color = -1;

  return VISIT_RES_OK;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_complex_unset_color(OBJECT *o_current)
{
  s_visit(o_current, &unset_color_one, NULL, VISIT_ANY, VISIT_DEPTH_FOREVER);
}

static enum visit_result set_color_save_only_one(OBJECT *o_current,
						 void *userdata)
{
  int const *color = userdata;

  o_current->saved_color = *color;

  return VISIT_RES_OK;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_complex_set_saved_color_only(OBJECT *o_current, int color)
{
  s_visit(o_current, &set_color_save_only_one, &color, VISIT_ANY,
	  VISIT_DEPTH_FOREVER);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_complex_rotate_world(int centerx, int centery,
                            int angle, OBJECT *object)
{
  int x, y;
  int newx, newy;

  g_return_if_fail (object!=NULL);
  g_return_if_fail ((object->type == OBJ_COMPLEX) ||
                    (object->type == OBJ_PLACEHOLDER));

  x = object->complex->x + (-centerx);
  y = object->complex->y + (-centery);

  rotate_point_90(x, y, angle, &newx, &newy);

  x = newx + (centerx);
  y = newy + (centery);

  o_complex_translate_world(-object->complex->x,
                            -object->complex->y, object);
  o_list_rotate_world(0, 0, angle, object->complex->prim_objs);

  object->complex->x = 0;
  object->complex->y = 0;

  o_complex_translate_world(x, y, object);

  object->complex->angle = ( object->complex->angle + angle ) % 360;
}


/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void o_complex_mirror_world(int world_centerx, int world_centery,
                            OBJECT *object)
{
  int x, y;

  g_return_if_fail( object != NULL );
  g_return_if_fail( (object->type == OBJ_COMPLEX ||
                     object->type == OBJ_PLACEHOLDER) );
  g_return_if_fail( object->complex != NULL );

  x = 2 * world_centerx - object->complex->x;
  y = object->complex->y;

  o_complex_translate_world(-object->complex->x,
                            -object->complex->y, object);

  o_list_mirror_world(0, 0, object->complex->prim_objs );

  switch(object->complex->angle) {
    case(90):
      object->complex->angle = 270;
      break;

    case(270):
      object->complex->angle = 90;
      break;

  }

  object->complex->mirror = !object->complex->mirror;

  o_complex_translate_world(x, y, object);
}

struct return_pin_context {
  char const *pin;
  OBJECT *found;
};

static enum visit_result return_pin_one(OBJECT *o_current, void *userdata)
{
  struct return_pin_context *ctx = userdata;
  OBJECT *found;

  switch(o_current->type) {
  case(OBJ_PIN):
    /* Search for the pin making sure that */
    /* any found attribute starts with "pinnumber" */
    found = o_attrib_search_attrib_value(o_current->attribs, ctx->pin,
					 "pinnumber", 0);
    if (found) {
      if (GEDA_DEBUG) {
	printf("%s: %s\n", found->name, o_text_get_string(found));
      }
      ctx->found = o_current;
      return VISIT_RES_EARLY;
    }
    break;
  }

  return VISIT_RES_OK;
}

/*! \brief search the pin with a given pin number
 *  \par Function Description
 *  This function searches a pin object inside the complex \a object.
 *  The pin name is a character string \a pin.

 *  \param  object  The complex object
 *  \param  pin     The pin number (string) to find
 *  \return a pin object if found, NULL otherwise
 */
OBJECT *o_complex_return_pin_object(OBJECT *object, char *pin) 
{
  struct return_pin_context ctx = {
    .pin = pin,
    .found = NULL,
  };

  /*
   * It is a semantic error to have multiple pins with the same pinnumber, so
   * VISIT_ANY is ok here.
   */
  s_visit_object(object, &return_pin_one, &ctx, VISIT_ANY, 1);

  return ctx.found;
}

/*! \brief check the symversion of a complex object
 *  \par Function Description
 *  This function compares the symversion of a symbol with it's 
 *  earlier saved symversion in a schematic.
 *  Major symversion changes are added to the toplevel object 
 *  (toplevel->major_changed_refdes), minor changes are reported
 *  to the messaging system.
 *  
 *  \param toplevel  The TOPLEVEL object
 *  \param object    The complex OBJECT
 */
void
o_complex_check_symversion(TOPLEVEL *toplevel, OBJECT const *object)
{
  char *inside = NULL;
  char *outside = NULL;
  char *refdes = NULL;
  double inside_value = -1.0;
  double outside_value = -1.0;
  char *err_check = NULL;
  int inside_present = FALSE;
  int outside_present = FALSE;
  double inside_major, inside_minor;
  double outside_major, outside_minor;
  
  g_return_if_fail (object != NULL);
  g_return_if_fail ((object->type == OBJ_COMPLEX || 
		     object->type == OBJ_PLACEHOLDER));
  g_return_if_fail (object->complex != NULL);

  /* first look on the inside for the symversion= attribute */
  inside = o_attrib_search_object(object, "symversion", 0);

  /* now look for the symversion= attached to object */
  outside = o_attrib_search_attrib_name(object->attribs, "symversion", 0);

  /* get the uref for future use */
  refdes = o_attrib_search_attrib_name(object->attribs, "refdes", 0);
  if (!refdes)
  {
    refdes = g_strdup ("unknown");
  }
  
  if (inside)
  {
    inside_value = strtod(inside, &err_check);
    if (inside_value == 0 && inside == err_check)
    {
      if (inside)
      { 
        s_log_message(_("WARNING: Symbol version parse error on refdes %s:\n"
                        "\tCould not parse symbol file symversion=%s\n"),
                      refdes, inside);
      } else {
        s_log_message(_("WARNING: Symbol version parse error on refdes %s:\n"
                        "\tCould not parse symbol file symversion=\n"),
                      refdes);
      }
      goto done;
    }
    inside_present = TRUE;
  } else {
    inside_present = FALSE;  /* attribute not inside */
  }

  if (outside)
  {
    outside_value = strtod(outside, &err_check);
    if (outside_value == 0 && outside == err_check)
    {
      s_log_message(_("WARNING: Symbol version parse error on refdes %s:\n"
                      "\tCould not parse attached symversion=%s\n"),
                    refdes, outside);
      goto done;
    }
    outside_present = TRUE; 
  } else {
    outside_present = FALSE;  /* attribute not outside */
  }

  if (GEDA_DEBUG) {
    printf("%s:\n\tinside: %.1f outside: %.1f\n\n", object->name,
	   inside_value, outside_value);
  }
  
  /* symversion= is not present anywhere */
  if (!inside_present && !outside_present)
  {
    /* symbol is legacy and versioned okay */
    goto done;
  }

  /* No symversion inside, but a version is outside, this is a weird case */
  if (!inside_present && outside_present)
  {
    s_log_message(_("WARNING: Symbol version oddity on refdes %s:\n"
                    "\tsymversion=%s attached to instantiated symbol, "
                    "but no symversion= inside symbol file\n"),
                  refdes, outside);
    goto done;
  }

  /* inside & not outside is a valid case, means symbol in library is newer */
  /* also if inside_value is greater than outside_value, then symbol in */
  /* library is newer */
  if ((inside_present && !outside_present) ||
      ((inside_present && outside_present) && (inside_value > outside_value)))
  {
    
    s_log_message(_("WARNING: Symbol version mismatch on refdes %s (%s):\n"
                    "\tSymbol in library is newer than "
                    "instantiated symbol\n"),
                  refdes, object->complex_basename);

    /* break up the version values into major.minor numbers */
    inside_major = floor(inside_value);
    inside_minor = inside_value - inside_major;

    if (outside_present)
    {
      outside_major = floor(outside_value);
      outside_minor = outside_value - outside_major;
    } else {
      /* symversion was not attached to the symbol, set all to zero */
      outside_major = 0.0;
      outside_minor = 0.0;
      outside_value = 0.0;
    }

    if (GEDA_DEBUG) {
      printf("i: %f %f %f\n", inside_value, inside_major, inside_minor);
      printf("o: %f %f %f\n", outside_value, outside_major, outside_minor);
    }
    
    if (inside_major > outside_major)
    {
      char* refdes_copy;
      s_log_message(_("\tMAJOR VERSION CHANGE (file %.3f, "
                      "instantiated %.3f, %s)!\n"),
                    inside_value, outside_value, refdes);

      /* add the refdes to the major_changed_refdes GList */
      /* make sure refdes_copy is freed somewhere */
      refdes_copy = g_strconcat (refdes, " (",
                                 object->complex_basename,
                                 ")", NULL);
      toplevel->major_changed_refdes =
        g_list_append(toplevel->major_changed_refdes, refdes_copy);

      /* don't bother checking minor changes if there are major ones*/
      goto done; 
    }

    if (inside_minor > outside_minor)
    {
      s_log_message(_("\tMinor version change (file %.3f, "
                      "instantiated %.3f)\n"),
                    inside_value, outside_value);
    }

    goto done;
  }

  /* outside value is greater than inside value, this is weird case */
  if ((inside_present && outside_present) && (outside_value > inside_value))
  {
    s_log_message(_("WARNING: Symbol version oddity on refdes %s:\n"
                    "\tInstantiated symbol is newer than "
                    "symbol in library\n"),
                  refdes);
    goto done;
  }

  /* if inside_value and outside_value match, then symbol versions are okay */

done:
  g_free(inside);
  g_free(outside);
  g_free(refdes);
}

/*! \brief Calculates the distance between the given point and the closest
 * point on an object within the complex object.
 *
 *  \param [in] complex The complex of the OBJECT
 *  \param [in] x The x coordinate of the given point.
 *  \param [in] y The y coordinate of the given point.
 *  \return The shortest distance from the object to the point. If the
 *  distance cannot be calculated, this function returns a really large
 *  number (G_MAXDOUBLE).  With an invalid parameter, this function returns
 *  G_MAXDOUBLE.
 */
gdouble o_complex_shortest_distance(OBJECT *o_current, gint x, gint y)
{
  gdouble distance;
  gdouble shortest_distance = G_MAXDOUBLE;
  OBJECT *temp;

  g_return_val_if_fail(o_current->complex, G_MAXDOUBLE);

  temp = o_current->complex->prim_objs;

  if (temp != NULL) {
    temp = temp->next;
  }

  while (temp != NULL) {
    distance = o_shortest_distance(temp, x, y);

    shortest_distance = min(shortest_distance, distance);

    temp = temp->next;
  }

  return shortest_distance;
}
