/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * m_rtree.c - A spatial index for finding schematic objects.
 * Copyright (C) 2009 Bernd Jendrissek <bernd.jendrissek@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include "config.h"

#include <glib.h>
#include <math.h>
#include <string.h>
#include "libgeda_priv.h"

typedef struct st_region REGION;

struct st_region {
  int w_x1;
  int w_y1;
  int w_x2;
  int w_y2;
};

struct st_rtree {
  RTREE *subtrees[2];
  GList *contained_objects;
  int weight;
  REGION r;
};

RTREE *m_rtree_new(int w_x1, int w_y1, int w_x2, int w_y2)
{
  RTREE *rtree;

  rtree = g_new(RTREE, 1);
  rtree->subtrees[0] = NULL;
  rtree->subtrees[1] = NULL;
  rtree->contained_objects = NULL;
  rtree->weight = 0;
  rtree->r.w_x1 = w_x1;
  rtree->r.w_y1 = w_y1;
  rtree->r.w_x2 = w_x2;
  rtree->r.w_y2 = w_y2;

  return rtree;
}

static gboolean m_rtree_region_contains(REGION const *region,
					int w_x1, int w_y1, int w_x2, int w_y2)
{
  if (w_x1 < region->w_x1 || w_x1 > region->w_x2) { return FALSE; }
  if (w_x2 < region->w_x1 || w_x2 > region->w_x2) { return FALSE; }
  if (w_y1 < region->w_y1 || w_y1 > region->w_y2) { return FALSE; }
  if (w_y2 < region->w_y1 || w_y2 > region->w_y2) { return FALSE; }

  return TRUE;
}

static gboolean m_rtree_overlaps(RTREE const *rtree,
				 int w_x1, int w_y1, int w_x2, int w_y2)
{
  if (w_x2 < rtree->r.w_x1 || w_x1 > rtree->r.w_x2) { return FALSE; }
  if (w_y2 < rtree->r.w_y1 || w_y1 > rtree->r.w_y2) { return FALSE; }

  return TRUE;
}

/** \brief Estimate entropy of a particular partition of objects.
 *
 *  \par Function Description
 *  This function estimates the imbalance in the partition of object into
 *  child nodes.  Zero is the most desired result.
 *
 *  \param [in] child1  Number of objects in first child node.
 *  \param [in] child2  Number of objects in second child node.
 *  \param [in] parent  Number of objects in parent node.
 *
 *  \return  A metric of balance goodness.
 */
static double m_rtree_entropy(int child1, int child2, int parent)
{
  /*
   * Entropy = log(number of possible states).  We know the number of
   * possible states; it is simply N! / (n1! * n2! * n3!).  So the
   * entropy is log(N!) - log(n1!) - log(n2!) - log(n3!).
   */
#ifdef HAS_LGAMMA
  return (lgamma(child1 + child2 + parent + 1) -
	  lgamma(child1 + 1) -
	  lgamma(child2 + 1) -
	  lgamma(parent + 1));
#else /* !HAS_LGAMMA */
  return (log(fact(child1 + child2 + parent) /
	      (fact(child1) * fact(child2) * fact(parent))));
#endif /* !HAS_LGAMMA */
  return ((child2 - child1) * parent); /* XXX BUG - this is not entropy. */
}

static void m_rtree_check_x_partition(OBJECT *o_current, int x, int y G_GNUC_UNUSED,
				      int *count1, int *count2, int *countp)
{
  int w_x1, w_y1, w_x2, w_y2;

  if (world_get_single_object_bounds(o_current, &w_x1, &w_y1, &w_x2, &w_y2)) {
    if (w_x1 > x) {
      (*count2)++;
    } else if (w_x2 < x) {
      (*count1)++;
    } else {
      (*countp)++;
    }
  }
}

static void m_rtree_check_y_partition(OBJECT *o_current, int x G_GNUC_UNUSED, int y,
				      int *count1, int *count2, int *countp)
{
  int w_x1, w_y1, w_x2, w_y2;

  if (world_get_single_object_bounds(o_current, &w_x1, &w_y1, &w_x2, &w_y2)) {
    if (w_y1 > y) {
      (*count2)++;
    } else if (w_y2 < y) {
      (*count1)++;
    } else {
      (*countp)++;
    }
  }
}

static double m_rtree_try_partition(GList const *objects, int x, int y,
				    void (*checker)(OBJECT *, int, int,
						    int *, int *, int *))
{
  GList const *iter;
  int count1 = 0, count2 = 0, countp = 0;

  for (iter = objects; iter != NULL; iter = iter->next) {
    OBJECT *o_current = iter->data;

    (*checker)(o_current, x, y, &count1, &count2, &countp);
  }

  return (m_rtree_entropy(count1, count2, countp));
}

static GList *m_rtree_reclaim_objects(RTREE *rtree, GList *retval)
{
  retval = g_list_concat(retval, rtree->contained_objects);
  if (rtree->subtrees[0]) {
    retval = m_rtree_reclaim_objects(rtree->subtrees[0], retval);
    g_free(rtree->subtrees[0]);
    rtree->subtrees[0] = NULL;
  }
  if (rtree->subtrees[1]) {
    retval = m_rtree_reclaim_objects(rtree->subtrees[1], retval);
    g_free(rtree->subtrees[1]);
    rtree->subtrees[1] = NULL;
  }

  rtree->contained_objects = NULL;

  return retval;
}

static void m_rtree_rebalance(RTREE *rtree)
{
  double best_entropy_x = 0;
  double best_entropy_y = 0;
  int best_x = rtree->r.w_x1;
  int best_y = rtree->r.w_y1;
  GList *iter, *o_all;

  /* Find the x or y partition that maximizes the entropy. */
  for (iter = rtree->contained_objects; iter != NULL; iter = iter->next) {
    OBJECT *o_current = iter->data;
    int w_x1, w_y1, w_x2, w_y2;

    if (world_get_single_object_bounds(o_current, &w_x1, &w_y1, &w_x2, &w_y2)) {
      double entropy_x, entropy_y;
      entropy_x = m_rtree_try_partition(rtree->contained_objects, w_x1, w_y1,
					&m_rtree_check_x_partition);
      entropy_y = m_rtree_try_partition(rtree->contained_objects, w_x1, w_y1,
					&m_rtree_check_y_partition);
      if (entropy_x > best_entropy_x) {
	best_entropy_x = entropy_x;
	best_x = w_x1;
      }
      if (entropy_y > best_entropy_y) {
	best_entropy_y = entropy_y;
	best_y = w_y1;
      }
    }
  }

  if (best_x == rtree->r.w_x1 && best_y == rtree->r.w_y1) {
    /* There was no partition that could split the node. */
    return;
  }

  o_all = m_rtree_reclaim_objects(rtree, NULL);

  if (best_entropy_x >= best_entropy_y) {
    /* best_x - 1 is safe since best_x is a nontrivial partition. */
    rtree->subtrees[0] = m_rtree_new(rtree->r.w_x1, rtree->r.w_y1,
				     best_x - 1, rtree->r.w_y2);
    rtree->subtrees[1] = m_rtree_new(best_x, rtree->r.w_y1,
				     rtree->r.w_x2, rtree->r.w_y2);
  } else {
    /* best_y - 1 is safe since best_y is a nontrivial partition. */
    rtree->subtrees[0] = m_rtree_new(rtree->r.w_x1, rtree->r.w_y1,
				     rtree->r.w_x2, best_y - 1);
    rtree->subtrees[1] = m_rtree_new(rtree->r.w_x1, best_y,
				     rtree->r.w_x2, rtree->r.w_y2);
  }

  for (iter = o_all; iter != NULL; iter = iter->next) {
    OBJECT *o_current = iter->data;
    int w_x1, w_y1, w_x2, w_y2;

    if (world_get_single_object_bounds(o_current, &w_x1, &w_y1, &w_x2, &w_y2)) {
      GList **sub1 = &rtree->subtrees[0]->contained_objects;
      GList **sub2 = &rtree->subtrees[1]->contained_objects;
      if (m_rtree_region_contains(&rtree->subtrees[0]->r, w_x1, w_y1, w_x2, w_y2)) {
	*sub1 = g_list_prepend(*sub1, o_current);
      } else if (m_rtree_region_contains(&rtree->subtrees[1]->r, w_x1, w_y1, w_x2, w_y2)) {
	*sub2 = g_list_prepend(*sub2, o_current);
      } else {
	/* Doesn't fit in either of the subtrees, keep it in the parent. */
	rtree->contained_objects = g_list_prepend(rtree->contained_objects,
						  o_current);
      }
    }
  }

  rtree->subtrees[0]->weight = g_list_length(rtree->subtrees[0]->contained_objects);
  rtree->subtrees[1]->weight = g_list_length(rtree->subtrees[1]->contained_objects);
}

void m_rtree_add(RTREE *rtree, OBJECT *o)
{
  int subweight[2];

  rtree->contained_objects = g_list_prepend(rtree->contained_objects, o);
  rtree->weight++;

  subweight[0] = rtree->subtrees[0] ? rtree->subtrees[0]->weight : 0;
  subweight[1] = rtree->subtrees[1] ? rtree->subtrees[1]->weight : 0;

  if (rtree->weight > subweight[0] + subweight[1]) {
    m_rtree_rebalance(rtree);
  }
}

GList *m_rtree_find(RTREE const *rtree, GList *retval,
		    int w_x1, int w_y1, int w_x2, int w_y2)
{
  GList *iter;
  REGION r;

  r.w_x1 = w_x1;
  r.w_y1 = w_y1;
  r.w_x2 = w_x2;
  r.w_y2 = w_y2;

  if (!m_rtree_overlaps(rtree, w_x1, w_y1, w_x2, w_y2)) {
    return retval;
  }

  if (rtree->subtrees[0] != NULL) {
    retval = m_rtree_find(rtree->subtrees[0], retval, w_x1, w_y1, w_x2, w_y2);
  }
  if (rtree->subtrees[1] != NULL) {
    retval = m_rtree_find(rtree->subtrees[1], retval, w_x1, w_y1, w_x2, w_y2);
  }

  for (iter = rtree->contained_objects; iter != NULL; iter = iter->next) {
    OBJECT *o_current = iter->data;

    if (world_get_single_object_bounds(o_current, &w_x1, &w_y1, &w_x2, &w_y2)) {
      if (m_rtree_region_contains(&r, w_x1, w_y1, w_x2, w_y2)) {
	retval = g_list_prepend(retval, o_current);
      }
    }
  }

  return retval;
}
