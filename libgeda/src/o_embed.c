/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */

/*! \file o_embed.c
 *  \brief functions to embed and unembed symbols
 */

#include <config.h>

#include <stdio.h>

#include <libgen.h>

#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif


/*! \brief embed an object into a schematic
 *  \par Function Description
 *  This functions embeds an object \a o_current into a
 *  libgeda. Currently complex objects are just marked to
 *  be embedded later. Picture objects are embedded immediately.
 *
 *  \param toplevel  The TOPLEVEL object
 *  \param o_current The OBJECT to embed
 */
void o_embed(TOPLEVEL *toplevel, OBJECT *o_current)
{
  if (o_current->embed_func) {
    (*o_current->embed_func)(toplevel, o_current, 1);
  }
}

/*! \brief unembed an object from a schematic
 *  \par Function Description
 *  This functions unembeds an object \a o_current from a
 *  libgeda structure. Complex objects are just marked to
 *  be not embedded. Picture objects are unembedded immediately.
 *
 *  \param toplevel  The TOPLEVEL object
 *  \param o_current The OBJECT to unembed
 */
void o_unembed(TOPLEVEL *toplevel, OBJECT *o_current)
{
  if (o_current->embed_func) {
    (*o_current->embed_func)(toplevel, o_current, 0);
  }
}
