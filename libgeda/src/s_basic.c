/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#include <ctype.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#ifdef HAVE_STRING_H
#include <string.h>
#endif

#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/*! this is modified here and in o_list.c */
int global_sid=0;

struct grip_foreach_context {
  enum grip_t whichone;
  gboolean found;
  int *x;
  int *y;
};

static OBJECT *s_basic_copy_error(TOPLEVEL *toplevel, OBJECT *prototype);
static void s_basic_bounds_recalc_error(OBJECT *o_current);
static void s_basic_psprint_error(TOPLEVEL *toplevel, FILE *fp, OBJECT *o,
				  double scale, GArray *unicode_table);
static enum visit_result
s_basic_visit_none(OBJECT *o_current,
		   enum visit_result (*fn)(OBJECT *, void *), void *context,
		   enum visit_order order, int depth);
static void s_basic_grips_foreach_none(OBJECT *o,
				       gboolean (*fn)(OBJECT *o,
						      int grip_x, int grip_y,
						      enum grip_t whichone,
						      void *userdata),
				       void *userdata);
static int s_basic_grips_move_none(OBJECT *o, int whichone, int x, int y);

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void error_if_called(void)
{
	fprintf(stderr, "Somebody called error_if_called!\n");
	g_assert(0);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void exit_if_null(void *ptr) 
{
  if (ptr == NULL) {
    fprintf(stderr, "gEDA: Got NULL ptr!, please e-mail maintainer\n");
    g_assert(0);
    exit(-1);
  }	
}

/*! \brief Return toplevel->page_current if toplevel is non-NULL
 *  \par Function Description
 *  \param [in] toplevel  The TOPLEVEL object, possibly NULL
 *  \return A pointer to the current page, or NULL if toplevel was NULL.
 */
PAGE *safe_page_current(TOPLEVEL *toplevel)
{
  PAGE *page = NULL;

  if (toplevel) {
    page = toplevel->page_current;
  }

  return page;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
/* hack rename this to be s_return_tail */
/* update object_tail or any list of that matter */
OBJECT *return_tail(OBJECT *head)
{
  OBJECT *o_current=NULL;
  OBJECT *ret_struct=NULL;

  o_current = head;
  while ( o_current != NULL ) { /* goto end of list */
    ret_struct = o_current;	
    o_current = o_current->next;
  }
	
  return(ret_struct);	
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
/* hack rename this to be s_return_head */
/* update object_tail or any list of that matter */
OBJECT *return_head(OBJECT *tail)
{
  OBJECT *o_current=NULL;
  OBJECT *ret_struct=NULL;

  o_current = tail;
  while ( o_current != NULL ) { /* goto end of list */
    ret_struct = o_current;	
    o_current = o_current->prev;
  }
	
  return(ret_struct);	
}

/*! \brief Initialize an already-allocated object.
 *  \par Function Description
 *  Initializes the members of the OBJECT base class.
 *
 *  \param [in] new_node  A pointer to an allocated OBJECT
 *  \return A pointer to the initialized object.
 */
OBJECT *s_basic_init_object(OBJECT *new_node)
{
  /* setup sid */
  new_node->sid = global_sid++;

  /* Start without indirection or back-annotation. */
  new_node->vpad_to_pad = NULL;
  new_node->pad_to_pin = NULL;

  /* Figure out who owns this OBJECT later. */
  new_node->owning_slot = NULL;
  new_node->uuid = NULL;

  /* Setup the bounding box */
  new_node->w_top = 0;
  new_node->w_left = 0;
  new_node->w_right = 0;
  new_node->w_bottom = 0;
  new_node->w_bounds_valid = FALSE;

  /* Setup line/circle structs */
  new_node->line = NULL;
  new_node->path = NULL;
  new_node->circle = NULL;
  new_node->arc = NULL;
  new_node->box = NULL;
  new_node->picture = NULL;
  new_node->text = NULL;
  new_node->complex = NULL;
  new_node->type_state = NULL;

  new_node->conn_list = NULL;

  new_node->visited = 0;
	
  new_node->complex_basename = NULL;
  new_node->complex_parent = NULL;
		
  /* Setup the color */
  new_node->color = WHITE;
  new_node->saved_color = -1;
  new_node->selectable = 1;
  new_node->selected = FALSE;
  new_node->dont_redraw = FALSE;
  new_node->locked_color = -1;
  new_node->draw_grips = FALSE;

  new_node->bus_ripper_direction = 0;

  new_node->copy_func = &s_basic_copy_error;
  new_node->bounds_recalc_func = &s_basic_bounds_recalc_error;
  new_node->draw_func = error_if_called; 
  new_node->psprint_func = &s_basic_psprint_error;
  new_node->embed_func = NULL; /* FIXME: Sticking out like a sore thumb... */
  new_node->visit_func = &s_basic_visit_none;
  new_node->grip_foreach_func = &s_basic_grips_foreach_none;
  new_node->grip_move_func = &s_basic_grips_move_none;
  new_node->destroy_func = NULL;

  new_node->line_end = END_NONE;
  new_node->line_type = TYPE_SOLID;
  new_node->line_width = 0;
  new_node->line_space = 0;
  new_node->line_length = 0;
  new_node->fill_width = 0;
  new_node->fill_angle1 = 0;
  new_node->fill_angle2 = 0;
  new_node->fill_pitch1 = 0;
  new_node->fill_pitch2 = 0;
	
  new_node->attribs = NULL;
  new_node->attached_to = NULL;
  new_node->copied_to = NULL;

  new_node->pin_type = PIN_TYPE_NET;
  new_node->whichend = -1;
	
  /* Setup link list stuff */
  new_node->prev = NULL;
  new_node->next = NULL;

  return(new_node);
}

OBJECT *s_basic_link_object( OBJECT *new_node, OBJECT *ptr ) 
{
  /* should never happen, but could */
  if (new_node == NULL) {
    fprintf(stderr, "Got a null new_node in link_object\n");
    return(ptr);
  }

  new_node->prev = ptr; /* setup previous link */

  if (ptr) {
    ptr->next = new_node;
  }

  return new_node;
}

OBJECT *s_basic_unlink_object(OBJECT *o_current)
{
  OBJECT *prev = o_current->prev;

  if (o_current->next)
    o_current->next->prev = o_current->prev;
  if (o_current->prev)
    o_current->prev->next = o_current->next;
  o_current->next = o_current->prev = NULL;

  return prev;
}

void s_basic_splice(OBJECT *before, OBJECT *first, OBJECT *last)
{
  g_assert(first->prev == NULL);
  g_assert(last->next == NULL);
  first->prev = before->prev;
  last->next = before;
  before->prev->next = first;
  before->prev = last;
}

char *s_describe_object(OBJECT *o, int detail)
{
  static char *s[10];
  static int cursor;
  char *parent, *description;

  if (!o) {
    int i;

    for (i = 0; i < 10; i++) {
      if (s[i]) {
	g_free(s[i]);
      }
    }

    return NULL;
  }

  if (s[cursor]) {
    g_free(s[cursor]);
  }

  if (o->complex_parent) {
    parent = g_strdup_printf("%s (%s) > ", o->complex_parent->name, o->complex_parent->complex_basename);
  } else {
    parent = NULL;
  }

  switch (o->type) {
  case OBJ_COMPLEX:
  case OBJ_PLACEHOLDER:
    s[cursor] = g_strdup_printf("%s%s (%s)", parent ? parent : "", o->name, o->complex_basename);
    break;
  case OBJ_TEXT:
    s[cursor] = g_strdup_printf("%s%s (%s)", parent ? parent : "", o->name, o_text_get_string(o));
    break;
  default:
    s[cursor] = g_strdup_printf("%s%s", parent ? parent : "", o->name);
    break;
  }

  g_free(parent);

  description = s[cursor];
  cursor = (cursor + 1) % 10;

  return description;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void print_struct_forw(OBJECT const *ptr)
{
  OBJECT const *o_current=NULL;

  o_current = ptr;

  if (o_current == NULL) {

    printf("AGGGGGGGGGGG NULLLLL PRINT\n");
  }
  printf("TRYING to PRINT\n");
  while (o_current != NULL) {
    printf("Name: %s\n", o_current->name);
    printf("Type: %d\n", o_current->type);
    printf("Sid: %d\n", o_current->sid);

    if (o_current->type == OBJ_COMPLEX || o_current->type == OBJ_PLACEHOLDER) {
      print_struct_forw(o_current->complex->prim_objs);
    }

    o_attrib_print (o_current->attribs);

    printf("----\n");
    o_current = o_current->next;
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void print_struct(OBJECT const *ptr)
{
  OBJECT const *o_current=NULL;

  o_current = ptr;

  if (o_current != NULL) {
    printf("Name: %s\n", o_current->name);
    printf("Type: %d\n", o_current->type);
    printf("Sid: %d\n", o_current->sid);
    if (o_current->line != NULL) {
      int x1, y1, x2, y2;

      s_basic_get_grip(o_current, GRIP_1, &x1, &y1);
      s_basic_get_grip(o_current, GRIP_2, &x2, &y2);
      printf("Line points.x1: %d\n", x1);
      printf("Line points.y1: %d\n", y1);
      printf("Line points.x2: %d\n", x2);
      printf("Line points.y2: %d\n", y2);
    }

    o_attrib_print (o_current->attribs);

    printf("----\n");
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
void
s_delete_object(TOPLEVEL *toplevel, OBJECT *o_current)
{
  GList *iter;
  PAGE *page;

  g_return_if_fail(o_current);

  if (o_current != NULL) {
    g_signal_emit_by_name(o_current, "remove");
    s_conn_remove(o_current);

    if (o_current->attached_to != NULL) {
      /* do the actual remove */
      o_attrib_remove(&o_current->attached_to->attribs, o_current);
    }

    /*
     * Check all pages, because reasoning about which pages hold the objects
     * in object_buffer is hard. Getting it wrong and leaving object_lastfound
     * as a dangling pointer is much worse than losing a few cycles to
     * traversing the list of pages.
     *
     * Besides, hopefully object_lastfound should perhaps be managed with weak
     * notifications.
     */
    for (iter = geda_list_get_glist(toplevel->pages);
	 iter != NULL;
	 iter = g_list_next(iter)) {
      page = (PAGE *) iter->data;
      if (page->object_lastfound == o_current) {
	g_warn_if_fail(toplevel->page_current == page);
	page->object_lastfound = NULL;
      }
    }

    if (o_current->destroy_func) {
      (*o_current->destroy_func)(o_current);
    }
    if (o_current->line) {
      /*	printf("sdeleting line\n");*/
      g_free(o_current->line);
    }
    o_current->line = NULL;

    if (o_current->path) {
      g_free(o_current->path);
    }
    o_current->path = NULL;

    /*	printf("sdeleting circle\n");*/
    g_free(o_current->circle);
    o_current->circle = NULL;

    /*	printf("sdeleting arc\n");*/
    g_free(o_current->arc);
    o_current->arc = NULL;

    /*	printf("sdeleting box\n");*/
    g_free(o_current->box);
    o_current->box = NULL;

    if (o_current->picture) {
      /*	printf("sdeleting picture\n");*/
      o_current->picture = NULL;
    }

    o_current->text = NULL;

    /*	printf("sdeleting name\n");*/
    g_free(o_current->name);
    o_current->name = NULL;

    g_free(o_current->uuid);
    o_current->uuid = NULL;

    /*	printf("sdeleting complex_basename\n");*/
    g_free(o_current->complex_basename); 
    o_current->complex_basename = NULL;

    if (o_current->complex) {
      if (o_current->complex->prim_objs) {
        /* printf("sdeleting complex->primitive_objects\n");*/
        s_delete_list_fromstart(toplevel,
                                o_current->complex->prim_objs);
      }
      o_current->complex->prim_objs = NULL;

      g_free(o_current->complex);
      o_current->complex = NULL;
    }

    if (o_current->attribs) {
      /* This is the only real use of toplevel that isn't just for page_current. */
      o_attrib_detach_all(toplevel, o_current);
    }

    g_object_unref(o_current);

    o_current=NULL;		/* misc clean up */
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
OBJECT *s_delete(TOPLEVEL *toplevel, OBJECT *o_current)
{
  OBJECT *o_prev = NULL;
  if (o_current != NULL) {
    if (GEDA_DEBUG) {
      printf("sdel: %s\n", o_current->name);
      printf("sdel: %d\n", o_current->sid);
    }

    o_prev = s_basic_unlink_object(o_current);
    s_delete_object(toplevel, o_current);
  }

  return o_prev;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
/* deletes everything include the head */
void s_delete_list_fromstart(TOPLEVEL *toplevel, OBJECT *start)
{
  OBJECT *o_current;

  o_current = return_tail(start);

  /* do the delete backwards */
  while(o_current != NULL) {
    o_current = s_delete(toplevel, o_current);
  }
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
/* deletes everything include the GList */
void
s_delete_object_glist(TOPLEVEL *toplevel, GList *list)
{
  OBJECT *o_current=NULL;
  GList *ptr;

  ptr = g_list_last(list);

  /* do the delete backwards */
  while(ptr != NULL) {
    o_current = ptr->data;
    s_delete_object(toplevel, o_current);
    ptr = g_list_previous (ptr);
  }
  g_list_free(list);
}

OBJECT *s_basic_copy(TOPLEVEL *toplevel, OBJECT *prototype)
{
  OBJECT *retval;

  retval = (*prototype->copy_func)(toplevel, prototype);
  if (retval) {
    /* make sure sid is the same! */
    retval->sid = prototype->sid;
    retval->uuid = g_strdup(prototype->uuid);
  }

  return retval;
}

static OBJECT *s_basic_copy_error(TOPLEVEL *toplevel, OBJECT *prototype)
{
  g_return_val_if_reached(NULL);
}

static void s_basic_bounds_recalc_error(OBJECT *o_current G_GNUC_UNUSED)
{
  g_return_if_reached();
}

static void s_basic_psprint_error(TOPLEVEL *toplevel, FILE *fp, OBJECT *o,
				  double scale, GArray *unicode_table)
{
  s_log_message("Object #<object %s> has no psprint_func\n", o->name);
  g_return_if_reached();
}

static enum visit_result
s_basic_visit_none(OBJECT *o_current G_GNUC_UNUSED,
		   enum visit_result (*fn)(OBJECT *, void *) G_GNUC_UNUSED,
		   void *context G_GNUC_UNUSED,
		   enum visit_order order G_GNUC_UNUSED,
		   int depth G_GNUC_UNUSED)
{
  return VISIT_RES_OK;
}

static void s_basic_grips_foreach_none(OBJECT *o G_GNUC_UNUSED,
				       gboolean (*fn)(OBJECT *o,
						      int grip_x, int grip_y,
						      enum grip_t whichone,
						      void *userdata) G_GNUC_UNUSED,
				       void *userdata G_GNUC_UNUSED)
{
  /* Some objects have no grips at all; do nothing. */
}

static int s_basic_grips_move_none(OBJECT *o, int whichone,
				   int x G_GNUC_UNUSED, int y G_GNUC_UNUSED)
{
  s_log_message("Object #<object %s> has no grip_move_func\n", o->name);

  return whichone;
}

/*! \brief Traverse a list of grip descriptors.
 *  \par Function Description
 *  Calls a user-supplied closure on each grip in the list, until any
 *  one of them returns TRUE.  The grip list sentinel is the entry whose
 *  #whichone field is set to GRIP_NONE.
 *
 *  \param [in] o         The OBJECT containing the grips.
 *  \param [in] grips     A pointer to a vector of grip descriptors.
 *  \param [in] fn        A function to call on each grip.
 *  \param [in] userdata  User-supplied context for \a fn.
 */
void s_basic_grip_foreach_helper(OBJECT *o,
				 const GRIP *grips,
				 gboolean (*fn)(OBJECT *o,
						int grip_x, int grip_y,
						enum grip_t whichone,
						void *userdata),
				 void *userdata)
{
  int i;

  for (i = 0; grips[i].whichone != GRIP_NONE; i++) {
    if ((*fn)(o, grips[i].x, grips[i].y, grips[i].whichone, userdata)) {
      return;
    }
  }
}

static gboolean check_one_grip(OBJECT *o G_GNUC_UNUSED, int grip_x, int grip_y,
			       enum grip_t whichone, void *userdata)
{
  struct grip_foreach_context *ctx = (struct grip_foreach_context *) userdata;

  if (ctx->whichone == whichone) {
    *ctx->x = grip_x;
    *ctx->y = grip_y;
    ctx->found = TRUE;
    return TRUE;
  }

  return FALSE;
}

/*! \brief Get a specific grip from an object.
 *  \par Function Description
 *  Gets a grip by a grip constant found earlier.
 *
 *  \param [in] o         The object to search.
 *  \param [in] whichone  The grip constant.
 *  \param [in] x         A pointer to the world X coordinate.
 *  \param [in] y         A pointer to the world Y coordinate.
 *  \return TRUE if the grip exists, FALSE otherwise.
 */
gboolean s_basic_get_grip(OBJECT const *o, enum grip_t whichone, int *x, int *y)
{
  struct grip_foreach_context grip_finder = {
    .whichone = whichone,
    .found = FALSE,
    .x = x,
    .y = y,
  };

  if (o->grip_foreach_func) {
    (*o->grip_foreach_func)((OBJECT *) o, &check_one_grip, &grip_finder);
  }

  return grip_finder.found;
}

static gboolean grip_list_accumulator(OBJECT *o G_GNUC_UNUSED,
				      int grip_x, int grip_y,
				      enum grip_t whichone,
				      void *userdata)
{
  GList **grip_list = userdata;
  GRIP *g;

  g = g_new(GRIP, 1);
  g->x = grip_x;
  g->y = grip_y;
  g->whichone = whichone;

  *grip_list = g_list_prepend(*grip_list, g);

  return FALSE;
}

/*! \brief Get a list of all of an OBJECT's grips.
 *  \par Function Description
 *  Gets a list of all grips in world coordinates on an object. Even coinciding
 *  and functionally duplicate grips are in the returned list.
 *
 *  \param [in] o     The object to query.
 *  \param [in] init  An existing list, or NULL
 *  \return A GList holding GRIP objects, which should be g_free'd.
 *
 *  \note Grips get added to the \em front of \a init.
 */
GList *s_basic_get_all_grips(OBJECT const *o, GList *init)
{
  if (o->grip_foreach_func) {
    (*o->grip_foreach_func)((OBJECT *) o, &grip_list_accumulator, &init);
  }

  return init;
}

/*! \brief Move one of an OBJECT's grips.
 *  \par Function Description
 *  Asks the OBJECT to move one of its grips while keeping others fixed.
 *
 *  #whichone may be GRIP_NONE, which means to reset the object to being
 *  freshly placed at (#x,#y).
 *
 *  \param [in] o         The object to manipulate.
 *  \param [in] whichone  The grip to move with the pointer.
 *  \param [in] x         The world X coordinate of the pointer.
 *  \param [in] y         The world Y coordinate of the pointer.
 *  \return A grip constant suggesting the next grip to move, or GRIP_NONE.
 */
int s_basic_move_grip(OBJECT *o, int whichone, int x, int y)
{
  return ((*o->grip_move_func)(o, whichone, x, y));
}

static gboolean search_one_grip(OBJECT *o G_GNUC_UNUSED, int grip_x, int grip_y,
				enum grip_t whichone, void *userdata)
{
  struct grip_foreach_context *ctx = (struct grip_foreach_context *) userdata;

  if (grip_x == *ctx->x && grip_y == *ctx->y) {
    ctx->whichone = whichone;
    return TRUE;
  }

  return FALSE;
}

/*! \brief Find a grip whose position is (#x,#y).
 *  \par Function Description
 *  Searches an object's grips for one that is exactly at the given point.
 *
 *  \param [in] o  The object to search.
 *  \param [in] x  The world X-coordinate of the point.
 *  \param [in] y  The world Y-coordinate of the point.
 *  \return A grip constant identifying the found grip, or GRIP_NONE.
 */
int s_basic_search_grips_exact(OBJECT const *o, int x, int y)
{
  struct grip_foreach_context grip_searcher = {
    .whichone = GRIP_NONE,
    .x = &x,
    .y = &y,
  };

  if (o->grip_foreach_func) {
    (*o->grip_foreach_func)((OBJECT *) o, &search_one_grip, &grip_searcher);
  }

  return (grip_searcher.whichone);
}

/** \brief Traverse an object, applying a closure to each sub-object.
 *  \par Function Description
 *  \param [in] object   The object to traverse.
 *  \param [in] fn       The closure function.
 *  \param [in] context  The closure context to pass to fn.
 *  \param [in] order    Whether to visit parents before or after children.
 *  \param [in] depth    Maximum depth of traversal.
 *  \todo Decide if depth should be a limit or level selector.
 *  \todo Maybe apply the closure only to specific object types?
 *  \note This function modifies \a object only if \a fn does.
 */
enum visit_result s_visit(OBJECT *object,
			  enum visit_result (*fn)(OBJECT *, void *),
			  void *context, enum visit_order order, int depth)
{
  enum visit_result res;

  g_return_val_if_fail(object, VISIT_RES_ERROR);

  switch (order) {
  case VISIT_ANY:
  case VISIT_DETERMINISTIC:
  case VISIT_LINEAR:
  case VISIT_PREFIX:
    switch (res = (*fn)(object, context)) {
    case VISIT_RES_OK:
      /* No special action; continue traversing. */
      break;
    case VISIT_RES_NORECURSE:
      return VISIT_RES_OK;
    case VISIT_RES_EARLY:
    case VISIT_RES_ABORT:
    case VISIT_RES_RESTART:
    case VISIT_RES_ERROR:
      return res;
    }
    break;
  case VISIT_POSTFIX:
    /* Visit the current object later. */
    break;
  }

  /* Limit the traversal depth.  Don't visit attributes */
  if (depth != 1 && object->attached_to == NULL) {
    int newdepth;

    if (depth != 0) {
      newdepth = depth - 1;
    } else {
      newdepth = 0;
    }

    switch (res = s_visit_object(object, fn, context, order, newdepth)) {
    case VISIT_RES_OK:
      /* No special action; continue traversing. */
      break;
    case VISIT_RES_NORECURSE:
      /* Only caller-supplied visitors may return VISIT_RES_NORECURSE. */
      g_warn_if_reached();
      break;
    case VISIT_RES_EARLY:
    case VISIT_RES_ABORT:
    case VISIT_RES_RESTART:
    case VISIT_RES_ERROR:
      return res;
    }
  }

  switch (order) {
  case VISIT_ANY:
  case VISIT_DETERMINISTIC:
  case VISIT_LINEAR:
  case VISIT_PREFIX:
    /* Current object has already been visited. */
    break;
  case VISIT_POSTFIX:
    switch (res = (*fn)(object, context)) {
    case VISIT_RES_OK:
      break;
    case VISIT_RES_NORECURSE:
      /* Too late, the parent object has already been visited. */
      g_warn_if_reached();
      break;
    case VISIT_RES_EARLY:
    case VISIT_RES_ABORT:
    case VISIT_RES_RESTART:
    case VISIT_RES_ERROR:
      return res;
    }
    break;
  }

  return VISIT_RES_OK;
}

enum visit_result s_visit_object(OBJECT *o_current,
				 enum visit_result (*fn)(OBJECT *, void *),
				 void *context, enum visit_order order,
				 int depth)
{
  return ((*o_current->visit_func)(o_current, fn, context, order, depth));
}

enum visit_result s_visit_list(OBJECT *o_current, enum list_kind kind,
			       enum visit_result (*fn)(OBJECT *, void *),
			       void *context, enum visit_order order, int depth)
{
  enum visit_result res;

  g_return_if_fail(o_current);

  switch (kind) {
  case LIST_KIND_HEAD:
    g_warn_if_fail(o_current->type == OBJ_HEAD);
    break;
  case LIST_KIND_REST:
    g_warn_if_fail(o_current->type != OBJ_HEAD);
    break;
  case LIST_KIND_CDR:
    /* XXX Should LIST_KIND_CDR be this strict? */
    g_warn_if_fail(o_current->type != OBJ_HEAD);
    break;
  }

  if (o_current->type == OBJ_HEAD || kind == LIST_KIND_CDR) {
    o_current = o_current->next;
  }

  for (; o_current != NULL; o_current = o_current->next) {
    switch (res = s_visit(o_current, fn, context, order, depth)) {
    case VISIT_RES_OK:
      /* No special action; continue traversing. */
      break;
    case VISIT_RES_NORECURSE:
      /* Only caller-supplied visitors may return VISIT_RES_NORECURSE. */
      g_warn_if_reached();
      break;
    case VISIT_RES_EARLY:
    case VISIT_RES_ABORT:
    case VISIT_RES_RESTART:
    case VISIT_RES_ERROR:
      return res;
    }
  }

  return VISIT_RES_OK;
}

enum visit_result s_visit_page(PAGE const *page,
			       enum visit_result (*fn)(OBJECT *, void *),
			       void *context, enum visit_order order, int depth)
{
  return (s_visit_list(page->object_head, LIST_KIND_HEAD, fn,
		       context, order, depth));
}

enum visit_result s_visit_toplevel(TOPLEVEL const *toplevel,
				   enum visit_result (*fn)(OBJECT *, void *),
				   void *context, enum visit_order order,
				   int depth)
{
  enum visit_result res;
  const GList *iter;
  PAGE *page;

  for (iter = geda_list_get_glist(toplevel->pages);
       iter != NULL;
       iter = g_list_next(iter)) {
    page = (PAGE *) iter->data;
    switch (res = s_visit_page(page, fn, context, order, depth)) {
    case VISIT_RES_OK:
      /* No special action; continue traversing. */
      break;
    case VISIT_RES_NORECURSE:
      /* Only caller-supplied visitors may return VISIT_RES_NORECURSE. */
      g_warn_if_reached();
      break;
    case VISIT_RES_EARLY:
    case VISIT_RES_ABORT:
    case VISIT_RES_RESTART:
    case VISIT_RES_ERROR:
      return res;
    }
  }

  return VISIT_RES_OK;
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
/* used by o_text_read */
char *remove_nl(char *string)
{
  int i;

  if (!string)
    return NULL;
  
  i = 0;
  while(string[i] != '\0' && string[i] != '\n' && string[i] != '\r') {
    i++; 
  }

  string[i] = '\0';

  return(string);
}

/*! \todo Finish function documentation!!!
 *  \brief
 *  \par Function Description
 *
 */
/* used by o_text_read */
char *remove_last_nl(char *string)
{
  int len;

  if (!string)
    return NULL;		

  len = strlen(string);
  if (string[len-1] == '\n' || string[len-1] == '\r')
    string[len-1] = '\0';
     
  return(string);
}

/*! \brief Expand environment variables in string.
 *  \par Function Description
 *  This function returns the passed string with environment variables
 *  expanded.
 *
 *  The invocations of environment variable MUST be in the form
 *  '${variable_name}', '$variable_name' is not valid here. Environment
 *  variable names consists solely of letters, digits and '_'. It is
 *  possible to escape a '$' character in the string by repeating it
 *  twice.
 *
 *  It outputs error messages to console and leaves the malformed and
 *  bad variable names in the returned string.
 *
 *  \param [in] string The string with variables to expand.
 *  \return A newly-allocated string with variables expanded or NULL
 *  if input string was NULL.
 */
gchar*
s_expand_env_variables (const gchar *string)
{
  GString *gstring;
  gint i;

  if (string == NULL) {
    return NULL;
  }

  gstring = g_string_sized_new (strlen (string));
  i = 0;
  while (TRUE) {
    gint start;

    start = i;
    /* look for end of string or possible variable name start */
    while (string[i] != '\0' && string[i] != '$') i++;
    g_string_append_len (gstring, string + start, i - start);
    if (string[i] == '\0') {
      /* end of string, return built string */
      return g_string_free (gstring, FALSE);
    }

    i++;
    switch (string[i]) {
        case ('{'):
          /* look for the end of the variable name */
          start = i;
          while (string[i] != '\0' && string[i] != '}') i++;
          if (string[i] == '\0') {
            /* problem: no closing '}' to variable */
            fprintf (stderr,
                     "Found malformed environment variable in '%s'\n",
                     string);
            g_string_append (gstring, "$");
            g_string_append_len (gstring, string + start, i - start + 1);
          } else {
            gint j;

            /* discard "${" */
            start = start + 1;
            /* test characters of variable name */
            for (j = start;
                 j < i && (g_ascii_isalnum (string[j]) || string[j] == '_');
                 j++);
            if (i != j) {
              /* illegal character detected in variable name */
              fprintf (stderr,
                       "Found bad character [%c] in variable name.\n",
                       string[j]);
              g_string_append (gstring, "${");
              g_string_append_len (gstring, string + start, i - start + 1);
            } else {
              /* extract variable name from string and expand it */
              gchar *variable_name = g_strndup (string + start, i - start);
              const gchar *env = g_getenv (variable_name);
              g_free (variable_name);
              g_string_append (gstring, (env == NULL) ? "" : env);
            }
            i++;
          }
          break;

        case ('$'):
          /* an escaped '$' */
          g_string_append_c (gstring, string[i++]);
          break;
          
        default:
          /* an isolated '$', put it in output */
          g_string_append_c (gstring, '$');
    }
  }

  /* never reached */
  return NULL;
}
