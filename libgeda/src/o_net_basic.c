/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#include <math.h>

#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

/*! \file o_net_basic.c 
 *  \brief functions for the net object
 */

/*! Default setting for net draw function. */
void (*net_draw_func)() = NULL;

/*! \brief calculate and return the boundaries of a net object
 *  \par Function Description
 *  This function calculates the object boundaries of a net \a object.
 *
 *  \param [in]  toplevel  The TOPLEVEL object.
 *  \param [in]  object    a net object
 *  \param [out] left      the left world coord
 *  \param [out] top       the top world coord
 *  \param [out] right     the right world coord
 *  \param [out] bottom    the bottom world coord
 */
void world_get_net_bounds(OBJECT *object, int *left,
                          int *top, int *right, int *bottom)
{
  world_get_line_bounds(object, left, top, right, bottom);
}

OBJECT *o_net_new_at_xy(TOPLEVEL *toplevel, char type, int color, int x, int y)
{
  OBJECT *net = o_line_new_at_xy(toplevel, type, color, x, y);

  return net;
}

/*! \brief create a new net object
 *  \par Function Description
 *  This function creates and returns a new net object.
 *  
 *  \param [in]     toplevel    The TOPLEVEL object.
 *  \param [in]     type        The OBJECT type (usually OBJ_NET)
 *  \param [in]     color       The color of the net
 *  \param [in]     x1          x-coord of the first point
 *  \param [in]     y1          y-coord of the first point
 *  \param [in]     x2          x-coord of the second point
 *  \param [in]     y2          y-coord of the second point
 *  \return A new net OBJECT
 */
OBJECT *o_net_new(TOPLEVEL *toplevel, char type,
		  int color, int x1, int y1, int x2, int y2)
{
  OBJECT *new_node;

  new_node = o_net_new_at_xy(toplevel, type, color, x1, y1);

  s_basic_move_grip(new_node, GRIP_1, x1, y1);
  s_basic_move_grip(new_node, GRIP_2, x2, y2);

  o_net_recalc(new_node);

  return new_node;
}

/*! \brief recalc the visual properties of a net object
 *  \par Function Description
 *  This function updates the visual coords of the \a o_current object.
 *  
 *  \param [in]     toplevel    The TOPLEVEL object.
 *  \param [in]     o_current   a net object.
 *
 */
void o_net_recalc(OBJECT *o_current)
{
  int left, right, top, bottom;

  if (o_current == NULL) {
    return;
  }

  if (o_current->line == NULL) {
    return;
  }

  world_get_net_bounds(o_current, &left, &top, &right, &bottom);

  o_current->w_left = left;
  o_current->w_top = top;
  o_current->w_right = right;
  o_current->w_bottom = bottom;
  o_current->w_bounds_valid = TRUE;
}

/*! \brief read a net object from a char buffer
 *  \par Function Description
 *  This function reads a net object from the buffer \a buf.
 *  
 *  \param [in] toplevel     The TOPLEVEL object
 *  \param [in] buf          a text buffer (usually a line of a schematic file)
 *  \param [in] release_ver  The release number gEDA
 *  \param [in] fileformat_ver a integer value of the file format
 *  \return The object list
 *
 */
OBJECT *o_net_read(TOPLEVEL *toplevel, char buf[],
		   unsigned int release_ver, unsigned int fileformat_ver)
{
  OBJECT *new_obj;
  char type;
  int x1, y1;
  int x2, y2;
  int d_x1, d_y1;
  int d_x2, d_y2;
  int color;

  sscanf(buf, "%c %d %d %d %d %d\n", &type, &x1, &y1, &x2, &y2, &color);
  d_x1 = x1;
  d_y1 = y1;
  d_x2 = x2;
  d_y2 = y2;

  if (x1 == x2 && y1 == y2) {
    s_log_message(_("Found a zero length net [ %c %d %d %d %d %d ]\n"),
                  type, x1, y1, x2, y2, color);
  }


  if (toplevel->override_net_color != -1) {
    color = toplevel->override_net_color;
  }

  if (color < 0 || color > MAX_COLORS) {
    s_log_message(_("Found an invalid color [ %s ]\n"), buf);
    s_log_message(_("Setting color to WHITE\n"));
    color = WHITE;
  }

  new_obj = o_net_new(toplevel, type, color, d_x1, d_y1, d_x2, d_y2);

  return new_obj;
}

/*! \brief Create a string representation of the net object
 *  \par Function Description
 *  This function takes a net \a object and return a string
 *  according to the file format definition.
 *
 *  \param [in] object  a net OBJECT
 *  \return the string representation of the net OBJECT
 */
char *o_net_save(OBJECT *object)
{
  int x1, x2, y1, y2;
  int color;
  char *buf;

  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);

  /* Use the right color */
  if (object->saved_color == -1) {
    color = object->color;
  } else {
    color = object->saved_color;
  }

  buf = g_strdup_printf("%c %d %d %d %d %d", object->type, x1, y1, x2, y2, color);
  return (buf);
}

/*! \brief move a net object
 *  \par Function Description
 *  This function changes the position of a net \a object.
 *
 *  \param [in] dx           The x-distance to move the object
 *  \param [in] dy           The y-distance to move the object
 *  \param [in] object       The net OBJECT to be moved
 *
 */
void o_net_translate_world(int dx, int dy, OBJECT *object)
{
  int x1, y1, x2, y2;

  if (object == NULL)
    printf("ntw NO!\n");

  /* Update world coords */
  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);
  s_basic_move_grip(object, GRIP_1, x1 + dx, y1 + dy);
  s_basic_move_grip(object, GRIP_2, x2 + dx, y2 + dy);

  /* Update bounding box */
  o_net_recalc(object);
}

/*! \brief create a copy of a net object
 *  \par Function Description
 *  This function creates a copy of the net object \a o_current.
 *
 *  \param [in] toplevel     The TOPLEVEL object
 *  \param [in] o_current    The object that is copied
 *  \return a new net object
 */
OBJECT *o_net_copy(TOPLEVEL *toplevel,  OBJECT *o_current)
{
  OBJECT *new_obj;
  int x1, y1, x2, y2;
  int color;

  if (o_current->saved_color == -1) {
    color = o_current->color;
  } else {
    color = o_current->saved_color;
  }

  s_basic_get_grip(o_current, GRIP_1, &x1, &y1);
  s_basic_get_grip(o_current, GRIP_2, &x2, &y2);

  /* make sure you fix this in pin and bus as well */
  /* still doesn't work... you need to pass in the new values */
  /* or don't update and update later */
  /* I think for now I'll disable the update and manually update */
  new_obj = o_net_new(toplevel, OBJ_NET, color, x1, y1, x2, y2);

  /* XXX What is the purpose of this?  It seems redundant. */
  s_basic_move_grip(new_obj, GRIP_1, x1, y1);
  s_basic_move_grip(new_obj, GRIP_2, x2, y2);

  return new_obj;
}

/*! \brief postscript print command for a net object
 *  \par Function Description
 *  This function writes the postscript command of the net object \a o_current
 *  into the FILE \a fp points to.
 *  
 *  \param [in] toplevel     The TOPLEVEL object
 *  \param [in] fp           pointer to a FILE structure
 *  \param [in] o_current    The OBJECT to print
 */
void o_net_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current,
		 double scale, GArray *unicode_table)
{
  int net_width;
  int x1, y1;
  int x2, y2;

  if (o_current == NULL) {
    printf("got null in o_net_print\n");
    return;
  }

  if (toplevel->print_color) {
    f_print_set_color(fp, o_current->color);
  }

  net_width = 2;
  if (toplevel->net_style == THICK) {
    net_width = NET_WIDTH;
  }

  s_basic_get_grip(o_current, GRIP_1, &x1, &y1);
  s_basic_get_grip(o_current, GRIP_2, &x2, &y2);

  fprintf(fp, "%d %d %d %d %d line\n", x1, y1, x2, y2, net_width);
}


/*! \brief rotate a net object around a centerpoint
 *  \par Function Description
 *  This function rotates a net \a object around the point
 *  (\a world_centerx, \a world_centery).
 *  
 *  \param [in] world_centerx x-coord of the rotation center
 *  \param [in] world_centery y-coord of the rotation center
 *  \param [in] angle         The angle to rotate the net object
 *  \param [in] object        The net object
 *  \note only steps of 90 degrees are allowed for the \a angle
 */
void o_net_rotate_world(int world_centerx, int world_centery, int angle,
			OBJECT *object)
{
  int x1, y1, x2, y2;
  int newx, newy;

  if (angle == 0)
    return;

  /* translate object to origin */
  o_net_translate_world(-world_centerx, -world_centery, object);

  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);

  rotate_point_90(x1, y1, angle, &newx, &newy);
  s_basic_move_grip(object, GRIP_1, newx, newy);

  rotate_point_90(x2, y2, angle, &newx, &newy);
  s_basic_move_grip(object, GRIP_2, newx, newy);

  o_net_translate_world(world_centerx, world_centery, object);
}

/*! \brief mirror a net object horizontally at a centerpoint
 *  \par Function Description
 *  This function mirrors a net \a object horizontally at the point
 *  (\a world_centerx, \a world_centery).
 *  
 *  \param [in] world_centerx x-coord of the mirror position
 *  \param [in] world_centery y-coord of the mirror position
 *  \param [in] object        The net object
 */
void o_net_mirror_world(int world_centerx, int world_centery, OBJECT *object)
{
  int x1, y1, x2, y2;

  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);

  /* translate object to origin */
  o_net_translate_world(-world_centerx, -world_centery, object);

  s_basic_move_grip(object, GRIP_1, -x1, y1);
  s_basic_move_grip(object, GRIP_2, -x2, y2);

  o_net_translate_world(world_centerx, world_centery, object);
}

/*! \brief calculate the orientation of a net object
 *  \par Function Description
 *  This function calculates the orientation of a net object.
 *
 *  \param [in] object   The net object
 *  \return The orientation: HORIZONTAL, VERTICAL or NEITHER
 */
int o_net_orientation(OBJECT *object)
{
  int x1, y1, x2, y2;

  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);

  if (y1 == y2) {
    return (HORIZONTAL);
  }

  if (x1 == x2) {
    return (VERTICAL);
  }

  return (NEITHER);
}


/*! \brief Destroy degenerate nets
 *  \par Function Description
 *  Checks that a net has nonzero length and destroys it if not.
 *  \param [in] o  A pointer to the net object.
 *
 *  \return A pointer to the same net object, or NULL if it was degenerate.
 */
OBJECT *o_net_normalize(TOPLEVEL *toplevel, OBJECT *o)
{
  int x[2], y[2];

  s_basic_get_grip(o, GRIP_1, &x[0], &y[0]);
  s_basic_get_grip(o, GRIP_2, &x[1], &y[1]);

  if (x[0] == x[1] && y[0] == y[1]) {
    s_delete_object(toplevel, o);
    return NULL;
  }

  return o;
}

/*! \brief merge two net object
 *  \par Function Description
 *  This function does the actual work of making one net segment out of two
 *  connected segments. The first net segment is extended to the length of 
 *  both objects.
 *  The second object (\a del_object) is the object that should be deleted.
 *  
 *  \param [in] object     A net object to extend
 *  \param [in] del_object A net object to be merged into \a object
 *  \param [in] orient     The orientation of both net objects
 *
 * this function does the actual work of making one net segment out of two
 * connected segments
 * The second object (del_object) is the object that should be deleted
 *
 *  \note The first net \a object gets the attributes of the second net 
 *  \a del_object if the two nets are merged together.
 *
 * \todo redo in terms of o_line_subset_p() and to-be-written
 * o_line_parallel_p().
 */
void o_net_consolidate_lowlevel(OBJECT *object, OBJECT *del_object,
				int orient)
{
  int x1, y1, x2, y2;
  int del_x1, del_y1, del_x2, del_y2;
  int temp1, temp2;
  int final1, final2;
  int changed = 0;
  GList *a_iter;
  OBJECT *a_current;

  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);
  s_basic_get_grip(del_object, GRIP_1, &del_x1, &del_y1);
  s_basic_get_grip(del_object, GRIP_2, &del_x2, &del_y2);

  if (GEDA_DEBUG) {
    printf("o %d %d %d %d\n", x1, y1, x2, y2);
    printf("d %d %d %d %d\n", del_x1, del_x1, del_x1, del_x1);
  }

  if (orient == HORIZONTAL) {
    temp1 = min(x1, del_x1);
    temp2 = min(x2, del_x2);

    final1 = min(temp1, temp2);

    temp1 = max(x1, del_x1);
    temp2 = max(x2, del_x2);

    final2 = max(temp1, temp2);

    s_basic_move_grip(object, GRIP_1, final1, y1);
    s_basic_move_grip(object, GRIP_2, final2, y2);
    changed = 1;
  }

  if (orient == VERTICAL) {
    temp1 = min(y1, del_y1);
    temp2 = min(y2, del_y2);

    final1 = min(temp1, temp2);

    temp1 = max(y1, del_y1);
    temp2 = max(y2, del_y2);

    final2 = max(temp1, temp2);

    s_basic_move_grip(object, GRIP_1, x1, final1);
    s_basic_move_grip(object, GRIP_2, x2, final2);
    changed = 1;
  }
  s_basic_get_grip(object, GRIP_1, &x1, &y1);
  s_basic_get_grip(object, GRIP_2, &x2, &y2);
  if (GEDA_DEBUG) {
    printf("fo %d %d %d %d\n", x1, y1, x2, y2);
  }

  /* Move any attributes from the deleted object*/
  if (changed && del_object->attribs != NULL) {

    /* Reassign the attached_to pointer on attributes from the del object */
    a_iter = del_object->attribs;
    while (a_iter != NULL) {
      a_current = a_iter->data;
      a_current->attached_to = object;
      a_iter = g_list_next (a_iter);
    }

    object->attribs = g_list_concat (object->attribs, del_object->attribs);

    /* Don't free del_object->attribs as it's relinked into object's list */
    del_object->attribs = NULL;
  }
}

/*! \brief Check if there's a midpoint connection at (x,y)
 *  \par Function Description
 *  This function checks if the \a object is connected to another net
 *  between it's endpoints. Net segment's only can be merged if there
 *  is no midpoint connection.
 *  
 *  \param object  a net OBJECT to check
 *  \param x       x-coord of the connection location
 *  \param y       y-coord of the connection location
 *  \return TRUE if there's no midpoint connection, else return FALSE
 */
int o_net_consolidate_nomidpoint(OBJECT *object, int x, int y)
{
  GList *c_current;
  CONN *conn;

  c_current = object->conn_list;
  while(c_current != NULL) {
    conn = (CONN *) c_current->data;
    if (conn->other_object) {
      if (conn->other_object->sid != object->sid &&
          conn->x == x && conn->y == y &&
          conn->type == CONN_MIDPOINT) {
	if (GEDA_DEBUG) {
	  printf("Found one! %s\n", conn->other_object->name);
	}
        return(FALSE);
      }
    }
    
    c_current = g_list_next(c_current);
  }

  return(TRUE);
}

/*! \brief try to consolidate a net object
 *  \par Function Description
 *  This function tries to consolidate a net with any other object
 *  that is connected to the current \a object.
 *  
 *  \param toplevel   The TOPLEVEL object
 *  \param page       The page on which the nets exist
 *  \param object     The object to consolidate
 *  \return 0 if no consolidation was possible, -1 otherwise
 *
 */
int o_net_consolidate_segments(TOPLEVEL *toplevel, PAGE *page, OBJECT *object)
{
  int object_orient;
  int other_orient;
  GList *c_current;
  CONN *conn;
  OBJECT *other_object;
  int changed = 0;
  int reselect_new=FALSE;

  if (object == NULL) {
    return(0);
  }

  if (object->type != OBJ_NET) {
    return(0);
  }

  object_orient = o_net_orientation(object);

  c_current = object->conn_list;
  while(c_current != NULL) {
    conn = (CONN *) c_current->data;
    other_object = conn->other_object;

    /* only look at end points which have a valid end on the other side */
    if (other_object != NULL && conn->type == CONN_ENDPOINT &&
        conn->other_whichone != -1 && conn->whichone != -1 &&
        o_net_consolidate_nomidpoint(object, conn->x, conn->y) ) {

      if (other_object->type == OBJ_NET) {
        other_orient = o_net_orientation(other_object);

        /* - both objects have the same orientation (either vert or horiz) */
        /* - it's not the same object */
        if (object_orient == other_orient &&
            object->sid != other_object->sid &&
            other_orient != NEITHER) {
	  if (GEDA_DEBUG) {
	    printf("consolidating %s to %s\n", object->name, other_object->name);
	  }

          o_net_consolidate_lowlevel(object, other_object, other_orient);

          changed++;
          if (other_object->selected == TRUE ) {
            o_selection_remove(page->selection_list, other_object );
            reselect_new=TRUE;
          }

          if (reselect_new == TRUE) {
            o_selection_remove(page->selection_list, object );
            o_selection_add(page->selection_list, object );
          }

          s_conn_remove(other_object);
          s_delete(toplevel, other_object);
          o_net_recalc(object);
          s_conn_update_object(page, object);
          page->object_tail = return_tail(page->object_head);
          return(-1);
        }
      }
    }

    c_current = g_list_next (c_current);
  }

  return(0);
}

/*! \brief consolidate all net objects on a page
 *  \par Function Description
 *  This function consolidates all net objects until no more consolidations
 *  are posible.
 *
 *  \param toplevel  The TOPLEVEL object
 *  \param page      The page whose nets should be consolidated.
 *
 */
void o_net_consolidate(TOPLEVEL *toplevel, PAGE *page)
{
  OBJECT *o_current;
  int status = 0;

  o_current = page->object_head;

  while (o_current != NULL) {
    if (o_current->type == OBJ_NET) {
      status = o_net_consolidate_segments(toplevel, page, o_current);
    }

    if (status == -1) {
      o_current = page->object_head;
      status = 0;
    } else {
      o_current = o_current->next;
    }
  }
}

/*! \brief modify one point of a net object
 *  \par Function Description
 *  This function modifies one point of a net \a object. The point
 *  is specified by the \a whichone variable and the new coordinate
 *  is (\a x, \a y).
 *  
 *  \param toplevel   The TOPLEVEL object
 *  \param object     The net OBJECT to modify
 *  \param x          new x-coord of the net point
 *  \param y          new y-coord of the net point
 *  \param whichone   net point to modify
 *
 */
void o_net_modify(TOPLEVEL *toplevel, OBJECT *object,
		  int x, int y, int whichone)
{
  s_basic_move_grip(object, whichone, x, y);

  o_net_recalc(object);
}
