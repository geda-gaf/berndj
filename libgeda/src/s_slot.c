/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * s_slot.c - A slotting mechanism to solve the "transistor problem".
 * Copyright (C) 2007, 2008 Bernd Jendrissek <bernd.jendrissek@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include "config.h"

#include <glib.h>
#include <libguile.h>
#include <string.h>
#include "libgeda_priv.h"
#include "factory.h"

struct slot_init_context {
  TOPLEVEL *toplevel;
  OBJECT *owner;
};

struct pin_available_context {
  GHashTable *allpins;
  gboolean available;
};

/*! Default setting for draw function. */
void (*slot_draw_func)() = NULL;

static void s_slot_attribs_changed(OBJECT *o);

static void s_slot_dump_uuidmap_one(gpointer key, gpointer value,
				    gpointer context G_GNUC_UNUSED)
{
  OBJECT *o = value;
  printf("  %s [%p] => %p (%c)\n", (char const *) key, key, o, o ? o->type : '?');
}

void s_slot_dumpall(TOPLEVEL *toplevel)
{
  printf("Caught SIGINT, dumping everything in TOPLEVEL %p...\n", toplevel);
  printf("UUID map:\n");
  g_hash_table_foreach(toplevel->uuidmap, &s_slot_dump_uuidmap_one, toplevel);
}

static void s_slot_destroy(OBJECT *o)
{
  if (o->type != OBJ_SLOT) {
    g_critical("s_slot_destroy: object is not a slot\n");
    return;
  }

  if (o->slot->symbol) {
    s_slot_reset_attribs(o->slot->symbol);
  }

  fprintf(stderr, "Destroying slot %p\n", o);
  g_hash_table_destroy(o->slot->function_to_vpad);
}

/*! \brief Constructor
 *  \par Function Description
 *  Initializes an OBJ_SLOT OBJECT.
 *
 *  \param [in] o        The empty object.
 */
void s_slot_init(OBJECT *o)
{
  GHashTable *tab;

  o->slot = g_malloc(sizeof (SLOT));

  tab = g_hash_table_new_full(&g_str_hash, &g_str_equal,
			      &g_free, &g_free);
  o->slot->function_to_vpad = tab;

  /*
   * XXX - We delete the hash table in the OBJECT_EVENT_DESTROY handler
   * (see below) that gets called via s_delete_object().
   */

  o->slot->owner = NULL;
  o->slot->symbol = NULL;
  o->slot->name = NULL;

  fprintf(stderr, "initialized slot %p\n", o);
  g_signal_connect(o, "remove", G_CALLBACK(s_slot_destroy), NULL);
  g_signal_connect(o, "attribs-changed", G_CALLBACK(s_slot_attribs_changed), NULL);
}

static void slot_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o, double scale,
		       GArray *unicode_table)
{
  /* No special action needed to print slot attributes. */
}

/*! \brief Create an OBJ_SLOT object from its serialization.
 *  \par Function Description
 *  Interprets the line of text read from the input file and creates a slot.
 *
 *  \param [in] toplevel     A TOPLEVEL in which everything lives.
 *  \param [in] buf          The line of text to interpret.
 *  \return The new object list.
 */
OBJECT *s_slot_read(TOPLEVEL *toplevel, char const *buf G_GNUC_UNUSED)
{
  OBJECT *o;

  o = toplevel->factory->new_object(toplevel->factory, OBJ_SLOT, "slot");
  o->slot->obj = o;

  o->draw_func = slot_draw_func;
  o->psprint_func = &slot_print;

  s_log_message("Reading slot\n");

  return o;
}

char *s_slot_save(OBJECT *object G_GNUC_UNUSED)
{
  return (g_strdup("S"));
}

static gboolean check_compatible(char const *key,
				 char const *v1,
				 char const *v2,
				 gpointer userdata)
{
  int *incompatible = (int *) userdata;

  if (v1 == NULL && v2 == NULL) {
    /* Two improper attributes are compatible. */
    fprintf(stderr, "Found compatible improper attribute %s\n", key);
    return TRUE;
  }

  if (v1 == NULL || v2 == NULL) {
    /* Proper attributes are never compatible with an improper attribute. */
    fprintf(stderr, "Found mixed proper/improper attribute %s=%s\n",
	    key, v1 ? v1 : v2);
    *incompatible = 1;
    return FALSE;
  }

  if (strcmp(v1, v2) != 0) {
    /* There's no point looking for more incompatible attributes. */
    fprintf(stderr, "Found incompatible attribute %s=%s, should be %s=%s\n",
	    key, v2, key, v1);
    *incompatible = 1;
    return FALSE;
  }

  fprintf(stderr, "Found compatible attribute %s=%s/%s\n", key, v2, v1);
  return TRUE;
}

static enum visit_result compatible_visitor(OBJECT *o, void *context)
{
  GHashTable *attrtab = context;
  char *name, *value;
  char const *attrib_chars;

  if (o->type != OBJ_TEXT) {
    return VISIT_RES_OK;
  }

  if (o->attached_to) {
    /* We want only the toplevel TEXT objects in a symbol, not attributes. */
    return VISIT_RES_OK;
  }

  attrib_chars = o_text_get_string(o);
  if (!o_attrib_get_name_value(attrib_chars, &name, &value)) {
    /* Improper attribute. */
    name = g_strdup(attrib_chars);
    value = NULL;
  }

  fprintf(stderr, "Default attribute %s=%s\n", name, value);
  g_hash_table_replace(attrtab, name, value);

  return VISIT_RES_OK;
}

/** \brief Determine if a symbol can fit into a slot.
 *  \par Function Description
 *  Determines if a symbol can sensibly fit into a slot.  We do this by
 *  checking if all the attributes that are set in both symbol and slot, have
 *  identical values.  The strategy is that a part COMPLEX representing, say,
 *  an LM324A might have slots whose attributes include "device=opamp", and
 *  only abstract symbols (like opamp.sym) whose "device=" attribute also has
 *  the value "opamp", would fit into the slot.  This way you won't be able to
 *  put an opamp symbol into a 74*00.
 *  \param [in] o     The symbol with default and attached attributes.
 *  \param [in] slot  The slot into which the symbol might fit.
 *  \return TRUE if the symbol can fit into the slot.
 */
gboolean s_slot_compatible(OBJECT const *o, OBJECT const *slot_object)
{
  GHashTable *union_attribs;
  int incompatible = 0;

  union_attribs = g_hash_table_new_full(&g_str_hash, &g_str_equal,
					&g_free, &g_free);

  /* TODO: Detect self-incompatible attributes. */

  fprintf(stderr, "s_slot_compatible(o = %s, slot = %s (in %s))\n",
	  o->name, slot_object->slot->name, slot_object->slot->owner->name);
  /* First get default attributes. */
  s_visit((OBJECT *) o, &compatible_visitor, union_attribs, VISIT_ANY, 2);
  /* Let the object attributes override default attributes. */
  o_attrib_populate_hash(union_attribs, o, NULL, NULL, NULL);

  /* Differences in the slotname= attribute value are okay. */
  g_hash_table_remove(union_attribs, "slotname");

  /* Now check if any of the symbol's attributes conflict with the slot. */
  o_attrib_populate_hash(union_attribs, slot_object, NULL,
			 &check_compatible, &incompatible);

  g_hash_table_destroy(union_attribs);

  fprintf(stderr, "s_slot_compatible() = %s\n", incompatible ? "FALSE" : "TRUE");

  return (incompatible == 0);
}

/** \brief Find pins in a slot and remember if they're used.
 *  \note Doesn't really modify \a o. Signature is necessary for passing
 *  this function as an argument.
 */
static enum visit_result pin_finder_visitor(OBJECT *o, void *context)
{
  GHashTable *pins = context;
  OBJECT *pinmap_attrib;
  char *slotmap, *pair;
  int i;

  if (o->type != OBJ_SLOT) {
    return VISIT_RES_OK;
  }

  slotmap = o_attrib_search_name_single(o, "slotmap", &pinmap_attrib);
  if (slotmap == NULL) {
    return VISIT_RES_OK;
  }

  /* We now have a slot that defines a mapping from function to pinnumber. */
  for (i = 0; (pair = u_basic_breakup_string(slotmap, ',', i)) != NULL; i++) {
    char *pinnumber;

    pinnumber = u_basic_breakup_string(pair, '=', 1);
    if (pinnumber) {
      if (o->slot->symbol) {
	/* The slot is using this pin; mark it used. */
	g_hash_table_replace(pins, pinnumber, "1");
      } else if (g_hash_table_lookup(pins, pinnumber) == NULL) {
	/* We've never seen this pin, and it's unused. */
	g_hash_table_replace(pins, pinnumber, "0");
      } else {
	/* The pin already exists in another slot; leave it alone. */
	g_free(pinnumber);
      }
    }

    g_free(pair);
  }

  return VISIT_RES_OK;
}

static void pin_available_checker(gpointer key, gpointer value G_GNUC_UNUSED,
				  gpointer context)
{
  struct pin_available_context *avail = context;
  char const *pinused;

  pinused = g_hash_table_lookup(avail->allpins, key);
  printf("pinused = %s\n", pinused);
  if (pinused == NULL || pinused[0] != '0') {
    printf("Pin %s is already used\n", (char const *) key);
    avail->available = FALSE;
  }
}

/** \brief Determine if a symbol has room in a slot.
 *  \par Function Description
 *  Determines if a symbol could fit into a slot without stomping on pins
 *  that are already assigned to another symbol.  Attribute compatibility
 *  is ignored.
 *
 *  \param [in] o     The symbol.
 *  \param [in] slot  The slot into which the symbol might fit.
 *  \return TRUE if the symbol can fit into the slot.
 */
gboolean s_slot_available(OBJECT const *o, OBJECT const *slot_object)
{
  GHashTable *allpins, *slotpins, *symbolpins;
  struct pin_available_context avail;

  allpins = g_hash_table_new_full(&g_str_hash, &g_str_equal, &g_free, NULL);
  slotpins = g_hash_table_new_full(&g_str_hash, &g_str_equal, &g_free, NULL);
  symbolpins = g_hash_table_new_full(&g_str_hash, &g_str_equal, &g_free, NULL);

  /* Get sets of used and requested pins. */
  s_visit(slot_object->slot->owner, &pin_finder_visitor, allpins,
	  VISIT_ANY, 2);
  pin_finder_visitor((OBJECT *) slot_object, slotpins);
  pin_finder_visitor((OBJECT *) o, symbolpins);

  avail.allpins = allpins;
  avail.available = TRUE;

  /* Check that each pin used in the slot is available. */
  g_hash_table_foreach(slotpins, pin_available_checker, &avail);

  /* Check that each pin used in the symbol is available. */
  g_hash_table_foreach(symbolpins, pin_available_checker, &avail);

  g_hash_table_unref(allpins);
  g_hash_table_unref(slotpins);
  g_hash_table_unref(symbolpins);

  return avail.available;
}

static enum visit_result pin_renumber_visitor(OBJECT *o, void *context)
{
  OBJECT *slot_object = context;
  OBJECT *pinfunction_attrib, *pinnumber_attrib;
  char *pinfunction, *old_pinnumber, *new_pinnumber = NULL;

  if (o->type != OBJ_PIN) {
    return VISIT_RES_OK;
  }

  pinfunction = o_attrib_search_name_single(o, "pinfunction",
					    &pinfunction_attrib);
  s_log_message("pin_renumber_visitor(#<object %s>, ...), pinfunction = %s\n", o->name, pinfunction);
  if (pinfunction == NULL) {
    return VISIT_RES_OK;
  }

  if (slot_object) {
    SLOT *slot = slot_object->slot;
    OBJECT *component = slot->owner;
    char *vpad, *pad = NULL;

    /* Look up the pinnumber in the owning slot's map. */
    vpad = g_hash_table_lookup(slot->function_to_vpad, pinfunction);
    if (vpad == NULL) {
      s_log_message("No mapping for pinfunction=%s, leaving pinnumber unchanged\n", pinfunction);
      g_free(pinfunction);
      return VISIT_RES_OK;
    }

    /*
     * Translate vpad to pad, then to pin number.  A nonexistent map, or a
     * map with no entry for the key, get treated as an identity function.
     */
    if (component->vpad_to_pad) {
      pad = g_hash_table_lookup(component->vpad_to_pad, vpad);
    }
    if (pad == NULL) {
      pad = vpad;
    }
    if (component->pad_to_pin) {
      new_pinnumber = g_hash_table_lookup(component->pad_to_pin, pad);
    }
    if (new_pinnumber == NULL) {
      new_pinnumber = pad;
    }
  } else {
    /* Reset the pinnumber to '?'.  FIXME: Don't hardcode.  But how? */
    new_pinnumber = "?";
  }

  g_free(pinfunction);

  /* Now replace the attribute text. */
  old_pinnumber = o_attrib_search_name_single(o, "pinnumber", &pinnumber_attrib);
  if (old_pinnumber) {
    char *new_text_string = g_strconcat("pinnumber=", new_pinnumber, NULL);

    fprintf(stderr, "Changing %s attribute from %s to %s\n",
	    o->name, o_text_get_string(pinnumber_attrib), new_text_string);

    g_free(old_pinnumber);
    o_text_take_string(pinnumber_attrib, new_text_string);
  }

  return VISIT_RES_OK;
}

/*! \brief Rewrite some subset of symbol attributes.
 *  \par Function Description
 *  Rewrite a subset of attributes to inherit values from the slot.
 *
 *  \param [in] symbol    The abstract symbol whose attribs must change.
 *  \param [in] slot      The slot into which \a symbol will go.
 */
static void s_slot_rewrite_attribs(OBJECT *symbol,
				   OBJECT *slot)
{
  char *slotrewrite;

  /* Rewrite attributes mentioned in slotrewrite= meta-attribute. */
  slotrewrite = o_attrib_search_name_single(symbol, "slotrewrite", NULL);
  if (slotrewrite != NULL) {
    char *attrib_name;
    int i;

    for (i = 0; (attrib_name = u_basic_breakup_string(slotrewrite, ',', i)); i++) {
      OBJECT *attrib;
      char *old_string, *new_value;

      old_string = o_attrib_search_name_single(symbol, attrib_name, &attrib);
      if (old_string == NULL) {
	g_warning("Can't rewrite %s attribute on #<object %s>: it doesn't exist",
		  attrib_name, symbol->name);
	continue;
      }

      g_free(old_string);

      new_value = o_attrib_search_name_single(slot, attrib_name, NULL);
      if (new_value == NULL) {
	g_warning("Can't rewrite %s attribute on #<object %s>: no new value\n",
		  attrib_name, symbol->name);
	continue;
      }

      o_text_take_string(attrib,
			 g_strconcat(attrib_name, "=", new_value, NULL));
      g_free(new_value);
    }
  }
}

static void s_slot_rewrite_slot(gpointer key G_GNUC_UNUSED, gpointer value,
				gpointer context G_GNUC_UNUSED)
{
  OBJECT *slot = value;

  if (slot->slot->symbol) {
    s_slot_rewrite_attribs(slot->slot->symbol, slot);
  }
}

/*! \brief Rewrite attributes in the symbols linked to a component.
 *  \par Function Description
 *  Propagates changed attributes into each of an object's symbol slots.
 *
 *  \param [in] component  A component whose linked symbols need rewriting.
 */
void s_slot_rewrite_symbols(OBJECT *component)
{
  if (component == NULL || component->type != OBJ_COMPLEX) {
    g_critical("s_slot_rewrite_symbols: component is not a complex!\n");
    return;
  }

  g_hash_table_foreach(component->complex->slots, &s_slot_rewrite_slot, NULL);
}

/*! \brief Link a symbol into a slot.
 *  \par Function Description
 *  Links a symbol into an empty slot.
 *
 *  \param [in] toplevel  A TOPLEVEL in which the slot and symbol live.
 *  \param [in] owner     A COMPLEX that has slots.
 *  \param [in] symbol    An object whose attribs want it to be in a slot.
 *  \return 0 for success, nonzero for failure.
 */
int s_slot_link(TOPLEVEL *toplevel, OBJECT *owner, OBJECT *symbol)
{
  char *wanted_slot;
  char const *uuid;
  OBJECT *slot_object = NULL;
  OBJECT *slotname;

  uuid = owner->uuid;
  if (!uuid) {
    s_log_message(_("Can't link symbols to #<object %s> since it has no UUID\n"),
		  owner->name);
    return -1;
  }

  wanted_slot = o_attrib_search_name_single(symbol, "slotname", &slotname);
  if (wanted_slot == NULL || strcmp(wanted_slot, "?") == 0) {
    s_log_message(_("Symbol #<object %s> needs a slotname= attribute\n"),
		  symbol->name);
    g_free(wanted_slot);
    return -1;
  } else {
    slot_object = g_hash_table_lookup(owner->complex->slots, wanted_slot);
  }

  if (slot_object == NULL) {
    fprintf(stderr, "s_slot_link(): OBJECT %s has no slot named %s",
	    owner->name, wanted_slot);
    g_free(wanted_slot);
    return -1;
  }

  fprintf(stderr, "Found slot %s in object %s\n", wanted_slot, owner->name);

  if (slot_object->slot->symbol) {
    s_log_message("Slot %s in #<object %s> (uuid=%s) already has a symbol\n",
		  wanted_slot, owner->name, uuid);
    g_free(wanted_slot);
    return -1;
  }

  /* Don't need the slot name anymore - we have the actual SLOT. */
  g_free(wanted_slot);

  s_slot_rewrite_attribs(symbol, owner);

  /* Rewrite the pinnumber attributes. */
  fprintf(stderr, "Rewriting pinnumber attributes on %s\n", symbol->name);
  s_visit(symbol, &pin_renumber_visitor, slot_object, VISIT_ANY, 2);

  /* Finally, link the symbol to its slot. */
  symbol->owning_slot = slot_object;
  slot_object->slot->symbol = symbol;

  toplevel->page_current->CHANGED = 1;

  return 0;
}

/*! \brief Unlink a symbol from a slot.
 *  \par Function Description
 *  Unlinks a symbol from a slot and marks that slotas available.
 *
 *  \param [in] o         The symbol to unlink.
 *  \return The OBJ_SLOT OBJECT representing the now empty slot.
 */
OBJECT *s_slot_unlink(OBJECT *o)
{
  OBJECT *old_slot = o->owning_slot;

  if (old_slot) {
    SLOT *slot;

    slot = o->owning_slot->slot;

    /* Unlink the objects. */
    o->owning_slot = NULL;
    slot->symbol = NULL;
    old_slot = slot->obj;

    /* Reset pinnumber attributes. */
    s_visit(o, &pin_renumber_visitor, NULL, VISIT_ANY, 2);
  }

  return old_slot;
}

/*! \brief Reset slotting attributes.
 */
void s_slot_reset_attribs(OBJECT *o)
{
  OBJECT *slotname = NULL, *slotowner = NULL;

  g_free(o_attrib_search_name_single(o, "slotname", &slotname));
  if (slotname) {
    o_text_set_string(slotname, "slotname=?");
  }
  g_free(o_attrib_search_name_single(o, "slotowner", &slotowner));
  if (slotowner) {
    o_text_set_string(slotowner, "slotowner=?");
  }
}

static void s_slot_reparent_visitor(gpointer key G_GNUC_UNUSED,
				    gpointer value, gpointer context)
{
  TOPLEVEL *toplevel = context;
  OBJECT *slot_object = value;

  if (slot_object->slot->symbol) {
    s_slot_reparent_from_attribs(toplevel, slot_object->slot->symbol);
  }
}

/** \brief Find slots and renumber pins in linked symbols.
 */
static enum visit_result
s_slot_renumber_pins(OBJECT *o, void *context G_GNUC_UNUSED)
{
  if (o->type != OBJ_SLOT || o->slot->symbol == NULL) {
    return VISIT_RES_OK;
  }

  return (s_visit(o->slot->symbol, &pin_renumber_visitor, o, VISIT_ANY, 2));
}

/** \brief Update pin numbers.
 */
void s_slot_update_pins(OBJECT *component, TOPLEVEL *toplevel)
{
  g_hash_table_foreach(component->complex->slots, &s_slot_reparent_visitor,
		       toplevel);
  s_visit(component, &s_slot_renumber_pins, NULL, VISIT_ANY, 2);
}

static enum visit_result slot_init_visitor(OBJECT *o, void *userdata)
{
  struct slot_init_context *context = userdata;

  if (o->type != OBJ_SLOT) {
    return VISIT_RES_OK;
  }

  /* Bail if the component has multiple slots with the same name; only the
   * first one counts.
   */
  if (g_hash_table_lookup(context->owner->complex->slots, o->slot->name)) {
    g_warning(_("#<object %s> has multiple slots named %s; I'll use only the first\n"),
	      context->owner->name, o->slot->name);
    return VISIT_RES_OK;
  }

  g_hash_table_replace(context->owner->complex->slots, g_strdup(o->slot->name), o);

  o->slot->owner = context->owner;

  return VISIT_RES_OK;
}

/** \brief Make the slots in a component easily accessible
 *  \par Function Description
 *  Populates a hash table with keys being the slot names and values
 *  being pointers to the corresponding slot objects.
 *
 *  \param [in] o        A slotted component.
 */
void s_slot_setup_complex(TOPLEVEL *toplevel, OBJECT *o)
{
  struct slot_init_context init_context = {
    .toplevel = toplevel,
    .owner = o,
  };

  if (o->type != OBJ_COMPLEX) {
    return;
  }

  o->complex->slots = g_hash_table_new_full(&g_str_hash, &g_str_equal,
					    &g_free, NULL);

  s_visit(o, &slot_init_visitor, &init_context, VISIT_ANY, 2);

  /*
   * Notice when pin mappings change.  It's easier to subscribe to
   * "attribs-changed" and do a few redundant renumberings, than to figure out
   * which attribute to monitor for "changed", or to monitor the component for
   * "attached" if the attribute is missing, only then to switch to monitoring
   * "changed" again once the attribute exists...
   */
  g_signal_connect(o, "attribs-changed", G_CALLBACK(s_slot_update_pins),
		   toplevel);
}

/** \brief Initialize a slot from its attached attributes.
 *  \par Function Description
 *  \param [in] o        A slot object.
 */
static void s_slot_attribs_changed(OBJECT *o)
{
  char *slotname, *pinmap;

  if (o->type != OBJ_SLOT) {
    return;
  }

  /* First clear out any stale data. */
  if (o->slot->name) {
    g_free(o->slot->name);
    o->slot->name = NULL;
  }
  g_hash_table_remove_all(o->slot->function_to_vpad);

  slotname = o_attrib_search_name_single(o, "slotname", NULL);
  if (slotname != NULL) {
    o->slot->name = slotname;
  }

  pinmap = o_attrib_search_name_single(o, "slotmap", NULL);
  if (pinmap != NULL) {
    u_basic_populate_hash(o->slot->function_to_vpad, pinmap, ',', '=');
    g_free(pinmap);
  }

  g_signal_emit_by_name(G_OBJECT(o), "changed");
}

static void slot_unlink_all_slots_visitor(gpointer key G_GNUC_UNUSED,
					  gpointer value,
					  gpointer context G_GNUC_UNUSED)
{
  OBJECT *slot_object = value;

  if (slot_object->slot->symbol) {
    OBJECT *symbol = slot_object->slot->symbol;

    s_slot_unlink(symbol);
    s_slot_reset_attribs(symbol);
  }
}

/** \brief Destroy slots to prepare for deleting a whole component.
 *  \par Function Description
 *  \param [in] toplevel   The toplevel environment.
 *  \param [in] o          Destroy slots in this object.
 */
void s_slot_destroy_complex(TOPLEVEL *toplevel, OBJECT *o)
{
  if (o->type != OBJ_COMPLEX) {
    return;
  }

  g_hash_table_foreach(o->complex->slots, &slot_unlink_all_slots_visitor,
		       NULL);
  g_hash_table_destroy(o->complex->slots);

  s_toplevel_unregister_object(toplevel, o);
}

void s_slot_reparent_from_attribs(TOPLEVEL *toplevel, OBJECT *symbol)
{
  OBJECT *owner;
  char *owner_uuid;

  if (symbol->type != OBJ_COMPLEX && symbol->type != OBJ_PLACEHOLDER) {
    /* If it can't have pins, we don't care. */
    return;
  }

  /* Unlink early to allow attribute-driven unlinking. */
  s_slot_unlink(symbol);

  owner_uuid = o_attrib_search_name_single(symbol, "slotowner", NULL);
  if (owner_uuid == NULL) {
    /* This one doesn't have an owner; just skip it. */
    return;
  }

  owner = g_hash_table_lookup(toplevel->uuidmap, owner_uuid);
  if (owner == NULL) {
    s_log_message("OBJECT #<object %s> belongs to OBJECT uuid=%s which is missing\n",
		  symbol->name, owner_uuid);
    g_free(owner_uuid);
    return;
  }

  s_log_message("OBJECT #<object %s> belongs to OBJECT uuid=%s which is OBJECT %s\n",
		symbol->name, owner_uuid, owner->name);
  g_free(owner_uuid);

  s_slot_link(toplevel, owner, symbol);
}

/** \brief Put a symbol into a specific slot.
 *  \par Function Description
 *  \param [in] toplevel   The toplevel environment.
 *  \param [in] symbol     The symbol to assign to a slot.
 *  \param [in] slot       The slot to accept the symbol.
 *  \param [in] slotname   The name of the slot.
 *
 *  \return Zero iff the slot linking succeeded.
 *
 *  \todo slotname should probably be a property of SLOT.
 */
int s_slot_reparent_specific_slot(TOPLEVEL *toplevel, OBJECT *symbol,
				  OBJECT *slot_object)
{
  OBJECT *slotowner_attr, *slotname_attr;
  char *old_slotowner, *old_slotname;
  char const *uuid;

  /* First make the slot available, evicting current occupants. */
  if (slot_object->slot->symbol) {
    OBJECT *symbol = slot_object->slot->symbol;

    s_slot_unlink(symbol);
    s_slot_reset_attribs(symbol);
  }

  uuid = slot_object->slot->owner->uuid;
  g_return_val_if_fail(uuid != NULL, -1);

  old_slotowner = o_attrib_search_name_single_exact(symbol, "slotowner",
						    &slotowner_attr);
  old_slotname = o_attrib_search_name_single_exact(symbol, "slotname",
						   &slotname_attr);

  /* Free the strings - we don't care about their value. */
  g_free(old_slotname);
  g_free(old_slotowner);

  if (old_slotowner == NULL || old_slotname == NULL) {
    s_log_message(_("#<object %s> must have both slotowner= and slotname=\n"),
		  symbol->name);
    return -1;
  }

  /* Replace the slotowner= and slotname= attributes. */
  o_text_take_string(slotowner_attr, g_strconcat("slotowner=", uuid, NULL));
  o_text_take_string(slotname_attr,
		     g_strconcat("slotname=", slot_object->slot->name, NULL));

  s_slot_unlink(symbol);
  s_slot_link(toplevel, slot_object->slot->owner, symbol);

  return 0;
}

static enum visit_result s_slot_visitor_reparent(OBJECT *o, void *context)
{
  TOPLEVEL *toplevel = (TOPLEVEL *) context;

  s_slot_reparent_from_attribs(toplevel, o);

  return VISIT_RES_OK;
}

void s_slot_make_links(TOPLEVEL *toplevel, PAGE *page)
{
  /* Look for complexes that belong in another complex's slots. */
  s_visit_page(page, &s_slot_visitor_reparent, toplevel, VISIT_ANY, 1);
}

OBJECT *s_slot_get_occupant(OBJECT const *slot_object)
{
  g_return_val_if_fail(slot_object->type == OBJ_SLOT, NULL);

  return slot_object->slot->symbol;
}

OBJECT *s_slot_get_owner(OBJECT const *slot_object)
{
  g_return_val_if_fail(slot_object->type == OBJ_SLOT, NULL);

  return slot_object->slot->owner;
}
/* vim: set sw=2: */
