/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998, 1999, 2000 Kazu Hirata / Ales Hvezda
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA
 */
#include <config.h>

#include <stdio.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif
#ifdef HAVE_STRARG_H
#include <stdarg.h>
#endif
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#include "factory.h"
#include "libgeda_priv.h"

#ifdef HAVE_LIBDMALLOC
#include <dmalloc.h>
#endif

static void s_toplevel_class_init(gpointer g_class, gpointer g_class_data);
static void s_toplevel_dispose(GObject* object);
static void s_toplevel_finalize(GObject* object);
static void s_toplevel_instance_init(GTypeInstance* instance, gpointer g_class);

/*! \brief Initializes TOPLEVEL class data.
 *
 *  \param [in] g_class A pointer to the TOPLEVEL_CLASS.
 *  \param [in] g_class_data Unknown.  Seems to be becoming deprecated.
 */
static void s_toplevel_class_init(gpointer g_class,
				  gpointer g_class_data G_GNUC_UNUSED)
{
  GObjectClass* object_class = G_OBJECT_CLASS(g_class);

  object_class->dispose  = s_toplevel_dispose;
  object_class->finalize = s_toplevel_finalize;
}

/*! \brief Disposes of a TOPLEVEL object.
 *
 *  This function performs the first stage of object destruction by
 *  unreferencing member objects.
 *
 *  \param [in] object A pointer to the TOPLEVEL object.
 */
static void s_toplevel_dispose(GObject* object)
{
  TOPLEVEL *toplevel = S_TOPLEVEL(object);

  /* delete all pages */
  s_page_delete_list (toplevel);

  /* Delete the page list */
  g_object_unref(toplevel->pages);

  g_hash_table_destroy(toplevel->uuidmap);

  toplevel->pages = NULL;
}

/*! \brief Finalizes a TOPLEVEL object.
 *
 *  This function performs the second stage of object destruction by freeing
 *  allocated resources.
 *
 *  \param [in] object A pointer to the TOPLEVEL object.
 */
static void s_toplevel_finalize(GObject* object)
{
  TOPLEVEL *toplevel = S_TOPLEVEL(object);

  if (toplevel->auto_save_timeout != 0) {
    /* Assume this works */
    g_source_remove (toplevel->auto_save_timeout);
  }

  g_free (toplevel->untitled_name);
  g_free (toplevel->font_directory);
  g_free (toplevel->scheme_directory);
  g_free (toplevel->bitmap_directory);
  g_free (toplevel->bus_ripper_symname);
}

/*! \brief Gets the TOPLEVEL's GType.
 *
 *  \return The TOPLEVEL's GType.
 */
GType s_toplevel_get_type(void)
{
  static GType type = 0;

  if (type == 0) {
    static const GTypeInfo info = {
      sizeof(TOPLEVEL_CLASS),
      NULL,
      NULL,
      s_toplevel_class_init,
      NULL,
      NULL,
      sizeof(TOPLEVEL),
      0,
      s_toplevel_instance_init,
      NULL
    };

    type = g_type_register_static(G_TYPE_OBJECT, "S_TOPLEVEL", &info, 0);
  }

  return type;
}

/*! \brief Initializes TOPLEVEL instance data.
 *
 *  \param [in] instance A pointer to the TOPLEVEL object.
 *  \param [in] g_class Unknown.  Seems to be becoming deprecated.
 */
static void s_toplevel_instance_init(GTypeInstance* instance,
				     gpointer g_class G_GNUC_UNUSED)
{
  TOPLEVEL *toplevel = S_TOPLEVEL(instance);

  toplevel->factory = &libgeda_factory;

  /* If there are ever duplicate UUIDs, we free their memory. */
  toplevel->uuidmap = g_hash_table_new_full(&g_str_hash, &g_str_equal,
					    &g_free, NULL);

  toplevel->RC_list = NULL;

  toplevel->untitled_name      = NULL;
  toplevel->font_directory     = NULL;
  toplevel->scheme_directory   = NULL;
  toplevel->bitmap_directory   = NULL;

  toplevel->init_left = 0;
  toplevel->init_top  = 0;
  /* init_right and _bottom are set before this function is called */

  toplevel->snap = 1;

  toplevel->override_color = -1;

  toplevel->ADDING_SEL        = 0;

  toplevel->pages = geda_list_new();
  toplevel->page_current = NULL;

  toplevel->show_hidden_default = 0;

  toplevel->major_changed_refdes = NULL;

  toplevel->snap_size = 100;

  /* BLOCK SET IN GSCHEM, BUT USED IN LIBGEDA - NEEDS A RETHINK */
  toplevel->attribute_color    = 0;
  toplevel->detachedattr_color = 0;
  toplevel->background_color   = 0;
  toplevel->net_endpoint_color = 0;
  toplevel->junction_color     = 0;
  toplevel->override_net_color = -1;
  toplevel->override_bus_color = -1;
  toplevel->override_pin_color = -1;
  toplevel->pin_style = 0;
  toplevel->net_style = 0;
  toplevel->bus_style = 0;
  toplevel->line_style = 0;
  /* END BLOCK - ALTHOUGH THERE ARE MORE CASES! */

  toplevel->text_output = 0;

  toplevel->print_orientation = 0;

  toplevel->image_color = FALSE;

  toplevel->print_color = FALSE;

  toplevel->print_color_background = 0;

  toplevel->setpagedevice_orientation = FALSE;

  toplevel->setpagedevice_pagesize = FALSE;

  toplevel->postscript_prolog = NULL;
  toplevel->postscript_font_scale = 1.0;

  toplevel->net_consolidate = FALSE;

  /* The following is an attempt at getting (deterministic) defaults */
  /* for the following variables */
  toplevel->attribute_promotion = FALSE;
  toplevel->promote_invisible   = FALSE;
  toplevel->keep_invisible      = FALSE;

  toplevel->print_output_type = 0;

  toplevel->print_output_capstyle = BUTT_CAP;

  toplevel->paper_width  = 0;
  toplevel->paper_height = 0;

  toplevel->bus_ripper_symname = NULL;

  toplevel->force_boundingbox = FALSE;

  toplevel->print_vector_threshold = 3;

  toplevel->always_promote_attributes = NULL;

  toplevel->net_naming_priority = 0;
  toplevel->hierarchy_traversal = 0;
  toplevel->hierarchy_uref_mangle = 0;
  toplevel->hierarchy_netname_mangle = 0;
  toplevel->hierarchy_netattrib_mangle = 0;
  toplevel->hierarchy_uref_separator      = NULL;
  toplevel->hierarchy_netname_separator   = NULL;
  toplevel->hierarchy_netattrib_separator = NULL;
  toplevel->hierarchy_netattrib_order = 0;
  toplevel->hierarchy_netname_order = 0;
  toplevel->hierarchy_uref_order = 0;
  toplevel->unnamed_netname = NULL;

  /* Auto-save interval */
  toplevel->auto_save_interval = 0;
  toplevel->auto_save_timeout = 0;
}

/*! \brief Creates a new TOPLEVEL object.
 *
 *  The new TOPLEVEL object starts with a reference count of 1.
 *
 *  \return A pointer to the newly created TOPLEVEL object.
 */
TOPLEVEL *s_toplevel_new (void)
{
  return S_TOPLEVEL(g_object_new(S_TOPLEVEL_TYPE, NULL));
}

/*! \brief Helper to allocate an object through the factory.
 *  \par Function Description
 *  Asks the registered factory to allocate and initialize an OBJECT.
 *  \param [in] toplevel  The TOPLEVEL environment.
 *  \param [in] type      The sub-type of the object to create; one of the OBJ_* constants.
 *  \param [in] prefix    The name prefix for the session-unique object name.
 *  \return A pointer to the fully constructed OBJECT.
 */
OBJECT *s_toplevel_new_object(TOPLEVEL *toplevel, char type, char const *prefix)
{
  OBJECT *obj_new;

  obj_new = toplevel->factory->new_object(toplevel->factory, type, prefix);
  g_signal_connect(G_OBJECT(obj_new), "attached",
		   G_CALLBACK(s_object_attrib_attached), toplevel);
  g_signal_connect(G_OBJECT(obj_new), "attribs-changed",
		   G_CALLBACK(s_object_attribs_changed), toplevel);

  return obj_new;
}

/*! \brief Change the current active page.
 *  \par Function Description
 *  Sets the current page and emits a "page-changed" signal on \a toplevel.
 *  \param [in] toplevel  The TOPLEVEL environment.
 *  \param [in] p_new     The PAGE to switch to.
 */
void s_toplevel_goto_page(TOPLEVEL *toplevel, PAGE *p_new)
{
  /*
   * At some point, I want to make the component selector sensitive to the
   * page on which the component is to be placed. Pass control to PAGE before
   * changing TOPLEVEL so that in a future interim stage where the current
   * directory still governs symbol lookups, the component selector will be
   * able to see the correct set of symbols after s_page_goto switches to the
   * PAGE's base directory.
   */
  if (p_new) {
    s_page_goto(p_new);
  }

  toplevel->page_current = p_new;
}

/*! \brief Call a function on each page in the page list.
 *  \par Function Description
 *  Calls \a fn on each page in \a toplevel's list of pages.
 *  \param [in] toplevel  The TOPLEVEL containing the pages of interest.
 *  \param [in] fn        A function to call on each page.
 *  \param [in] userdata  Caller-supplied context to pass to \a fn.
 */
void s_toplevel_foreach_page(TOPLEVEL const *toplevel,
			     void (*fn)(PAGE *page, void *userdata),
			     void *userdata)
{
  const GList *iter;
  PAGE *page;

  for (iter = geda_list_get_glist(toplevel->pages);
       iter != NULL;
       iter = g_list_next(iter)) {
    page = (PAGE *) iter->data;
    (*fn)(page, userdata);
  }
}

void s_toplevel_register_object(TOPLEVEL *toplevel, OBJECT *o_current)
{
  OBJECT *conflict;

  if (!o_current->uuid) {
    /* Nothing to do; the object has no UUID. */
    return;
  }

  conflict = g_hash_table_lookup(toplevel->uuidmap, o_current->uuid);
  if (conflict) {
    s_log_message("#<object %s> and #<object %s> have conflicting UUID %s\n",
		  o_current->name, conflict->name, o_current->uuid);
    g_return_if_reached();
  }
  g_hash_table_replace(toplevel->uuidmap, g_strdup(o_current->uuid), o_current);
}

void s_toplevel_unregister_object(TOPLEVEL *toplevel, OBJECT *o_current)
{
  if (!o_current->uuid) {
    /* Nothing to do; the object has no UUID. */
    return;
  }

  g_hash_table_remove(toplevel->uuidmap, o_current->uuid);
}
