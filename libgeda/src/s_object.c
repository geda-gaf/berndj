/* gEDA - GPL Electronic Design Automation
 * factory.c - Abstract Factory pattern for libgeda objects
 * Copyright (C) 2009 Bernd Jendrissek
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#include <glib.h>
#include <glib-object.h>
#include "libgeda_priv.h"

enum {
  PROPERTY_BASENAME = 1,
  PROPERTY_TYPE,
};

enum {
  GEDA_OBJECT_SIGNAL_ATTRIB_ATTACHED,
  GEDA_OBJECT_SIGNAL_ATTRIBS_CHANGED,
  GEDA_OBJECT_SIGNAL_CHANGED,
  GEDA_OBJECT_SIGNAL_REMOVE, /* TODO: Remove when PAGE becomes a GObject. */
  GEDA_OBJECT_SIGNAL_LAST
};

static GObjectClass *geda_object_parent_class;

static guint geda_object_signals[GEDA_OBJECT_SIGNAL_LAST];

static void s_object_set_property(GObject *object, guint property_id,
				  const GValue *value, GParamSpec *pspec G_GNUC_UNUSED)
{
  OBJECT *o = GEDA_OBJECT(object);

  switch (property_id) {
  case PROPERTY_BASENAME:
    /* XXX - This is G_PARAM_CONSTRUCT_ONLY so there is no previous name. */
    o->name = g_strdup_printf("%s.%d", g_value_get_string(value), o->sid);
    break;
  case PROPERTY_TYPE:
    o->type = g_value_get_char(value);

    switch (o->type) {
    case OBJ_SLOT:
      s_slot_init(o);
      break;
    }
    break;
  }
}

static void s_object_dispose(GObject *object) {
  G_OBJECT_CLASS(geda_object_parent_class)->dispose(object);
}

static void s_object_finalize(GObject *object) {
  G_OBJECT_CLASS(geda_object_parent_class)->finalize(object);
}

static void s_object_class_init(gpointer g_class, gpointer g_class_data G_GNUC_UNUSED)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(g_class);
  GParamSpec *pspec;

  geda_object_parent_class = g_type_class_peek_parent(g_class);

  gobject_class->set_property = s_object_set_property;
  gobject_class->dispose  = s_object_dispose;
  gobject_class->finalize = s_object_finalize;

  pspec = g_param_spec_string("basename", "Object base name",
			      "Set object's base name", "object",
			      G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
  g_object_class_install_property(gobject_class, PROPERTY_BASENAME, pspec);

  pspec = g_param_spec_char("type", "Object type",
			    "Set object's type",
			    G_MININT8, G_MAXINT8, -1,
			    G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
  g_object_class_install_property(gobject_class, PROPERTY_TYPE, pspec);

  /* XXX Consider adding an accumulator that would allow attribute validation. */
  geda_object_signals[GEDA_OBJECT_SIGNAL_ATTRIB_ATTACHED] =
    g_signal_new("attached", G_TYPE_FROM_CLASS(gobject_class),
		 G_SIGNAL_RUN_LAST, 0, NULL, NULL,
		 &g_cclosure_marshal_VOID__OBJECT,
		 G_TYPE_NONE, 1, G_TYPE_OBJECT);

  geda_object_signals[GEDA_OBJECT_SIGNAL_ATTRIBS_CHANGED] =
    g_signal_new("attribs-changed", G_TYPE_FROM_CLASS(gobject_class),
		 G_SIGNAL_RUN_LAST, 0, NULL, NULL,
		 &g_cclosure_marshal_VOID__VOID,
		 G_TYPE_NONE, 0);

  geda_object_signals[GEDA_OBJECT_SIGNAL_CHANGED] =
    g_signal_new("changed", G_TYPE_FROM_CLASS(gobject_class),
		 G_SIGNAL_RUN_LAST, 0, NULL, NULL,
		 &g_cclosure_marshal_VOID__VOID,
		 G_TYPE_NONE, 0);

  geda_object_signals[GEDA_OBJECT_SIGNAL_REMOVE] =
    g_signal_new("remove", G_TYPE_FROM_CLASS(gobject_class),
		 G_SIGNAL_RUN_LAST, 0, NULL, NULL,
		 &g_cclosure_marshal_VOID__VOID,
		 G_TYPE_NONE, 0);
}

static void s_object_map_changed(OBJECT *o, void *userdata)
{
  GHashTable **map = userdata;
  OBJECT *component = o->attached_to;
  char *value;

  /* We only connect to this signal on attributes attached to something. */
  g_return_if_fail(component != NULL);

  g_hash_table_remove_all(*map);

  if (!o_attrib_get_name_value(o_text_get_string(o), NULL, &value)) {
    *map = NULL;
    return;
  }

  if (*map == NULL) {
    *map = g_hash_table_new_full(&g_str_hash, &g_str_equal, &g_free, &g_free);
  }

  u_basic_populate_hash(*map, value, ',', '=');

  printf("Map %p changed to %s\n", *map, value);

  g_free(value);
}

/*! \brief Per-page callback to relink symbols after UUID change.
 */
static void relink_symbols(PAGE *page, void *userdata)
{
  TOPLEVEL *toplevel = userdata;

  s_slot_make_links(toplevel, page);
}

void s_object_attrib_attached(OBJECT *o, OBJECT *attr, void *userdata)
{
  TOPLEVEL *toplevel = userdata;
  char *key, *value;

  if (attr->type != OBJ_TEXT) {
    /* Don't care about non-text attributes. */
    return;
  }

  if (!o_attrib_get_name_value(o_text_get_string(attr), &key, &value)) {
    return;
  }

  if (strcmp(key, "uuid") == 0) {
    if (o->uuid) {
      g_free(o->uuid);
    }
    o->uuid = value;
    /* TODO: Emit a uuid-changed signal? */
    /* XXX How to determine if the object is on-page? */
    /* s_toplevel_register_object(toplevel, o); */
    s_toplevel_foreach_page(toplevel, &relink_symbols, toplevel);
  } else if (strcmp(key, "vpads") == 0) {
    g_assert(o->vpad_to_pad == NULL);
    o->vpad_to_pad = g_hash_table_new_full(&g_str_hash, &g_str_equal,
					   &g_free, &g_free);
    u_basic_populate_hash(o->vpad_to_pad, value, ',', '=');
    g_free(value);

    g_signal_connect(G_OBJECT(attr), "changed",
		     G_CALLBACK(s_object_map_changed),
		     &o->vpad_to_pad);
  } else if (strcmp(key, "packagepins") == 0) {
    g_assert(o->pad_to_pin == NULL);
    o->pad_to_pin = g_hash_table_new_full(&g_str_hash, &g_str_equal,
					  &g_free, &g_free);
    u_basic_populate_hash(o->pad_to_pin, value, ',', '=');
    g_free(value);

    g_signal_connect(G_OBJECT(attr), "changed",
		     G_CALLBACK(s_object_map_changed),
		     &o->pad_to_pin);
  } else {
    g_free(value);
  }
  g_free(key);
}

void s_object_attribs_changed(OBJECT *o, void *userdata)
{
  TOPLEVEL *toplevel = userdata;
  char *new_uuid;

  new_uuid = o_attrib_search_name_single_exact(o, "uuid", NULL);
  if ((o->uuid && new_uuid && strcmp(o->uuid, new_uuid) == 0) ||
      (!o->uuid && !new_uuid)) {
    /* UUID hasn't changed. */
  } else {
    gboolean was_registered = FALSE;

    if (o->uuid) {
      if (g_hash_table_lookup(toplevel->uuidmap, o->uuid) == o) {
	was_registered = TRUE;
	s_toplevel_unregister_object(toplevel, o);
      }
      g_free(o->uuid);
    }
    o->uuid = new_uuid;
    /* TODO: Emit a uuid-changed signal? */
    if (was_registered) {
      s_toplevel_register_object(toplevel, o);
    }
    s_toplevel_foreach_page(toplevel, &relink_symbols, toplevel);
  }
}

static void s_object_instance_init(GTypeInstance *instance,
				   gpointer g_class G_GNUC_UNUSED)
{
  OBJECT *o_current = GEDA_OBJECT(instance);

  s_basic_init_object(o_current);

  /* XXX ::type and ::name are not yet valid. */
}

/*! \brief Gets the OBJECT's GType.
 *
 *  \return The OBJECT's GType.
 */
GType s_object_get_type(void)
{
  static GType type = 0;

  if (type == 0) {
    static const GTypeInfo info = {
      .class_size = sizeof (GObjectClass),
      .class_init = &s_object_class_init,
      .instance_size = sizeof (OBJECT),
      .instance_init = &s_object_instance_init,
    };

    type = g_type_register_static(G_TYPE_OBJECT, "GEDA_OBJECT", &info, 0);
  }

  return type;
}

OBJECT *s_object_new(char kind, char const *name)
{
  return (g_object_new(GEDA_OBJECT_TYPE,
		       "basename", name,
		       "type", kind,
		       NULL));
}
/* vim: set sw=2: */
