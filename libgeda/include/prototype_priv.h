#ifndef H_LIBGEDA_PROTOTYPE_PRIV_H
#define H_LIBGEDA_PROTOTYPE_PRIV_H

#include <stdio.h>

#include <glib.h>
#include <libguile.h>

#include <libgeda/struct.h>

/* a_basic.c */
gchar *o_save_objects(OBJECT *object_list);
OBJECT *o_basic_read(TOPLEVEL *toplevel, char buf[], unsigned int release_ver, unsigned int fileformat_ver);

/* f_print.c */
void f_print_set_line_width(FILE *fp, int width);
void f_print_set_color(FILE *fp, int color);
void f_print_footer(FILE *fp);
void f_print_objects(TOPLEVEL *toplevel, FILE *fp, OBJECT *head, int start_x, int start_y, float scale, GArray *unicode_table);
int f_print_initialize_glyph_table(void);

/* g_rc.c */
int vstbl_lookup_str(const vstbl_entry *table, int size, const char *str);
int vstbl_get_val(const vstbl_entry *table, int index);
SCM g_rc_component_library(SCM path, SCM name);
SCM g_rc_component_library_command (SCM listcmd, SCM getcmd, SCM name);
SCM g_rc_component_library_funcs (SCM listfunc, SCM getfunc, SCM name);
SCM g_rc_component_library_search(SCM path);
SCM g_rc_source_library(SCM path);
SCM g_rc_source_library_search(SCM path);
SCM g_rc_world_size(SCM width, SCM height, SCM border);
SCM g_rc_reset_component_library(void);
SCM g_rc_reset_source_library(void);
SCM g_rc_untitled_name(SCM name);
SCM g_rc_font_directory(SCM path);
SCM g_rc_bitmap_directory(SCM path);
SCM g_rc_scheme_directory(SCM path);
SCM g_rc_bus_ripper_symname(SCM scmsymname);
SCM g_rc_postscript_prolog(SCM scmsymname);
SCM g_rc_map_font_character_to_file(SCM character_param, SCM file_param);
SCM g_rc_attribute_promotion(SCM mode);
SCM g_rc_promote_invisible(SCM mode);
SCM g_rc_keep_invisible(SCM mode);
SCM g_rc_always_promote_attributes(SCM scmsymname);

/* g_register.c */
void g_register_libgeda_funcs(void);
void g_register_libgeda_vars (void);

/* g_smob.c */
void g_init_attrib_smob(void);
SCM g_get_attrib_name_value(SCM attrib_smob);
SCM g_calculate_new_attrib_bounds(SCM attrib_smob, SCM scm_alignment, SCM scm_angle, SCM scm_x, SCM scm_y);
SCM g_get_attrib_bounds(SCM attrib_smob);
SCM g_get_attrib_angle(SCM attrib_smob);
SCM g_get_attrib_value_by_attrib_name(SCM object_smob, SCM scm_attrib_name);
SCM g_set_attrib_value_x(SCM attrib_smob, SCM scm_value);
SCM g_get_object_bounds (SCM object_smob, SCM scm_exclude_attribs, SCM scm_exclude_object_type);
SCM g_get_object_pins (SCM object_smob);
SCM g_get_pin_ends(SCM object);
SCM g_swap_pins(SCM component, SCM n1, SCM n2);
void g_init_object_smob(void);
SCM g_get_object_type(SCM object_smob);
SCM g_get_line_width(SCM object_smob);
void g_init_page_smob(void);
SCM g_get_page_filename(SCM page_smob);
SCM g_add_component(SCM page_smob, SCM scm_comp_name, SCM scm_x, SCM scm_y,
		    SCM scm_angle, SCM scm_selectable, SCM scm_mirror);
SCM g_get_objects_in_page(SCM page_smob);
void g_init_toplevel_smob(void);
SCM g_get_toplevel_from(SCM smob);
SCM g_get_toplevel_pages(SCM toplevel_smob);
SCM g_lookup_uuid(SCM toplevel_smob, SCM uuid_smob);

/* m_bounds.c */
void m_bounds_init(BOUNDS *bounds);
void m_bounds_of_points(BOUNDS *bounds, sPOINT points[], gint count);

/* m_hatch.c */
void m_hatch_box(OBJECT *box, gint angle, gint pitch, GArray *lines);
void m_hatch_circle(OBJECT *circle, gint angle, gint pitch, GArray *lines);
void m_hatch_polygon(GArray *points, gint angle, gint pitch, GArray *lines);
void m_hatch_print(GArray *lines, FILE *fp, int fill_width);

/* m_polygon.c */
gboolean m_polygon_interior_point(GArray *points, gint x, gint y);
gdouble m_polygon_shortest_distance(GArray *points, gint x, gint y, gboolean solid);

/* m_transform.c */
void m_transform_combine(TRANSFORM *result, TRANSFORM *a, TRANSFORM *b );
void m_transform_init(TRANSFORM *transform);
void m_transform_invert(TRANSFORM *transform, TRANSFORM *inverse);
void m_transform_point(TRANSFORM *transform, gint *x, gint *y);
void m_transform_points(TRANSFORM *transform, GArray *points);
void m_transform_rotate(TRANSFORM *transform, gdouble angle);
void m_transform_scale(TRANSFORM *transform, gdouble factor);
void m_transform_translate(TRANSFORM *transform, gdouble dx, gdouble dy);

/* o_arc_basic.c */
OBJECT *o_arc_read(TOPLEVEL *toplevel, char buf[], unsigned int release_ver, unsigned int fileformat_ver);
char *o_arc_save(OBJECT *object);
void o_arc_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current, double scale, GArray *unicode_table);
void o_arc_print_solid(TOPLEVEL *toplevel, FILE *fp, int x, int y, int radius, int angle1, int angle2, int color, int arc_width, int length, int space);
void o_arc_print_dotted(TOPLEVEL *toplevel, FILE *fp, int x, int y, int radius, int angle1, int angle2, int color, int arc_width, int length, int space);
void o_arc_print_dashed(TOPLEVEL *toplevel, FILE *fp, int x, int y, int radius, int angle1, int angle2, int color, int arc_width, int length, int space);
void o_arc_print_center(TOPLEVEL *toplevel, FILE *fp, int x, int y, int radius, int angle1, int angle2, int color, int arc_width, int length, int space);
void o_arc_print_phantom(TOPLEVEL *toplevel, FILE *fp, int x, int y, int radius, int angle1, int angle2, int color, int arc_width, int length, int space);
gdouble o_arc_shortest_distance(OBJECT const *o_current, gint x, gint y);
gboolean o_arc_within_sweep(ARC const *arc, gint x, gint y);
void world_get_arc_bounds(OBJECT *object, int *left, int *top, int *right, int *bottom);

/* o_attrib.c */
OBJECT *o_read_attribs(TOPLEVEL *toplevel,
		       OBJECT *object_to_get_attribs, 
		       TextBuffer *tb,
		       unsigned int release_ver, 
		       unsigned int fileformat_ver);
gchar *o_save_attribs(GList *attribs);

/* o_box_basic.c */
OBJECT *o_box_read(TOPLEVEL *toplevel, char buf[], unsigned int release_ver, unsigned int fileformat_ver);
char *o_box_save(OBJECT *object);
void o_box_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current, double scale, GArray *unicode_table);
void o_box_print_solid(TOPLEVEL *toplevel, FILE *fp, int x, int y, int width, int height, int color, int line_width, int length, int space);
void o_box_print_dotted(TOPLEVEL *toplevel, FILE *fp, int x, int y, int width, int height, int color, int line_width, int length, int space);
void o_box_print_dashed(TOPLEVEL *toplevel, FILE *fp, int x, int y, int width, int height, int color, int line_width, int length, int space);
void o_box_print_center(TOPLEVEL *toplevel, FILE *fp, int x, int y, int width, int height, int color, int line_width, int length, int space);
void o_box_print_phantom(TOPLEVEL *toplevel, FILE *fp, int x, int y, int width, int height, int color, int line_width, int length, int space);
void o_box_print_filled(TOPLEVEL *toplevel, OBJECT *o_current, FILE *fp, int x, int y, int width, int height, int color, int fill_width, int angle1, int pitch1, int angle2, int pitch2);
void o_box_print_mesh(TOPLEVEL *toplevel, OBJECT *o_current, FILE *fp, int x, int y, int width, int height, int color, int fill_width, int angle1, int pitch1, int angle2, int pitch2);
void o_box_print_hatch(TOPLEVEL *toplevel, OBJECT *o_current, FILE *fp, int x, int y, int width, int height, int color, int fill_width, int angle1, int pitch1, int angle2, int pitch2);
gdouble o_box_shortest_distance(OBJECT const *o_current, gint x, gint y);
void world_get_box_bounds(OBJECT *object, int *left, int *top, int *right, int *bottom);

/* o_bus_basic.c */
OBJECT *o_bus_read(TOPLEVEL *toplevel, char buf[], unsigned int release_ver, unsigned int fileformat_ver);
char *o_bus_save(OBJECT *object);
void o_bus_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current, double scale, GArray *unicode_table);
void world_get_bus_bounds(OBJECT *object, int *left, int *top, int *right, int *bottom);
void o_bus_recalc(OBJECT *o_current);

/* o_circle_basic.c */
OBJECT *o_circle_read(TOPLEVEL *toplevel, char buf[], unsigned int release_ver, unsigned int fileformat_ver);
char *o_circle_save(OBJECT *object);
void o_circle_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current, double scale, GArray *unicode_table);
void o_circle_print_solid(TOPLEVEL *toplevel, FILE *fp, int x, int y, int radius, int color, int circle_width, int length, int space);
void o_circle_print_dotted(TOPLEVEL *toplevel, FILE *fp, int x, int y, int radius, int color, int circle_width, int length, int space);
void o_circle_print_dashed(TOPLEVEL *toplevel, FILE *fp, int x, int y, int radius, int color, int circle_width, int length, int space);
void o_circle_print_center(TOPLEVEL *toplevel, FILE *fp, int x, int y, int radius, int color, int circle_width, int length, int space);
void o_circle_print_phantom(TOPLEVEL *toplevel, FILE *fp, int x, int y, int radius, int color, int circle_width, int length, int space);
void o_circle_print_filled(TOPLEVEL *toplevel, OBJECT *o_current, FILE *fp, int x, int y, int radius, int color, int fill_width, int angle1, int pitch1, int angle2, int pitch2);
void o_circle_print_mesh(TOPLEVEL *toplevel, OBJECT *o_current, FILE *fp, int x, int y, int radius, int color, int fill_width, int angle1, int pitch1, int angle2, int pitch2);
void o_circle_print_hatch(TOPLEVEL *toplevel, OBJECT *o_current, FILE *fp, int x, int y, int radius, int color, int fill_width, int angle1, int pitch1, int angle2, int pitch2);
gdouble o_circle_shortest_distance(OBJECT const *o_current, gint x, gint y);
void world_get_circle_bounds(OBJECT *object, int *left, int *top, int *right, int *bottom);

/* o_complex_basic.c */
OBJECT *o_complex_read(TOPLEVEL *toplevel, char buf[], unsigned int release_ver, unsigned int fileformat_ver);
char *o_complex_save(OBJECT *object);
gdouble o_complex_shortest_distance(OBJECT *o_current, gint x, gint y);

/* o_line_basic.c */
OBJECT *o_line_read(TOPLEVEL *toplevel, char buf[], unsigned int release_ver, unsigned int fileformat_ver);
char *o_line_save(OBJECT *object);
void o_line_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current, double scale, GArray *unicode_table);
void o_line_print_solid(TOPLEVEL *toplevel, FILE *fp, int x1, int y1, int x2, int y2, int color, int line_width, int length, int space);
void o_line_print_dotted(TOPLEVEL *toplevel, FILE *fp, int x1, int y1, int x2, int y2, int color, int line_width, int length, int space);
void o_line_print_dashed(TOPLEVEL *toplevel, FILE *fp, int x1, int y1, int x2, int y2, int color, int line_width, int length, int space);
void o_line_print_center(TOPLEVEL *toplevel, FILE *fp, int x1, int y1, int x2, int y2, int color, int line_width, int length, int space);
void o_line_print_phantom(TOPLEVEL *toplevel, FILE *fp, int x1, int y1, int x2, int y2, int color, int line_width, int length, int space);
gdouble o_line_shortest_distance_raw(int x1, int y1, int x2, int y2, gint x, gint y);
gdouble o_line_shortest_distance(OBJECT const *o_current, gint x, gint y);
void world_get_line_bounds(OBJECT *object, int *left, int *top, int *right, int *bottom);
void o_line_recalc(OBJECT *o_current);

/* o_net_basic.c */
OBJECT *o_net_read(TOPLEVEL *toplevel, char buf[], unsigned int release_ver, unsigned int fileformat_ver);
char *o_net_save(OBJECT *object);
void o_net_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current, double scale, GArray *unicode_table);
void world_get_net_bounds(OBJECT *object, int *left, int *top, int *right, int *bottom);
void o_net_recalc(OBJECT *o_current);

/* o_path_basic.c */
OBJECT *o_path_read(TOPLEVEL *toplevel, const char *first_line, TextBuffer *tb, unsigned int release_ver, unsigned int fileformat_ver);
char *o_path_save(OBJECT *object);
void o_path_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current, double scale, GArray *unicode_table);
gdouble o_path_shortest_distance(OBJECT const *object, gint x, gint y);
void world_get_path_bounds(OBJECT *object, int *left, int *top, int *right, int *bottom);


/* o_picture.c */
OBJECT *o_picture_read(TOPLEVEL *toplevel,
		       const char *first_line,
		       TextBuffer *tb,
		       unsigned int release_ver,
		       unsigned int fileformat_ver);
char *o_picture_save(OBJECT *object);
void o_picture_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current, double scale, GArray *unicode_table);
gdouble o_picture_shortest_distance(OBJECT const *o_current, gint x, gint y);
void world_get_picture_bounds(OBJECT *object, int *left, int *top, int *right, int *bottom);

/* o_pin_basic.c */
OBJECT *o_pin_read(TOPLEVEL *toplevel, char buf[], unsigned int release_ver, unsigned int fileformat_ver);
char *o_pin_save(OBJECT *object);
void o_pin_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current, double scale, GArray *unicode_table);
void world_get_pin_bounds(OBJECT *object, int *left, int *top, int *right, int *bottom);
void o_pin_recalc(OBJECT *o_current);

/* o_text_basic.c */
OBJECT *o_text_read(TOPLEVEL *toplevel,
		    const char *first_line,
		    TextBuffer *tb,
		    unsigned int release_ver,
		    unsigned int fileformat_ver);
char *o_text_save(OBJECT *object);
void o_text_print_text_string(FILE *fp, char *string, GArray *unicode_table);
void o_text_print(TOPLEVEL *toplevel, FILE *fp, OBJECT *o_current, double scale, GArray *unicode_table);
gdouble o_text_shortest_distance(OBJECT *o_current, gint x, gint y);
int world_get_text_bounds(OBJECT *o_current, int *left, int *top, int *right, int *bottom);

/* s_clib.c */
void s_clib_init (void);

/* s_color.c */
void s_color_init(void);
char *s_color_ps_string(int color);

/* s_encoding.c */
gchar* s_encoding_base64_encode (gchar* src, guint srclen, guint* dstlenp, gboolean strict);
gchar* s_encoding_base64_decode (gchar* src, guint srclen, guint* dstlenp);

/* s_object.c */
void s_object_attrib_attached(OBJECT *o, OBJECT *attr, void *userdata);
void s_object_attribs_changed(OBJECT *o, void *userdata);

/* s_path.c */
void s_path_to_polygon(PATH const *path, GArray *points);

/* s_textbuffer.c */
TextBuffer *s_textbuffer_new (gchar const *data, const gint size);
TextBuffer *s_textbuffer_free (TextBuffer *tb);
gchar *s_textbuffer_next (TextBuffer *tb, const gsize count);
gchar *s_textbuffer_next_line (TextBuffer *tb);
int s_textbuffer_line_number (TextBuffer *tb);

#endif
