/* gEDA - GPL Electronic Design Automation
 * s_page.h - GObject definitions for gEDA
 * Copyright (C) 2009 Bernd Jendrissek
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef H_O_PICTURE_H
#define H_O_PICTURE_H

#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include <libgeda/struct.h>

OBJECT *o_picture_new(TOPLEVEL *toplevel, GdkPixbuf *pixbuf,
                      gchar *file_content, gsize file_length, char *filename,
                      double ratio, char type,
                      int x1, int y1, int x2, int y2, int angle, char mirrored,
                      char embedded);
void o_picture_set(OBJECT *o_current, GdkPixbuf *pixbuf, char const *filename);
GdkPixbuf *o_picture_make_drawable(OBJECT *o_current, int dx, int dy);
GdkPixbuf *o_picture_original_pixbuf(OBJECT *o_current);
char *o_picture_filename(OBJECT *o_current);
double o_picture_ratio(OBJECT *o_current);
guint8 *o_picture_rgb_data(GdkPixbuf *image);
guint8 *o_picture_mask_data(GdkPixbuf *image);
GdkPixbuf *o_picture_pixbuf_from_buffer (gchar *file_content, gsize file_length, GError **err);

#endif
