#ifndef H_LIBGEDA_PROTOTYPE_H
#define H_LIBGEDA_PROTOTYPE_H

#include <stdio.h>

#include <glib.h>
#include <libguile.h>

#include <libgeda/struct.h>

/* TODO: #include "struct.h" when that is safe. */

/* a_basic.c */
const gchar *o_file_format_header();
gchar *o_save_buffer(TOPLEVEL *toplevel, PAGE *page);
int o_save(TOPLEVEL *toplevel, PAGE *page, const char *filename);
OBJECT *o_read_buffer(TOPLEVEL *toplevel, OBJECT *object_list, char const *buffer, const int size, const char *name);
OBJECT *o_read(TOPLEVEL *toplevel, OBJECT *object_list, char const *filename, GError **err);
void o_scale(OBJECT *list, int x_scale, int y_scale);

/* f_basic.c */
gchar *f_get_autosave_filename (const gchar *filename);
gboolean f_has_active_autosave (const gchar *filename, GError **err);
int f_open(TOPLEVEL *toplevel, PAGE *page, const gchar *filename, GError **err);
int f_open_flags(TOPLEVEL *toplevel, PAGE *page, const gchar *filename, const gint flags, GError **err);
int f_save(TOPLEVEL *toplevel, PAGE *page, const char *filename);
gchar *f_normalize_filename (const gchar *filename, GError **error);
char *follow_symlinks (const gchar *filename, GError **error);

/* f_print.c */
int f_print_file(TOPLEVEL *toplevel, PAGE *page, const char *filename);
int f_print_command(TOPLEVEL *toplevel, PAGE *page, const char *command);

/* g_basic.c */
gchar *g_strdup_scm_string(SCM scm_s);
SCM g_scm_eval_protected (SCM exp, SCM module_or_state);
SCM g_scm_eval_string_protected (SCM str);
SCM g_scm_c_eval_string_protected (const gchar *str);
SCM g_scm_apply_protected(SCM proc, SCM args, scm_t_catch_handler handler, void *userdata);
SCM g_scm_exception_witness(void *userdata, SCM key, SCM args);
SCM g_scm_safe_ref_lookup(char const *name);
SCM g_scm_file_get_contents(SCM f);
int g_read_file(const gchar *filename);

/* g_rc.c */
SCM g_rc_mode_general(SCM scmmode, const char *rc_name, int *mode_var, 
                      const vstbl_entry *table, int table_size);
gint g_rc_parse_general(TOPLEVEL *toplevel,
                        const gchar *fname, 
                        const gchar *ok_msg, const gchar *err_msg);
const char *g_rc_parse_path(void);
gint g_rc_parse_system_rc(TOPLEVEL *toplevel, const gchar *rcname);
gint g_rc_parse_home_rc(TOPLEVEL *toplevel, const gchar *rcname);
gint g_rc_parse_local_rc(TOPLEVEL *toplevel, const gchar *rcname);
void g_rc_parse(TOPLEVEL *toplevel, const gchar* rcname,
                const gchar* specified_rc_filename);
gint g_rc_parse_specified_rc(TOPLEVEL *toplevel, const gchar *rcfilename);

/* g_smob.c */
SCM g_make_attrib_smob(OBJECT *curr_attr);
SCM g_make_attrib_smob_list(OBJECT *object);
gboolean g_get_data_from_object_smob(SCM object_smob, TOPLEVEL **toplevel, 
				     OBJECT **object);
SCM g_make_object_smob(TOPLEVEL *curr_w, OBJECT *object);
SCM g_get_object_attributes(SCM object_smob);
SCM g_make_page_smob(TOPLEVEL *curr_w, PAGE *page);
gboolean g_get_data_from_page_smob(SCM object_smob, TOPLEVEL **toplevel, 
				   PAGE **object);
SCM g_make_toplevel_smob(TOPLEVEL *toplevel);
SCM g_debug_dump_page(SCM page_smob);

/* i_vars.c */
void i_vars_libgeda_set(TOPLEVEL *toplevel);
void i_vars_libgeda_freenames();

/* gdk-pixbuf-hacks.c */
#if 0
/* These two prototypes are in their own header to avoid pulling in GDK. */
GdkPixbuf *gdk_pixbuf_rotate (GdkPixbuf *pixbuf, guint angle);
GdkPixbuf *gdk_pixbuf_mirror_flip(GdkPixbuf *src, gint mirror, gint flip);
#endif

/* libgeda.c */
void libgeda_init(gboolean use_guile);
void libgeda_fini(void);

/* m_basic.c */
int snap_grid(TOPLEVEL *toplevel, int input);
int on_snap(int val);
int clip_nochange(PAGE const *page, int x1, int y1, int x2, int y2);
int visible(PAGE const *page, int wleft, int wtop, int wright, int wbottom);
void rotate_point(int x, int y, int angle, int *newx, int *newy);
void rotate_point_90(int x, int y, int angle, int *newx, int *newy);
void PAPERSIZEtoWORLD(int width, int height, int border, int *right, int *bottom);
double round_5_2_1(double unrounded);
double fact(int n);

/* m_hatch.c */
void m_hatch_path(PATH const *path, gint angle, gint pitch, GArray *lines);
GArray *m_hatch_new(void);
GList *m_hatch_get_lines(TOPLEVEL *toplevel, GArray *lines, int color);

/* m_polygon.c */
void m_polygon_append_bezier(GArray *points, BEZIER *bezier, gint segments);
void m_polygon_append_point(GArray *points, gint x, gint y);

/* m_rtree.c */
RTREE *m_rtree_new(int w_x1, int w_y1, int w_x2, int w_y2);
void m_rtree_add(RTREE *rtree, OBJECT *o);
GList *m_rtree_find(RTREE const *rtree, GList *retval, int w_x1, int w_y1, int w_x2, int w_y2);

/* o_arc_basic.c */
OBJECT *o_arc_new_at_xy(TOPLEVEL *toplevel, char type, int color, int x, int y);
OBJECT *o_arc_new(TOPLEVEL *toplevel, char type, int color, int x, int y, int radius, int start_angle, int end_angle);
void o_arc_translate_world(int dx, int dy, OBJECT *object);
void o_arc_rotate_world(int world_centerx, int world_centery, int angle, OBJECT *object);
void o_arc_mirror_world(int world_centerx, int world_centery, OBJECT *object);
int o_arc_get_radius(OBJECT const *o_current);
int o_arc_get_start_angle(OBJECT const *o_current);
int o_arc_get_end_angle(OBJECT const *o_current);
void o_arc_set_radius(OBJECT *o_current, int radius);
void o_arc_set_start_angle(OBJECT *o_current, int angle);
void o_arc_set_end_angle(OBJECT *o_current, int angle);

/* o_attrib.c */
OBJECT *o_attrib_search(GList *list, OBJECT const *item);
void o_attrib_add(TOPLEVEL *toplevel, OBJECT *object, OBJECT *item);
void o_attrib_attach(TOPLEVEL *toplevel, OBJECT *attrib, OBJECT *object);
void o_attrib_attach_list(TOPLEVEL *toplevel, GList *attr_list, OBJECT *object);
void o_attrib_detach_all(TOPLEVEL *toplevel, OBJECT *object);
void o_attrib_print(GList *attributes);
void o_attrib_remove(GList **list, OBJECT *remove);
gboolean o_attrib_get_name_value (const gchar *string, gchar **name_ptr, gchar **value_ptr);
char *o_attrib_search_object(OBJECT const *o, char const *name, int counter);
OBJECT *o_attrib_search_string_list(OBJECT *list, char const *string);
char *o_attrib_search_string_partial(OBJECT const *object, char const *search_for, int counter);
OBJECT *o_attrib_search_string_single(OBJECT *object, char const *search_for);
OBJECT *o_attrib_search_attrib_value(GList const *list, char const *value, char const *name, int counter);
char *o_attrib_search_attrib_name(GList const *list, char const *name, int counter);
char *o_attrib_search_toplevel(OBJECT const *list, char const *name, int counter);
char *o_attrib_search_name_single_exact(OBJECT *object, char const *name, OBJECT **return_found);
char *o_attrib_search_name_single(OBJECT *object, char const *name, OBJECT **return_found);
char *o_attrib_search_name_single_force(TOPLEVEL *w_current, OBJECT *o, char const *name, char *(*generator)(), void *context, OBJECT **return_found);
char *o_attrib_search_name_single_count(OBJECT const *object, char const *name, int counter);
char *o_attrib_search_slot(OBJECT *object, OBJECT **return_found);
char *o_attrib_search_numslots(OBJECT *object, OBJECT **return_found);
char *o_attrib_search_default_slot(OBJECT const *object);
OBJECT *o_attrib_search_pinseq(OBJECT *list, int pin_number);
char *o_attrib_search_slotdef(OBJECT const *object, int slotnumber);
char *o_attrib_search_component(OBJECT const *object, char const *name);
void o_attrib_init_uuid(TOPLEVEL *toplevel, OBJECT *o_current);
void o_attrib_fix_uuid(OBJECT *o_current);
void o_attrib_decay_uuid(OBJECT *o_current);
void o_attrib_slot_update(TOPLEVEL *toplevel, OBJECT *object);
void o_attrib_slot_copy(TOPLEVEL *toplevel, OBJECT *original, OBJECT *target);
char *o_attrib_search_toplevel_all(GedaPageList *page_list, char const *name);
OBJECT **o_attrib_return_attribs(OBJECT const *sel_object);
void o_attrib_free_returned(OBJECT **found_objects);
int o_attrib_populate_hash(GHashTable *attrtab, OBJECT const *o,
			   char const *improper_value,
			   gboolean (*resolver)(char const *key,
						char const *v1,
						char const *v2,
						gpointer userdata),
			   gpointer userdata);

/* o_basic.c */
int inside_region(int xmin, int ymin, int xmax, int ymax, int x, int y);
void o_recalc_single_object(OBJECT *o_current);
void o_set_line_options(OBJECT *o_current, OBJECT_END end, OBJECT_TYPE type, int width, int length, int space);
void o_set_fill_options(OBJECT *o_current, OBJECT_FILLING type, int width, int pitch1, int angle1, int pitch2, int angle2);
void o_translate_world(gint dx, gint dy, OBJECT *object);
void o_rotate_world(int world_centerx, int world_centery, int angle, OBJECT *object);
void o_mirror_world(int world_centerx, int world_centery, OBJECT *object);
gdouble o_shortest_distance(OBJECT *object, gint x, gint y);

/* o_box_basic.c */
OBJECT *o_box_new_at_xy(TOPLEVEL *toplevel, char type, int color, int x, int y);
OBJECT *o_box_new(TOPLEVEL *toplevel, char type, int color, int x1, int y1, int x2, int y2);
void o_box_translate_world(int dx, int dy, OBJECT *object);
void o_box_rotate_world(int world_centerx, int world_centery, int angle, OBJECT *object);
void o_box_mirror_world(int world_centerx, int world_centery, OBJECT *object);

/* o_bus_basic.c */
OBJECT *o_bus_new_at_xy(TOPLEVEL *toplevel, char type, int color, int x, int y);
OBJECT *o_bus_new(TOPLEVEL *toplevel, char type, int color, int x1, int y1, int x2, int y2, int bus_ripper_direction);
void o_bus_translate_world(int dx, int dy, OBJECT *object);
OBJECT *o_bus_copy(TOPLEVEL *toplevel, OBJECT *o_current);
void o_bus_rotate_world(int world_centerx, int world_centery, int angle, OBJECT *object);
void o_bus_mirror_world(int world_centerx, int world_centery, OBJECT *object);
void o_bus_modify(TOPLEVEL *toplevel, OBJECT *object, int x, int y, int whichone);

/* o_circle_basic.c */
int dist(int x1, int y1, int x2, int y2);
OBJECT *o_circle_new_at_xy(TOPLEVEL *toplevel, char type, int color, int x, int y);
OBJECT *o_circle_new(TOPLEVEL *toplevel, char type, int color, int x, int y, int radius);
void o_circle_translate_world(int dx, int dy, OBJECT *object);
int o_circle_get_radius(OBJECT const *o_current);
void o_circle_rotate_world(int world_centerx, int world_centery, int angle, OBJECT *object);
void o_circle_mirror_world(int world_centerx, int world_centery, OBJECT *object);

/* o_complex_basic.c */
int world_get_single_object_bounds(OBJECT *o_current,
			      int *rleft, int *rtop, 
			      int *rright, int *rbottom);
int world_get_object_list_bounds(OBJECT *complex, enum list_kind kind,
			    int *left, int *top, int *right, int *bottom);
int world_get_object_glist_bounds(GList const *o_list,
			     int *left, int *top, 
			     int *right, int *bottom);
OBJECT *new_head(TOPLEVEL *toplevel);
char *o_complex_get_refdes(OBJECT const *o_current, char const *default_value);
int o_complex_is_embedded(OBJECT const *o_current);
GList *o_complex_get_toplevel_attribs(OBJECT *o_head);
GList *o_complex_get_promotable (TOPLEVEL *toplevel, OBJECT *object, int detach);
void o_complex_promote_attribs(TOPLEVEL *toplevel, PAGE *page, OBJECT *object);
void o_complex_remove_promotable_attribs (TOPLEVEL *toplevel, OBJECT *object);
OBJECT *o_complex_new(TOPLEVEL *toplevel, char type, int color, int x, int y, int angle, int mirror, const CLibSymbol *clib_sym, const gchar *basename, int selectable);
OBJECT *o_complex_new_embedded(TOPLEVEL *toplevel, char type, int color, int x, int y, int angle, int mirror, const gchar *basename, int selectable);
void o_complex_translate_world(int dx, int dy, OBJECT *object);
void o_complex_set_color(OBJECT *prim_objs, int color);
void o_complex_set_color_save(OBJECT *complex, int color);
void o_complex_unset_color(OBJECT *complex);
void o_complex_set_saved_color_only(OBJECT *complex, int color);
void o_complex_rotate_world(int world_centerx, int world_centery, int angle, OBJECT *object);
void o_complex_mirror_world(int world_centerx, int world_centery, OBJECT *object);
OBJECT *o_complex_return_pin_object(OBJECT *object, char *pin);
void o_complex_check_symversion(TOPLEVEL *toplevel, OBJECT const *object);

/* o_embed.c */
void o_embed(TOPLEVEL *toplevel, OBJECT *o_current);
void o_unembed(TOPLEVEL *toplevel, OBJECT *o_current);

/* o_line_basic.c */
OBJECT *o_line_new_at_xy(TOPLEVEL *toplevel, char type, int color, int x, int y);
OBJECT *o_line_new(TOPLEVEL *toplevel, char type, int color, int x1, int y1, int x2, int y2);
OBJECT *o_line_copy(TOPLEVEL *toplevel, OBJECT *o_current);
void o_line_translate_world(int dx, int dy, OBJECT *object);
void o_line_grip_foreach(OBJECT *o,
			 gboolean (*fn)(OBJECT *o,
					int grip_x, int grip_y,
					enum grip_t whichone,
					void *userdata),
			 void *userdata);
int o_line_grip_move(OBJECT *o, int whichone, int x, int y);
void o_line_rotate_world(int world_centerx, int world_centery, int angle, OBJECT *object);
void o_line_mirror_world(int world_centerx, int world_centery, OBJECT *object);
void o_line_scale_world(int x_scale, int y_scale, OBJECT *object);
double o_line_length(OBJECT const *object);
gboolean o_line_subset_p(OBJECT const *o, int x, int y);

/* o_list.c */
OBJECT *o_list_copy_to(TOPLEVEL *toplevel, OBJECT *list_head, OBJECT *selected, int flag, OBJECT **return_end);
OBJECT *o_list_copy_all(TOPLEVEL *toplevel, OBJECT *src_list_head, OBJECT *dest_list_head, int flag);
GList *o_glist_copy_all_to_glist(TOPLEVEL *toplevel, GList const *src_list, GList *dest_list, int flag);
void o_glist_relink_objects(GList const *o_glist);
void o_list_delete_rest(TOPLEVEL *toplevel, OBJECT *list);
void o_list_translate_world(int dx, int dy, OBJECT *list);
void o_glist_translate_world(int dx, int dy, GList const *list);
void o_list_rotate_world(int x, int y, int angle, OBJECT *list);
void o_glist_rotate_world(int x, int y, int angle, GList const *list);
void o_list_mirror_world(int x, int y, OBJECT *list);
void o_glist_mirror_world(int x, int y, GList const *list);

/* o_net_basic.c */
OBJECT *o_net_new_at_xy(TOPLEVEL *toplevel, char type, int color, int x, int y);
OBJECT *o_net_new(TOPLEVEL *toplevel, char type, int color, int x1, int y1, int x2, int y2);
void o_net_translate_world(int dx, int dy, OBJECT *object);
OBJECT *o_net_copy(TOPLEVEL *toplevel, OBJECT *o_current);
void o_net_rotate_world(int world_centerx, int world_centery, int angle, OBJECT *object);
void o_net_mirror_world(int world_centerx, int world_centery, OBJECT *object);
int o_net_orientation(OBJECT *object);
OBJECT *o_net_normalize(TOPLEVEL *toplevel, OBJECT *o);
void o_net_consolidate_lowlevel(OBJECT *object, OBJECT *del_object, int orient);
int o_net_consolidate_nomidpoint(OBJECT *object, int x, int y);
int o_net_consolidate_segments(TOPLEVEL *toplevel, PAGE *page, OBJECT *object);
void o_net_consolidate(TOPLEVEL *toplevel, PAGE *page);
void o_net_modify(TOPLEVEL *toplevel, OBJECT *object, int x, int y, int whichone);

/* o_path_basic.c */
OBJECT *o_path_new(TOPLEVEL *toplevel, char type, int color, const char *path_string);
void o_path_translate_world(int x, int y, OBJECT *object);
void o_path_rotate_world(int world_centerx, int world_centery, int angle, OBJECT *object);
void o_path_mirror_world(int world_centerx, int world_centery, OBJECT *object);

/* o_picture.c */
/* More prototypes are in their own header to avoid pulling in GDK. */
void o_picture_modify(OBJECT *object, int x, int y, int whichone);
void o_picture_rotate_world(int world_centerx, int world_centery, int angle,OBJECT *object);
void o_picture_mirror_world(int world_centerx, int world_centery, OBJECT *object);
void o_picture_translate_world(int dx, int dy, OBJECT *object);
void o_picture_embed(OBJECT *object);

/* o_pin_basic.c */
OBJECT *o_pin_new_at_xy(TOPLEVEL *toplevel, char type, int color, int x, int y);
OBJECT *o_pin_new(TOPLEVEL *toplevel, char type, int color, int x1, int y1, int x2, int y2, int pin_type, int whichend);
void o_pin_translate_world(int dx, int dy, OBJECT *object);
OBJECT *o_pin_copy(TOPLEVEL *toplevel, OBJECT *o_current);
void o_pin_rotate_world(int world_centerx, int world_centery, int angle, OBJECT *object);
void o_pin_mirror_world(int world_centerx, int world_centery, OBJECT *object);
void o_pin_modify(TOPLEVEL *toplevel, OBJECT *object, int x, int y, int whichone);
void o_pin_update_whichend(TOPLEVEL *toplevel, OBJECT *object_list, int num_pins);

/* o_selection.c */
SELECTION *o_selection_new( void );
void o_selection_add(SELECTION *selection, OBJECT *o_selected);
void o_selection_print_all(const SELECTION *selection);
void o_selection_remove(SELECTION *selection, OBJECT *o_selected);
void o_selection_select(OBJECT *object); /* DEPRECATED */
void o_selection_unselect(OBJECT *object);          /* DEPRECATED */

/* o_text_basic.c */
OBJECT *o_text_new_head(TOPLEVEL *toplevel);
void o_text_init(void);
void o_text_print_set(void);
OBJECT *o_text_load_font(TOPLEVEL *toplevel, gunichar needed_char);
int o_text_num_lines(const char *string);
int o_text_height(OBJECT *obj, const char *string);
int o_text_width(TOPLEVEL *toplevel, OBJECT *obj, char const *string);
OBJECT *o_text_new(TOPLEVEL *toplevel, char type, int color, int x, int y, int alignment, int angle, const char *string, int size, int visibility, int show_name_value);
void o_text_set_info_font(char buf[]);
void o_text_translate_world(int dx, int dy, OBJECT *o_current);
void o_text_freeallfonts(TOPLEVEL *toplevel);
void o_text_rotate_world(int world_centerx, int world_centery, int angle, OBJECT *object);
void o_text_mirror_world(int world_centerx, int world_centery, OBJECT *object);
int o_text_display_p(OBJECT *o);
int o_text_printing_p(OBJECT *o);
void o_text_show_hidden(OBJECT *o, int show);
int o_text_show_name_value(OBJECT *o);
TOPLEVEL *o_text_get_toplevel(OBJECT *obj);
void o_text_get_xy(OBJECT const *obj, int *x, int *y);
void o_text_set_xy(OBJECT *obj, int x, int y);
int o_text_get_size(OBJECT const *obj);
void o_text_set_size(OBJECT *obj, int size);
int o_text_get_alignment(OBJECT const *obj);
void o_text_set_alignment(OBJECT *obj, int alignment);
int o_text_get_angle(OBJECT const *obj);
void o_text_set_angle(OBJECT *obj, int angle);
void o_text_set_string(OBJECT *obj, const gchar *new_string);
void o_text_take_string(OBJECT *obj, gchar *new_string);
const gchar *o_text_get_string(OBJECT const *obj);
void o_text_change(OBJECT *object, char const *string, int visibility, int show);

/* s_attrib.c */
int s_attrib_add_entry(char const *new_attrib);
void s_attrib_print(void);
int s_attrib_uniq(char const *name);
void s_attrib_free(void);
void s_attrib_init(void);
char *s_attrib_get(int counter);

/* s_basic.c */
void error_if_called(void);
void exit_if_null(void *ptr);
PAGE *safe_page_current(TOPLEVEL *toplevel);
OBJECT *return_tail(OBJECT *head);
OBJECT *return_head(OBJECT *tail);
OBJECT *s_basic_init_object(OBJECT *new_node);
OBJECT *s_basic_link_object(OBJECT *new_node, OBJECT *ptr);
OBJECT *s_basic_unlink_object(OBJECT *o_current);
void s_basic_splice(OBJECT *before, OBJECT *first, OBJECT *last);
char *s_describe_object(OBJECT *o, int detail);
void print_struct_forw(OBJECT const *ptr);
void print_struct(OBJECT const *ptr);
void s_delete_object(TOPLEVEL *toplevel, OBJECT *o_current);
OBJECT *s_delete(TOPLEVEL *toplevel, OBJECT *o_current);
void s_delete_list_fromstart(TOPLEVEL *toplevel, OBJECT *start);
void s_delete_object_glist(TOPLEVEL *toplevel, GList *list);
OBJECT *s_basic_copy(TOPLEVEL *toplevel, OBJECT *prototype);
gboolean s_basic_get_grip(OBJECT const *o, enum grip_t whichone, int *x, int *y);
int s_basic_search_grips_exact(OBJECT const *o, int x, int y);
GList *s_basic_get_all_grips(OBJECT const *o, GList *init);
int s_basic_move_grip(OBJECT *o, int whichone, int x, int y);
void s_basic_grip_foreach_helper(OBJECT *o,
				 const GRIP *grips,
				 gboolean (*fn)(OBJECT *o,
						int grip_x, int grip_y,
						enum grip_t whichone,
						void *userdata),
				 void *userdata);
enum visit_result s_visit(OBJECT *object, enum visit_result (*fn)(OBJECT *, void *), void *context, enum visit_order order, int depth);
enum visit_result s_visit_object(OBJECT *o_current, enum visit_result (*fn)(OBJECT *, void *), void *context, enum visit_order order, int depth);
enum visit_result s_visit_list(OBJECT *o_current, enum list_kind kind, enum visit_result (*fn)(OBJECT *, void *), void *context, enum visit_order order, int depth);
enum visit_result s_visit_page(PAGE const *page, enum visit_result (*fn)(OBJECT *, void *), void *context, enum visit_order order, int depth);
enum visit_result s_visit_toplevel(TOPLEVEL const *toplevel, enum visit_result (*fn)(OBJECT *, void *), void *context, enum visit_order order, int depth);
char *remove_nl(char *string);
char *remove_last_nl(char *string);
gchar *s_expand_env_variables (const gchar *string);

/* s_clib.c */
void s_clib_free (void);
GList *s_clib_get_sources (const gboolean sorted);
const CLibSource *s_clib_get_source_by_name (const gchar *name);
void s_clib_refresh ();
const CLibSource *s_clib_add_directory (const gchar *directory, 
					const gchar *name);
const CLibSource *s_clib_add_command (const gchar *list_cmd,
                                      const gchar *get_cmd,
				      const gchar *name);
const CLibSource *s_clib_add_scm(SCM listfunc, SCM getfunc, SCM name_scm);
const CLibSource *s_clib_add_memory(char const *buf, char const *name);
const gchar *s_clib_source_get_name (const CLibSource *source);
GList *s_clib_source_get_symbols (const CLibSource *source);
const gchar *s_clib_symbol_get_name (const CLibSymbol *symbol);
gchar *s_clib_symbol_get_filename (const CLibSymbol *symbol);
const CLibSource *s_clib_symbol_get_source (const CLibSymbol *symbol);
gchar *s_clib_symbol_get_data (const CLibSymbol *symbol);
GList *s_clib_search (const gchar *pattern, const CLibSearchMode mode);
void s_clib_flush_search_cache ();
void s_clib_flush_symbol_cache ();
const CLibSymbol *s_clib_get_symbol_by_name (const gchar *name);
gchar *s_clib_symbol_get_data_by_name (const gchar *name);
GList *s_toplevel_get_symbols (const TOPLEVEL *toplevel);

/* s_color.c */
int s_color_request(int color_index, char const *color_name, char const *outline_color_name, char const *ps_color_string);
void s_color_destroy_all(void);

/* s_conn.c */
gboolean s_conn_connectible_p(OBJECT const *object);
CONN *s_conn_return_new(OBJECT *other_object, int type, int x, int y, int whichone, int other_whichone);
int s_conn_uniq(GList const *conn_list, CONN const *input_conn);
int s_conn_remove_other(OBJECT *other_object, OBJECT *to_remove);
void s_conn_remove(OBJECT *to_remove);
void s_conn_remove_complex(OBJECT *to_remove);
void s_conn_update_object(PAGE *page, OBJECT *object);
void s_conn_update_complex(PAGE *page, OBJECT *complex);
void s_conn_print(GList const *conn_list);
GList *s_conn_net_search(OBJECT const *new_net, int whichone, GList const *conn_list);
GList *s_conn_return_others(GList *input_list, OBJECT const *object);
GList *s_conn_return_complex_others(GList *input_list, OBJECT const *object);

/* s_cue.c */
void s_cue_postscript_fillbox(TOPLEVEL *toplevel, FILE *fp, int x, int y);
void s_cue_postscript_fillcircle(TOPLEVEL *toplevel, FILE *fp, int x, int y, int size_flag);
void s_cue_output_all(TOPLEVEL *toplevel, OBJECT *head, FILE *fp, int type);
void s_cue_output_lowlevel(TOPLEVEL *toplevel, OBJECT *object, int whichone, FILE *fp, int output_type);
void s_cue_output_lowlevel_midpoints(TOPLEVEL *toplevel, OBJECT *object, FILE *fp, int output_type);
void s_cue_output_single(TOPLEVEL *toplevel, OBJECT *object, FILE *fp, int type);

/* s_factory.c */
void s_factory_derive(struct st_factory *specialization, struct st_factory const *base);

/* s_hierarchy.c */
int s_hierarchy_down_schematic_single(TOPLEVEL *toplevel, const gchar *filename, PAGE *parent, int page_control, int flag);
void s_hierarchy_down_symbol (TOPLEVEL *toplevel, const CLibSymbol *symbol, PAGE *parent);
PAGE *s_hierarchy_find_up_page(GedaPageList *page_list, PAGE *current_page);
void s_hierarchy_replace_page(TOPLEVEL *toplevel, PAGE *p_old, PAGE *p_new);
GList* s_hierarchy_traversepages(TOPLEVEL *toplevel, PAGE *page, gint flags);
gint s_hierarchy_print_page(PAGE *p_current, void * data);
PAGE *s_hierarchy_find_prev_page(GedaPageList *page_list, PAGE *current_page);
PAGE *s_hierarchy_find_next_page(GedaPageList *page_list, PAGE *current_page);

/* s_log.c */
void s_log_init (const gchar *filename);
void s_log_close (void);
gchar *s_log_read (void);

/* s_menu.c */
int s_menu_return_num(void);
SCM s_menu_return_entry(int index, char **menu_name);
int s_menu_add_entry(char const *new_menu, SCM menu_items);
void s_menu_print(void);
void s_menu_free(void);
void s_menu_init(void);

/* s_page.c */
PAGE *s_page_new (TOPLEVEL *toplevel, const gchar *filename);
void s_page_add_place_list(PAGE *page, GList *objects);
GList const *s_page_borrow_place_list(PAGE const *page);
void s_page_replace_place_list(TOPLEVEL *toplevel, PAGE *page, GList *new_list);
GList *s_page_forget_place_list(PAGE *page, gboolean return_old);
void s_page_delete (TOPLEVEL *toplevel, PAGE *page);
void s_page_delete_list(TOPLEVEL *toplevel);
void s_page_goto(PAGE *p_new);
PAGE *s_page_search (TOPLEVEL *toplevel, const gchar *filename);
void s_page_print_all(TOPLEVEL const *toplevel);
gboolean s_page_save(TOPLEVEL *toplevel, PAGE *page, char *new_filename);
gint s_page_save_all (TOPLEVEL *toplevel);
void s_page_autosave_init(TOPLEVEL *toplevel);
gint s_page_autosave (TOPLEVEL *toplevel);
void s_page_append(PAGE *page, OBJECT *object);
GList *s_page_objects (PAGE *page);

/* s_papersizes.c */
int s_papersizes_add_entry(char const *new_papersize, int width, int height);
int s_papersizes_uniq(char const *name);
void s_papersizes_free(void);
char *s_papersizes_get(int counter);
void s_papersizes_get_size(char const *string, int *width, int *height);

/* s_path.c */
PATH *s_path_parse (const char *path_str);
char *s_path_string_from_path(const PATH *path);
void s_path_moveto(PATH *path, double x, double y);
void s_path_lineto(PATH *path, double x, double y);
void s_path_curveto(PATH *path, double x1, double y1, double x2, double y2, double x3, double y3);
void s_path_art_finish(PATH *path);
void s_path_mutate(PATH *path, PATH_CODE new_code);

/* s_project.c */
TOPLEVEL *s_toplevel_new (void);
OBJECT *s_toplevel_new_object(TOPLEVEL *toplevel, char type, char const *prefix);
void s_toplevel_goto_page(TOPLEVEL *toplevel, PAGE *p_new);
void s_toplevel_foreach_page(TOPLEVEL const *toplevel, void (*fn)(PAGE *page, void *userdata), void *userdata);
void s_toplevel_register_object(TOPLEVEL *toplevel, OBJECT *o_current);
void s_toplevel_unregister_object(TOPLEVEL *toplevel, OBJECT *o_current);

/* s_slib.c */
int s_slib_add_entry(char const *new_path);
int s_slib_search_for_dirname(char const *dir_name);
char *s_slib_search_dirs(const char *basename);
char *s_slib_search_single(const char *basename);
void s_slib_free(void);
void s_slib_init(void);
char *s_slib_getdir(int index);
char *s_slib_getfiles(char const *directory, int flag);
void s_slib_print(void);
int s_slib_uniq(char *path);
void s_slib_print_dirs(void);

/* s_slot.c */
OBJECT *s_slot_read(TOPLEVEL *toplevel, char const *buf);
char *s_slot_save(OBJECT *object);
gboolean s_slot_compatible(OBJECT const *o, OBJECT const *slot);
gboolean s_slot_available(OBJECT const *o, OBJECT const *slot);
void s_slot_rewrite_symbols(OBJECT *component);
int s_slot_link(TOPLEVEL *toplevel, OBJECT *o, OBJECT *symbol);
OBJECT *s_slot_unlink(OBJECT *o);
void s_slot_reset_attribs(OBJECT *o);
void s_slot_setup_complex(TOPLEVEL *toplevel, OBJECT *o);
void s_slot_init(OBJECT *o);
void s_slot_destroy_complex(TOPLEVEL *toplevel, OBJECT *o);
void s_slot_reparent_from_attribs(TOPLEVEL *toplevel, OBJECT *symbol);
int s_slot_reparent_specific_slot(TOPLEVEL *toplevel, OBJECT *symbol, OBJECT *slot_object);
void s_slot_make_links(TOPLEVEL *toplevel, PAGE *page);
OBJECT *s_slot_get_occupant(OBJECT const *slot_object);
OBJECT *s_slot_get_owner(OBJECT const *slot_object);

/* s_stretch.c */
STRETCH *s_stretch_return_tail(STRETCH *head);
STRETCH *s_stretch_new_head(void);
STRETCH *s_stretch_add(STRETCH *head, OBJECT *object, CONN *connection, int whichone);
void s_stretch_remove(STRETCH *head, OBJECT *object);
void s_stretch_remove_most(STRETCH *head);
void s_stretch_print_all(STRETCH const *head);
void s_stretch_destroy_all(STRETCH *head);

/* s_undo.c */
GList *s_undo_add(TOPLEVEL *toplevel, PAGE *page, int type, char *filename, OBJECT *object_head, int left, int top, int right, int bottom, int page_control, PAGE *up_page);
void s_undo_print_all(PAGE *page);
void s_undo_trim(TOPLEVEL *toplevel, PAGE *page, int levels);
int s_undo_levels(PAGE const *page);
void o_undo_copy_prev_memento(GList *link, enum undo_type_t undo_type);
void s_undo_init(PAGE *p_current);
void s_undo_free_all(TOPLEVEL *toplevel, PAGE *p_current);

/* u_basic.c */
char *u_basic_breakup_string(char const *string, char delimiter, int count);
char *u_basic_split(char **s, char delimiter);
void u_basic_populate_hash(GHashTable *tab, char *s,
			   char comma, char equals);
char *u_basic_make_uuid(void);
int u_basic_casecmp(char const *s1, char const *s2);

#endif
