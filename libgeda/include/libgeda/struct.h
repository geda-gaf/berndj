/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's Library
 * Copyright (C) 1998-2004 Ales V. Hvezda
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02111-1301 USA.
 */

#ifndef STRUCT_H
#define STRUCT_H

#include <glib.h>  /* Include needed to make GList work. */
#include <glib-object.h>
#include "defines.h"
#include "o_types.h"

struct st_factory;

/* Wrappers around a new list mechanism */
typedef struct _GedaList SELECTION;
typedef struct _GedaList GedaPageList;

/* gschem structures (gschem) */
typedef struct st_complex COMPLEX;
typedef struct st_slot SLOT;
typedef struct st_line LINE;
typedef struct st_path_section PATH_SECTION;
typedef struct st_path PATH;
typedef struct st_circle CIRCLE;
typedef struct st_arc ARC;
typedef struct st_box BOX;
typedef struct st_picture PICTURE;
typedef struct st_text TEXT;
typedef struct st_point sPOINT;
typedef struct st_transform TRANSFORM;
typedef struct st_bezier BEZIER;

typedef struct st_object OBJECT;
typedef struct st_page PAGE;
typedef struct st_toplevel TOPLEVEL;
typedef struct st_color COLOR;
typedef struct st_undo UNDO;
typedef struct st_bounds BOUNDS;

typedef struct st_conn CONN;
typedef struct st_bus_ripper BUS_RIPPER;

/* Used when you move objects and you want the nets/pins to stretch */
typedef struct st_stretch STRETCH;

/* netlist structures (gnetlist) */
typedef struct st_netlist NETLIST;
typedef struct st_cpinlist CPINLIST;
typedef struct st_net NET;

/* sch check structures (gschcheck) */
typedef struct st_schcheck SCHCHECK;
typedef struct st_chkerrs CHKERRS;

/* Managed text buffers */
typedef struct _TextBuffer TextBuffer;

/* Component library objects */
typedef struct _CLibSource CLibSource;
typedef struct _CLibSymbol CLibSymbol;

/* Component library search modes */
typedef enum { CLIB_EXACT=0, CLIB_GLOB } CLibSearchMode;

/* f_open behaviour flags.  See documentation for f_open_flags() in
   f_basic.c. */
typedef enum { F_OPEN_RC           = 1,
               F_OPEN_CHECK_BACKUP = 2,
               F_OPEN_RESTORE_CWD  = 4,
} FOpenFlags;

/* PB : change begin */
/* PB : these enum are constant to define :
   - the end of open line of an object ;
   - the type of the line of an object ;
   - the filling of a closed object. */
/* PB : used in struct st_object (predefined type OBJECT)*/
typedef enum {END_NONE, END_SQUARE, END_ROUND} OBJECT_END;
typedef enum {TYPE_SOLID, TYPE_DOTTED, TYPE_DASHED, TYPE_CENTER, TYPE_PHANTOM, TYPE_ERASE} OBJECT_TYPE;
typedef enum {FILLING_HOLLOW, FILLING_FILL, FILLING_MESH, FILLING_HATCH, FILLING_VOID} OBJECT_FILLING;
/* PB : change end */

/*! \brief */
typedef struct st_halfspace HALFSPACE;

typedef struct st_rtree RTREE;

/*! \brief */
struct st_halfspace {
  int left; /* these are booleans */
  int top;
  int right;
  int bottom;
};

struct st_point {
  gint x;
  gint y;
};

typedef enum {
    PATH_MOVETO,
    PATH_MOVETO_OPEN,
    PATH_CURVETO,
    PATH_LINETO,
    PATH_END
} PATH_CODE;

struct st_path_section {
  PATH_CODE code;
  int x1;
  int y1;
  int x2;
  int y2;
  int x3;
  int y3;
};

struct st_path {
  PATH_SECTION *sections; /* Bezier path segments  */
  int num_sections;       /* Number with data      */
  int num_sections_max;   /* Number allocated      */
};

struct st_bezier {
  gint x[4];
  gint y[4];
};

struct st_complex {
  int x, y;		/* world origin */

  int angle;				/* orientation, only multiples
                                         * of 90 degrees allowed */   
  /* in degrees */
  int mirror;

  gboolean is_embedded;

  OBJECT *prim_objs;			/* Primitive objects */
  /* objects which make up the */
  /* complex */

  GHashTable *slots;
};

struct st_slot {
  OBJECT *owner;
  GHashTable *function_to_vpad;
  OBJECT *symbol;
  OBJECT *obj; /* The OBJECT read from file.  TODO: Should be const. */
  char *name;
};

struct st_object {
  GObject parent;

  int type;				/* Basic information */
  int sid;
  char *name;
  GHashTable *vpad_to_pad;
  GHashTable *pad_to_pin;
  OBJECT *owning_slot;
  char *uuid;

  int w_top;				/* Bounding box information */
  int w_left;				/* in world coords */
  int w_right;
  int w_bottom;
  gboolean w_bounds_valid;

  COMPLEX *complex;
  SLOT *slot;
  LINE *line; 
  CIRCLE *circle; 
  ARC *arc;
  BOX *box;
  TEXT *text;
  PICTURE *picture;
  PATH *path;
  void *type_state;

  GList *conn_list;			/* List of connections */
  /* to and from this object */

  /* PB : change begin */
  /* PB : every graphical primitive have more or less the same options. */
  /* PB : depending on its nature a primitive is concerned with one or more */
  /* PB : of these fields. If not, value must be ignored. */
  OBJECT_END line_end;
  OBJECT_TYPE line_type;
  int line_width;
  int line_space;
  int line_length;

  OBJECT_FILLING fill_type;
  int fill_width;
  int fill_angle1, fill_pitch1;
  int fill_angle2, fill_pitch2;
  /* PB : change end */	

  int visited;                          /* used in gnetlist for traversal purposes */

  gchar *complex_basename;              /* Component Library Symbol name */
  OBJECT *complex_parent;		/* Complex parent object pointer */
  /* used only in complex head nodes */

  /* Copy constructor. */
  OBJECT *(*copy_func)(TOPLEVEL *toplevel, OBJECT *prototype);

  void (*bounds_recalc_func)(OBJECT *);
  void (*draw_func)();			/* Draw function */
  void (*psprint_func)(TOPLEVEL *toplevel, FILE *fp, OBJECT *o, double scale,
		       GArray *unicode_table);
  void (*embed_func)(TOPLEVEL *toplevel, OBJECT *o, char yes);
  /*! \brief Traverse an object with a client function
   *  \par Function Description
   *  Traverse an object and call the client-supplied function \a fn on each
   *  of the constituent objects.
   *
   *  \param [in] o_current The object to traverse.
   *  \param [in] fn        The client supplied function to call.
   *  \param [in] context   Arbitrary pointer to pass to \a fn.
   *  \param [in] order     The order in which to traverse sub-objects.
   *  \param [in] depth     The maximum depth of sub-objects to traverse.
   *
   *  \return A traversal result.
   */
  enum visit_result (*visit_func)(OBJECT *o_current,
				  enum visit_result (*fn)(OBJECT *, void *),
				  void *context,
				  enum visit_order order, int depth);
  /*! \brief An accumulator for object grips.
   *  \param [in] o   The object whose grips we want to traverse.
   *  \param [in] fn  A pointer to a function that receives grips from the object.
   *  \note This method should not change \a o itself; only \a fn may change it.
   */
  void (*grip_foreach_func)(OBJECT *o,
			    gboolean (*fn)(OBJECT *o,
					   int grip_x, int grip_y,
					   enum grip_t whichone,
					   void *userdata),
			    void *userdata);
  /* Called by s_basic_move_grip(). */
  int (*grip_move_func)(OBJECT *o, int whichone, int x, int y);
  void (*destroy_func)(OBJECT *o);

  int color; 				/* Which color */
  int saved_color; 			/* Saved color */
  int dont_redraw;			/* Flag to skip redrawing */
  int selectable;
  int selected;				/* object selected flag */
  int locked_color; 			/* Locked color (used to save */
  /* the object's real color */
  /* when the object is locked) */

  int draw_grips;				/* if selected, enables 
						   drawing of grips */

  /* controls which direction bus rippers go */
  /* it is either 0 for un-inited, */
  /* 1 for right, -1 for left (horizontal bus) */
  /* 1 for up, -1 for down (vertical bus) */
  int bus_ripper_direction;             /* only valid on buses */


  int font_text_size;			/* used only with fonts defs */
  OBJECT *font_prim_objs;			/* used only with fonts defs */

  int whichend;    /* for pins only, either 0 or 1 */
  int pin_type;    /* for pins only, either NET or BUS */

  GList *attribs;       /* attribute stuff */

  OBJECT *attached_to;  /* when object is an attribute */
  OBJECT *copied_to;    /* used when copying attributes */

  OBJECT *prev;
  OBJECT *next;
}; 


/*! \brief Structure for connections between OBJECTs
 *
 * The st_conn structure contains a single connection
 * to another object.
 * The connection system in s_conn.c uses this struct
 */
struct st_conn {
  /*! \brief The "other" object connected to this one */
  OBJECT *other_object;
  /*! \brief type of connection. Always in reference to how the "other"
    object is connected to the current one */
  int type;
  /*! \brief x coord of the connection position */
  int x;
  /*! \brief y coord of the connection position */
  int y;		
  /*! \brief which endpoint of the current object caused this connection */
  int whichone;
  /*! \brief which endpoint of the "other" object caused this connection */
  int other_whichone;
};

/* this structure is used in gschem to add rippers when drawing nets */
/* it is never stored in any object, it is only temporary */
struct st_bus_ripper
{
  int x[2];
  int y[2];
};

struct st_stretch
{
  OBJECT *object;
  CONN *connection;

  int whichone;

  STRETCH *prev;
  STRETCH *next;
};

struct st_bounds {
  gint min_x;
  gint min_y;
  gint max_x;
  gint max_y;
};

/** A structure to store a 2D affine transform.
 *
 *  The transforms get stored in a 3x3 matrix. Code assumes the bottom row to
 *  remain constant at [0 0 1].
 */
struct st_transform {
  gdouble m[2][3];    /* m[row][column] */
};

enum undo_type_t {
  UNDO_DISK,
  UNDO_MEMORY
};

struct st_undo {

  /* one of these is used, depending on if you are doing in-memory */
  /* or file based undo state saving */	
  char *filename;
  OBJECT *object_head;

  /* either UNDO_ALL or UNDO_VIEWPORT_ONLY */
  int type;

  /* viewport information */
  int left, top, right, bottom;

  /* up and down the hierarchy */
  PAGE *up_page;
  /* used to control which pages are viewable when moving around */
  int page_control;
};

struct st_page {
  GObject parent;

  OBJECT *object_head;
  OBJECT *object_tail;
  SELECTION *selection_list; /* new selection mechanism */
  struct {
    GList *place_list;
  } page_private;
  OBJECT *object_lastfound;
  STRETCH *stretch_head; 
  STRETCH *stretch_tail; 

  char *page_filename; 
  int CHANGED;			/* changed flag */
  /*int zoom_factor; no longer used*/
  int left, right, top, bottom;		/* World coord limits */

  float to_screen_x_constant;
  float to_screen_y_constant;
  int width, height;

  float to_world_x_constant;
  float to_world_y_constant;

  /* Undo/Redo Stacks and pointers */	
  /* needs to go into page mechanism actually */
  GList *undo_current;
  GList *undo_newest;

  /* up and down the hierarchy */
  PAGE *up_page;
  /* int down; not needed */

  /* used to control which pages are viewable when moving around */
  int page_control;

  /* backup variables */
  GTimeVal last_load_or_save_time;
  char saved_since_first_loaded;
  gint ops_since_last_backup;
  gchar do_autosave_backup;

  /* Function which asks the user whether to load a newer backup file */
  int (*load_newer_backup_func)();

};

struct st_toplevel {
  GObject parent;

  struct st_factory *factory;

  GHashTable *uuidmap;

  /* have to decided on component list stuff */
  /* if it should go in here or not */
  /* leave outside for now */

  GList *RC_list;                       /* List of RC files which have been read in. */

  char *untitled_name;			/* untitled sch basename */
  char *font_directory; 		/* path of the vector fonts */
  char *scheme_directory; 		/* path of the scheme scripts */
  char *bitmap_directory; 		/* path of the bitmaps */

  int init_left, init_right; 		/* Starting values for above */
  int init_top, init_bottom; 

  int snap;				/* Snap on/off*/

  int override_color;			/* used in doing selections */

  /* misc flags */
  int ADDING_SEL;

  /* page system */
  PAGE *page_current;
  GedaPageList *pages;

  int show_hidden_default;              /* Whether to show new invisible objects. */

  GList* major_changed_refdes;          /* A list of all refdes's that have */
                                        /* major symbol version changes */

  /* backup variables */
  int auto_save_interval;
  gint auto_save_timeout;

  /* used by math funcs for the snapping */
  int snap_size;		

  /* BLOCK SET IN GSCHEM, BUT USED IN LIBGEDA - NEEDS A RETHINK */
  int attribute_color;
  int detachedattr_color;
  int background_color;
  int net_endpoint_color;
  int junction_color;
  int override_net_color;
  int override_bus_color;
  int override_pin_color;
  int pin_style;
  int net_style;
  int bus_style;
  int line_style;
  /* END BLOCK - ALTHOUGH THERE ARE MORE CASES! */

  /* controls how text is printed (vector / PS font) */ 
  int text_output; 

  /* either landscape or portrait */
  int print_orientation; 

  /* either TRUE or FALSE (color or no color) */
  int image_color; 

  /* either TRUE or FALSE (color or no color) */
  int print_color; 

  /* color used color ouput for background */ 
  int print_color_background;

  /* setpagedevice orientation option enable (TRUE or FALSE) */
  int setpagedevice_orientation;

  /* setpagedevice pagesize option enable (TRUE or FALSE) */
  int setpagedevice_pagesize;

  /* The name of the prolog file to paste into the Postscript output */
  char *postscript_prolog;

  /* Use this as a scaling factor for the output font */
  float postscript_font_scale;

  /* controls if the net consolidation code is used */ 
  int net_consolidate; 

  /*controls if attribute promotion happens */
  int attribute_promotion; 

  /* controls if invisible attribs are promoted */
  int promote_invisible; 

  /* controls if invisible attribs are kept and not deleted */
  int keep_invisible;   

  /* either window or limits */
  int print_output_type;

  /* BUTT, ROUND, SQUARE caps */
  int print_output_capstyle;		

  /* landscape printing only */
  int paper_width, paper_height;

  /* filename of the bus ripper component if set above */
  char *bus_ripper_symname;

  /* controls if the whole bounding box is used in the auto whichend code */
  int force_boundingbox;

  /* controls the threshold (in lines of text) when the multi-line text */
  /* output font is forced to vector */
  int print_vector_threshold;

  /* List of attributes to always promote */
  GList *always_promote_attributes;

  /* gnetlist specific */
  int net_naming_priority;
  int hierarchy_traversal;
  int hierarchy_uref_mangle;
  int hierarchy_netname_mangle;
  int hierarchy_netattrib_mangle;
  char *hierarchy_uref_separator;
  char *hierarchy_netname_separator;
  char *hierarchy_netattrib_separator;
  int hierarchy_netattrib_order;
  int hierarchy_netname_order;
  int hierarchy_uref_order;
  char *unnamed_netname;
};

/* structures below are for gnetlist */

/* for every component in the object database */
struct st_netlist {

  int nlid;

  char *component_uref;

  OBJECT *object_ptr;

  CPINLIST *cpins;		

  char *hierarchy_tag;
  int composite_component;

  NETLIST *prev;
  NETLIST *next;
};


/* for every pin on a component */
struct st_cpinlist {
  int plid;

  char *pin_number;
  char *net_name;			/* this is resolved at very end */
  char *pin_label;

  NET *nets;

  CPINLIST *prev;
  CPINLIST *next;
};

/* the net run connected to a pin */
struct st_net {

  int nid;

  int net_name_has_priority;
  char *net_name;
  char *pin_label;

  char *connected_to; /* new to replace above */

  NET *prev;
  NET *next;
};

/* By Jamil Khatib */
/* typedef struct st_chkerrs CHKERRS; */

/* Schem check struct */
struct st_schcheck {
  int no_errors;                /* No of Errors */
  int no_warnings;              /* No of Warinings */

  CHKERRS * sheet_errs;

  CHKERRS *float_nets;           /* Header of the list of floating nets */
  int net_errs;                 /* No of floating nets */

  OBJECT *float_pins;           /* Header of List of floating pins*/
  int pin_errs;                 /* No of floating pins */

  int net_names;                /* No of mismatched net names */
};


struct st_chkerrs{

  OBJECT * err_obj;
  CHKERRS * next;

};



struct st_attrib_smob {
  OBJECT   *attribute;
};

/* used by the rc loading mechanisms */
typedef struct {
  int   m_val;
  char *m_str;
} vstbl_entry;

typedef struct {
  int x;
  int y;
  enum grip_t whichone;
} GRIP;

#endif
