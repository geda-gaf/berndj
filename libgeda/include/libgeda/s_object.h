/* gEDA - GPL Electronic Design Automation
 * s_object.h - GObject definitions for gEDA
 * Copyright (C) 2009 Bernd Jendrissek
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef H_LIBGEDA_OBJECT_H
#define H_LIBGEDA_OBJECT_H

#include <glib-object.h>

#include <libgeda/struct.h>

#define GEDA_OBJECT_TYPE (s_object_get_type())
#define GEDA_OBJECT(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), GEDA_OBJECT_TYPE, OBJECT))
#define GEDA_OBJECT_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST((cls), GEDA_OBJECT_TYPE, GObjectClass))
#define GEDA_IS_OBJECT(obj) (G_TYPE_INSTANCE_TYPE((obj), GEDA_OBJECT_TYPE))
#define GEDA_IS_OBJECT_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE((cls), GEDA_OBJECT_TYPE))
#define GEDA_OBJECT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), GEDA_OBJECT_TYPE, GObjectClass))

/* Class metaobject structure is identical to GObjectClass. */

/* Instance structure located in struct.h */

GType s_object_get_type(void);
OBJECT *s_object_new(char kind, char const *name);

#endif
