/* gEDA - GPL Electronic Design Automation
 * s_page.h - GObject definitions for gEDA
 * Copyright (C) 2009 Bernd Jendrissek
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef H_S_COLOR_H
#define H_S_COLOR_H

#include <gdk/gdk.h>

struct st_color {
  char *color_name;
  char *outline_color_name;
  char *ps_color_string;

  GdkColor *gdk_color;
  GdkColor *gdk_outline_color;
};

#endif
