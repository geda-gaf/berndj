/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's library
 * Copyright (C) 1998-2008 Ales V. Hvezda
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef H_LIBGEDA_S_TOPLEVEL_H
#define H_LIBGEDA_S_TOPLEVEL_H

#include <glib-object.h>

#define S_TOPLEVEL_TYPE (s_toplevel_get_type())
#define S_TOPLEVEL(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), S_TOPLEVEL_TYPE, TOPLEVEL))
#define S_TOPLEVEL_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST((cls), S_TOPLEVEL_TYPE, TOPLEVEL_CLASS))
#define S_IS_TOPLEVEL(obj) (G_TYPE_INSTANCE_TYPE((obj), S_TOPLEVEL_TYPE))
#define S_IS_TOPLEVEL_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE((cls), S_TOPLEVEL_TYPE))
#define S_TOPLEVEL_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), S_TOPLEVEL_TYPE, TOPLEVEL_CLASS))

typedef struct st_toplevel_class TOPLEVEL_CLASS;

struct st_toplevel_class {
  GObjectClass parent_class;
};

/* Instance structure located in struct.h */

GType s_toplevel_get_type(void);

#endif
