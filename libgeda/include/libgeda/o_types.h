/* gEDA - GPL Electronic Design Automation
 * libgeda - gEDA's Library
 * Copyright (C) 1998-2004 Ales V. Hvezda
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02111-1301 USA.
 */

#ifndef _O_TYPES_H_INCL
#define _O_TYPES_H_INCL

/* Object types */
/* Object types are also available in scheme */
/* If there is any addition, add them also in g_register.c
   (function g_register_libgeda_vars) and in s_factory.c
   (function s_factory_new_object). */
#define OBJ_INVALID	'!'
#define OBJ_HEAD       	(-1)
#define OBJ_LINE        'L'
#define OBJ_PATH        'H'
#define OBJ_BOX         'B'
#define OBJ_PICTURE     'G'
#define OBJ_CIRCLE      'V'
#define OBJ_NET         'N'
#define OBJ_BUS         'U'
#define OBJ_COMPLEX     'C'
#define OBJ_SLOT        'S'
#define OBJ_TEXT        'T'
#define OBJ_PIN         'P'
#define OBJ_ARC         'A' 
#define OBJ_PLACEHOLDER 'X'  /* added 1.19.2005 by SDB to prevent
			      * deletion of unfound symbol files */
#define OBJ_EXTENSION   'E' /* For non-libgeda objects. */


#define STARTATTACH_ATTR	'{'	
#define ENDATTACH_ATTR		'}'	
#define START_EMBEDDED		'['	
#define END_EMBEDDED		']'	

/* font stuff */
#define INFO_FONT         'F' 
#define VERSION_CHAR      'v' 

/* misc stuff */
#define COMMENT         '#' 

enum grip_t {
  GRIP_1 = 0, /* XXX - We need this for compatibility with foo[LINE_END1]. */
  GRIP_2 = 1, /* XXX - We need this for compatibility with foo[LINE_END2]. */
  GRIP_UPPER_LEFT,
  GRIP_UPPER_RIGHT,
  GRIP_LOWER_LEFT,
  GRIP_LOWER_RIGHT,
  GRIP_ARC_CENTER,
  GRIP_ARC_RADIUS,
  GRIP_CIRCLE_CENTER,
  GRIP_CIRCLE_RADIUS,
  GRIP_CIRCLE_RADIUS_LEFT,
  GRIP_CIRCLE_RADIUS_RIGHT,
  GRIP_CIRCLE_RADIUS_TOP,
  GRIP_CIRCLE_RADIUS_BOTTOM,
  GRIP_PATH_NEXT_SECTION,
  GRIP_PATH_LAST_SECTION,
  GRIP_PATH_CONTROL_1,
  GRIP_PATH_CONTROL_2,
  GRIP_START_ANGLE,
  GRIP_END_ANGLE,
  GRIP_COMPLEX_ORIGIN,
  GRIP_TEXT_ORIGIN,

  /* Special value for the first available arbitrary-integer grip constant. */
  GRIP_FIRST_OPAQUE,

  /* Special value that cannot conflict with cookie-based objects. */
  GRIP_NONE = -1
};

#endif
