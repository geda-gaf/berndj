/* gEDA - GPL Electronic Design Automation
 * factory.h - Abstract Factory pattern for libgeda objects
 * Copyright (C) 2007, 2008 Bernd Jendrissek
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef H_LIBGEDA_FACTORY_H
#define H_LIBGEDA_FACTORY_H

#include "struct.h"

struct st_factory {
  OBJECT *(*new_object)(struct st_factory *factory, char kind, char const *name);
  PAGE *(*new_page)(struct st_factory *factory, TOPLEVEL *toplevel, gchar const *filename);
};

/* Libgeda knows how to instantiate all its base classes. */
extern struct st_factory libgeda_factory;

/* vim: set sw=2: */
#endif
