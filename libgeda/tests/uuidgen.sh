#!/bin/sh

if test "x$srcdir" = x; then
  srcdir=.
fi

uuids=`mktemp -t ugXXXXXX`
for i in 1 2 3 4 5 6 7 8 9 10; do
  $srcdir/uuidgen >>$uuids
done

lines=`sort -u $uuids |wc -l`
test $lines = 10
status=$?

rm -f uuids

exit $status
