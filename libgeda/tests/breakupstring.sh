#!/bin/sh

if test "x$srcdir" = x; then
  srcdir=.
fi

expected=`mktemp -t bsXXXXXX`
input=`mktemp -t bsXXXXXX`

cat >$input <<EOF
slotrewrite=refdes, type
slotmap=  output=1,inv=2,  noninv=3
EOF

cat >$expected <<EOF
refdes, type
refdes
type
  output=1,inv=2,  noninv=3
output=1
inv=2
noninv=3
EOF

sed -e 's/^[^=]*=//' <$input |tr '\n' '\0' |xargs -r0 $srcdir/breakupstring |diff $expected -

status=$?

rm -f $expected $input

exit $status
