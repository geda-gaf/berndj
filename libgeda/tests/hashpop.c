#include "config.h"

#include <glib.h>
#include <libguile.h>
#include <stdio.h>
#include "defines.h"
#include "struct.h"
#include "prototype.h"

void hashprinter(gpointer key, gpointer value, gpointer user_data)
{
  printf("key=%s, value=%s\n", (char *) key, (char *) value);
}

int main(int argc, char *argv[])
{
  char *pairs = "foo=bar,test=hashpop,answer=42";
  GHashTable *tab;

  if (argc > 1) {
    pairs = argv[1];
  }

  tab = g_hash_table_new_full(&g_str_hash, &g_str_equal, &g_free, &g_free);

  /* u_basic_populate_hash() needs to modify its string argument. */
  pairs = g_strdup(pairs);

  u_basic_populate_hash(tab, pairs, ',', '=');
  g_hash_table_foreach(tab, &hashprinter, NULL);

  g_free(pairs);

  return 0;
}
