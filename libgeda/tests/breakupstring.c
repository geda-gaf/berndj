#include "config.h"

#include <glib.h>
#include <libguile.h>
#include <stdio.h>

#include "defines.h"
#include "struct.h"
#include "prototype.h"

int main(int argc, char *argv[])
{
	int i, argno;
	char *quux;

	for (argno = 1; argno < argc; argno++) {
		quux = g_strdup(argv[argno]);
		i = 0;

		do {
			printf("%s\n", quux);
			g_free(quux);
			quux = u_basic_breakup_string(argv[argno], ',', i++);
		} while (quux);
	}

	return 0;
}
