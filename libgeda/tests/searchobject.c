#include "config.h"

#include <glib.h>
#include <stdio.h>
#include <string.h>
#include "libgeda_priv.h"

char const *symbol_data[] = {
  "v 20031231 2",
  "L 300 0 200 200 3 0 0 0 -1 -1",
  "T 300 400 5 10 0 0 0 0 1",
  "device=RESISTOR",
  "P 900 100 750 100 1 0 0",
  "{",
  "T 800 150 5 8 0 1 0 0 1",
  "pinnumber=2",
  "}",
  "P 0 100 152 100 1 0 0",
  "{",
  "T 100 150 5 8 0 1 0 0 1",
  "pinnumber=1",
  "}",
  "L 201 200 150 100 3 0 0 0 -1 -1",
  "T 200 300 8 10 0 1 0 0 1",
  "refdes=R?",
  "T 0 0 8 10 0 1 0 0 1",
  "pins=2",
  "", NULL
};

int main()
{
  TOPLEVEL *toplevel;
  PAGE *page;
  OBJECT *o;
  CLibSymbol *clib;
  char *symbol_buf, *attrib_value;

  libgeda_init(FALSE);

  symbol_buf = g_strjoinv("\n", symbol_data);
  s_clib_add_memory(symbol_buf, "resistor");

  toplevel = s_toplevel_new();
  page = s_page_new(toplevel, "test");
  s_toplevel_goto_page(toplevel, page);

  clib = s_clib_get_symbol_by_name("resistor");

  o = o_complex_new(toplevel, 'C', WHITE, 100, 100, 0, 0, clib, "foo", 1);
  g_free(symbol_buf);

  attrib_value = o_attrib_search_object(o, "pinnumber", 0);
  if (!attrib_value || strcmp(attrib_value, "2") != 0) {
    printf("pinnumber[0] = %s\n", attrib_value ? attrib_value : "NULL");
    return 1;
  }
  g_free(attrib_value);

  attrib_value = o_attrib_search_object(o, "pinnumber", 2);
  if (!attrib_value || strcmp(attrib_value, "1") != 0) {
    printf("pinnumber[1] = %s\n", attrib_value ? attrib_value : "NULL");
    return 1;
  }
  g_free(attrib_value);

  attrib_value = o_attrib_search_object(o, "pinnumber", 4);
  if (attrib_value) {
    printf("pinnumber[2] = %s\n", attrib_value ? attrib_value : "NULL");
    g_free(attrib_value);
    return 1;
  }

  return 0;
}
