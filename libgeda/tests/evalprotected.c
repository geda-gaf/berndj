#include "config.h"

#include <glib.h>
#include <stdio.h>
#include <string.h>
#include "libgeda_priv.h"

void main_prog(void *userdata, int argc, char *argv[])
{
  TOPLEVEL *toplevel;
  SCM x;

  libgeda_init(TRUE);

  toplevel = s_toplevel_new();

  s_log_init("/dev/null");

  /* Check that we get the right return value. */
  x = g_scm_c_eval_string_protected("(+ 17 42)");
  if (!scm_is_integer(x) || scm_to_int(x) != (17 + 42)) {
    exit(1);
  }

  /* Evaluate just a number as an s-expr rather than a string. */
  x = g_scm_eval_protected(scm_from_int(42), scm_current_module());
  if (!scm_is_integer(x) || scm_to_int(x) != 42) {
    exit(1);
  }

  /* Force an exception. */
  x = g_scm_eval_protected(scm_list_3(scm_from_locale_symbol("throw"),
				      scm_list_2(scm_from_locale_symbol("quote"),
						 scm_from_locale_symbol("pro-forma")),
				      scm_from_int(17)),
			   scm_current_module());
  if (!scm_is_false(x)) {
    exit(1);
  }

  exit(0);
}

int main(int argc, char *argv[])
{
  scm_boot_guile(argc, argv, &main_prog, NULL);

  /* scm_boot_guile doesn't return! */
  return 1;
}
