#include "config.h"

#include <glib.h>
#include <stdio.h>
#include <string.h>
#include "libgeda_priv.h"

/* Omit TEXT objects as that requires fonts. */
char const *grips_test_data[] = {
  "v 20080706 2",
  "N 45800 47300 46400 47300 4",
  "L 46500 46600 46500 47200 3 0 0 0 -1 -1",
  "U 46400 46500 45800 46500 10 0",
  "P 45700 46600 45700 47200 1 0 0",
  "{",
  "}",
  "V 46100 46900 300 3 0 0 0 -1 -1 0 -1 -1 -1 -1 -1",
  "A 46100 46900 200 135 225 3 0 0 0 -1 -1",
  "B 45500 46300 1100 1700 3 0 0 0 -1 -1 0 -1 -1 -1 -1 -1",
  "H 3 0 0 0 -1 -1 0 -1 -1 -1 -1 -1 7",
  "M 45800,47400",
  "L 45800,47500",
  "L 45900,47600",
  "L 46200,47600",
  "L 46300,47700",
  "L 46300,47800",
  "z",
  "", NULL
};

const GRIP all_grips[] = {
  { .x = 45800, .y = 47300, .whichone = GRIP_1 },
  { .x = 46400, .y = 47300, .whichone = GRIP_2 },
  { .x = 46500, .y = 46600, .whichone = GRIP_1 },
  { .x = 46500, .y = 47200, .whichone = GRIP_2 },
  { .x = 46400, .y = 46500, .whichone = GRIP_1 },
  { .x = 45800, .y = 46500, .whichone = GRIP_2 },
  { .x = 45700, .y = 46600, .whichone = GRIP_1 },
  { .x = 45700, .y = 47200, .whichone = GRIP_2 },
  { .x = 46100, .y = 46900, .whichone = GRIP_CIRCLE_CENTER },
  { .x = 46400, .y = 46600, .whichone = GRIP_CIRCLE_RADIUS },
  { .x = 45800, .y = 46900, .whichone = GRIP_CIRCLE_RADIUS_LEFT },
  { .x = 46400, .y = 46900, .whichone = GRIP_CIRCLE_RADIUS_RIGHT },
  { .x = 46100, .y = 47200, .whichone = GRIP_CIRCLE_RADIUS_TOP },
  { .x = 46100, .y = 46600, .whichone = GRIP_CIRCLE_RADIUS_BOTTOM },
  { .x = 46100, .y = 46900, .whichone = GRIP_ARC_CENTER },
  { .x = 46100, .y = 46900, .whichone = GRIP_ARC_RADIUS },
  { .x = 45958, .y = 47041, .whichone = GRIP_START_ANGLE },
  { .x = 46300, .y = 46900, .whichone = GRIP_END_ANGLE },
  { .x = 45500, .y = 48000, .whichone = GRIP_UPPER_LEFT },
  { .x = 46600, .y = 46300, .whichone = GRIP_LOWER_RIGHT },
  { .x = 46600, .y = 48000, .whichone = GRIP_UPPER_RIGHT },
  { .x = 45500, .y = 46300, .whichone = GRIP_LOWER_LEFT },
  { .x = 45800, .y = 47400, .whichone = GRIP_FIRST_OPAQUE + 0*3 + 2 },
  { .x = 45800, .y = 47500, .whichone = GRIP_FIRST_OPAQUE + 1*3 + 2 },
  { .x = 45900, .y = 47600, .whichone = GRIP_FIRST_OPAQUE + 2*3 + 2 },
  { .x = 46200, .y = 47600, .whichone = GRIP_FIRST_OPAQUE + 3*3 + 2 },
  { .x = 46300, .y = 47700, .whichone = GRIP_FIRST_OPAQUE + 4*3 + 2 },
  { .x = 46300, .y = 47800, .whichone = GRIP_FIRST_OPAQUE + 5*3 + 2 },
  { .x = 0, .y = 0, .whichone = GRIP_NONE } /* Sentinel. */
};

static enum visit_result update_object(OBJECT *o, void *userdata)
{
  PAGE *page = userdata;

  /* This would also be a place to emit page signals. */
  s_conn_update_object(page, o);

  return VISIT_RES_OK;
}

static enum visit_result get_grip(OBJECT *o, void *userdata)
{
  GList **l = userdata;

  *l = s_basic_get_all_grips(o, *l);

  return VISIT_RES_OK;
}

int main()
{
  TOPLEVEL *toplevel;
  PAGE *page;
  OBJECT *o_current;
  GList *grips = NULL;
  GList *cursor;
  GRIP *g;
  char *buf;
  int i;

  libgeda_init(FALSE);

  toplevel = s_toplevel_new();
  page = s_page_new(toplevel, "test");
  s_toplevel_goto_page(toplevel, page);

  buf = g_strjoinv("\n", grips_test_data);
  o_read_buffer(toplevel, toplevel->page_current->object_tail,
		buf, strlen(buf), "test");
  g_free(buf);

  s_visit_page(page, &update_object, page, VISIT_LINEAR, 1);
  s_visit_page(page, &get_grip, &grips, VISIT_LINEAR, 1);

  grips = g_list_reverse(grips);

  for (cursor = grips, i = 0; cursor; cursor = cursor->next, i++) {
    g = cursor->data;
    if (all_grips[i].whichone == GRIP_NONE) {
      /* More grips than expected! */
      return 1;
    }

    if (all_grips[i].whichone != g->whichone ||
	all_grips[i].x != g->x ||
	all_grips[i].y != g->y) {
      /* Grip data is wrong. */
      return 1;
    }
  }

  if (all_grips[i].whichone != GRIP_NONE) {
    /* Not enough grips from the objects. */
    return 1;
  }

  return 0;
}
