#include "config.h"

#include <dejagnu.h>
#include <glib.h>
#include <stdio.h>
#include <string.h>
#include "libgeda_priv.h"

char const *rtree_test_data[] = {
  "v 20080706 2",
  "L 40500 40500 40500 50500 3 0 0 0 -1 -1",
  "L 56500 40500 56500 50500 3 0 0 0 -1 -1",
  "B 41000 40500 15000 10000 3 0 0 0 -1 -1 0 -1 -1 -1 -1 -1",
  "B 41500 40900 7500 9100 3 0 0 0 -1 -1 0 -1 -1 -1 -1 -1",
  "B 42000 45000 13500 4500 3 0 0 0 -1 -1 0 -1 -1 -1 -1 -1",
  "B 49500 44000 5000 5000 3 0 0 0 -1 -1 0 -1 -1 -1 -1 -1",
  "B 50000 43500 5000 1000 3 0 0 0 -1 -1 0 -1 -1 -1 -1 -1",
  "", NULL
};

static enum visit_result rtree_add(OBJECT *o_current, void *context)
{
  RTREE *rtree = context;

  m_rtree_add(rtree, o_current);

  return VISIT_RES_OK;
}

int main()
{
  TOPLEVEL *toplevel;
  PAGE *page;
  RTREE *rtree;
  GList *found_objects;
  char *buf;

  libgeda_init(FALSE);

  toplevel = s_toplevel_new();
  page = s_page_new(toplevel, "test");
  s_toplevel_goto_page(toplevel, page);

  buf = g_strjoinv("\n", rtree_test_data);
  o_read_buffer(toplevel, toplevel->page_current->object_tail,
		buf, strlen(buf), "test");
  g_free(buf);

  rtree = m_rtree_new(0, 0, G_MAXINT, G_MAXINT);
  s_visit_page(toplevel->page_current, rtree_add, rtree, VISIT_ANY, 1);

  found_objects = m_rtree_find(rtree, NULL, 0, 0, G_MAXINT, G_MAXINT);
  if (g_list_length(found_objects) == 7) {
    /* Assume that all 7 are right. */
    pass("rtree find-all");
  } else {
    GList *iter;
    for (iter = found_objects; iter != NULL; iter = iter->next) {
      OBJECT *o_current = iter->data;
      switch (o_current->type) {
	char *representation;
      case OBJ_LINE:
	representation = o_line_save(o_current);
	printf("%s\n", representation);
	break;
      case OBJ_BOX:
	representation = o_box_save(o_current);
	printf("%s\n", representation);
	break;
      default:
	printf("%c ...\n", o_current->type);
	break;
      }
    }
    fail("rtree find-all (%d)", g_list_length(found_objects));
  }
  g_list_free(found_objects);

  found_objects = m_rtree_find(rtree, NULL, 40500, 40500, 40500, 50500);
  if (g_list_length(found_objects) == 1) {
    OBJECT *o_current = found_objects->data;
    if (o_current->type == OBJ_LINE) {
      char *representation = o_line_save(o_current);
      if (strcmp(representation, rtree_test_data[1]) == 0) {
	pass("rtree find-line");
      } else {
	fail("rtree find-line (details)");
      }
      g_free(representation);
    } else {
      fail("rtree find-line (type)");
    }
  } else {
    fail("rtree find-line (number)");
  }
  g_list_free(found_objects);

  found_objects = m_rtree_find(rtree, NULL, 41000, 40500, 49500, 50500);
  if (g_list_length(found_objects) == 1) {
    OBJECT *o_current = found_objects->data;
    if (o_current->type == OBJ_BOX) {
      char *representation = o_box_save(o_current);
      if (strcmp(representation, rtree_test_data[4]) == 0) {
	pass("rtree find-box-simple");
      } else {
	fail("rtree find-box-simple (details)");
      }
      g_free(representation);
    } else {
      fail("rtree find-box-simple (type)");
    }
  } else {
    fail("rtree find-box-simple (number)");
  }
  g_list_free(found_objects);

  return failed ? 1 : 0;
}
