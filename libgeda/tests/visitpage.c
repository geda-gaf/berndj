#include "config.h"

#include <glib.h>
#include <stdio.h>
#include <string.h>
#include "libgeda_priv.h"

char const *test_page[] = {
  "v 20080706 2",
  "C 46400 48000 1 270 0 EMBEDDEDopamp.sym",
  "[",
  "P 600 1000 600 800 1 0 0",
  "{",
  "T 900 500 8 10 1 1 0 0 1",
  "foo=bar",
  "}",
  "P 600 200 600 0 1 0 1",
  "{",
  "}",
  "V 500 501 315 3 0 0 0 -1 -1 0 -1 -1 -1 -1 -1",
  "L 600 200 400 400 3 0 0 0 -1 -1",
  "L 600 800 400 600 3 0 0 0 -1 -1",
  "L 400 700 400 300 3 0 0 0 -1 -1",
  "L 400 500 184 500 3 0 0 0 -1 -1",
  "L 600 200 564 272 3 0 0 0 -1 -1",
  "L 600 200 528 236 3 0 0 0 -1 -1",
  "L 528 236 564 272 3 0 0 0 -1 -1",
  "]",
  "", NULL
};
char prim_objs[] = "CPTPVLLLLLLL*";

enum visit_result
summarize_object(OBJECT *o_current, void *context)
{
  char **control = context;

  switch (**control) {
  case 'f':
    /* Failure is idempotent. */
    break;
  case '*':
    /* Visiting more objects than expected. */
    **control = 'f';
    break;
  default:
    if (o_current->type == **control) {
      /* Skip to next object. */
      *(*control)++ = 'p';
    } else {
      /* Mark failure, and don't skip past the test. */
      **control = 'f';
    }
    break;
  }

  return VISIT_RES_OK;
}

int main()
{
  TOPLEVEL *toplevel;
  PAGE *page;
  char *buf;
  char *cursor = prim_objs;

  libgeda_init(FALSE);

  toplevel = s_toplevel_new();
  toplevel->font_directory = "../../symbols/font";
  page = s_page_new(toplevel, "test");
  s_toplevel_goto_page(toplevel, page);

  buf = g_strjoinv("\n", test_page);
  o_read_buffer(toplevel, page->object_tail, buf, strlen(buf), "test");
  g_free(buf);

  s_visit_page(page, &summarize_object, &cursor, VISIT_LINEAR, 2);

  return(!!strcmp(prim_objs, "pppppppppppp*"));
}
