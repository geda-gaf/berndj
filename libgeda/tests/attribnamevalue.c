#include "config.h"

#include <glib.h>
#include <libguile.h>
#include <stdio.h>

#include "defines.h"
#include "struct.h"
#include "prototype.h"

int main(int argc, char *argv[])
{
	int i, argno;
	char *name, *value;

	for (i = 1; i < argc; i++) {
		if (o_attrib_get_name_value(argv[i], &name, &value)) {
			printf("%s=%s\n", name, value);
			g_free(name);
			g_free(value);
		} else {
			printf("NO_ATTRIB\n");
		}
	}

	return 0;
}
