#!/bin/sh

if test "x$srcdir" = x; then
  srcdir=.
fi

expected=`mktemp -t hpXXXXXX`
cat >$expected <<EOF
key=answer, value=42
key=foo, value=bar
key=test, value=hashpop
EOF

$srcdir/hashpop |sort |diff $expected -
status=$?

rm -f $expected

exit $status
