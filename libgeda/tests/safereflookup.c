#include "config.h"

#include <glib.h>
#include <stdio.h>
#include <string.h>
#include "libgeda_priv.h"

void main_prog(void *userdata, int argc, char *argv[])
{
  TOPLEVEL *toplevel;
  SCM magic_value, gensym, x;
  int exception_caught = 0;
  char *undefined_name;

  libgeda_init(TRUE);

  s_log_init("/dev/null");

  toplevel = s_toplevel_new();

  magic_value = scm_from_locale_symbol("magic");
  scm_c_define("magic-value", magic_value);

  /* Check that we can look up a known variable. */
  x = g_scm_safe_ref_lookup("magic-value");
  if (!scm_is_symbol(x) || !scm_is_eq(x, magic_value)) {
    exit(1);
  }

  /* Generate a quasi-unique name that won't be bound. */
  gensym = scm_gensym(scm_from_locale_string(" safereflookup"));
  undefined_name = scm_to_locale_string(scm_symbol_to_string(gensym));

  /* Force an exception. */
  x = g_scm_safe_ref_lookup(undefined_name);
  free(undefined_name);
  if (!scm_is_eq(x, SCM_UNDEFINED)) {
    exit(1);
  }

  exit(0);
}

int main(int argc, char *argv[])
{
  scm_boot_guile(argc, argv, &main_prog, NULL);

  /* scm_boot_guile doesn't return! */
  return 1;
}
