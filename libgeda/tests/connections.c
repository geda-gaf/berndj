#include "config.h"

#include <glib.h>
#include <stdio.h>
#include <string.h>
#include "libgeda_priv.h"

char const *connections_test_data[] = {
  "v 20080706 2",
  "N 48200 45600 49300 45600 4",
  "N 49300 45600 49300 46300 4",
  "", NULL
};

static enum visit_result update_object(OBJECT *o, void *userdata)
{
  PAGE *page = userdata;

  /* This would also be a place to emit page signals. */
  s_conn_update_object(page, o);

  return VISIT_RES_OK;
}

int main()
{
  TOPLEVEL *toplevel;
  PAGE *page;
  OBJECT *o_current, *net1, *net2;
  CONN *conn;
  char *buf;

  libgeda_init(FALSE);

  toplevel = s_toplevel_new();
  page = s_page_new(toplevel, "test");
  s_toplevel_goto_page(toplevel, page);

  buf = g_strjoinv("\n", connections_test_data);
  o_read_buffer(toplevel, toplevel->page_current->object_tail,
		buf, strlen(buf), "test");
  g_free(buf);

  s_visit_page(page, &update_object, page, VISIT_ANY, 1);

  /* Basic sanity checks. */
  o_current = page->object_head->next;
  if (o_current->type != OBJ_NET) {
    return 1;
  }
  if (!o_current->next || o_current->next->type != OBJ_NET) {
    return 1;
  }
  net1 = o_current;
  net2 = o_current->next;

  /* The real meat of this test: nets should be connected at first. */

  if (!net1->conn_list || g_list_length(net1->conn_list) != 1) {
    return 1;
  }

  conn = net1->conn_list->data;
  if (conn->other_object != net2 ||
      conn->type != CONN_ENDPOINT ||
      conn->x != 49300 || conn->y != 45600 ||
      conn->whichone != GRIP_2 ||
      conn->other_whichone != GRIP_1) {
    return 1;
  }

  if (!net2->conn_list || g_list_length(net2->conn_list) != 1) {
    return 1;
  }
  conn = net2->conn_list->data;
  if (conn->other_object != net1 ||
      conn->type != CONN_ENDPOINT ||
      conn->x != 49300 || conn->y != 45600 ||
      conn->whichone != GRIP_1 ||
      conn->other_whichone != GRIP_2) {
    return 1;
  }

  /* And then the connection must break. */
  s_basic_move_grip(net1, GRIP_2, 49200, 45600);
  s_conn_update_object(page, net1);

  if (net1->conn_list || net2->conn_list) {
    return 1;
  }

  return 0;
}
