#include "config.h"

#include <glib.h>
#include <libguile.h>
#include <stdio.h>
#include "defines.h"
#include "struct.h"
#include "prototype.h"

int main()
{
  char *uuid;

  uuid = u_basic_make_uuid();
  if (uuid == NULL) {
    return 1;
  }

  printf("%s\n", uuid);

  g_free(uuid);

  return 0;
}
