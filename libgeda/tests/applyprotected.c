#include "config.h"

#include <glib.h>
#include <stdio.h>
#include <string.h>
#include "libgeda_priv.h"

SCM magic_return;

SCM fail_if_called(void *userdata, SCM key, SCM args)
{
  exit(1);
}

/* XXX Don't replace with g_scm_exception_witness; testing return value too. */
SCM mark_if_called(void *userdata, SCM key, SCM args)
{
  int *witness = userdata;

  *witness = 1;

  return magic_return;
}

void main_prog(void *userdata, int argc, char *argv[])
{
  TOPLEVEL *toplevel;
  SCM x;
  int exception_caught = 0;

  libgeda_init(TRUE);

  s_log_init("/dev/null");

  toplevel = s_toplevel_new();

  /* A witness object that is not eq? to anything else. */
  magic_return = scm_from_locale_symbol("magic-return");

  /* Check that we get the right return value. */
  x = g_scm_apply_protected(scm_variable_ref(scm_c_lookup("+")),
			    scm_list_2(scm_from_int(17), scm_from_int(42)),
			    &fail_if_called, NULL);
  if (!scm_is_integer(x) || scm_to_int(x) != (17 + 42)) {
    exit(1);
  }

  /* Force an exception. */
  x = g_scm_apply_protected(scm_variable_ref(scm_c_lookup("throw")),
			    scm_list_2(scm_from_locale_symbol("pro-forma"),
				       scm_from_int(17)),
			    &mark_if_called, &exception_caught);
  if (!exception_caught || !scm_is_eq(x, magic_return)) {
    exit(1);
  }

  exit(0);
}

int main(int argc, char *argv[])
{
  scm_boot_guile(argc, argv, &main_prog, NULL);

  /* scm_boot_guile doesn't return! */
  return 1;
}
