#include "config.h"

#include <glib.h>
#include <libguile.h>
#include <stdio.h>
#include "defines.h"
#include "struct.h"
#include "prototype.h"

int (*load_newer_backup_func)() = NULL;
void (*arc_draw_func)() = NULL;
void (*line_draw_func)() = NULL;
void (*text_draw_func)() = NULL;
void (*x_log_update_func)() = NULL;
void (*net_draw_func)() = NULL;
void (*complex_draw_func)() = NULL;
void (*circle_draw_func)() = NULL;
void (*box_draw_func)() = NULL;
int do_logging=TRUE;
void (*bus_draw_func)() = NULL;
void (*pin_draw_func)() = NULL;
void (*slot_draw_func)() = NULL;
void (*picture_draw_func)() = NULL;
void (*variable_set_func)() = NULL;
