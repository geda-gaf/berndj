#!/bin/sh

if test "x$srcdir" = x; then
  srcdir=.
fi

expected=`mktemp -t nvXXXXXX`
input=`mktemp -t nvXXXXXX`

cat >$input <<EOF
foo=bar
foo[17]=$bar[42]
foo = bar
foo= bar
foo =bar
EOF

cat >$expected <<EOF
foo=bar
foo[17]=$bar[42]
NO_ATTRIB
NO_ATTRIB
NO_ATTRIB
EOF

tr '\n' '\0' <$input |xargs -r0 -n 1 $srcdir/attribnamevalue |diff $expected -

status=$?

rm -f $expected $input

exit $status
