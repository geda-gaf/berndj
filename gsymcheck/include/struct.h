
/* sym check structures (gsymcheck) */
typedef struct st_symcheck SYMCHECK;

/* gsymcheck structure */
struct st_symcheck {

  /* unused for now */
  int unattached_attribs;

  /* info / warning / error messages */
  GList* info_messages;
  GList* warning_messages;
  GList* error_messages;

  /* device= check */
  gboolean graphical_symbol;
  char *device_attribute;
  gboolean device_attribute_incorrect;

  /* pinseq= check */
  GList *found_numbers;

  /* multiple pinnumber= check */
  GList *net_numbers;
  GList *pin_numbers;

  /* slotting checks */ 
  char *numslots;
  GList *slotdef;
  int slotting_errors;

  /* old pin#=# and slot#=# checks */
  int found_oldpin_attrib;
  int found_oldslot_attrib;

  /* net, bus, connection checks */
  int found_net;
  int found_bus;
  int found_connection;

  /* obsolete attribute checks */
  /* int found_label; */
  /* int found_uref; */

  /* forbidden attributes */
  /* int found_name; */
  /* int found_type; */

  /* misc attributes */
  int found_footprint;
  int found_refdes;
  
  /* number of pins */
  int numpins;
  /* number of net pins */
  int numnetpins;
  /* number of distinct slot pins */
  int numslotpins;
  
  /* total error counter */
  int error_count;

  /* total warning counter */
  int warning_count;
};
