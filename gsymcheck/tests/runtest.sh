#!/bin/sh

INPUT=$1
BUILDDIR=$2
SRCDIR=$3
EXTRADIFF=$4
here=`pwd`
rundir=${here}/run

# create temporary run directory and required subdirs
if [ ! -d $rundir ]
then
   mkdir -p $rundir
fi

symbasename=`basename $INPUT .sym`

tmpfile=$rundir/tmp$$
../src/gsymcheck -vv $INPUT 1> $tmpfile 2> $rundir/allerrors.output 

cat $tmpfile | \
	grep -v "gEDA/gsymcheck version" | \
	grep -v "ABSOLUTELY NO WARRANTY" | \
	grep -v "This is free software" | \
	grep -v "the COPYING file" | \
	grep -v "Checking: " | \
	grep -v '^$' > $rundir/new_${symbasename}.output
rm -f $tmpfile

diff -u $EXTRADIFF ${BUILDDIR}/${symbasename}.output \
	 $rundir/new_${symbasename}.output
status=$?

rm -rf $rundir

if [ "$status" != 0 ]
then
	exit 2
fi

exit 0
