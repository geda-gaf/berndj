/* gEDA - GPL Electronic Design Automation
 * gsymcheck - gEDA Symbol Check 
 * Copyright (C) 1998-2007 Ales Hvezda
 * Copyright (C) 1998-2007 gEDA Contributors (see ChangeLog for details)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02111-1301 USA.
 */

#include <config.h>

#include <stdio.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif
#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <libgeda/libgeda.h>

#include "../include/struct.h"
#include "../include/globals.h"
#include "../include/prototype.h"


static enum visit_result s_check_symbol_structure(OBJECT *o_current, void *userdata);
static enum visit_result s_check_graphical(OBJECT *o_current, void *userdata);
static enum visit_result s_check_device(OBJECT *o_current, void *userdata);
static void s_check_device_digest(SYMCHECK *s_current);
static enum visit_result s_check_pinseq(OBJECT *o_current, void *userdata);
static void s_check_pinseq_digest(SYMCHECK *s_current);
static enum visit_result s_check_netpins(OBJECT *o_current, void *userdata);
static void s_check_netpins_digest(SYMCHECK *s_current);
static enum visit_result s_check_pinnumber(OBJECT *o_current, void *userdata);
static void s_check_pinnumber_digest(SYMCHECK *s_current);
static enum visit_result s_check_pin_ongrid(OBJECT *o_current, void *userdata);
static enum visit_result s_check_slotdef(OBJECT *o_current, void *userdata);
static void s_check_slotdef_digest(SYMCHECK *s_current);
static enum visit_result s_check_oldpin(OBJECT *o_current, void *userdata);
static enum visit_result s_check_oldslot(OBJECT *o_current, void *userdata);
static enum visit_result s_check_nets_buses(OBJECT *o_current, void *userdata);
static enum visit_result s_check_connections(OBJECT *o_current, void *userdata);
static void s_check_missing_attribute(OBJECT *object, char *attribute, SYMCHECK *s_current);
static enum visit_result s_check_missing_attributes(OBJECT *o_current, void *userdata);
static void s_check_missing_attributes_digest(SYMCHECK *s_current);
static enum visit_result s_check_pintype(OBJECT *o_current, void *userdata);

int
s_check_all(TOPLEVEL *pr_current)
{
  GList *iter;
  PAGE *p_current;
  int return_status=0;


  for ( iter = geda_list_get_glist( pr_current->pages );
        iter != NULL;
        iter = g_list_next( iter ) ) {

    p_current = (PAGE *)iter->data;

    if (p_current->object_head) {
      return_status = return_status +
        s_check_symbol(pr_current, p_current);
      if (!quiet_mode) s_log_message("\n");
    }
  }

  return(return_status);
}


int
s_check_symbol(TOPLEVEL *pr_current, PAGE *page)
{
  SYMCHECK *s_symcheck=NULL;
  int errors=0, warnings=0;
  int i;
  struct {
    enum visit_result (*test)(OBJECT *, void *);
    void (*digest)(SYMCHECK *);
  } checks[] = {
    /* overall symbol structure test */
    { &s_check_symbol_structure, NULL },
    /* check for graphical attribute */
    { &s_check_graphical, NULL },
    /* check for device attribute */
    { &s_check_device, *s_check_device_digest },
    /* check for missing attributes */
    { &s_check_missing_attributes, &s_check_missing_attributes_digest },
    /* check for pintype attribute (and multiples) on all pins */
    { &s_check_pintype, NULL },
    /* check for pinseq attribute (and multiples) on all pins */
    { &s_check_pinseq, &s_check_pinseq_digest },
    /* check for net attribute pin connections */
    { &s_check_netpins, &s_check_netpins_digest },
    /* check for pinnumber attribute (and multiples) on all pins */
    { &s_check_pinnumber, &s_check_pinnumber_digest },
    /* check for whether all pins are on grid */
    { &s_check_pin_ongrid, NULL },
    /* check for slotdef attribute on all pins (if numslots exists) */
    { &s_check_slotdef, &s_check_slotdef_digest },
    /* check for old pin#=# attributes */
    { &s_check_oldpin, NULL },
    /* check for old pin#=# attributes */
    { &s_check_oldslot, NULL },
    /* check for nets or buses within the symbol (completely disallowed) */
    { &s_check_nets_buses, NULL },
    /* check for connections with in a symbol (completely disallowed) */
    { &s_check_connections, NULL },

    { NULL, NULL }
  };

  s_symcheck = s_symstruct_init();
  
  if (!quiet_mode) {
    s_log_message("Checking: %s\n", page->page_filename);
  }
  
  for (i = 0; checks[i].test; i++) {
    s_visit_page(page, checks[i].test, s_symcheck, VISIT_ANY, 1);
    if (checks[i].digest) {
      (*checks[i].digest)(s_symcheck);
    }
  }

  /* now report the info/warnings/errors to the user */
  if (!quiet_mode) {
    /* done, now print out the messages */
    s_symstruct_print(s_symcheck);
    
    if (s_symcheck->warning_count > 0) {
      s_log_message("%d warnings found",
                    s_symcheck->warning_count);
      if (verbose_mode < 2) {
        s_log_message(" (use -vv to view details)\n");
      } else {
        s_log_message("\n");
      }
    }
  
    if (s_symcheck->error_count == 0) {
      s_log_message("No errors found\n");
    } else if (s_symcheck->error_count == 1) {
      s_log_message("1 ERROR found");
      if (verbose_mode < 1) {
        s_log_message(" (use -v to view details)\n");
      } else {
        s_log_message("\n");
      }

    } else if (s_symcheck->error_count > 1) {
      s_log_message("%d ERRORS found",
                    s_symcheck->error_count);
      if (verbose_mode < 1) {
        s_log_message(" (use -v to view details)\n");
      } else {
        s_log_message("\n");
      }
    }
  }

  errors = s_symcheck->error_count;
  warnings = s_symcheck->warning_count;
  s_symstruct_free(s_symcheck);
  if (errors) {
    return(2);
  } else if (warnings) {
    return(1);
  } else {
    return(0);
  }
}


gboolean 
s_check_list_has_item(char **list , char *item)
{
  gint cur;
  for (cur = 0; list[cur] != NULL; cur++) {
    if (strcmp(item, list[cur]) == 0)
      return TRUE;
  }
  return FALSE;
}

static enum visit_result
s_check_symbol_structure(OBJECT *o_current, void *userdata)
{
  SYMCHECK *s_current = userdata;
  gchar *message;
  gchar **tokens;

  char *valid_pin_attributes[] = {"pinlabel", "pintype",
				  "pinseq", "pinnumber",
				  NULL};
  char *valid_attributes[] = {"device", "graphical", "description",
			      "author", "comment", "numslots",
			      "slotdef", "footprint", "documentation",
			      "refdes", "slot", "net", "value",
			      "symversion", "dist-license", "use-license",
			      NULL};
  char *obsolete_attributes[] = {"uref", "label", "email", 
				 NULL};
  char *forbidden_attributes[] = {"type", "name", 
				  NULL};
  /* pin# ?, slot# ? */
  
  if (o_current->type == OBJ_TEXT) {
    tokens = g_strsplit(o_text_get_string(o_current), "=", 2);
    if (tokens[0] != NULL && tokens[1] != NULL) {
      if (s_check_list_has_item(forbidden_attributes, tokens[0])) {
	message = g_strdup_printf ("Found forbidden %s= attribute: [%s=%s]\n",
				   tokens[0], tokens[0], tokens[1]);
	s_current->error_messages =
	  g_list_append(s_current->error_messages, message);
	s_current->error_count++;
      }
      else if (s_check_list_has_item(obsolete_attributes, tokens[0])) {
	message = g_strdup_printf ("Found obsolete %s= attribute: [%s=%s]\n",
				   tokens[0], tokens[0], tokens[1]);
	s_current->warning_messages =
	  g_list_append(s_current->warning_messages, message);
	s_current->warning_count++;
      }
      else if (s_check_list_has_item(valid_pin_attributes, tokens[0])) {
	if (o_current->attached_to == NULL
	    || o_current->attached_to->type != OBJ_PIN) {
	  message = g_strdup_printf ("Found misplaced pin attribute:"
				     " [%s=%s]\n", tokens[0], tokens[1]);
	  s_current->error_messages =
	    g_list_append(s_current->error_messages, message);
	  s_current->error_count++;
	}
      }
      else if (!s_check_list_has_item(valid_attributes, tokens[0])) {
	message = g_strdup_printf ("Found unknown %s= attribute: [%s=%s]\n",
				   tokens[0], tokens[0], tokens[1]);
	s_current->warning_messages =
	  g_list_append(s_current->warning_messages, message);
	s_current->warning_count++;
      }
      else if (o_current->attached_to != NULL) {
	message = g_strdup_printf ("Found wrongly attached attribute: "
				   "[%s=%s]\n",
				   tokens[0], tokens[1]);
	s_current->error_messages =
	  g_list_append(s_current->error_messages, message);
	s_current->error_count++;
      }
    }
    g_strfreev(tokens);
  }

  return VISIT_RES_OK;
}

static enum visit_result
s_check_graphical(OBJECT *o_current, void *userdata)
{
  SYMCHECK *s_current = userdata;
  char *temp;
  
  /* look for special graphical tag */
  temp = o_attrib_search_name_single_exact(o_current, "graphical", NULL);

  if (temp) {
    s_current->graphical_symbol=TRUE;
    g_free(temp);
  }

  return VISIT_RES_OK;
}

static enum visit_result
s_check_device(OBJECT *o_current, void *userdata)
{
  SYMCHECK *s_current = userdata;
  char *temp;
  char *message;
  
  temp = o_attrib_search_name_single_exact(o_current, "device", NULL);

  if (temp) {
    if (s_current->device_attribute) {
      message = g_strdup ("Found multiple device= attributes\n");
      s_current->error_messages = g_list_append(s_current->error_messages,
						message);
      s_current->error_count++;

      /* Ignore the duplicate numslots= attribute. */
      g_free(temp);
    } else {
      s_current->device_attribute = temp;
    }
  }

  return VISIT_RES_OK;
}

static void s_check_device_digest(SYMCHECK *s_current)
{
  char *message;

  /* search for device attribute */
  if (!s_current->device_attribute) {
    /* did not find device= attribute */
    message = g_strdup ("Missing device= attribute\n");
    s_current->error_messages = g_list_append(s_current->error_messages,
		                              message);
    s_current->error_count++;
  } else {
    /* found device= attribute */
    message = g_strdup_printf("Found device=%s\n", s_current->device_attribute);
    s_current->info_messages = g_list_append(s_current->info_messages,
		                             message);
  }

  /* check for device = none for graphical symbols */
  if (s_current->device_attribute && s_current->graphical_symbol &&
      (strcmp(s_current->device_attribute, "none") == 0)) {
    s_current->device_attribute_incorrect=FALSE;
    message = g_strdup ("Found graphical symbol, device=none\n");
    s_current->info_messages = g_list_append(s_current->info_messages,
                                             message);
  } else if (s_current->graphical_symbol) {
    s_current->device_attribute_incorrect=TRUE;
    message = g_strdup ("Found graphical symbol, device= should be set to none\n");
    s_current->warning_messages = g_list_append(s_current->warning_messages,
                                                message);
    s_current->warning_count++;
  } 

  g_free(s_current->device_attribute);
  s_current->device_attribute = NULL;
}


static enum visit_result
s_check_pinseq(OBJECT *o_current, void *userdata)
{
  SYMCHECK *s_current = userdata;
  char *pinseq;
  int found_first=FALSE;
  int counter=0;

  char *number;
  char *message;

  if (o_current->type == OBJ_PIN)
  {
    found_first = FALSE;
    counter = 0;

    pinseq = o_attrib_search_name_single_count(o_current, "pinseq",
					       counter);
    if (!pinseq) {
      message = g_strdup ("Missing pinseq= attribute\n");
      s_current->error_messages = g_list_append(s_current->error_messages,
						message);
      s_current->error_count++;
    }

    while (pinseq) {
      message = g_strdup_printf("Found pinseq=%s attribute\n", pinseq);
      s_current->info_messages = g_list_append(s_current->info_messages,
					       message);

      number = g_strdup(pinseq);

      if (strcmp(number, "0") == 0) {
	message = g_strdup ("Found pinseq=0 attribute\n");
	s_current->error_messages = g_list_append(s_current->error_messages,
						  message);
	s_current->error_count++;
      }

      if (found_first) {
	message = g_strdup_printf (
				   "Found multiple pinseq=%s attributes on one pin\n",
				   pinseq);
	s_current->error_messages = g_list_append(s_current->error_messages,
						  message);
	s_current->error_count++;
      }

      g_free(pinseq);

      /* this is the first attribute found */
      if (!found_first) {
	s_current->found_numbers = g_list_append(s_current->found_numbers,
						 number);
	found_first=TRUE;
      } else {
	g_free(number);
      }

      counter++;
      pinseq = o_attrib_search_name_single_count(o_current, "pinseq",
						 counter);
    }
  }

  return VISIT_RES_OK;
}

static void s_check_pinseq_digest(SYMCHECK *s_current)
{
  GList *ptr1 = NULL;
  GList *ptr2 = NULL;
  char *message;

  ptr1 = s_current->found_numbers;
  while (ptr1)
  {
    char *string = (char *) ptr1->data;
    int found = 0;
    
    ptr2 = s_current->found_numbers;
    while(ptr2 && string)
    {
      char *current = (char *) ptr2->data;

      if (current && strcmp(string, current) == 0) {
        found++;
      }
      
      ptr2 = g_list_next(ptr2);
    }

    if (found > 1)
    {
      message = g_strdup_printf (
        "Found duplicate pinseq=%s attribute in the symbol\n",
        string);
      s_current->error_messages = g_list_append(s_current->error_messages,
                                                message);
      s_current->error_count++;
    }
    
    ptr1 = g_list_next(ptr1);
  }

  ptr1 = s_current->found_numbers;
  while (ptr1)
  {
    g_free(ptr1->data);
    ptr1 = g_list_next(ptr1);
  }
  g_list_free(s_current->found_numbers);
  s_current->found_numbers = NULL;
}


static enum visit_result
s_check_netpins(OBJECT *o_current, void *userdata)
{
  SYMCHECK *s_current = userdata;
  int i;

  gchar **net_tokens;
  gchar **pin_tokens;
  char *message;
  char *net, *attrib_name, *attrib_value;
    
  if (o_current->type == OBJ_TEXT) {
    if (o_attrib_get_name_value(o_text_get_string(o_current),
				&attrib_name, &attrib_value)) {
      if (strcmp(attrib_name, "net") == 0) {
	/* collect all net pins */
	net = attrib_value;

	message = g_strdup_printf ("Found net=%s attribute\n", net);
	s_current->info_messages = g_list_append(s_current->info_messages,
						 message);

	net_tokens = g_strsplit(net,":", -1);
	/* length of net tokens have to be 2 */
	if (net_tokens[1] == NULL) {
	  message = g_strdup_printf ("Bad net= attribute [net=%s]\n", net);
	  s_current->error_messages = g_list_append(s_current->error_messages,
						    message);
	  s_current->error_count++;
	  g_strfreev(net_tokens);
	  g_free(attrib_name);
	  g_free(attrib_value);
	  return VISIT_RES_OK;
	} else if (net_tokens[2] != NULL) { /* more than 2 tokens */
	  message = g_strdup_printf ("Bad net= attribute [net=%s]\n", net);
	  s_current->error_messages = g_list_append(s_current->error_messages,
						    message);
	  s_current->error_count++;
	  g_strfreev(net_tokens);
	  g_free(attrib_name);
	  g_free(attrib_value);
	  return VISIT_RES_OK;
	}

	pin_tokens = g_strsplit(net_tokens[1],",",-1);

	for (i = 0; pin_tokens[i] != NULL; i++) {
	  s_current->net_numbers = g_list_append(s_current->net_numbers, g_strdup(pin_tokens[i]));
	  message = g_strdup_printf ("Found pin number %s in net attribute\n",
				     pin_tokens[i]);
	  s_current->info_messages = g_list_append(s_current->info_messages,
						   message);
	  s_current->numnetpins++;
	}
	g_strfreev(net_tokens);
	g_strfreev(pin_tokens);
      }
      g_free(attrib_name);
      g_free(attrib_value);
    }
  }

  return VISIT_RES_OK;
}

static void s_check_netpins_digest(SYMCHECK *s_current)
{
  char *message;
  GList *cur;

  /* check for duplicate net pin numbers */
  s_current->net_numbers = g_list_sort(s_current->net_numbers, (GCompareFunc)strcmp);

  for (cur = s_current->net_numbers;
       cur != NULL && g_list_next(cur) != NULL;
       cur = g_list_next(cur)) {
    if (strcmp((gchar*)cur->data, (gchar*) cur->next->data) == 0) {
      message = g_strdup_printf ("Found duplicate pin in net= "
				 "attributes [%s]\n", (gchar*) cur->data);
      s_current->error_messages = g_list_append(s_current->error_messages,
						message);
      s_current->error_count++;
    }
    if (strcmp((gchar*) cur->data, "0") == 0) {
      message = g_strdup ("Found pinnumber 0 in net= attribute\n");
      s_current->error_messages = g_list_append(s_current->error_messages,
                                                message);
      s_current->error_count++;
    }
  }
}

static enum visit_result
s_check_pinnumber(OBJECT *o_current, void *userdata)
{
  SYMCHECK *s_current = userdata;
  char *string;
  char *message;
  int counter=0;

  /* collect all pin numbers */
  if (o_current->type == OBJ_PIN) {
    s_current->numpins++;

    for (counter = 0;
	 (string = o_attrib_search_name_single_count(o_current, "pinnumber",
						     counter)) != NULL;
	 counter++) {

      message = g_strdup_printf ("Found pinnumber=%s attribute\n", string);
      s_current->info_messages = g_list_append(s_current->info_messages,
					       message);

      if (counter == 0) { /* collect the first appearance */
	s_current->pin_numbers = g_list_append(s_current->pin_numbers, string);
      }
      if (counter >= 1) {
	message = g_strdup_printf ("Found multiple pinnumber=%s attributes"
				   " on one pin\n", string);
	s_current->error_messages = g_list_append(s_current->error_messages,
						  message);
	s_current->error_count++;
	g_free(string);
      }
    }

    if (counter == 0) {
      message = g_strdup ("Missing pinnumber= attribute\n");
      s_current->error_messages = g_list_append(s_current->error_messages,
						message);
      s_current->error_count++;
    }
  }

  return VISIT_RES_OK;
}

static void s_check_pinnumber_digest(SYMCHECK *s_current)
{
  GList *cur;
  GList *cur2;
  char *message;
  int i;

  /* check for duplicate pinlabel numbers */
  s_current->pin_numbers = g_list_sort(s_current->pin_numbers, (GCompareFunc)strcmp);
  for (cur = s_current->pin_numbers;
       cur != NULL && g_list_next(cur) != NULL;
       cur = g_list_next(cur)) { 
    if (strcmp((gchar*)cur->data, (gchar*) cur->next->data) == 0) {
      message = g_strdup_printf ("Found duplicate pinnumber=%s attribute "
				 "in the symbol\n", (gchar*) cur->data);
      s_current->error_messages = g_list_append(s_current->error_messages,
						message);
      s_current->error_count++;
    }
    if (strcmp((gchar*) cur->data, "0") == 0) {
      message = g_strdup ("Found pinnumber=0 attribute\n");
      s_current->error_messages = g_list_append(s_current->error_messages,
						message);
      s_current->error_count++;
    }
  }

  /* Check for all pins that are in both lists and print a warning.
     Sometimes this is useful and sometimes it's an error. */

  cur = s_current->net_numbers;
  cur2 = s_current->pin_numbers;

  while (cur != NULL && cur2 != NULL) {
    i = strcmp((gchar*)cur->data, (gchar*)cur2->data);

    if (i == 0) {
      message = g_strdup_printf ("Found the same number in a pinnumber "
				 "attribute and in a net attribute [%s]\n",
				 (gchar*) cur->data);
      s_current->warning_messages = g_list_append(s_current->warning_messages,
						  message);
      s_current->warning_count++;
      cur = g_list_next(cur);
    } else if ( i > 0 ) {
      cur2 = g_list_next(cur2);
    } else { /* i < 0 */
      cur = g_list_next(cur);
    }
  }

  /* FIXME: this is not correct if a pinnumber is defined as pinnumber and
     inside a net. We have to calculate the union set */
  message = g_strdup_printf ("Found %d pins inside symbol\n", 
			     s_current->numpins + s_current->numnetpins);
  s_current->info_messages = g_list_append(s_current->info_messages,
                                           message);

  g_list_foreach(s_current->pin_numbers, (GFunc) g_free, NULL);
  g_list_free(s_current->pin_numbers);
  s_current->pin_numbers = NULL;
  g_list_foreach(s_current->net_numbers, (GFunc) g_free, NULL);
  g_list_free(s_current->net_numbers);
  s_current->net_numbers = NULL;
}

static enum visit_result
s_check_pin_ongrid(OBJECT *o_current, void *userdata)
{
  SYMCHECK *s_current = userdata;
  int x1, x2, y1, y2;
  char *message;


  if (o_current->type == OBJ_PIN) {
    s_basic_get_grip(o_current, GRIP_1, &x1, &y1);
    s_basic_get_grip(o_current, GRIP_2, &x2, &y2);

    if (x1 % 100 != 0 || y1 % 100 != 0) {
      message = g_strdup_printf("Found offgrid pin at location"
				" (x1=%d,y1=%d)\n", x1, y1);
      /* error if it is the whichend, warning if not */
      if (o_current->whichend == 0) {
	s_current->error_messages = g_list_append(s_current->error_messages,
						  message);
	s_current->error_count++;
      }
      else {
	s_current->warning_messages = g_list_append(s_current->warning_messages,
						    message);
	s_current->warning_count++;
      }
    }
    if (x2 % 100 != 0 || y2 % 100 != 0) {
      message = g_strdup_printf("Found offgrid pin at location"
				" (x2=%d,y2=%d)\n", x2, y2);
      /* error when whichend, warning if not */
      if (o_current-> whichend != 0) {
	s_current->error_messages = g_list_append(s_current->error_messages,
						  message);
	s_current->error_count++;
      }
      else {
	s_current->warning_messages = g_list_append(s_current->warning_messages,
						    message);
	s_current->warning_count++;
      }
    }
  }

  return VISIT_RES_OK;
}


static enum visit_result
s_check_slotdef(OBJECT *o_current, void *userdata)
{
  SYMCHECK *s_current = userdata;
  char *numslots, *slotdef;
  char *message;

  /* look for numslots to see if this symbol has slotting info */
  numslots = o_attrib_search_name_single_exact(o_current, "numslots", NULL);

  if (numslots) {
    if (s_current->numslots) {
      message = g_strdup("Found multiple numslots= attributes\n");
      s_current->error_messages = g_list_append(s_current->error_messages,
						message);
      s_current->error_count++;

      /* Ignore the duplicate numslots= attribute. */
      g_free(numslots);
    } else {
      s_current->numslots = numslots;
    }
  }

  slotdef = o_attrib_search_name_single_exact(o_current, "slotdef", NULL);
  if (slotdef) {
    s_current->slotdef = g_list_prepend(s_current->slotdef, slotdef);
  }

  return VISIT_RES_OK;
}

static void s_check_slotdef_digest(SYMCHECK *s_current)
{
  char* slotnum = NULL;
  char* pins = NULL;
  char* temp = NULL;
  int slot, numslots;
  int i,j;
  GList *iter;
  char *message;
  /*  pinlist will store the pin definitions for each slot */
  /* example: pinlist[0] = 3,2,8,4,1 ; pinlist[1] = 5,6,8,4,7 */
  char** pinlist = NULL;
  int n,m;
  char* pin;
  char* cmp;
  int match;
  gboolean error_parsing = FALSE;
  int errors_found = 0;

  s_current->slotdef = g_list_reverse(s_current->slotdef);

  if (!s_current->numslots) {
    message = g_strdup ("Did not find numslots= attribute, not checking slotting\n");
    s_current->warning_messages = g_list_append(s_current->warning_messages,
                                                message);
    s_current->warning_count++;
    message = g_strdup ("If this symbol does not need slotting, set numslots to zero (numslots=0)\n");
    s_current->info_messages = g_list_append(s_current->info_messages,
                                             message);
    return;
  }

  numslots=atoi(s_current->numslots);
  g_free(s_current->numslots);
  s_current->numslots = NULL;

  message = g_strdup_printf("Found numslots=%d attribute\n", numslots);
  s_current->info_messages = g_list_append(s_current->info_messages,
	 	    			   message);

  if (numslots == 0) {
    message = g_strdup ("numslots set to zero, symbol does not have slots\n");
    s_current->info_messages = g_list_append(s_current->info_messages,
                                             message);
    return;
  }
  

  pinlist = (char **) g_malloc0(sizeof (*pinlist) * numslots);

  for (iter = s_current->slotdef, i = 0;
       iter && !error_parsing;
       iter = iter->next, i++) {
    char const * const slotdef = iter->data;

    if (i > numslots-1) {
      message = g_strdup_printf (
        "Found %d slotdef= attributes.  Expecting %d slotdef= attributes\n",
        i+1, numslots);
      s_current->error_messages = g_list_append(s_current->error_messages,
                                                message);

      s_current->error_count++;
      s_current->slotting_errors++;
    }
    
    message = g_strdup_printf ("Found slotdef=%s attribute\n", slotdef);
    s_current->info_messages = g_list_append(s_current->info_messages,
	 	    			     message);

    slotnum = u_basic_breakup_string(slotdef, ':', 0);
    if (!slotnum)
    {
      message = g_strdup_printf (
        "Invalid slotdef=%s attributes, not continuing\n",
        slotdef);
      s_current->error_messages = g_list_append(s_current->error_messages,
                                                message);
      s_current->error_count++;
      s_current->slotting_errors++;
      /* XXX 'continue' to mark this slotdef as processed. */
      error_parsing = TRUE;
      continue;
    }

    if (strcmp(slotnum, "0") == 0) {
      message = g_strdup_printf (
        "Found a zero slot in slotdef=%s\n",
        slotdef);
      s_current->error_messages = g_list_append(s_current->error_messages,
                                                message);
      s_current->error_count++;
    }
  
    slot = atoi(slotnum);
    g_free(slotnum);

    /* make sure that the slot # is less than the number of slots */
    if (slot > numslots) {
      message = g_strdup_printf (
        "Slot %d is larger then the maximum number (%d) of slots\n",
        slot, numslots);
      s_current->error_messages = g_list_append(s_current->error_messages,
		      			        message);

      s_current->error_count++;
      s_current->slotting_errors++;
    }

    /* skip over the : */
    pins = u_basic_breakup_string(slotdef, ':', 1);
    if (!pins) {
      message = g_strdup_printf (
        "Invalid slotdef=%s attributes, not continuing\n",
        slotdef);
      s_current->error_messages = g_list_append(s_current->error_messages,
                                                message);
      s_current->error_count++;
      s_current->slotting_errors++;
      /* XXX 'continue' to mark this slotdef as processed. */
      error_parsing = TRUE;
      continue;
    }

    if (*pins == '\0') {
      message = g_strdup_printf (
        "Invalid slotdef=%s attributes, not continuing\n",
        slotdef);
      s_current->error_messages = g_list_append(s_current->error_messages,
                                                message);
      s_current->error_count++;
      s_current->slotting_errors++;
      /* XXX 'continue' to mark this slotdef as processed. */
      error_parsing = TRUE;
      continue;
    }

    if ((slot > 0) && (slot <= numslots)) {
      if (pinlist[slot-1]) {
        message = g_strdup_printf ("Duplicate slot number in slotdef=%s\n",
				   slotdef);
        s_current->error_messages = g_list_append(s_current->error_messages,
	    			                  message);
        s_current->error_count++;
        s_current->slotting_errors++;
      } else {
	pinlist[slot-1] = g_strdup_printf(",%s,", pins);
      }
    }
    
    j = 0;
    do {
      if (temp) {
        g_free(temp);
        temp = NULL;
      }
        
      temp = u_basic_breakup_string(pins, ',', j);

      if (!temp && j < s_current->numpins) {
        message = g_strdup_printf (
          "Not enough pins in slotdef=%s\n",
          slotdef);
        s_current->error_messages = g_list_append(s_current->error_messages,
	    			                  message);
        s_current->error_count++;
        s_current->slotting_errors++;
        break;
      }

      if (j > s_current->numpins) {
        message = g_strdup_printf (
          "Too many pins in slotdef=%s\n",
          slotdef);
        s_current->error_messages = g_list_append(s_current->error_messages,
	    			                  message);
        s_current->error_count++;
        s_current->slotting_errors++;
        g_free(temp);
        temp = NULL;
        break;
      }
      
      if (temp && strcmp(temp, "0") == 0) {
        message = g_strdup_printf (
          "Found a zero pin in slotdef=%s\n",
          slotdef);
        s_current->error_messages = g_list_append(s_current->error_messages,
                                                  message);
        s_current->error_count++;
      }
     
      j++;
    } while (temp);

    g_free(temp);

    g_free(iter->data);
    iter->data = NULL;
  }

  if (!iter && i < numslots) {
    /* Processed all slotdefs, so we know there are too few. */
    message = g_strdup_printf (
      "Missing slotdef= (there should be %d slotdef= attributes)\n",
      numslots);
    s_current->error_messages = g_list_append(s_current->error_messages,
			                      message);
    s_current->error_count++;
    s_current->slotting_errors++;
  } else {
    /* Validate that pinslist does not contain a null entry.  If any entry */
    /* is null, that means the slotdef= attribute was malformed to start */
    /* with. */
    for (i = 0; i < numslots; i++) {
      if (pinlist[i] == NULL) {
        errors_found++;
      }
    }

    if (errors_found) {
      message = g_strdup_printf(
               "Malformed slotdef= (the format is #:#,#,#,...)\n");
      s_current->error_messages = g_list_append(s_current->error_messages,
                                                message);
      s_current->error_count++;
      s_current->slotting_errors++;
    } else { 
      /* Now compare each pin with the rest */
      s_current->numslotpins = 0;
      for (i = 0; i < numslots; i++) {
        for (n = 1; n <= s_current->numpins; n++) {
          /* Get the number of one pin */
          pin = u_basic_breakup_string(pinlist[i], ',', n);
          if (pin && *pin) {
            match = FALSE;
            for (j = i - 1; j >= 0 && !match; j--) {
              for (m = 1; m <= s_current->numpins && !match; m++) {
                /* Get the number of the other pin */
                cmp = u_basic_breakup_string(pinlist[j], ',', m);
                if (cmp && *cmp) {
                  match = (0 == strcmp (pin, cmp));
                  g_free(cmp);
                }
              }
            }
            if (!match) {
              /* If they don't match, then increase the number of pins */
              s_current->numslotpins++;
            }
            g_free(pin);
          }
        }
      }
      message = g_strdup_printf ("Found %d distinct pins in slots\n", 
                                 s_current->numslotpins);
      s_current->info_messages = g_list_append(s_current->info_messages,
                                               message);
    }
  }
  
  if (pinlist) {
    /* Free the pinlist */
    for (i = 0; i < numslots; i++) {
      g_free(pinlist[i]);
    }
    g_free(pinlist);
  }
 
  g_list_free(s_current->slotdef);
  s_current->slotdef = NULL;
}

static enum visit_result
s_check_oldpin(OBJECT *o_current, void *userdata)
{
  SYMCHECK *s_current = userdata;
  char const *ptr;
  int found_old = FALSE;
  int number_counter = 0;
  char *message;

  if (o_current->type == OBJ_TEXT)
  {
    char const *text_chars = o_text_get_string(o_current);

    if (strstr(text_chars, "pin")) {
      /* skip over "pin" */
      ptr = text_chars + 3;

      found_old = FALSE;
      number_counter = 0;
      while (ptr && *ptr >= '0' && *ptr <= '9')
      {
	number_counter++;
	ptr++;
      }

      if (ptr && *ptr == '=')
      {
	found_old++;
      }

      if (!ptr)
      {
	return VISIT_RES_OK;
      }

      /* found no numbers inbetween pin and = */
      if (number_counter == 0)
      {
	return VISIT_RES_OK;
      }

      /* skip over = char */
      ptr++;

      while (ptr && *ptr >= '0' && *ptr <= '9')
      {
	ptr++;
      }

      if (*ptr == '\0')
      {
	found_old++;
      }

      /* 2 matches -> number found after pin and only numbers after = sign */
      if (found_old == 2)
      {
	message = g_strdup_printf (
				   "Found old pin#=# attribute: %s\n",
				   text_chars);
	s_current->error_messages = g_list_append(s_current->error_messages,
						  message);

	s_current->found_oldpin_attrib += found_old;
	s_current->error_count++;
      }
    }
  }

  return VISIT_RES_OK;
}


static enum visit_result
s_check_oldslot(OBJECT *o_current, void *userdata)
{
  SYMCHECK *s_current = userdata;
  char const *ptr;
  int found_old = FALSE;
  int number_counter = 0;
  char *message;

  if (o_current->type == OBJ_TEXT)
  {
    char const *text_chars = o_text_get_string(o_current);
    if (strstr(text_chars, "slot")) {
      /* skip over "slot" */
      ptr = text_chars + 4;

      found_old = FALSE;
      number_counter = 0;
      while (ptr && *ptr > '0' && *ptr < '9')
      {
	number_counter++;
	ptr++;
      }

      if (ptr && *ptr == '=')
      {
	found_old++;
      }

      if (!ptr)
      {
	return VISIT_RES_OK;
      }

      /* found no numbers inbetween pin and = */
      if (number_counter == 0)
      {
	return VISIT_RES_OK;
      }

      /* skip over = char */
      ptr++;

      while ((ptr && (*ptr > '0') && (*ptr < '9')) || (*ptr == ','))
      {
	ptr++;
      }

      if (*ptr == '\0')
      {
	found_old++;
      }

      /* 2 matches -> number found after slot and only numbers after = */
      if (found_old == 2)
      {
	message = g_strdup_printf (
				   "Found old slot#=# attribute: %s\n",
				   text_chars);
	s_current->error_messages = g_list_append(s_current->error_messages,
						  message);
	s_current->found_oldslot_attrib += found_old;
	s_current->error_count++;

      }
    }
  }

  return VISIT_RES_OK;
}


static enum visit_result
s_check_nets_buses(OBJECT *o_current, void *userdata)
{
  SYMCHECK *s_current = userdata;
  char *message;

  if (o_current->type == OBJ_NET)
  {
    message =
      g_strdup ("Found a net inside a symbol\n");
    s_current->error_messages = g_list_append(s_current->error_messages,
					      message);
    s_current->found_net++;
    s_current->error_count++;
  }

  if (o_current->type == OBJ_BUS)
  {
    message =
      g_strdup ("Found a bus inside a symbol\n");
    s_current->error_messages = g_list_append(s_current->error_messages,
					      message);
    s_current->found_bus++;
    s_current->error_count++;
  }

  return VISIT_RES_OK;
}

static enum visit_result
s_check_connections(OBJECT *o_current, void *userdata)
{
  SYMCHECK *s_current = userdata;
  char *message;
  
  if (o_current->conn_list) {
    message =
      g_strdup ("Found a connection inside a symbol\n");
    s_current->error_messages = g_list_append(s_current->error_messages,
					      message);
    s_current->found_connection++;
    s_current->error_count++;
  }

  return VISIT_RES_OK;
}

static void
s_check_missing_attribute(OBJECT *object, char *attribute, SYMCHECK *s_current)
{
  char *string;
  int found_first=FALSE;
  int counter=0;
  char *message;

  if (!attribute) {
    return;
  }

  string = o_attrib_search_name_single_count(object, attribute,
                                             counter);
  if (!string)
  {
    message = g_strdup_printf (
      "Missing %s= attribute\n",
      attribute);
    s_current->warning_messages = g_list_append(s_current->warning_messages,
                                                message);
    s_current->warning_count++;
  }

  while (string)
  {
    if (found_first) {
      message = g_strdup_printf (
        "Found multiple %s=%s attributes on one pin\n",
        attribute, string);
      s_current->error_messages = g_list_append(s_current->error_messages,
                                                message);
      s_current->error_count++;
    }
        
    /* this is the first attribute found */
    if (!found_first) {

      message = g_strdup_printf (
        "Found %s=%s attribute\n",
        attribute, string);
      s_current->info_messages = g_list_append(s_current->info_messages,
                                               message);
      found_first=TRUE;
    }

    g_free(string);

    counter++;
    string = o_attrib_search_name_single_count(object, attribute,
                                               counter);
  }
}

static enum visit_result
s_check_missing_attributes(OBJECT *o_current, void *userdata)
{
  SYMCHECK *s_current = userdata;
  char *message;

  if (o_current->type == OBJ_PIN)
  {
    s_check_missing_attribute(o_current, "pinlabel", s_current);
    s_check_missing_attribute(o_current, "pintype", s_current);
  }

  if (o_current->type == OBJ_TEXT)
  {
    char const *text_chars = o_text_get_string(o_current);

    if (strstr(text_chars, "footprint=")) {
      message = g_strdup_printf (
				 "Found %s attribute\n",
				 text_chars);
      s_current->info_messages = g_list_append(s_current->info_messages,
					       message);
      s_current->found_footprint++;
    }

    if (strstr(text_chars, "refdes=")) {
      message = g_strdup_printf (
				 "Found %s attribute\n",
				 text_chars);
      s_current->info_messages = g_list_append(s_current->info_messages,
					       message);
      s_current->found_refdes++;
    }
  }

  return VISIT_RES_OK;
}

static void s_check_missing_attributes_digest(SYMCHECK *s_current)
{
  char *message;

  if (s_current->found_footprint == 0) {
    message = g_strdup ("Missing footprint= attribute\n");
    s_current->warning_messages = g_list_append(s_current->warning_messages,
                                                message);
    s_current->warning_count++;
  }

    if (s_current->found_footprint > 1) {
    message = g_strdup ("Multiple footprint= attributes found\n");
    s_current->error_messages = g_list_append(s_current->error_messages,
                                                message);
    s_current->error_count++;

  }
  
  if (s_current->found_refdes == 0) {
    message = g_strdup ("Missing refdes= attribute\n");
    s_current->warning_messages = g_list_append(s_current->warning_messages,
                                                message);
    s_current->warning_count++;

  }

  if (s_current->found_refdes > 1) {
    message = g_strdup ("Multiple refdes= attributes found\n");
    s_current->error_messages = g_list_append(s_current->error_messages,
                                                message);
    s_current->error_count++;
  }
}

static enum visit_result
s_check_pintype(OBJECT *o_current, void *userdata)
{
  SYMCHECK *s_current = userdata;
  int counter=0;
  char *pintype;
  char *message;
  char *pintypes[] = {"in", "out", "io", "oc", "oe",
		      "pas", "tp", "tri", "clk", "pwr",
		      NULL};
  
  if (o_current->type == OBJ_PIN) {
    for (counter = 0;
	 (pintype = o_attrib_search_name_single_count(o_current, "pintype",
						      counter)) != NULL;
	 counter++) {

      message = g_strdup_printf("Found pintype=%s attribute\n", pintype);
      s_current->info_messages = g_list_append(s_current->info_messages,
					       message);

      if ( ! s_check_list_has_item(pintypes, pintype)) {
	message = g_strdup_printf ("Invalid pintype=%s attribute\n", pintype);
	s_current->error_messages = g_list_append(s_current->error_messages,
						  message);
	s_current->error_count++;
      }

      g_free(pintype);
    }
  }

  return VISIT_RES_OK;
}
