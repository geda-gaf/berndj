Use relative path for symbol names, not just basename.  Otherwise we get only
the first symbol with a name like resistor-1.sch.

Get rid of duplicated papersizes.h.

Replace random #define constants with enums.

Use object signalling to change colours on o_attrib_free().

Use object signalling to run s_conn_update_*(). Deleting the tile system
has introduced many new references to page_current.

Factor out
  switch (o->type) {
  case OBJ_BUS:
  case OBJ_NET:
  case OBJ_PIN:
    /* Only these objects participate in connections. */
    break;
  }

Page iterator macros a la Freeciv.

Replace sqrt(x^2 + y^2) with hypot(x, y).

Add conn_foreach_func OBJECT method.

Disambiguate library references; COMPLEX objects should name the library to
use, instead of just picking the first symbol the component library finds
with a generic name like "resistor-1.sym".

Get rid of connected_to and keep pointers to single objects around for as long
as possible instead of smooshing them together into a single string.

Get Guile to provide a user_data variant of scm_c_define_gsubr().

Move s_menu.c to gschem.

Audit all guile-interacting code to see if any of it really needs SCM_SYMBOLP
to be true. I suspect things could be less magic by storing SCMs instead of
symbol->string strings, and by using non-string eval.

A grip cache to help s_basic_search_grips_exact and s_conn_update_object?

Update connection system only when adding an object to a page. Ideally in a
signal handler that gets attached to the object only when it is on a page.

Let individual objects report what file format version they require. Add
parameter to OBJECT::save() (to be done) that specifies permissible file
format version.

Run o_pin_update_whichend on each pin as we read it from file.
 - No, the heuristic is to make the end closest to the bounding box active.

Replace g_list_append in loops with prepend / reverse.

Lots of f_open_flags doesn't belong in libgeda.

Sort out the f_open_flags malware vector: running arbitrary gafrc.

Use GtkBuilder instead of ad-hoc C code.

Turn event_state into a real object, with methods such as ::cancel. Better
yet, send it all off to scheme.

x_window_set_current_page should clear the place_list, otherwise o_drawbounding
(through o_place_rubberplace_xor) complains.

Check all uses of scm_is_true / scm_is_false: sometimes semantic truth is
the relevant test, and sometimes object identity compared to #t or #f is.

Tortoise and hare to detect COMPLEX prim_objs loops.

Run destroy_func only at dispose/finalize.

Don't close log window before confirming quit.

Attach to "changed" signal on selection_list.

Use G_DEFINE_TYPE_WITH_CODE instead of ad-hoc code.

Fix up the log window's w_current confusion - it's a singleton.

Don't call x_set_window_current so often. Maybe current-toplevel should be a
fluid that can be set temporarily while processing X events.

Move packagepins and vpads maps out of the components they control; the pin
swapping mechanism should also work in hierarchical schematics.

Slotting and pin swapping attributes should be floating attributes that may
exist on any page, in order to support multiply-referenced sub-schematics.

Replace all ad-hoc SCM_ASSERT type checking with scm_assert_smob_type.

Symbols need to get their UUID when placed, not only lazily when assigning
them to a slot. Otherwise s_slot_link will get way too complicated, having to
take multiple pages as arguments - one for each attribute that might get added.

Expose graph nature of hierarchy - it isn't just a tree. When editing or
viewing a schematic, the path used to reach that page matters. The path
determines slotting associations.

up_page won't be enough to support path-dependent hierarchy navigation. And
that is why page_current needs to mostly die: wherever we need to know the
current page, we really also need to know how we got there.

Object checkpointing mechanism, to use both as a generic undo facility, and to
revert objects to previous good state during editing.

Rename and move:
  o_complex_get_refdes

Remake slotting links after pasting objects that may participate in slotting.

Audit all uses of OBJECT::sid. I suspect most sid comparisons could be
replaced by pointer comparisons.

Split copy_func into an allocator and an initializer when any libgeda client
extends any base type in a way that requires a greater memory allocation.
Gschem already adds behaviour to OBJ_TEXT, but it doesn't override how much
memory to allocate.

Get rid of ADDING_SEL since it doesn't seem really to be used anywhere.

gschem/src/o_move.c frees place_list - audit for OBJECT leaks.

Factor out place_list interface to deal with whether it holds copies, or the
on-page objects as during a move.
  struct place_list_strategies {
    void (*clear)();
    void (*add)(GList *objects);
  }
or just define away the problem: don't use place_list for moves.

Remove sel_func / select_func and use return value from o_find_object.
