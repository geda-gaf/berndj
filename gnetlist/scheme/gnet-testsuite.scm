;; This is really just a no-op gnetlist backend, whose only purpose so far
;; is to give gnetlist a backend to "execute". The real testing happens in
;; Scheme files under gnetlist/tests/ where Makefile.am defines
;; SCM_LOG_COMPILER.
(define (testsuite output-filename)
  (if (defined? 'runtest)
    (runtest output-filename)
    #t))
